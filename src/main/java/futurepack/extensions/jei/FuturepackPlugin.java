package futurepack.extensions.jei;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import futurepack.api.Constants;
import futurepack.common.FPLog;
import futurepack.common.block.inventory.InventoryBlocks;
import futurepack.common.block.inventory.WaterCoolerManager;
import futurepack.common.block.inventory.WaterCoolerManager.CoolingEntry;
import futurepack.common.block.modification.ModifiableBlocks;
import futurepack.common.gui.inventory.GuiAssemblyRezeptCreator;
import futurepack.common.gui.inventory.GuiAssemblyTable;
import futurepack.common.gui.inventory.GuiConstructionTable;
import futurepack.common.gui.inventory.GuiCrusher;
import futurepack.common.gui.inventory.GuiIndFurnace;
import futurepack.common.gui.inventory.GuiIndNeonFurnace;
import futurepack.common.gui.inventory.GuiTechTable;
import futurepack.common.gui.inventory.GuiZentrifuge;
import futurepack.common.item.misc.MiscItems;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.recipes.ISyncedRecipeManager;
import futurepack.common.recipes.RecipeManagerSyncer;
import futurepack.common.recipes.airbrush.AirbrushRecipeJEI;
import futurepack.common.recipes.airbrush.AirbrushRegistry;
import futurepack.common.recipes.assembly.AssemblyRecipe;
import futurepack.common.recipes.centrifuge.ZentrifugeRecipe;
import futurepack.common.recipes.crafting.ShapedRecipeWithResearch;
import futurepack.common.recipes.crafting.ShapelessRecipeWithResearch;
import futurepack.common.recipes.crushing.CrushingRecipe;
import futurepack.common.recipes.industrialfurnace.IndNeonRecipe;
import futurepack.common.recipes.industrialfurnace.IndRecipe;
import futurepack.extensions.jei.airbrush.AirbrushCategory;
import futurepack.extensions.jei.assembly.AssemblyCategory;
import futurepack.extensions.jei.crafting.ShapedResearchCraftingExtension;
import futurepack.extensions.jei.crafting.ShapelessResearchCraftingExtension;
import futurepack.extensions.jei.crusher.CrusherCategory;
import futurepack.extensions.jei.indfurnace.IndustrialCategory;
import futurepack.extensions.jei.indneonfurnace.IndNeonCategorie;
import futurepack.extensions.jei.partpress.PartPressCategory;
import futurepack.extensions.jei.partpress.PartPressJeiFakeRecipe;
import futurepack.extensions.jei.recycler.RecyclerCategory;
import futurepack.extensions.jei.recycler.RecyclerJeiFakeRecipe;
import futurepack.extensions.jei.watercooler.WaterCoolerCategory;
import futurepack.extensions.jei.zentrifuge.ZentrifugeCategory;
import it.unimi.dsi.fastutil.objects.Object2LongArrayMap;
import mezz.jei.api.IModPlugin;
import mezz.jei.api.JeiPlugin;
import mezz.jei.api.constants.RecipeTypes;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.helpers.IJeiHelpers;
import mezz.jei.api.recipe.IRecipeManager;
import mezz.jei.api.recipe.RecipeType;
import mezz.jei.api.recipe.category.extensions.IExtendableRecipeCategory;
import mezz.jei.api.recipe.category.extensions.vanilla.crafting.ICraftingCategoryExtension;
import mezz.jei.api.registration.IRecipeCatalystRegistration;
import mezz.jei.api.registration.IRecipeCategoryRegistration;
import mezz.jei.api.registration.IRecipeRegistration;
import mezz.jei.api.registration.IRecipeTransferRegistration;
import mezz.jei.api.registration.IVanillaCategoryExtensionRegistration;
import mezz.jei.api.runtime.IJeiRuntime;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.CraftingRecipe;

@JeiPlugin
public class FuturepackPlugin implements IModPlugin
{
	public static IJeiHelpers jei;
	public static IRecipeManager registry;

	private static WeakReference<FuturepackPlugin> WEAK_INSTANCE = null;

	private Set<ISyncedRecipeManager<?>> bufferedManagers = Collections.synchronizedSet(new HashSet<>());
	private boolean airBrushRecipesInit = false;

	public static RecipeType<CraftingRecipe> CRAFTING;
	public static RecipeType<AssemblyRecipe> ASSEMBLY;
	public static RecipeType<CrushingRecipe> CRUSHER;
	public static RecipeType<IndRecipe> INDUTRIALFURNACE;
	public static RecipeType<IndNeonRecipe> INDUSTRIALNEONFURNACE;
	public static RecipeType<ZentrifugeRecipe> ZENTRIFUGE;
	public static RecipeType<AirbrushRecipeJEI> AIRBRUSH;
	public static RecipeType<CoolingEntry> WATERCOOLER;
	public static RecipeType<PartPressJeiFakeRecipe> PARTPRESS;
	public static RecipeType<RecyclerJeiFakeRecipe> RECYCLER;

	private static Map<ResourceLocation, RecipeType<?>> res2type = new HashMap<>();

	static
	{
		CRAFTING = new RecipeType<CraftingRecipe>(FuturepackUids.CRAFTING, CraftingRecipe.class);
		ASSEMBLY = new RecipeType<AssemblyRecipe>(FuturepackUids.ASSEMBLY, AssemblyRecipe.class);
		CRUSHER = new RecipeType<CrushingRecipe>(FuturepackUids.CRUSHER, CrushingRecipe.class);
		INDUTRIALFURNACE = new RecipeType<IndRecipe>(FuturepackUids.INDUTRIALFURNACE, IndRecipe.class);
		INDUSTRIALNEONFURNACE = new RecipeType<IndNeonRecipe>(FuturepackUids.INDUSTRIALNEONFURNACE, IndNeonRecipe.class);
		ZENTRIFUGE = new RecipeType<ZentrifugeRecipe>(FuturepackUids.ZENTRIFUGE, ZentrifugeRecipe.class);
		AIRBRUSH = new RecipeType<AirbrushRecipeJEI>(FuturepackUids.AIRBRUSH, AirbrushRecipeJEI.class);
		WATERCOOLER = new RecipeType<CoolingEntry>(FuturepackUids.WATERCOOLER, CoolingEntry.class);
		PARTPRESS = new RecipeType<PartPressJeiFakeRecipe>(FuturepackUids.PARTPRESS, PartPressJeiFakeRecipe.class);
		RECYCLER = new RecipeType<RecyclerJeiFakeRecipe>(FuturepackUids.RECYCLER, RecyclerJeiFakeRecipe.class);

		res2type.put(FuturepackUids.CRAFTING, CRAFTING);
		res2type.put(FuturepackUids.ASSEMBLY, ASSEMBLY);
		res2type.put(FuturepackUids.CRUSHER, CRUSHER);
		res2type.put(FuturepackUids.INDUTRIALFURNACE, INDUTRIALFURNACE);
		res2type.put(FuturepackUids.INDUSTRIALNEONFURNACE, INDUSTRIALNEONFURNACE);
		res2type.put(FuturepackUids.ZENTRIFUGE, ZENTRIFUGE);
		res2type.put(FuturepackUids.AIRBRUSH, AIRBRUSH);
		res2type.put(FuturepackUids.WATERCOOLER, WATERCOOLER);
		res2type.put(FuturepackUids.PARTPRESS, PARTPRESS);
		res2type.put(FuturepackUids.RECYCLER, RECYCLER);
	}

	public FuturepackPlugin()
	{
		if(PluginRecipeListener.INSTANCE == null)
		{
			PluginRecipeListener.INSTANCE = new PluginRecipeListener();
			RecipeManagerSyncer.INSTANCE.registerRecipeListener(PluginRecipeListener.INSTANCE);
		}

		WEAK_INSTANCE = new WeakReference<FuturepackPlugin>(this);
		registry = null;
		jei = null;
		airBrushRecipesInit = false;
	}

	@Override
	public void onRuntimeAvailable(IJeiRuntime jeiRuntime)
	{
		registry = jeiRuntime.getRecipeManager();
		bufferedManagers.forEach(this::addRecipes);
		bufferedManagers.clear();

		if(!airBrushRecipesInit)
		{
			airBrushRecipesInit = true;
			registry.addRecipes(AIRBRUSH, AirbrushRegistry.asJEIRecipes().toList());
		}
//		new RuntimeEditor(registry);
	}

	@Override
	public void registerCategories(IRecipeCategoryRegistration registry)
	{
		IGuiHelper guiHelper = registry.getJeiHelpers().getGuiHelper();

//		registry.addRecipeCategories(new ResearchRecipeCategory(guiHelper));
		registry.addRecipeCategories(new AssemblyCategory(guiHelper));
		registry.addRecipeCategories(new ZentrifugeCategory(guiHelper));
		registry.addRecipeCategories(new IndustrialCategory(guiHelper));
		registry.addRecipeCategories(new IndNeonCategorie(guiHelper));
		registry.addRecipeCategories(new CrusherCategory(guiHelper));
		registry.addRecipeCategories(new AirbrushCategory(guiHelper));
		registry.addRecipeCategories(new RecyclerCategory(guiHelper));
		registry.addRecipeCategories(new WaterCoolerCategory(guiHelper));
		registry.addRecipeCategories(new PartPressCategory(guiHelper));
	}

	@Override
	public void registerRecipeTransferHandlers(IRecipeTransferRegistration registration)
	{
		registration.addRecipeTransferHandler(GuiTechTable.ContainerTechTable.class,   RecipeTypes.CRAFTING, 0, 9, 10, 54);
		registration.addRecipeTransferHandler(GuiTechTable.ContainerTechTable.class, CRAFTING, 0, 9, 10, 54);
		registration.addRecipeTransferHandler(GuiAssemblyTable.ContainerAssemblyTable.class, ASSEMBLY, 1, 3, 6, 36);
		registration.addRecipeTransferHandler(GuiCrusher.ContainerCrusher.class, CRUSHER, 0, 1, 2, 36);
		registration.addRecipeTransferHandler(GuiIndFurnace.ContainerIndFurnace.class, INDUTRIALFURNACE, 1, 3, 8, 36);
		registration.addRecipeTransferHandler(GuiIndNeonFurnace.ContainerIndNeonFurnace.class, INDUSTRIALNEONFURNACE, 0, 3, 6, 36);
		registration.addRecipeTransferHandler(GuiZentrifuge.ContainerZentrifuge.class, ZENTRIFUGE, 0, 1, 4, 36);

		registration.addRecipeTransferHandler(GuiConstructionTable.ContainerConstructionTable.class,  RecipeTypes.CRAFTING, 3, 9, 12, 36);
		registration.addRecipeTransferHandler(GuiAssemblyRezeptCreator.ContainerAssemblyRezeptCreator.class, ASSEMBLY, 0, 3, 6, 36);
	}

	@Override
	public void registerRecipeCatalysts(IRecipeCatalystRegistration registry)
	{
		registry.addRecipeCatalyst(new ItemStack(InventoryBlocks.techtable), CRAFTING,  RecipeTypes.CRAFTING);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.opti_bench_w), CRAFTING, RecipeTypes.CRAFTING);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.opti_bench_g), CRAFTING, RecipeTypes.CRAFTING);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.opti_bench_b), CRAFTING, RecipeTypes.CRAFTING);
//
		registry.addRecipeCatalyst(new ItemStack(InventoryBlocks.assembly_table_w), ASSEMBLY);
		registry.addRecipeCatalyst(new ItemStack(InventoryBlocks.assembly_table_g), ASSEMBLY);
		registry.addRecipeCatalyst(new ItemStack(InventoryBlocks.assembly_table_b), ASSEMBLY);
		registry.addRecipeCatalyst(new ItemStack(InventoryBlocks.industrial_furnace), INDUTRIALFURNACE);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.industrial_neon_furnace_w), INDUSTRIALNEONFURNACE);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.industrial_neon_furnace_g), INDUSTRIALNEONFURNACE);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.industrial_neon_furnace_b), INDUSTRIALNEONFURNACE);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.zentrifuge_w), ZENTRIFUGE);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.zentrifuge_g), ZENTRIFUGE);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.zentrifuge_b), ZENTRIFUGE);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.crusher_w), CRUSHER);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.crusher_g), CRUSHER);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.crusher_b), CRUSHER);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.recycler_w), RECYCLER);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.recycler_g), RECYCLER);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.recycler_b), RECYCLER);
		registry.addRecipeCatalyst(new ItemStack(MiscItems.shredder), RECYCLER);
		registry.addRecipeCatalyst(new ItemStack(MiscItems.timemanipulator), RECYCLER);
		registry.addRecipeCatalyst(new ItemStack(MiscItems.lasercutter), RECYCLER);
		registry.addRecipeCatalyst(new ItemStack(MiscItems.analyzer), RECYCLER);
		registry.addRecipeCatalyst(new ItemStack(ToolItems.airBrush), AIRBRUSH);
		registry.addRecipeCatalyst(new ItemStack(InventoryBlocks.water_cooler), WATERCOOLER);
		registry.addRecipeCatalyst(new ItemStack(InventoryBlocks.part_press), PARTPRESS);
	}

	@Override
	public void registerRecipes(IRecipeRegistration registry)
	{
		long now = System.currentTimeMillis();
		recipeTime.forEach((manager, time) -> {
			if(now - time < 5*60*1000)//got added less then 5 minutes ago
			{
				RecipeType<?> type = res2type.get(manager.getName());
				if(type!=null)
				{
					registry.addRecipes(type, new ArrayList(manager.getRecipes()));
				}

			}
		});

//		registry.addRecipes(FPAssemblyManager.instance.recipes, ASSEMBLY);
//		registry.addRecipes(FPCrushingManager.instance.recipes, CRUSHER);
//		registry.addRecipes(FPIndustrialFurnaceManager.instance.recipes, INDUTRIALFURNACE);
//		registry.addRecipes(FPIndustrialNeonFurnaceManager.instance.recipes, INDUSTRIALNEONFURNACE);
//		registry.addRecipes(FPZentrifugeManager.instance.recipes, ZENTRIFUGE);
//		registry.addRecipes(FPRecyclerShredderManager.instance.getRecipes(), RECYCLER);
//		registry.addRecipes(FPRecyclerLaserCutterManager.instance.getRecipes(), RECYCLER);
//		registry.addRecipes(FPRecyclerTimeManipulatorManager.instance.getRecipes(), RECYCLER);

		if(AirbrushRegistry.asJEIRecipes()!=null)
		{
			airBrushRecipesInit = true;
			registry.addRecipes(AIRBRUSH, AirbrushRegistry.asJEIRecipes().collect(Collectors.toList()));
		}

		else
			airBrushRecipesInit = false;
		registry.addRecipes(WATERCOOLER, new ArrayList<>(WaterCoolerManager.INSTANCE.recipes.values()));
		FakeRecipeGenerator.registerRecyclerAnalyserRecipes(registry);
		FakeRecipeGenerator.registerPartPressRecipes(registry);

		addDescription(MiscItems.spawn_note, registry);
		addDescription(ToolItems.escanner, registry);
	}

	private void addDescription(Item item, IRecipeRegistration registry)
	{
		registry.addIngredientInfo(new ItemStack(item), VanillaTypes.ITEM_STACK, new TranslatableComponent(item.getDescriptionId()+".desc"));
	}


	@Override
	public void registerVanillaCategoryExtensions(IVanillaCategoryExtensionRegistration registration)
	{
		IExtendableRecipeCategory<CraftingRecipe, ICraftingCategoryExtension> craftingCategory = registration.getCraftingCategory();
		craftingCategory.addCategoryExtension(ShapedRecipeWithResearch.class, ShapedResearchCraftingExtension::new);
		craftingCategory.addCategoryExtension(ShapelessRecipeWithResearch.class, ShapelessResearchCraftingExtension::new);
	}

	@Override
	public ResourceLocation getPluginUid()
	{
		return new ResourceLocation(Constants.MOD_ID, "jei");
	}


	private Map<ISyncedRecipeManager<?>, Long> recipeTime = new Object2LongArrayMap<>(20);

	@SuppressWarnings({ "unchecked", "removal", "rawtypes" })
	public void addRecipes(ISyncedRecipeManager<?> t)
	{
		if(registry != null)
		{
			ResourceLocation res = t.getName();

			if(res!=null)
			{
				RecipeType<?> type = res2type.get(res);
				Collection<?> col = t.getRecipes();

				if(type!=null)
				{
					registry.addRecipes(type, new ArrayList(col));
				}
				else
				{
					col.forEach(recipe -> registry.addRecipe(recipe, res));
					FPLog.logger.error("[JEI] Unknown recype type '%s' ", res);
				}
				FPLog.logger.info("Added %s recipes for %s", col.size(), res);
				recipeTime.put(t, System.currentTimeMillis());
			}
		}
		else
		{
			bufferedManagers.add(t);
		}
	}

//	@Override
//	public void register(IModRegistry registry)
//	{
//		jei = registry.getJeiHelpers();
//		IGuiHelper guiHelper = jei.getGuiHelper();
//
//		registry.handleRecipes(ShapedRecipeWithResearch.class, r ->{ return new ShapedResearchWrapper(FuturepackPlugin.jei.getGuiHelper(), r);}, "minecraft.crafting");
//		registry.handleRecipes(RecipeMaschin.class, r ->{ return new ShapedResearchWrapper(FuturepackPlugin.jei.getGuiHelper(), r);}, "minecraft.crafting");
//		registry.handleRecipes(RecipeOwnerBased.class, r ->{ return new ShapedResearchWrapper(FuturepackPlugin.jei.getGuiHelper(), r);}, "minecraft.crafting");
//		registry.handleRecipes(ShapelessRecipeWithResearch.class, r ->{ return new ShapelessResearchWrapper(FuturepackPlugin.jei.getGuiHelper(), r);}, "minecraft.crafting");
//
//		registry.handleRecipes(AssemblyRecipe.class, r ->{ return new AssemblyWrapper(FuturepackPlugin.jei.getGuiHelper(), r);}, FuturepackUids.ASSEMBLY);
//		registry.handleRecipes(ZentrifugeRecipe.class, r ->{ return new ZentrifugeWrapper(FuturepackPlugin.jei.getGuiHelper(), r);}, FuturepackUids.ZENTRIFUGE);
//		registry.handleRecipes(IndRecipe.class, r ->{ return new IndustrialWrapper(FuturepackPlugin.jei.getGuiHelper(), r);}, FuturepackUids.INDUTRIALFURNACE);
//		registry.handleRecipes(IndNeonRecipe.class, r ->{ return new IndNeonWrapper(FuturepackPlugin.jei.getGuiHelper(), r);}, FuturepackUids.INDUSTRIALNEONFURNACE);
//		registry.handleRecipes(CrushingRecipe.class, r ->{ return new CrusherWrapper(FuturepackPlugin.jei.getGuiHelper(), r);}, FuturepackUids.CRUSHER);
//		registry.handleRecipes(AirbrushRecipeJEI.class, r ->{ return new AirbrushWrapper(FuturepackPlugin.jei.getGuiHelper(), r);}, FuturepackUids.AIRBRUSH);
//		registry.handleRecipes(IRecyclerRecipe.class, r ->{ return new RecyclerWrapper(FuturepackPlugin.jei.getGuiHelper(), r);}, FuturepackUids.RECYCLER);
//		registry.handleRecipes(WaterCoolerManager.CoolingEntry.class, r ->{ return new WaterCoolerWrapper(r);}, FuturepackUids.WATERCOOLER);
//		registry.handleRecipes(PartPressJeiFakeRecipe.class, r ->{ return new PartPressWrapper(r);}, FuturepackUids.PARTPRESS);
//
//
////		IRecipeRegistryPlugin
////		registry.addRecipeRegistryPlugin(recipeRegistryPlugin);
//	}

//	public static class RuntimeEditor implements IRecipeEditor
//	{
//		private final IRecipeRegistry registry;
//
//		public RuntimeEditor(IRecipeRegistry registry)
//		{
//			this.registry = registry;
//			RecipeRuntimeEditor.editor = this;
//		}
//
//		@Override
//		public void addRecipe(Object o)
//		{
//			registry.addRecipe(o);
//		}
//
//		@Override
//		public void removeRecipe(Object o)
//		{
//			registry.removeRecipe(o);
//		}
//	}

	static class PluginRecipeListener implements Consumer<ISyncedRecipeManager<?>>
	{
		static PluginRecipeListener INSTANCE;

		@Override
		public void accept(ISyncedRecipeManager<?> t)
		{
			FuturepackPlugin plugin = FuturepackPlugin.WEAK_INSTANCE.get();

			if(plugin != null)
				plugin.addRecipes(t);

		}
	};

}
