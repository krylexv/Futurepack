package futurepack.extensions.jei.partpress;

import java.util.ArrayList;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.common.block.inventory.InventoryBlocks;
import futurepack.depend.api.VanillaTagPredicate;
import futurepack.extensions.jei.BaseRecipeCategory;
import futurepack.extensions.jei.FuturepackUids;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.drawable.IDrawableAnimated;
import mezz.jei.api.gui.drawable.IDrawableAnimated.StartDirection;
import mezz.jei.api.gui.drawable.IDrawableStatic;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import net.minecraft.resources.ResourceLocation;

public class PartPressCategory extends BaseRecipeCategory<PartPressJeiFakeRecipe>
{
	private IDrawableAnimated arrow;
	private IDrawableStatic staticFlame;
	private IDrawableAnimated animatedFlame;
	
	public PartPressCategory(IGuiHelper gui)
	{
		super(gui, InventoryBlocks.part_press, FuturepackUids.PARTPRESS, 0, 0);
		
		
	}
	
	@Override
	public void draw(PartPressJeiFakeRecipe recipe, PoseStack matrixStack, double mouseX, double mouseY) 
	{
		arrow.draw(matrixStack, 72, 21);
		animatedFlame.draw(matrixStack, 1, 23);
		super.draw(recipe, matrixStack, mouseX, mouseY);
	}



	@Override
	public Class<? extends PartPressJeiFakeRecipe> getRecipeClass() 
	{
		return PartPressJeiFakeRecipe.class;
	}
	
	
	@Override
	public void setRecipe(IRecipeLayoutBuilder builder, PartPressJeiFakeRecipe recipe, IFocusGroup focuses) 
	{
		builder.addSlot(RecipeIngredientRole.INPUT, 72+1, 0+1).addItemStacks((new VanillaTagPredicate(recipe.input)).collectAcceptedItems(new ArrayList<>()));
		builder.addSlot(RecipeIngredientRole.OUTPUT, 72+1, 40+1).addItemStack(recipe.output);
	}
	
//	@Override
//	public void setIngredients(PartPressJeiFakeRecipe rec, IIngredients ingredients) 
//	{
//		ingredients.setInputs(VanillaTypes.ITEM, (new VanillaTagPredicate(rec.input)).collectAcceptedItems(new ArrayList<>()));
//		ingredients.setOutput(VanillaTypes.ITEM, rec.output); 
//	}
//
//	@Override
//	public void setRecipe(IRecipeLayout recipeLayout, PartPressJeiFakeRecipe recipe, IIngredients ingredients) 
//	{
//		IGuiItemStackGroup itemstacks = recipeLayout.getItemStacks();
//		
//		itemstacks.init(0, true, 72, 0);
//		itemstacks.init(1, false, 72, 40);
//		
//		itemstacks.set(ingredients);
//	}

	@Override
	protected IDrawable createBackground(IGuiHelper gui) 
	{
		ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/stanze.png");
		
		IDrawableStatic base = gui.createDrawable(res, 176, 17, 36, 16);
		arrow = gui.createAnimatedDrawable(base, 50, StartDirection.TOP, false);
		staticFlame = gui.createDrawable(res, 176, 0, 14, 14);
		animatedFlame = gui.createAnimatedDrawable(staticFlame, 300, IDrawableAnimated.StartDirection.TOP, true);
		return  gui.createDrawable(res, 7, 13, 108, 58);
	}

	@Override
	public boolean isResearched(PartPressJeiFakeRecipe rec) 
	{
		return true;
	}

}
