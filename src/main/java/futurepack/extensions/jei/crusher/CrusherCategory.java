package futurepack.extensions.jei.crusher;

import java.util.ArrayList;

import futurepack.api.Constants;
import futurepack.common.block.modification.ModifiableBlocks;
import futurepack.common.recipes.crushing.CrushingRecipe;
import futurepack.extensions.jei.BaseRecipeCategory;
import futurepack.extensions.jei.FuturepackUids;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import net.minecraft.resources.ResourceLocation;

public class CrusherCategory extends BaseRecipeCategory<CrushingRecipe>
{

	public CrusherCategory(IGuiHelper gui)
	{
		super(gui, ModifiableBlocks.crusher_w, FuturepackUids.CRUSHER, 80-47, 31-21);
	}

	@Override
	public Class<? extends CrushingRecipe> getRecipeClass() 
	{
		return CrushingRecipe.class;
	}

	@Override
	public void setRecipe(IRecipeLayoutBuilder builder, CrushingRecipe recipe, IFocusGroup focuses) 
	{
		builder.addSlot(RecipeIngredientRole.INPUT, 1, 5).addItemStacks(recipe.getInput().collectAcceptedItems(new ArrayList<>()));
		
		builder.addSlot(RecipeIngredientRole.OUTPUT, 61, 5).addItemStack(recipe.getOutput());
	}
	
	@Override
	protected IDrawable createBackground(IGuiHelper gui) 
	{
		ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/super_crusher.png");
		return gui.createDrawable(res, 55, 30, 82, 27);
	}

	@Override
	public boolean isResearched(CrushingRecipe rec) 
	{
		return rec.isLocalResearched();
	}
	
}
