package futurepack.extensions.ichunutil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.function.Predicate;

import futurepack.world.protection.FPDungeonProtection;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.eventbus.EventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class PortalGunMain
{
	public static final PortalGunMain INSTANCE = new PortalGunMain();
	
	private Method m_getPos, m_getPoses;
	
	public void init()
	{
		try
		{
			Class<?> c_Pickup = Class.forName("me.ichun.mods.ichunutil.api.event.BlockEntityEvent$Pickup");
			Class<?> c_Place = Class.forName("me.ichun.mods.ichunutil.api.event.BlockEntityEvent$Place");
				
			m_getPos = c_Place.getMethod("getPos");
			m_getPoses = c_Pickup.getMethod("getPoses");
				
				
			Method m1 = PortalGunMain.class.getMethod("onBlockPickup", WorldEvent.class);
			Method m2 = PortalGunMain.class.getMethod("onBlockPlace", WorldEvent.class);
				
			//Hacking the EventBus... again...
			Method m_register = EventBus.class.getDeclaredMethod("register", Class.class, Object.class, Method.class);
			m_register.setAccessible(true);
			m_register.invoke(MinecraftForge.EVENT_BUS, c_Pickup, this, m1); //if any one will see this, them will kill me.
			m_register.invoke(MinecraftForge.EVENT_BUS, c_Place, this, m2);
		}
		catch(ClassNotFoundException e)
		{
			//IChunUtil is not installed
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private BlockPos getPos(WorldEvent e) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		return (BlockPos) m_getPos.invoke(e);
	}
	
	private Collection<BlockPos> getPoses(WorldEvent e) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		return (Collection<BlockPos>) m_getPoses.invoke(e);
	}
	
	@SubscribeEvent
	public void onBlockPickup(WorldEvent o)
	{
		try 
		{
			Collection<BlockPos> col = getPoses(o);
			col.removeIf(new Predicate<BlockPos>()
			{
				@Override
				public boolean test(BlockPos t)
				{
					return FPDungeonProtection.isUnbreakable((Level) o.getWorld(), t);
				}
				
			});
		}
		catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
		{
			e.printStackTrace();
		}
		
	}
	
	@SubscribeEvent
	public void onBlockPlace(WorldEvent o)
	{
		try
		{
			BlockPos pos = getPos(o);
			if(FPDungeonProtection.isUnplaceable((Level) o.getWorld(), pos))
			{
				o.setCanceled(true);
			}
		}
		catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
		{
			e.printStackTrace();
		}
		
	}
}
