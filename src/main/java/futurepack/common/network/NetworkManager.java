package futurepack.common.network;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.base.Predicates;

import futurepack.api.PacketBase;
import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockSelector;
import futurepack.api.interfaces.IBlockValidator;
import futurepack.api.interfaces.INetworkUser;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import futurepack.common.FPBlockSelector;
import futurepack.common.FPSelectorHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.core.BlockPos;
import net.minecraft.util.thread.BlockableEventLoop;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraft.world.level.material.Material;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;

public class NetworkManager implements Runnable
{
	private static NetworkManager instance = new NetworkManager();
 	private static IBlockSelector selectorNetwork = new IBlockSelector()
	{
		@Override
		public boolean isValidBlock(Level w, BlockPos pos, Material m, boolean dia, ParentCoords parent)
		{
			if(dia)
				return false;

			if(w.getBlockEntity(pos) instanceof ITileNetwork)
			{
				ITileNetwork net = (ITileNetwork) w.getBlockEntity(pos);
				return net.isNetworkAble();
			}
			return false;
		}

		@Override
		public boolean canContinue(Level w, BlockPos pos, Material m, boolean dia, ParentCoords parent)
		{
			ITileNetwork net = (ITileNetwork) w.getBlockEntity(pos);
			return net.isWire();
		}
	};
	private static IBlockValidator selectorMachines = new IBlockValidator()
	{
		@Override
		public boolean isValidBlock(Level w, ParentCoords pos)
		{
			ITileNetwork net = (ITileNetwork) w.getBlockEntity(pos);
			return net!=null && !net.isWire();
		}

	};


	public static Set<ITileNetwork> pingNetwork(BlockEntity tile)
	{
		FunkPacketPing pingPkt = new FunkPacketPing(tile.getBlockPos(), (ITileNetwork) tile);
		try {
			sendPacketAndWait(new INetworkUser() {

				@Override
				public void postPacketSend(PacketBase pkt)
				{
				}

				@Override
				public Level getSenderWorld()
				{
					return tile.getLevel();
				}

				@Override
				public BlockPos getSenderPosition()
				{
					return tile.getBlockPos();
				}
			}, pingPkt);
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}

		return pingPkt.getReachable();
	}

	/**
	 * Send the packet and waits on the current thread. This will lock-up if called on the server thread. This will run {@link INetworkUser#postPacketSend(PacketBase)} on the current thread.
	 *
	 * @param user
	 * @param base
	 * @throws InterruptedException
	 */
	public static void sendPacketAndWait(INetworkUser user, PacketBase base) throws InterruptedException
	{
		if(Thread.currentThread() == instance.thread)
		{
			throw new IllegalCallerException("Dont call this from the NetworkThread!");
		}

		boolean[] waiting = {true};

		sendPacketThreaded(new INetworkUser() {

			@Override
			public void postPacketSend(PacketBase pkt)
			{
				if(pkt == base)
				{
					waiting[0] = false;
					synchronized (base) {
						base.notify();
					}

				}
			}

			@Override
			public Level getSenderWorld()
			{
				return user.getSenderWorld();
			}

			@Override
			public BlockPos getSenderPosition()
			{
				return user.getSenderPosition();
			}
		}, base);
		if(waiting[0])
		{
			synchronized (base)
			{
				base.wait(1000);
			}
		}
		user.postPacketSend(base);
	}

	public static void sendPacketThreaded(INetworkUser user, PacketBase pkt)
	{
		instance.sendSave(user, pkt, 0);
	}

	public static void sendPacketUnthreaded(INetworkUser user, PacketBase pkt, int networkDepth)
	{
		instance.send(user, pkt, networkDepth);
	}

	private Thread thread;
	private final List<Entry> todo = Collections.synchronizedList(new ArrayList<Entry>());
	private long ping = 0;
	private final List<Runnable> asyncTasks = Collections.synchronizedList(new ArrayList<Runnable>());


	public NetworkManager()
	{
		thread = new Thread(this, "Network-Thread");
		thread.setDaemon(true);
		thread.start();
	}

	private void send(INetworkUser user, PacketBase pkt, int networkDepth)
	{
		//System.out.println("Ping "+(System.currentTimeMillis() - ping));
		//System.out.println("NetworkManager.send()");
		FPBlockSelector sel = FPSelectorHelper.getSelectorSave(user.getSenderWorld(), user.getSenderPosition(), selectorNetwork, true); //TileEnties are only available in main thread
		if(!isWorldThread(user.getSenderWorld()))
		{
			Collection<ParentCoords> machines = supplyAsync(user.getSenderWorld(), () -> sel.getValidBlocks(selectorMachines));//only in mian thread!
			Set<ITileNetwork> tiles = machines.parallelStream()
					.map(p -> supplyAsync(user.getSenderWorld(), () -> user.getSenderWorld().getBlockEntity(p)))//this will take some ticks so make it in parallel for large networks
					.filter(t -> !t.isRemoved())
					.map(t -> (ITileNetwork)t)
					.collect(Collectors.toSet());
			for(ITileNetwork net : tiles)
			{
				pkt.post(net, networkDepth);
			}
		}
		else
		{
			Collection<ParentCoords> machines = sel.getValidBlocks(selectorMachines);//only in mian thread!
			for(ParentCoords p : machines)
			{
				BlockEntity t =  user.getSenderWorld().getBlockEntity(p);
				if(t.isRemoved())
					continue;
				pkt.post((ITileNetwork)t, networkDepth);
			}
		}
		if(networkDepth==0)
			user.postPacketSend(pkt);
	}



	public void sendSave(INetworkUser user, PacketBase pkt, int networkDepth)
	{
		if(isBusy())
		{
			send(user, pkt, networkDepth); //Tunneling new packets so even if the network thread is blocked it is not important.
		}
		else if(thread.isAlive())
		{
			todo.add(new Entry(user, pkt, networkDepth));
		}
		else
		{
			System.err.println("Network Thread DIED");
		}
	}



	public void submitAsync(Runnable r)
	{
		this.asyncTasks.add(r);
	}



	@Override
	public void run()
	{
		while(true)
		{
			try
			{

				Entry[] todo;
				synchronized (this.todo)
				{
					todo = this.todo.toArray(Entry[]::new);
					this.todo.clear();
				}


				ping = System.currentTimeMillis();
				for(Entry next : todo)
				{
					if(next!=null)
					{
						send(next.user(), next.pkt(), next.networkDepth());
					}
				}


				Runnable[] asyncTask ;
				synchronized (this.asyncTasks)
				{
					asyncTask = this.asyncTasks.toArray(Runnable[]::new);
					this.asyncTasks.clear();
				}
				for(Runnable r : asyncTask)
				{
					r.run();
				}


				if(this.todo.isEmpty() && this.asyncTasks.isEmpty())
				{
					try
					{
						Thread.sleep(50);
					}
					catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	public boolean isBusy()
	{
		return System.currentTimeMillis() - ping > 1000*120 && this.todo.size() > 1000;
	}

	private static record Entry(INetworkUser user, PacketBase pkt, int networkDepth)
	{
	}

	public static boolean isWorldThread(Level w)
	{
		return getExecutor(w).isSameThread();
	}

	public static BlockableEventLoop<? extends Runnable> getExecutor(Level w)
	{
		if(w.getServer()!=null)
		{
			return w.getServer();
		}
		else
		{
			return DistExecutor.callWhenOn(Dist.CLIENT, () -> new Callable<BlockableEventLoop<Runnable>>()
			{
				@Override
				public BlockableEventLoop<Runnable> call() throws Exception
				{
					return Minecraft.getInstance();
				}
			});
		}
	}

	public static <V> V supplyAsync(Level w, Supplier<V> supplier)
	{
		BlockableEventLoop<? extends Runnable> t = getExecutor(w);
		return t.isSameThread() ? supplier.get() : CompletableFuture.supplyAsync(encapsuledSupplier(supplier), t).join();
	}

	private static <V> Supplier<V> encapsuledSupplier(Supplier<V> supplier)
	{
		return () -> {
			try
			{
				return supplier.get();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		};
	}

	public static class Wireless
	{
		private static AtomicBoolean isSendingEvents = new AtomicBoolean(false);
		private static final List<Runnable> tasks = Collections.synchronizedList(new ArrayList<Runnable>());

		private static Map<DimensionType, AntennaNetwork> Networks = Collections.synchronizedMap(new WeakHashMap<DimensionType, AntennaNetwork>());

		static record AntennaNetwork(Set<WeakReference<Consumer<EventWirelessFunk>>> listeners, ReentrantReadWriteLock lock)
		{
			public AntennaNetwork()
			{
				this(new HashSet<>(), new ReentrantReadWriteLock());
			}

			public void add(WeakReference<Consumer<EventWirelessFunk>> weakReference)
			{
				this.listeners().add(weakReference);
			}

			public Iterator<WeakReference<Consumer<EventWirelessFunk>>> iterator()
			{
				return this.listeners().iterator();
			}
		}

		public static void registerWirelessTile(Consumer<EventWirelessFunk> t, Level w)
		{
			if(Wireless.isSendingEvents.get())
			{
				Wireless.tasks.add(() -> registerWirelessTile(t, w));
			}
			else
			{
				AntennaNetwork set = Wireless.Networks.computeIfAbsent(w.dimensionType(), type -> new AntennaNetwork());

				Lock wL = set.lock().writeLock();
				wL.lock();
				set.add(new WeakReference<Consumer<EventWirelessFunk>>(t));
				wL.unlock();

			}
		}

		public static void unregisterWirelessTile(Consumer<EventWirelessFunk> t, Level w)
		{
			if(Wireless.isSendingEvents.get())
			{
				Wireless.tasks.add(() -> unregisterWirelessTile(t, w));
			}
			else
			{
				AntennaNetwork set = Wireless.Networks.get(w.dimensionType());
				Lock wL = set.lock().writeLock();
				wL.lock();
				Iterator<WeakReference<Consumer<EventWirelessFunk>>> iter = set.iterator();
				while(iter.hasNext())
				{
					WeakReference<Consumer<EventWirelessFunk>> ref = iter.next();
					if(ref == null)
						iter.remove();
					else if(ref.get() == null)
						iter.remove();
					else if(ref.get() == t)
					{
						iter.remove();
						break;
					}
				}
				wL.unlock();
			}
		}

		public static void sendEvent(EventWirelessFunk e, Level dim)
		{
			AntennaNetwork set = Wireless.Networks.get(dim.dimensionType());
			Lock rL = set.lock().readLock();
			try
			{
				rL.tryLock(5000, TimeUnit.NANOSECONDS);//1 tick = 50ms -> 100 antenna blocks * 100 packets/tick -> 10000 calls in 50ms -> each call 1000/5 = 2000/10 = 200calls/1ms;  1/200ms = 5000nano sec

				boolean alreadySending = Wireless.isSendingEvents.get();
				Wireless.isSendingEvents.set(true);

				Stream<Consumer<EventWirelessFunk>> s = set.listeners().stream().map(w -> w.get());
				s.filter(Predicates.notNull()).forEach(c -> c.accept(e));
				rL.unlock();

				if(!alreadySending)
				{
					Wireless.isSendingEvents.set(false);

					Runnable[] tasksArr;
					synchronized (Wireless.tasks)
					{
						tasksArr = Wireless.tasks.toArray(Runnable[]::new);
						Wireless.tasks.clear();
					}
					for(Runnable r : tasksArr)
					{
						r.run();//dont run the tasks in the synchrinized block as that will cuase thread locks if those runnables call sendEvent again
					}
				}
			}
			catch (InterruptedException e1)
			{
				NetworkManager.instance.submitAsync(() -> sendEvent(e, dim));
			}
		}


	}
}
