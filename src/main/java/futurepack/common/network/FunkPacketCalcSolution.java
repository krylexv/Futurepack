package futurepack.common.network;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import futurepack.common.network.FunkPacketCalculation.EnumCalculationType;
import net.minecraft.core.BlockPos;

public class FunkPacketCalcSolution extends PacketBase
{
	public final EnumCalculationType type;
	public final int solutionAmout;
	public final BlockPos receiver;
	
	
	public FunkPacketCalcSolution(BlockPos src, ITileNetwork net, EnumCalculationType type, int amount, BlockPos receiver)
	{
		super(src, net);
		this.type=type;
		solutionAmout = amount;
		this.receiver = receiver;
	}
}
