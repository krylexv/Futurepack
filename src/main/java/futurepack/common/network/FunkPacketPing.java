package futurepack.common.network;

import java.util.Set;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import net.minecraft.core.BlockPos;

public class FunkPacketPing extends PacketBase
{
	
	public FunkPacketPing(BlockPos src, ITileNetwork net)
	{
		super(src, net);
	}

	public Set<ITileNetwork> getReachable()
	{
		return super.getReceivers();
	}
}
