package futurepack.common.network;

import java.util.HashSet;
import java.util.Set;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import futurepack.common.block.misc.TileEntityBlockTeleporter;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.state.BlockState;

public class FunkPacketBlockTeleporter extends PacketBase
{
	public final BlockState state;
	private final Set<TileEntityBlockTeleporter> otherTeleporters = new HashSet<>();

	
	public FunkPacketBlockTeleporter(BlockPos src, ITileNetwork net, BlockState state)
	{
		super(src, net);
		this.state = state;
	}

	public Set<TileEntityBlockTeleporter> getTeleporters()
	{
		return otherTeleporters;
	}
	
	public void addTeleporter(TileEntityBlockTeleporter teleporter)
	{
		if(NetworkManager.supplyAsync(teleporter.getLevel(), () -> teleporter.getState()) == state)
		{
			otherTeleporters.add(teleporter);
		}
	}
}
