package futurepack.common.network;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import net.minecraft.core.BlockPos;

public class FunkPacketNeon extends PacketBase
{
	public final int needed;
	public int collected = 0;
//	public int speedmodifier = 0;
	
	public FunkPacketNeon(BlockPos src, ITileNetwork net, int needed)
	{
		super(src, net);
		this.needed = needed;
	}

}
