package futurepack.common;

import futurepack.api.Constants;
import futurepack.common.block.TileEntityLinkedLight;
import futurepack.common.block.deco.DecoBlocks;
import futurepack.common.block.deco.TileEntityNeonLamp;
import futurepack.common.block.inventory.InventoryBlocks;
import futurepack.common.block.inventory.TileEntityAdvancedBoardComputer;
import futurepack.common.block.inventory.TileEntityAssemblyTable;
import futurepack.common.block.inventory.TileEntityBatteryBox;
import futurepack.common.block.inventory.TileEntityBlockBreaker;
import futurepack.common.block.inventory.TileEntityBlockPlacer;
import futurepack.common.block.inventory.TileEntityBoardComputer;
import futurepack.common.block.inventory.TileEntityBrennstoffGenerator;
import futurepack.common.block.inventory.TileEntityCompositeChest;
import futurepack.common.block.inventory.TileEntityDroneStation;
import futurepack.common.block.inventory.TileEntityFermentationBarrel;
import futurepack.common.block.inventory.TileEntityFlashServer;
import futurepack.common.block.inventory.TileEntityForscher;
import futurepack.common.block.inventory.TileEntityFuelCell;
import futurepack.common.block.inventory.TileEntityIndustrialFurnace;
import futurepack.common.block.inventory.TileEntityLogisticChest;
import futurepack.common.block.inventory.TileEntityModulT1;
import futurepack.common.block.inventory.TileEntityModulT2;
import futurepack.common.block.inventory.TileEntityModulT3;
import futurepack.common.block.inventory.TileEntityOptiBenchCraftingModule;
import futurepack.common.block.inventory.TileEntityPartPress;
import futurepack.common.block.inventory.TileEntityPusher;
import futurepack.common.block.inventory.TileEntityScannerBlock;
import futurepack.common.block.inventory.TileEntitySharedResearcher;
import futurepack.common.block.inventory.TileEntityTechtable;
import futurepack.common.block.inventory.TileEntityTickingPusher;
import futurepack.common.block.inventory.TileEntityWardrobe;
import futurepack.common.block.inventory.TileEntityWaterCooler;
import futurepack.common.block.logistic.LogisticBlocks;
import futurepack.common.block.logistic.TileEntityFluidIntake;
import futurepack.common.block.logistic.TileEntityFluidTank;
import futurepack.common.block.logistic.TileEntityFluidTube;
import futurepack.common.block.logistic.TileEntityInsertNode;
import futurepack.common.block.logistic.TileEntityLaserTransmitter;
import futurepack.common.block.logistic.TileEntityPipeNeon;
import futurepack.common.block.logistic.TileEntityPipeNormal;
import futurepack.common.block.logistic.TileEntityPipeSupport;
import futurepack.common.block.logistic.TileEntitySpaceLink;
import futurepack.common.block.logistic.TileEntitySyncronizer;
import futurepack.common.block.logistic.TileEntityWireNetwork;
import futurepack.common.block.logistic.TileEntityWireNormal;
import futurepack.common.block.logistic.TileEntityWireRedstone;
import futurepack.common.block.logistic.TileEntityWireSuper;
import futurepack.common.block.logistic.TileEntityWireSupport;
import futurepack.common.block.logistic.frames.TileEntityMovingBlocks;
import futurepack.common.block.logistic.frames.TileEntitySpaceshipMover;
import futurepack.common.block.logistic.monorail.TileEntityMonorailCharger;
import futurepack.common.block.logistic.monorail.TileEntityMonorailStation;
import futurepack.common.block.logistic.monorail.TileEntityMonorailWaypoint;
import futurepack.common.block.logistic.plasma.TileEntityPlasma2NeonT0;
import futurepack.common.block.logistic.plasma.TileEntityPlasmaConverter;
import futurepack.common.block.logistic.plasma.TileEntityPlasmaTransporter;
import futurepack.common.block.misc.MiscBlocks;
import futurepack.common.block.misc.TileEntityAirlockDoor;
import futurepack.common.block.misc.TileEntityAntenna;
import futurepack.common.block.misc.TileEntityBedrockRift;
import futurepack.common.block.misc.TileEntityBlockTeleporter;
import futurepack.common.block.misc.TileEntityClaime;
import futurepack.common.block.misc.TileEntityDungeonCheckpoint;
import futurepack.common.block.misc.TileEntityDungeonCore;
import futurepack.common.block.misc.TileEntityDungeonSpawner;
import futurepack.common.block.misc.TileEntityDungeonTeleporter;
import futurepack.common.block.misc.TileEntityExternCooler;
import futurepack.common.block.misc.TileEntityFallingTree;
import futurepack.common.block.misc.TileEntityForceField;
import futurepack.common.block.misc.TileEntityMagnet;
import futurepack.common.block.misc.TileEntityModularDoor;
import futurepack.common.block.misc.TileEntityNeonEngine;
import futurepack.common.block.misc.TileEntityOreTeleporter;
import futurepack.common.block.misc.TileEntityPulsit;
import futurepack.common.block.misc.TileEntityRFtoNEConverter;
import futurepack.common.block.misc.TileEntityResearchExchange;
import futurepack.common.block.misc.TileEntityRsTimer;
import futurepack.common.block.misc.TileEntitySaplingHolder;
import futurepack.common.block.misc.TileEntityStructureFixHelper;
import futurepack.common.block.misc.TileEntityTeleporter;
import futurepack.common.block.misc.TileEntityTyrosTreeGen;
import futurepack.common.block.modification.ModifiableBlocks;
import futurepack.common.block.modification.TileEntityElektroMagnet;
import futurepack.common.block.modification.TileEntityEntityEater;
import futurepack.common.block.modification.TileEntityEntityHealer;
import futurepack.common.block.modification.TileEntityEntityKiller;
import futurepack.common.block.modification.TileEntityExternalCore;
import futurepack.common.block.modification.TileEntityFluidPump;
import futurepack.common.block.modification.TileEntityModulT1Calculation;
import futurepack.common.block.modification.TileEntityRadar;
import futurepack.common.block.modification.TileEntityRocketLauncher;
import futurepack.common.block.modification.TileEntityWaterTurbine;
import futurepack.common.block.modification.machines.TileEntityCrusher;
import futurepack.common.block.modification.machines.TileEntityGasTurbine;
import futurepack.common.block.modification.machines.TileEntityImproveComponents;
import futurepack.common.block.modification.machines.TileEntityIndustrialNeonFurnace;
import futurepack.common.block.modification.machines.TileEntityInfusionGenerator;
import futurepack.common.block.modification.machines.TileEntityIonCollector;
import futurepack.common.block.modification.machines.TileEntityLifeSupportSystem;
import futurepack.common.block.modification.machines.TileEntityNeonFurnace;
import futurepack.common.block.modification.machines.TileEntityOptiAssembler;
import futurepack.common.block.modification.machines.TileEntityOptiBench;
import futurepack.common.block.modification.machines.TileEntityRecycler;
import futurepack.common.block.modification.machines.TileEntitySolarPanel;
import futurepack.common.block.modification.machines.TileEntitySorter;
import futurepack.common.block.modification.machines.TileEntityZentrifuge;
import futurepack.common.block.multiblock.MultiblockBlocks;
import futurepack.common.block.multiblock.TileEntityDeepCoreMinerInventory;
import futurepack.common.block.multiblock.TileEntityDeepCoreMinerMain;
import futurepack.common.block.plants.PlantBlocks;
import futurepack.common.block.plants.TileEntityOxades;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class FPTileEntitys 
{
	public static final BlockEntityType<TileEntityTechtable> TECHTABLE = register("techtable", TileEntityTechtable::new, InventoryBlocks.techtable);
	public static final BlockEntityType<TileEntityForscher> FORSCHER = register("forscher", TileEntityForscher::new, InventoryBlocks.forscher_w, InventoryBlocks.forscher_g, InventoryBlocks.forscher_b);
	public static final BlockEntityType<TileEntityScannerBlock> SCANNER_BLOCK = register("scanner_block", TileEntityScannerBlock::new, InventoryBlocks.scanner_block_w, InventoryBlocks.scanner_block_g, InventoryBlocks.scanner_block_b);
	public static final BlockEntityType<TileEntityNeonLamp> NEON_LAMP = register("neon_lamp", TileEntityNeonLamp::new,
			DecoBlocks.color_neon_lamp_black, DecoBlocks.color_neon_lamp_blue, DecoBlocks.color_neon_lamp_brown, DecoBlocks.color_neon_lamp_cyan, DecoBlocks.color_neon_lamp_gray, DecoBlocks.color_neon_lamp_green, DecoBlocks.color_neon_lamp_light_blue, DecoBlocks.color_neon_lamp_light_gray, DecoBlocks.color_neon_lamp_lime, DecoBlocks.color_neon_lamp_magenta, DecoBlocks.color_neon_lamp_orange, DecoBlocks.color_neon_lamp_pink, DecoBlocks.color_neon_lamp_purple, DecoBlocks.color_neon_lamp_purple, DecoBlocks.color_neon_lamp_red, DecoBlocks.color_neon_lamp_white, DecoBlocks.color_neon_lamp_yellow);
	public static final BlockEntityType<TileEntityLinkedLight> LINKED_LIGHT = register("linked_light", TileEntityLinkedLight::new, MiscBlocks.linked_light);
	public static final BlockEntityType<TileEntityExternCooler> EXTERN_COOLER = register("extern_cooler", TileEntityExternCooler::new, MiscBlocks.extern_cooler);
	public static final BlockEntityType<TileEntityCrusher> CRUSHER = register("crusher", TileEntityCrusher::new, ModifiableBlocks.crusher_w, ModifiableBlocks.crusher_g, ModifiableBlocks.crusher_b);
	public static final BlockEntityType<TileEntityNeonFurnace> NEON_FURNACE = register("neon_furnace", TileEntityNeonFurnace::new, ModifiableBlocks.neon_furnace_w, ModifiableBlocks.neon_furnace_g, ModifiableBlocks.neon_furnace_b);
	public static final BlockEntityType<TileEntityIonCollector> ION_COLLECTOR = register("ion_collector", TileEntityIonCollector::new, ModifiableBlocks.ion_collector_w, ModifiableBlocks.ion_collector_g, ModifiableBlocks.ion_collector_b);
	public static final BlockEntityType<TileEntitySolarPanel> SOLAR_PANEL = register("solar_panel", TileEntitySolarPanel::new, ModifiableBlocks.solar_panel_w, ModifiableBlocks.solar_panel_g, ModifiableBlocks.solar_panel_b);
	public static final BlockEntityType<TileEntityZentrifuge> ZENTRIFUGE = register("zentrifuge", TileEntityZentrifuge::new, ModifiableBlocks.zentrifuge_w, ModifiableBlocks.zentrifuge_g, ModifiableBlocks.zentrifuge_b);
	public static final BlockEntityType<TileEntityInfusionGenerator> PLASMA_GENERATOR = register("plasma_generator", TileEntityInfusionGenerator::new, ModifiableBlocks.infusion_generator_w, ModifiableBlocks.infusion_generator_g, ModifiableBlocks.infusion_generator_b);
	public static final BlockEntityType<TileEntityIndustrialNeonFurnace> INDUSTRIAL_NEON_FURNACE = register("industrial_neon_furnace", TileEntityIndustrialNeonFurnace::new, ModifiableBlocks.industrial_neon_furnace_w, ModifiableBlocks.industrial_neon_furnace_g, ModifiableBlocks.industrial_neon_furnace_b);
	public static final BlockEntityType<TileEntityRecycler> RECYCLER = register("recycler", TileEntityRecycler::new, ModifiableBlocks.recycler_w, ModifiableBlocks.recycler_g, ModifiableBlocks.recycler_b);
	public static final BlockEntityType<TileEntityOptiBench> OPTI_BENCH = register("opti_bench", TileEntityOptiBench::new, ModifiableBlocks.opti_bench_w, ModifiableBlocks.opti_bench_g, ModifiableBlocks.opti_bench_b);
	public static final BlockEntityType<TileEntityGasTurbine> GAS_TURBINE = register("gas_turbine", TileEntityGasTurbine::new, ModifiableBlocks.gas_turbine_w, ModifiableBlocks.gas_turbine_g, ModifiableBlocks.gas_turbine_b);
	public static final BlockEntityType<TileEntityOptiAssembler> OPTI_ASSEMBLER = register("opti_assembler", TileEntityOptiAssembler::new, ModifiableBlocks.opti_assembler_w, ModifiableBlocks.opti_assembler_g, ModifiableBlocks.opti_assembler_b);
	public static final BlockEntityType<TileEntitySorter> SORTER = register("sorter", TileEntitySorter::new, ModifiableBlocks.sorter);
	
	public static final BlockEntityType<TileEntityIndustrialFurnace> INDUSTRIAL_FURNACE = register("industrial_furnace", TileEntityIndustrialFurnace::new, InventoryBlocks.industrial_furnace);
	public static final BlockEntityType<TileEntityAssemblyTable> ASSEMBLY_TABLE = register("assembly_table", TileEntityAssemblyTable::new, InventoryBlocks.assembly_table_w, InventoryBlocks.assembly_table_g, InventoryBlocks.assembly_table_b);
	public static final BlockEntityType<TileEntityFlashServer> FLASH_SERVER = register("flash_server", TileEntityFlashServer::new, InventoryBlocks.flash_server_w, InventoryBlocks.flash_server_g, InventoryBlocks.flash_server_b);
	public static final BlockEntityType<TileEntityBlockPlacer> BLOCK_PLACER = register("block_placer", TileEntityBlockPlacer::new, InventoryBlocks.block_placer);
	public static final BlockEntityType<TileEntityBrennstoffGenerator> T0_GENERATOR = register("t0_generator", TileEntityBrennstoffGenerator::new, InventoryBlocks.t0_generator);
	public static final BlockEntityType<TileEntityBedrockRift> BEDROCK_RIFT = register("bedrock_rift", TileEntityBedrockRift::new, MiscBlocks.bedrock_rift);
	public static final BlockEntityType<TileEntityBatteryBox> BATTERY_BOX = register("battery_box", TileEntityBatteryBox::new, InventoryBlocks.battery_box_w, InventoryBlocks.battery_box_g, InventoryBlocks.battery_box_b);
	public static final BlockEntityType<TileEntityPusher> PUSHER = register("pusher", TileEntityPusher::new, InventoryBlocks.pusher);
	public static final BlockEntityType<TileEntityPartPress> PART_PRESS = register("part_press", TileEntityPartPress::new, InventoryBlocks.part_press);
	public static final BlockEntityType<TileEntityBlockBreaker> BLOCK_BREAKER = register("block_breaker", TileEntityBlockBreaker::new, InventoryBlocks.block_breaker);
	public static final BlockEntityType<TileEntityWardrobe.Normal> WARDROBE_N = register("wardrobe_n", TileEntityWardrobe.Normal::new, 
			InventoryBlocks.wardrobe_white_normal_1, InventoryBlocks.wardrobe_white_normal_2, InventoryBlocks.wardrobe_light_gray_normal_1, InventoryBlocks.wardrobe_light_gray_normal_2, InventoryBlocks.wardrobe_black_normal_1, InventoryBlocks.wardrobe_black_normal_2);
	public static final BlockEntityType<TileEntityWardrobe.Large> WARDROBE_L = register("wardrobe_l", TileEntityWardrobe.Large::new,
			InventoryBlocks.wardrobe_white_large_1, InventoryBlocks.wardrobe_white_large_2, InventoryBlocks.wardrobe_light_gray_large_1, InventoryBlocks.wardrobe_light_gray_large_2, InventoryBlocks.wardrobe_black_large_1, InventoryBlocks.wardrobe_black_large_2);
	public static final BlockEntityType<TileEntityFuelCell> FUEL_CELL = register("fuel_cell", TileEntityFuelCell::new, InventoryBlocks.fuel_cell);
	
	public static final BlockEntityType<TileEntityOxades> OXADES = register("oxades", TileEntityOxades::new, PlantBlocks.oxades);
	public static final BlockEntityType<TileEntityPipeNormal> PIPE_NORMAL = register("pipe_normal", TileEntityPipeNormal::new, LogisticBlocks.pipe_normal, LogisticBlocks.pipe_redstone);
	public static final BlockEntityType<TileEntityPipeNeon> PIPE_NEON = register("pipe_neon", TileEntityPipeNeon::new, LogisticBlocks.pipe_neon);
	public static final BlockEntityType<TileEntityPipeSupport> PIPE_SUPPORT = register("pipe_support", TileEntityPipeSupport::new, LogisticBlocks.pipe_support);
	
	public static final BlockEntityType<TileEntityWireNormal> WIRE_NORMAL = register("wire_normal", TileEntityWireNormal::new, LogisticBlocks.wire_normal);
	public static final BlockEntityType<TileEntityWireSupport> WIRE_SUPPORT = register("wire_support", TileEntityWireSupport::new, LogisticBlocks.wire_support);
	public static final BlockEntityType<TileEntityWireNetwork> WIRE_NETWORK = register("wire_network", TileEntityWireNetwork::new, LogisticBlocks.wire_network);
	public static final BlockEntityType<TileEntityWireSuper> WIRE_SUPER = register("wire_super", TileEntityWireSuper::new, LogisticBlocks.wire_super);
	public static final BlockEntityType<TileEntityWireRedstone> WIRE_REDSTONE = register("wire_redstone", TileEntityWireRedstone::new, LogisticBlocks.wire_redstone);
	
	public static final BlockEntityType<TileEntityExternalCore> EXTERNAL_CORE = register("external_core", TileEntityExternalCore::new, ModifiableBlocks.external_core);
	public static final BlockEntityType<TileEntityFallingTree> FALLING_TREE = register("falling_tree", TileEntityFallingTree::new, MiscBlocks.falling_tree);
	public static final BlockEntityType<TileEntityAntenna> ANTENNA = register("antenna",TileEntityAntenna::new, MiscBlocks.antenna_white, MiscBlocks.antenna_gray, MiscBlocks.antenna_black);
	public static final BlockEntityType<TileEntityTeleporter> TELEPORTER = register("teleporter", TileEntityTeleporter::new, MiscBlocks.teleporter, MiscBlocks.teleporter_both, MiscBlocks.teleporter_down, MiscBlocks.teleporter_up);
	public static final BlockEntityType<TileEntityRocketLauncher> ROCKET_LAUNCHER = register("rocket_launcher", TileEntityRocketLauncher::new, ModifiableBlocks.rocket_lancher);
	public static final BlockEntityType<TileEntityEntityKiller> ENTITY_KILLER = register("entity_killer", TileEntityEntityKiller::new, ModifiableBlocks.entity_killer);
	public static final BlockEntityType<TileEntityEntityHealer> ENTITY_HEALER = register("entity_healer", TileEntityEntityHealer::new, ModifiableBlocks.entity_healer);
	public static final BlockEntityType<TileEntityEntityEater> ENTITY_EATER = register("entity_eater", TileEntityEntityEater::new, ModifiableBlocks.entity_eater);
	
	public static final BlockEntityType<TileEntityModulT1> MODUL_T1 = register("modul_t1", TileEntityModulT1::new, InventoryBlocks.modul_1_w, InventoryBlocks.modul_1_g, InventoryBlocks.modul_1_s);
	public static final BlockEntityType<TileEntityModulT1Calculation> MODUL_T1_CALC = register("modul_t1_calc", TileEntityModulT1Calculation::new, InventoryBlocks.modul_1_calc_w, InventoryBlocks.modul_1_calc_g, InventoryBlocks.modul_1_calc_s);
	public static final BlockEntityType<TileEntityModulT2> MODUL_T2 = register("mdul_t2", TileEntityModulT2::new, InventoryBlocks.modul_2_w, InventoryBlocks.modul_2_g, InventoryBlocks.modul_2_s);
	public static final BlockEntityType<TileEntityModulT3> MODUL_T3 = register("modul_t3", TileEntityModulT3::new, InventoryBlocks.modul_3_w, InventoryBlocks.modul_3_g, InventoryBlocks.modul_3_s);
	
	public static final BlockEntityType<TileEntityNeonEngine> NEON_ENGINE = register("neone_engine", TileEntityNeonEngine::new, MiscBlocks.neon_engine);
	public static final BlockEntityType<TileEntitySaplingHolder> SAPLING_HOLDER = register("sapling_holder", TileEntitySaplingHolder::new, MiscBlocks.sapling_holder_plains, MiscBlocks.sapling_holder_desert, MiscBlocks.sapling_holder_nether);
	public static final BlockEntityType<TileEntityDroneStation> DRONE_STATION = register("drone_stattion", TileEntityDroneStation::new, InventoryBlocks.drone_station);
	public static final BlockEntityType<TileEntityMagnet> MAGNET = register("magnet", TileEntityMagnet::new, MiscBlocks.magnet);
	public static final BlockEntityType<TileEntityElektroMagnet> ELECTRO_MAGNET = register("electro_magnet", TileEntityElektroMagnet::new, ModifiableBlocks.electro_magnet);
	public static final BlockEntityType<TileEntityPulsit> GRAVITY_PULSER = register("gravity_pulser", TileEntityPulsit::new, MiscBlocks.gravity_pulser);
	public static final BlockEntityType<TileEntityRsTimer> RS_TIMER = register("rs_timer", TileEntityRsTimer::new, MiscBlocks.rs_timer);
	public static final BlockEntityType<TileEntityForceField> FORCE_FIELD = register("force_field", TileEntityForceField::new, MiscBlocks.force_field);
	public static final BlockEntityType<TileEntityClaime> CLAIME = register("claime", TileEntityClaime::new, MiscBlocks.claime);
	public static final BlockEntityType<TileEntityDungeonSpawner> DUNGEON_SPAWNER = register("dungeon_spanwer", TileEntityDungeonSpawner::new, MiscBlocks.dungeon_spawner);
	
	public static final BlockEntityType<TileEntityInsertNode> INSERT_NODE = register("insert_node", TileEntityInsertNode::new, LogisticBlocks.insert_node);
	public static final BlockEntityType<TileEntityAirlockDoor> AIRLOCK_DDOR = register("airlock_door", TileEntityAirlockDoor::new, MiscBlocks.airlock_door);
	public static final BlockEntityType<TileEntityDungeonCore> DUNGEON_CORE = register("dungeon_core", TileEntityDungeonCore::new, MiscBlocks.dungeon_core);
	
	public static final BlockEntityType<TileEntityTyrosTreeGen> TYROS_TREE_GEN = register("tyros_tree_gen", TileEntityTyrosTreeGen::new, MiscBlocks.tyros_tree_gen);
	public static final BlockEntityType<TileEntityLaserTransmitter> LASER_TRASNMITTER = register("laser_transmitter", TileEntityLaserTransmitter::new, LogisticBlocks.laser_transmitter);
	public static final BlockEntityType<TileEntityWaterTurbine> WATER_TURBINE = register("water_turbine", TileEntityWaterTurbine::new, ModifiableBlocks.water_turbine_w, ModifiableBlocks.water_turbine_g, ModifiableBlocks.water_turbine_b);
	public static final BlockEntityType<TileEntitySyncronizer> SYNCRONIZER = register("syncronizer", TileEntitySyncronizer::new, LogisticBlocks.syncronizer);
//	public static final BlockEntityType<TileEntityPlasmaStorageCoreTier1> PLASMA_CORE_T1 = register("plasma_core_tier1", TileEntityPlasmaStorageCoreTier1::new, LogisticBlocks.plasma_core_t1);
	public static final BlockEntityType<TileEntityRFtoNEConverter> RF2NE_CONVERTER = register("rf2ne_converter", TileEntityRFtoNEConverter::new, MiscBlocks.rf2ne_converter_white, MiscBlocks.rf2ne_converter_gray, MiscBlocks.rf2ne_converter_black);
	
	public static final BlockEntityType<TileEntityMonorailStation> MONORAIL_STATION = register("monorail_station", TileEntityMonorailStation::new, LogisticBlocks.monorail_station);
	public static final BlockEntityType<TileEntityMonorailWaypoint> MONORAIL_WAYPOINT = register("monorail_waypoint", TileEntityMonorailWaypoint::new, LogisticBlocks.monorail_waypoint);
	public static final BlockEntityType<TileEntityMonorailCharger> MONORAIL_CHARGER = register("monorail_charger", TileEntityMonorailCharger::new, LogisticBlocks.monorail_charger);
	
	public static final BlockEntityType<TileEntityBoardComputer> BOARD_COMPUTER = register("board_computer", TileEntityBoardComputer::new, InventoryBlocks.board_computer_w, InventoryBlocks.board_computer_g, InventoryBlocks.board_computer_s);
	public static final BlockEntityType<TileEntityAdvancedBoardComputer> ADVANCED_BOARD_COMPUTER = register("advanced_board_computer", TileEntityAdvancedBoardComputer::new, InventoryBlocks.advanced_board_computer_w, InventoryBlocks.advanced_board_computer_g, InventoryBlocks.advanced_board_computer_s);
	public static final BlockEntityType<TileEntityCompositeChest> COMPOSITE_CHEST = register("composite_chest", TileEntityCompositeChest::new, InventoryBlocks.composite_chest);
	public static final BlockEntityType<TileEntityDeepCoreMinerInventory> DEEPCORE_INVENTORY = register("deepcore_inventory", TileEntityDeepCoreMinerInventory::new, MultiblockBlocks.deepcore_miner);
	public static final BlockEntityType<TileEntityDeepCoreMinerMain> DEEPCORE_MAIN = register("deepcore_main", TileEntityDeepCoreMinerMain::new, MultiblockBlocks.deepcore_miner);
	public static final BlockEntityType<TileEntityModularDoor> MODULAR_DOOR = register("modular_door", TileEntityModularDoor::new, MiscBlocks.modular_door);
	
	public static final BlockEntityType<TileEntitySharedResearcher> SHARED_RESEARCHER = register("shared_researcher", TileEntitySharedResearcher::new, InventoryBlocks.shared_researcher_w, InventoryBlocks.shared_researcher_g, InventoryBlocks.shared_researcher_s);
	public static final BlockEntityType<TileEntityPlasmaTransporter> PLASMA_PIPE_T1 = register("plasma_pipe_t1", TileEntityPlasmaTransporter::pipeT1, LogisticBlocks.plasma_pipe_t1);
	public static final BlockEntityType<TileEntityPlasmaTransporter> PLASMA_PIPE_T0 = register("plasma_pipe_t0", TileEntityPlasmaTransporter::pipeT0, LogisticBlocks.plasma_jar);
	public static final BlockEntityType<TileEntityResearchExchange> RESEARCH_EXCHANGE = register("research_exchange", TileEntityResearchExchange::new, MiscBlocks.tectern);
	
	
	public static final BlockEntityType<TileEntityFluidPump> FLUID_PUMP = register("fluid_pump", TileEntityFluidPump::new, ModifiableBlocks.fluid_pump);
	public static final BlockEntityType<TileEntityFluidIntake> FLUID_INTAKE = register("fluid_intake", TileEntityFluidIntake::new, LogisticBlocks.fluid_intake);
	public static final BlockEntityType<TileEntityFluidTube> FLUID_TUBE = register("fluid_tube", TileEntityFluidTube::new, LogisticBlocks.fluid_tube);
	public static final BlockEntityType<TileEntityFluidTank> FLUID_TANK = register("fluid_tank", TileEntityFluidTank::new, LogisticBlocks.fluid_tank, LogisticBlocks.fluid_tank_mk2, LogisticBlocks.fluid_tank_mk3);
	public static final BlockEntityType<TileEntityWaterCooler> WATER_COOLER = register("water_cooler", TileEntityWaterCooler::new, InventoryBlocks.water_cooler);
	public static final BlockEntityType<TileEntityFermentationBarrel> FERMENTATION_BARREL = register("fermentation_barrel", TileEntityFermentationBarrel::new, InventoryBlocks.fermentation_barrel);
	public static final BlockEntityType<TileEntityStructureFixHelper> STRUCTURE_FIX_HELPER = register("structure_fix_helpet", TileEntityStructureFixHelper::new, MiscBlocks.structure_fix_helper);
	public static final BlockEntityType<TileEntityDungeonTeleporter> TELEPORTER_DUNGEON = register("teleporter_dungeon", TileEntityDungeonTeleporter::new, MiscBlocks.beam, MiscBlocks.beam_both, MiscBlocks.beam_down, MiscBlocks.beam_up);
	public static final BlockEntityType<TileEntityDungeonCheckpoint> DUNGEON_CHECKPOINT = register("dungeon_teleporter", TileEntityDungeonCheckpoint::new, MiscBlocks.dungeon_checkpoint);
	public static final BlockEntityType<TileEntityImproveComponents> IMPROVE_COMPONENTS = register("improve_components", TileEntityImproveComponents::new, ModifiableBlocks.improve_components_w, ModifiableBlocks.improve_components_g, ModifiableBlocks.improve_components_b);
	public static final BlockEntityType<TileEntityPlasmaConverter> PLASMA_CONVRTER_T1 = register("plasma_converter_t1", TileEntityPlasmaConverter::converterT1, LogisticBlocks.plasma_converter_t1);
	public static final BlockEntityType<TileEntityPlasmaConverter> PLASMA_CONVRTER_T0 = register("plasma_converter_t0", TileEntityPlasmaConverter::converterT0, LogisticBlocks.plasma_converter_t0);
	public static final BlockEntityType<TileEntityPlasma2NeonT0> PLASMA_2_NEON_T0 = register("plasma_2_neon_t0", TileEntityPlasma2NeonT0::converterT0, LogisticBlocks.plasma_2_neon_t0);
	
	public static final BlockEntityType<TileEntityLogisticChest> LOGISTIC_CHEST = register("logistic_chest", TileEntityLogisticChest::new, InventoryBlocks.logistic_chest);
	public static final BlockEntityType<TileEntityOptiBenchCraftingModule> OPTIBENCH_CRAFTING_MODULE = register("opti_bench_crafting_module", TileEntityOptiBenchCraftingModule::new, InventoryBlocks.optibench_crafting_module_w);
	public static final BlockEntityType<TileEntityTickingPusher> PUSHER_TICKING = register("pusher_ticking", TileEntityTickingPusher::new, InventoryBlocks.pusher_ticking);
	public static final BlockEntityType<TileEntityMovingBlocks> MOVING_BLOCKS = register("moving_blocks", TileEntityMovingBlocks::new, LogisticBlocks.moving_blocks);
	public static final BlockEntityType<TileEntitySpaceshipMover> SPACESHIP_MOVER = register("spaceship_mover", TileEntitySpaceshipMover::new, DecoBlocks.thruster_maneuver_black_blue, DecoBlocks.thruster_maneuver_black_purple, DecoBlocks.thruster_maneuver_black_yellow,  DecoBlocks.thruster_maneuver_light_gray_blue, DecoBlocks.thruster_maneuver_light_gray_purple, DecoBlocks.thruster_maneuver_light_gray_yellow, DecoBlocks.thruster_maneuver_white_blue, DecoBlocks.thruster_maneuver_white_purple, DecoBlocks.thruster_maneuver_white_yellow);

	public static final BlockEntityType<TileEntityRadar> RADAR = register("radar", TileEntityRadar::new, ModifiableBlocks.radar);
	public static final BlockEntityType<TileEntityOreTeleporter> ORE_TELEPORTER = register("ore_teleporter", TileEntityOreTeleporter::new, MiscBlocks.ore_teleporter);
	public static final BlockEntityType<TileEntityLifeSupportSystem> LIFE_SUPPORT_SYSTEM = register("life_support_system", TileEntityLifeSupportSystem::new, ModifiableBlocks.life_support_system_w, ModifiableBlocks.life_support_system_g, ModifiableBlocks.life_support_system_b);
	public static final BlockEntityType<TileEntitySpaceLink> SPACE_LINK = register("space_link", TileEntitySpaceLink::new, LogisticBlocks.space_link);
	public static final BlockEntityType<TileEntityBlockTeleporter> BLOCK_TELEPORTER = register("block_teleporter", TileEntityBlockTeleporter::new, MiscBlocks.block_teleporter);
	
	
	public static<T extends BlockEntity> BlockEntityType<T> register(String id, BlockEntityType.BlockEntitySupplier<T> constructor, Block...valid)
	{
		BlockEntityType<T> t = BlockEntityType.Builder.of(constructor, valid).build(null);
		HelperItems.setRegistryName(t, Constants.MOD_ID, id);
		return t;
	}
	
	public static void registerTileEntitys(RegistryEvent.Register<BlockEntityType<?>> event)
	{
		IForgeRegistry<BlockEntityType<?>> r = event.getRegistry();
		
		r.register(TECHTABLE);
		r.register(FORSCHER);
		r.register(SCANNER_BLOCK);
		r.registerAll(NEON_LAMP, LINKED_LIGHT);
		r.registerAll(EXTERN_COOLER, CRUSHER, NEON_FURNACE, ION_COLLECTOR, SOLAR_PANEL, ZENTRIFUGE, PLASMA_GENERATOR, INDUSTRIAL_NEON_FURNACE, RECYCLER, OPTI_BENCH, GAS_TURBINE, OPTI_ASSEMBLER, SORTER);
		r.registerAll(INDUSTRIAL_FURNACE, ASSEMBLY_TABLE, FLASH_SERVER, BLOCK_PLACER, T0_GENERATOR, BEDROCK_RIFT, BATTERY_BOX, PUSHER, PART_PRESS, BLOCK_BREAKER, WARDROBE_N, WARDROBE_L, FUEL_CELL);
		r.registerAll(OXADES, PIPE_NORMAL, PIPE_NEON, PIPE_SUPPORT, WIRE_NORMAL, WIRE_SUPPORT, WIRE_NETWORK, WIRE_SUPER, WIRE_REDSTONE);
		r.registerAll(EXTERNAL_CORE, FALLING_TREE, ANTENNA, TELEPORTER, ROCKET_LAUNCHER);
		r.registerAll(ENTITY_KILLER, ENTITY_HEALER, ENTITY_EATER);
		r.registerAll(MODUL_T1, MODUL_T2, MODUL_T3, MODUL_T1_CALC);
		r.registerAll(NEON_ENGINE, SAPLING_HOLDER, DRONE_STATION, MAGNET, ELECTRO_MAGNET, GRAVITY_PULSER, RS_TIMER, FORCE_FIELD,CLAIME, DUNGEON_SPAWNER);
		r.registerAll(INSERT_NODE, AIRLOCK_DDOR, DUNGEON_CORE);
		r.registerAll(TYROS_TREE_GEN, LASER_TRASNMITTER, WATER_TURBINE, SYNCRONIZER, RF2NE_CONVERTER);
//		r.registerAll(PLASMA_CORE_T1,, );
		r.registerAll(PLASMA_PIPE_T1, PLASMA_PIPE_T0, PLASMA_CONVRTER_T1, PLASMA_CONVRTER_T0, PLASMA_2_NEON_T0);
		r.registerAll(MONORAIL_STATION, MONORAIL_WAYPOINT, MONORAIL_CHARGER);
		r.registerAll(BOARD_COMPUTER, ADVANCED_BOARD_COMPUTER, COMPOSITE_CHEST, DEEPCORE_INVENTORY, DEEPCORE_MAIN, MODULAR_DOOR);
		r.registerAll(SHARED_RESEARCHER, RESEARCH_EXCHANGE);
		r.registerAll(FLUID_PUMP, FLUID_INTAKE, FLUID_TUBE, FLUID_TANK, WATER_COOLER, FERMENTATION_BARREL, STRUCTURE_FIX_HELPER, TELEPORTER_DUNGEON, DUNGEON_CHECKPOINT, IMPROVE_COMPONENTS);
		r.registerAll(LOGISTIC_CHEST, OPTIBENCH_CRAFTING_MODULE, PUSHER_TICKING, MOVING_BLOCKS, SPACESHIP_MOVER);
		r.registerAll(RADAR, LIFE_SUPPORT_SYSTEM, SPACE_LINK, ORE_TELEPORTER, BLOCK_TELEPORTER);
	}
}
