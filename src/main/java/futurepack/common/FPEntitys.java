package futurepack.common;

import java.util.Random;
import java.util.function.Consumer;

import futurepack.api.Constants;
import futurepack.common.block.terrain.TerrainBlocks;
import futurepack.common.entity.EntityForceField;
import futurepack.common.entity.EntityForstmaster;
import futurepack.common.entity.EntityMiner;
import futurepack.common.entity.EntityMonitor;
import futurepack.common.entity.living.EntityAlphaJawaul;
import futurepack.common.entity.living.EntityCrawler;
import futurepack.common.entity.living.EntityCyberZombie;
import futurepack.common.entity.living.EntityDungeonSpider;
import futurepack.common.entity.living.EntityEvilRobot;
import futurepack.common.entity.living.EntityGehuf;
import futurepack.common.entity.living.EntityHeuler;
import futurepack.common.entity.living.EntityJawaul;
import futurepack.common.entity.living.EntityWolba;
import futurepack.common.entity.monocart.EntityMonocart;
import futurepack.common.entity.throwable.EntityEgger;
import futurepack.common.entity.throwable.EntityGrenadeBase;
import futurepack.common.entity.throwable.EntityHook;
import futurepack.common.entity.throwable.EntityLaser;
import futurepack.common.entity.throwable.EntityLaserProjectile;
import futurepack.common.entity.throwable.EntityRocket;
import futurepack.common.entity.throwable.EntityWakurumIngot;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EntityType.Builder;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.SpawnPlacements;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.entity.animal.Cow;
import net.minecraft.world.entity.animal.Sheep;
import net.minecraft.world.entity.animal.Wolf;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.entity.EntityAttributeCreationEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class FPEntitys 
{
	public static final EntityType<EntityWakurumIngot> WAKURIUM_THROWN = register("wakurium_thorwn", MobCategory.MISC, EntityWakurumIngot::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityCyberZombie> CYBER_ZOMBIE = register("cyber_zombie", MobCategory.MONSTER, EntityCyberZombie::new, EnumTrackingType.LIVINGS);
	public static final EntityType<EntityWolba> WOLBA = register("wolba", MobCategory.CREATURE, EntityWolba::new, EnumTrackingType.LIVINGS, 1.1F, 1.4F);
	public static final EntityType<EntityEvilRobot> EVIL_ROBOT = register("evil_robot", MobCategory.MONSTER, EntityEvilRobot::new, EnumTrackingType.LIVINGS);
	public static final EntityType<EntityMonitor> MONITOR = register("monitor", MobCategory.MISC, EntityMonitor::new, EnumTrackingType.NONE);//TODO add custom spawn information to monitor
	public static final EntityType<EntityEgger> ENTITY_EGGER = register("egger", MobCategory.MISC, EntityEgger::new, EnumTrackingType.THROWABLE);
	
	public static final EntityType<EntityRocket.EntityPlasmaRocket> ROCKET_PLASMA = register("rocket_plasma", MobCategory.MISC, EntityRocket.EntityPlasmaRocket::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityRocket.EntityBlazeRocket> ROCKET_BLAZE = register("rocket_blaze", MobCategory.MISC, EntityRocket.EntityBlazeRocket::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityRocket.EntityBioteriumRocket> ROCKET_BIOTERIUM = register("rocket_bioterium", MobCategory.MISC, EntityRocket.EntityBioteriumRocket::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityRocket.EntityNormalRocket> ROCKET_NORMAL = register("rocket_normal", MobCategory.MISC, EntityRocket.EntityNormalRocket::new, EnumTrackingType.THROWABLE);
	
	public static final EntityType<EntityMonocart> MONOCART = register("monocart", MobCategory.MISC, EntityMonocart::new, EnumTrackingType.ROBOTS, 0.8F, 0.6F);
	
	public static final EntityType<EntityGrenadeBase.Plasma> GRENADE_PLASMA = register("grenade_plasma", MobCategory.MISC, EntityGrenadeBase.Plasma::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityGrenadeBase.Normal> GRENADE_NORMAL = register("grenade_normal", MobCategory.MISC, EntityGrenadeBase.Normal::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityGrenadeBase.Blaze> GRENADE_BLAZE = register("grenade_blaze", MobCategory.MISC, EntityGrenadeBase.Blaze::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityGrenadeBase.Slime> GRENADE_SLIME = register("grenade_slime", MobCategory.MISC, EntityGrenadeBase.Slime::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityGrenadeBase.Kompost> GRENADE_KOMPOST = register("grenade_kompost", MobCategory.MISC, EntityGrenadeBase.Kompost::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityGrenadeBase.Futter> GRENADE_FUTTER = register("grenade_futter", MobCategory.MISC, EntityGrenadeBase.Futter::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityGrenadeBase.Saat> GRENADE_SAAT = register("grenade_saat", MobCategory.MISC, EntityGrenadeBase.Saat::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityGrenadeBase.EnityEggerFullGranade> GRENADE_ENTITY_EGGER = register("grenade_entity_egger", MobCategory.MISC, EntityGrenadeBase.EnityEggerFullGranade::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityLaserProjectile> LASER_PROJECTILE = register("laser_projectile", MobCategory.MISC, EntityLaserProjectile::new, EnumTrackingType.THROWABLE);
	
	public static final EntityType<EntityGehuf> GEHUF = register("gehuf", MobCategory.CREATURE, EntityGehuf::new, EnumTrackingType.LIVINGS, 1.1F, 1.4F);
	public static final EntityType<EntityCrawler> CRAWLER = register("crawler", MobCategory.MONSTER, EntityCrawler::new, EnumTrackingType.LIVINGS, 1.4F, 0.9F);
	public static final EntityType<EntityHeuler> HEULER = register("heuler", MobCategory.CREATURE, EntityHeuler::new, EnumTrackingType.LIVINGS, 0.5F, 0.5F);
	
	public static final EntityType<EntityLaser> LASER = register("laser", MobCategory.MISC, EntityLaser::new, EnumTrackingType.THROWABLE);
	public static final EntityType<EntityForceField> FORCE_FIELD = register("force_field", MobCategory.MISC, EntityForceField::new, EnumTrackingType.ROBOTS);
	public static final EntityType<EntityForstmaster> FORESTMASTER = register("forestmaster", MobCategory.MISC, EntityForstmaster::new, EnumTrackingType.ROBOTS, 0.8F, 0.6F);
	public static final EntityType<EntityMiner> MINER = register("miner", MobCategory.MISC, EntityMiner::new, EnumTrackingType.ROBOTS, 0.6F, 0.6F, b -> b.fireImmune());
	public static final EntityType<EntityHook> HOOK = register("hook", MobCategory.MISC, EntityHook::new, EnumTrackingType.THROWABLE);
	
	public static final EntityType<EntityJawaul> JAWAUL = register("jawaul", MobCategory.CREATURE, EntityJawaul::new, EnumTrackingType.LIVINGS, 0.6F, 0.85F);
	public static final EntityType<EntityDungeonSpider> DUNGEON_SPIDER = register("dungeon_spider", MobCategory.CREATURE, EntityDungeonSpider::new, EnumTrackingType.LIVINGS, 0.6F, 0.3F);
	public static final EntityType<EntityAlphaJawaul> ALPHA_JAWAUL = register("alpha_jawaul", MobCategory.CREATURE, EntityAlphaJawaul::new, EnumTrackingType.LIVINGS, 1.5F, 1.2F);
	
	
	private static <T extends Entity> EntityType<T> register(String id, MobCategory classificationIn, EntityType.EntityFactory<T> constructor, EnumTrackingType type, float width, float height, Consumer<EntityType.Builder> c)
	{
		Builder<T> build = EntityType.Builder.of(constructor, classificationIn);
		type.setTracking(build).sized(width, height);
//		build.setCustomClientFactory((s,w) -> s.)
		if(c!=null)
			c.accept(build);
		EntityType<T> t = build.build("");
		HelperItems.setRegistryName(t, Constants.MOD_ID, id);
		return t;
	}
	
	private static <T extends Entity> EntityType<T> register(String id, MobCategory classification, EntityType.EntityFactory<T> constructor, EnumTrackingType type, float width, float height)
	{
		return register(id, classification, constructor, type, width, height, null);
	}
	
	
	private static <T extends Entity> EntityType<T> register(String id, MobCategory classificationIn, EntityType.EntityFactory<T> constructor, EnumTrackingType type)
	{
		if(type==EnumTrackingType.THROWABLE)
		{
			return register(id, classificationIn, constructor, type, 0.25F, 0.25F);
		}
		else if(type == EnumTrackingType.LIVINGS)
		{
			return register(id, classificationIn, constructor, type,  0.6F, 1.95F);
		}
		else
		{
			return register(id, classificationIn, constructor, type, 1F, 1F);
		}
	}

	public static void registerEntitys(RegistryEvent.Register<EntityType<?>> event)
	{
		IForgeRegistry<EntityType<?>> r = event.getRegistry();
		
		r.registerAll(WAKURIUM_THROWN, CYBER_ZOMBIE, WOLBA, EVIL_ROBOT, MONITOR, ENTITY_EGGER);
		r.registerAll(ROCKET_PLASMA, ROCKET_BLAZE, ROCKET_BIOTERIUM, ROCKET_NORMAL);
		r.registerAll(MONOCART);
		r.registerAll(GRENADE_PLASMA, GRENADE_NORMAL, GRENADE_BLAZE, GRENADE_SLIME, GRENADE_KOMPOST, GRENADE_FUTTER, GRENADE_SAAT, GRENADE_ENTITY_EGGER);
		r.registerAll(LASER_PROJECTILE);
		r.registerAll(GEHUF, CRAWLER, HEULER);
		r.registerAll(LASER, FORCE_FIELD, FORESTMASTER, MINER, HOOK);
		r.registerAll(JAWAUL, DUNGEON_SPIDER, ALPHA_JAWAUL);
		
		SpawnPlacements.register(CRAWLER, SpawnPlacements.Type.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, Monster::checkMonsterSpawnRules);
		SpawnPlacements.register(CYBER_ZOMBIE, SpawnPlacements.Type.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, Monster::checkMonsterSpawnRules);
		SpawnPlacements.register(EVIL_ROBOT, SpawnPlacements.Type.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, Monster::checkMonsterSpawnRules);
		
		SpawnPlacements.register(WOLBA, SpawnPlacements.Type.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, FPEntitys::canAnimalSpawnMenelaus);
		SpawnPlacements.register(GEHUF, SpawnPlacements.Type.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, FPEntitys::canAnimalSpawnMenelaus);
		SpawnPlacements.register(HEULER, SpawnPlacements.Type.NO_RESTRICTIONS, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, FPEntitys::canAnimalSpawnMenelaus);
		SpawnPlacements.register(JAWAUL, SpawnPlacements.Type.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, FPEntitys::canAnimalSpawnMenelaus);
		
	}
	
	public static boolean canAnimalSpawnMenelaus(EntityType<? extends Animal> animal, LevelAccessor worldIn, MobSpawnType reason, BlockPos pos, Random random) 
	{
		return worldIn.getBlockState(pos.below()).getBlock() == TerrainBlocks.sand_m && worldIn.getRawBrightness(pos, 0) > 8;
	}
	
	public static enum EnumTrackingType
	{
		NONE(40,Integer.MAX_VALUE,false),
		LIVINGS(30,3,true),
		THROWABLE(50,10,true),
		ROBOTS(30,15,true),
		FAST(40,5,true),
		;
		
		final int updateFrequenzy, range;
		final boolean sendVelocity;
		
		EnumTrackingType(int range, int updateFrequenzy, boolean sendVelocity)
		{
			this.range = range;
			this.updateFrequenzy = updateFrequenzy;
			this.sendVelocity = sendVelocity;
		}
		
		public <T extends Entity> EntityType.Builder<T> setTracking(EntityType.Builder<T> t)
		{			
			t.setTrackingRange(range).setUpdateInterval(updateFrequenzy).setShouldReceiveVelocityUpdates(sendVelocity);
			return t;
		}
	}
	
	public static void registerGlobalEntityAttributes(EntityAttributeCreationEvent event)
	{
		event.put(CRAWLER, EntityCrawler.registerAttributes().build());
		event.put(CYBER_ZOMBIE, EntityCyberZombie.registerAttributes().build());
		event.put(WOLBA, Sheep.createAttributes().build());
		event.put(EVIL_ROBOT, EntityEvilRobot.registerAttributes().build());
		event.put(GEHUF, Cow.createAttributes().build());
		event.put(HEULER, EntityHeuler.registerAttributes().build());
		event.put(JAWAUL, Wolf.createAttributes().build());
		event.put(DUNGEON_SPIDER, EntityDungeonSpider.registerAttributes().build());
		event.put(ALPHA_JAWAUL, EntityAlphaJawaul.registerAttributes().build());
		
	}
}
