package futurepack.common.gui;

import com.mojang.blaze3d.platform.Lighting;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.interfaces.IGuiRenderable;
import net.minecraft.client.Minecraft;
import net.minecraft.world.item.ItemStack;

public class RenderableItem implements IGuiRenderable
{
	private ItemStack icon;
	
	public RenderableItem(ItemStack item)
	{
		if(item==null)
			throw new NullPointerException("null is not an ItemStack!");
		icon = item;
	}
	
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, int x, int y, int blitOffset)
	{
		Lighting.setupForFlatItems();
		RenderSystem.enableDepthTest();
		Minecraft.getInstance().getItemRenderer().blitOffset=(float)blitOffset - 100; 	
		RenderSystem.setShaderColor(1F, 1F, 1F, 1F);
		
		Minecraft.getInstance().getItemRenderer().renderAndDecorateItem(icon, x, y);
		Minecraft.getInstance().getItemRenderer().blitOffset=0F;
		
	}

	@Override
	public String toString()
	{
		return "Rendering{" + icon +"}";
	}
}
