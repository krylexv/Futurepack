package futurepack.common.gui;

import java.util.Arrays;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.sync.KeyManager;
import futurepack.common.sync.KeyManager.EnumKeyTypes;
import futurepack.common.sync.NetworkHandler;
import futurepack.depend.api.helper.HelperGui;
import futurepack.depend.api.helper.HelperRendering;
import net.minecraft.client.gui.components.AbstractWidget;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.TranslatableComponent;

public class GuiAllKeys extends Screen
{

	public GuiAllKeys()
	{
		super(new TranslatableComponent("futurepack.gui.all_keys"));
	}
	
	@Override
	public void init()
	{
		super.init();
		
		int i=-1;
		
		EnumKeyTypes[] keys = KeyManager.EnumKeyTypes.getKeys();
		int l = (int) (Math.sqrt(keys.length)) +1;
		int buttonWidth = 20;
		int totalWidth = l * buttonWidth;
		int totalHeight = (1 + keys.length / l)* buttonWidth;
		
		int guiLeft = (width - totalWidth) / 2;
		int guiTop = (height - totalHeight) / 2;
		
		for(EnumKeyTypes key : keys)
		{
			i++;
			int x = buttonWidth * (i%l);
			int y = buttonWidth * (i/l);
			
			ButtonAspect ba = new ButtonAspect(i, guiLeft + x, guiTop + y, new RenderableItem(key.getDisplayItemStack(minecraft.player).get()), null);
			ba.click = () -> {
				onClose();
				NetworkHandler.sendKeyPressedToServer(key);
			};
			ba.setMessage(new TranslatableComponent(key.translationKey));
			addRenderableWidget(ba);
		}
	}	

	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
	{
		super.render(matrixStack, mouseX, mouseY, partialTicks);
		
		HelperRendering.glColor4f(1F, 1F, 1F, 1F);
		
		for(int i = 0; i < this.renderables.size(); ++i) 
		{
			AbstractWidget w = (AbstractWidget) this.renderables.get(i);
			
			if(w.isMouseOver(mouseX, mouseY))
			{
				HelperGui.renderHoverText(matrixStack, mouseX+9, mouseY+7, 450, Arrays.asList(w.getMessage()), this.width / 2);
			}
		}
		
	}
}
