package futurepack.common.gui.escanner;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.client.gui.components.Button;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;


public class GuiTouchScreenButton extends Button
{

	public GuiTouchScreenButton(int x, int y, int widthIn, int heightIn, Component buttonText, Button.OnPress onPress)
	{
		super(x, y, widthIn, heightIn, buttonText, onPress);
	}
	
	public GuiTouchScreenButton(int x, int y, int widthIn, int heightIn, String buttonText, Button.OnPress onPress)
	{
		this(x,y,widthIn, heightIn, new TextComponent(buttonText), onPress);
	}
	

	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
	{
		if (this.visible)
		{
			Minecraft mc = Minecraft.getInstance();
			Font font = mc.font;
			RenderSystem.setShaderTexture(0, WIDGETS_LOCATION);
            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
            this.isHovered = mouseX >= this.x && mouseY >= this.y && mouseX < this.x + this.width && mouseY < this.y + this.height;
            int k = this.getYImage(this.isHovered);
            RenderSystem.enableBlend();
           // GlStateManager.blendFuncSeparate(770, 771, 1, 0);
            RenderSystem.blendFunc(770, 771);
            
            int c0 = isHovered?0xffa5eeff:0xff99d9ea;
            int c1 = isHovered?0xffe3ffff:0xffcfeef5;
            int c2 = isHovered?0xff779ecf:0xff7092be;
            
            this.setBlitOffset(100);
            RenderSystem.enableDepthTest();
            GuiComponent.fill(matrixStack, this.x, this.y, this.x+this.width, this.y+this.height, c0);
            GuiComponent.fill(matrixStack, this.x, this.y, this.x+this.width-1, this.y+this.height-1, c1);
            GuiComponent.fill(matrixStack, this.x+1, this.y+1, this.x+this.width, this.y+this.height, c2);
            GuiComponent.fill(matrixStack, this.x+1, this.y+1, this.x+this.width-1, this.y+this.height-1, c0);
            
//          this.blit(matrixStack, this.xPosition, this.yPosition, 0, 46 + k * 20, this.width / 2, this.height);
//          this.blit(matrixStack, this.xPosition + this.width / 2, this.yPosition, 200 - this.width / 2, 46 + k * 20, this.width / 2, this.height);
            
//          this.mouseDragged(mc, mouseX, mouseY);
           
            
            int l = 0;

           /* if (packedFGColor != 0)
            {
                l = packedFGColor;
            }
            else*/
            if (!this.active)
            {
                l = 10526880;
            }
            else if (this.isHovered)
            {
                l = 0xff777777;
            }

//            this.drawCenteredString(matrixStack, font, this.displayString, this.xPosition + this.width / 2, this.yPosition + (this.height - 8) / 2, l);
            font.draw(matrixStack, this.getMessage().getString(), ((this.x + this.width / 2)-font.width(this.getMessage()) / 2), this.y + (this.height - 8) / 2, l);
        }
	}
}
