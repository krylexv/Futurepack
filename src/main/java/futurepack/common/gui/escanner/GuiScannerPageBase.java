package futurepack.common.gui.escanner;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.FPConfig;
import futurepack.depend.api.helper.HelperGui;
import futurepack.depend.api.interfaces.IGuiComponent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.TextComponent;

public abstract class GuiScannerPageBase extends GuiScannerBase
{
	IGuiComponent[] components;
	
	protected int textWidth = 110; //start pos x:56 y:69
	protected int textHeight = 143;
	
	protected int textX = 59, textY = 69;
	
	protected int scrollIndex = 0; //in pixel
	protected int totalHeight = 1;
	
	protected boolean moving = false;
	protected boolean showInDungeon;
	
	public GuiScannerPageBase(IGuiComponent[] comps, Screen bef, boolean showInDungeon)
	{
		super(bef);
		components = comps;
		for(IGuiComponent c : comps)
		{
			assert c!=null;
		}
		this.showInDungeon = showInDungeon;
	}
	
	public static final String base_URL = "https://auth.redsnake-games.de";
	public static final String get_token_URL = "/get_token";
	public static final String check_token_URL = "/check_token";
	public static final String get_message_URL = "/get_message";
	public static final String add_messagae_URL = "/add_messagae";
	public static final Gson GSON = new Gson();
	
	public static String token = "";
	
	@Override
	public void init()
	{
		super.init();
		
		if(insideDungeon && !showInDungeon)
		{
			components = new IGuiComponent[1];
			components[0] = new ComponentTransmission(ComponentTransmission.Transmission.s, textHeight-10);
		}
		totalHeight = 1;
		for(IGuiComponent com : this.components)
		{
			com.init(textWidth, this);
			com.setAdditionHeight(totalHeight);
			totalHeight += com.getHeight() + 5;
		}
		
		if(FPConfig.IS_RESEARCH_DEBUG.getAsBoolean())
		{
			this.addRenderableWidget(new Button(guiX-60, guiY, 60, 16, new TextComponent("Request Token"), new Button.OnPress() 
			{
				
				@Override
				public void onPress(Button pButton) 
				{
					try
					{
						HttpClient httpclient = HttpClients.createDefault();
						HttpPost httppost = new HttpPost(base_URL + get_token_URL);
		
						// Request parameters and other properties.
						List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>(2);
						params.add(new BasicNameValuePair("uuid", Minecraft.getInstance().getUser().getUuid()));
						params.add(new BasicNameValuePair("session", Minecraft.getInstance().getUser().getAccessToken()));
						httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
		
						//Execute and get the response.
						HttpResponse response = httpclient.execute(httppost);
						HttpEntity entity = response.getEntity();
	
						
						if (entity != null) {
						    try (InputStream instream = entity.getContent()) {
						    	JsonObject obj = GSON.fromJson(new InputStreamReader(instream, StandardCharsets.UTF_8), JsonObject.class);
						    	System.out.println(obj);
						    	
						    	token = obj.get("token").getAsString();
						    	
						    }
						}
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			}));
			this.addRenderableWidget(new Button(guiX-60, guiY+20, 60, 16, new TextComponent("Check Token"), new Button.OnPress() 
			{
				
				@Override
				public void onPress(Button pButton) 
				{
					try
					{
						HttpClient httpclient = HttpClients.createDefault();
						HttpPost httppost = new HttpPost(base_URL + check_token_URL);
		
						// Request parameters and other properties.
						List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>(2);
						params.add(new BasicNameValuePair("token",token));
						httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
		
						//Execute and get the response.
						HttpResponse response = httpclient.execute(httppost);
						HttpEntity entity = response.getEntity();
	
						
						if (entity != null) {
						    try (InputStream instream = entity.getContent()) {
						    	JsonObject obj = GSON.fromJson(new InputStreamReader(instream, StandardCharsets.UTF_8), JsonObject.class);
						    	System.out.println(obj);
						    }
						}
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			}));
			this.addRenderableWidget(new Button(guiX-60, guiY+40, 60, 16, new TextComponent("Get Messages"), new Button.OnPress() 
			{
				
				@Override
				public void onPress(Button pButton) 
				{
					try
					{
						HttpClient httpclient = HttpClients.createDefault();
						HttpPost httppost = new HttpPost(base_URL + get_message_URL);
		
						// Request parameters and other properties.
						List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>(2);
						params.add(new BasicNameValuePair("research", "story.test"));
						httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
		
						//Execute and get the response.
						HttpResponse response = httpclient.execute(httppost);
						HttpEntity entity = response.getEntity();
	
						
						if (entity != null) {
						    try (InputStream instream = entity.getContent()) {
						    	JsonObject obj = GSON.fromJson(new InputStreamReader(instream, StandardCharsets.UTF_8), JsonObject.class);
						    	System.out.println(obj);
						    }
						}
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			}));
			this.addRenderableWidget(new Button(guiX-60, guiY+60, 60, 16, new TextComponent("Add Messages"), new Button.OnPress() 
			{
				
				@Override
				public void onPress(Button pButton) 
				{
					try
					{
						HttpClient httpclient = HttpClients.createDefault();
						HttpPost httppost = new HttpPost(base_URL + add_messagae_URL);
		
						// Request parameters and other properties.
						List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>(2);
						params.add(new BasicNameValuePair("token",token));
						params.add(new BasicNameValuePair("research", "story.test"));
						params.add(new BasicNameValuePair("uuid", Minecraft.getInstance().getUser().getUuid()));
						params.add(new BasicNameValuePair("name", Minecraft.getInstance().getUser().getName()));
						httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
		
						//Execute and get the response.
						HttpResponse response = httpclient.execute(httppost);
						HttpEntity entity = response.getEntity();
	
						
						if (entity != null) {
						    try (InputStream instream = entity.getContent()) {
						    	JsonObject obj = GSON.fromJson(new InputStreamReader(instream, StandardCharsets.UTF_8), JsonObject.class);
						    	System.out.println(obj);
						    }
						}
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			}));
			
		}
	}
	
	@Override
	public boolean mouseScrolled(double mx, double my, double dw) 
	{
		if(dw>0)
		{
			if(scrollIndex > 0 )
			{
				scrollIndex-= this.font.lineHeight * 2.5F;
			}
		}
		else if(dw<0)
		{
			if(scrollIndex < totalHeight - textHeight)
			{
				scrollIndex+= this.font.lineHeight * 2.5F;
			}
		}
		if(scrollIndex > totalHeight - textHeight)
		{
			scrollIndex = totalHeight - textHeight;
		}
		if(scrollIndex < 0)
		{
			scrollIndex = 0;
		}
		return super.mouseScrolled(mx, my, dw);
	}
	
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
	{
		if(font==null) //sometimes this cause crashes
			return;
		
		// Draws Background and Icons
		super.render(matrixStack, mouseX, mouseY, partialTicks);
		
		RenderSystem.enableDepthTest();
		
		drawInsisibilityCloak(matrixStack);
		
		drawComponents(matrixStack, mouseX, mouseY);
		
		drawScrollBar(matrixStack, mouseX, mouseY);
		
		drawHover(matrixStack, mouseX, mouseY);
	}
	
	protected void drawComponents(PoseStack matrixStack, int mouseX, int mouseY)
	{
		for(IGuiComponent com : this.components)
		{
			if(isComponentVisible(com))
			{
				int xpos = guiX + textX;
				int ypos = guiY + textY + com.getAdditionHeight() - this.scrollIndex;
				
				xpos += (this.textWidth - com.getWidth()) / 2;
				
				com.render(matrixStack, xpos, ypos, HelperGui.ZLEVEL_BACKGROUND + 10, mouseX, mouseY, this);
			}
		}	
	}
	
	protected void drawHover(PoseStack matrixStack, int mouseX, int mouseY)
	{
		for(IGuiComponent com : this.components)
		{
			if(isComponentVisible(com))
			{
				int xpos = guiX + textX;
				int ypos = guiY + textY + com.getAdditionHeight() - this.scrollIndex;
				
				xpos += (this.textWidth - com.getWidth()) / 2;
				
				boolean hover = isHovering(mouseX, mouseY, xpos, ypos, xpos+com.getWidth(), ypos+com.getHeight());
				com.postRendering(matrixStack, xpos, ypos, getBlitOffset(), mouseX, mouseY, hover, this);
			}
		}	
	}
	
	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int mouseButton)
	{
		for(IGuiComponent com : this.components)
		{
			if(isComponentVisible(com))
			{
				int xpos = guiX + textX;
				int ypos = guiY + textY + com.getAdditionHeight() - this.scrollIndex;
				
				xpos += (this.textWidth - com.getWidth()) / 2;
				
				boolean hover = isHovering(mouseX, mouseY, xpos, ypos, xpos+com.getWidth(), ypos+com.getHeight());
				if(hover)
				{
					com.onClicked(xpos, ypos, mouseButton, mouseX, mouseY, this);
				}
				
			}
		}	
		
		if(mouseButton == 0)
		{
			if(isHovering(mouseX, mouseY, guiX + textWidth+textX, guiY +textY, guiX + textWidth+textX +4, guiY +textY + 141) || moving)
			{
				moving = true;
				
				double h = mouseY - (guiY +textY);
				double pp = h / 141.0;
				this.scrollIndex = (int) (pp * (totalHeight-textHeight) );
				if(scrollIndex > totalHeight - textHeight)
				{
					scrollIndex = totalHeight - textHeight;
				}
				if(scrollIndex < 0)
				{
					scrollIndex = 0;
				}
			}
		}
		else
		{
			moving = false;
		}
		return super.mouseClicked(mouseX, mouseY, mouseButton);
	}
	
	protected boolean isComponentVisible(IGuiComponent comp)
	{
		int y = comp.getAdditionHeight() - this.scrollIndex + comp.getHeight();	
		return y > 0 && y < textHeight+ comp.getHeight();
	}
	
	/**
	 * Draws an Inisible Cloack for the Entrys
	 * @param matrixStack TODO
	 */
	protected void drawInsisibilityCloak(PoseStack matrixStack)
	{
		
//		RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
		RenderSystem.enableBlend();
//		RenderSystem.setShader(GameRenderer::getPositionTexShader);
//		RenderSystem.setShaderTexture(0, WINDOW_LOCATION);
//		this.blit(MatrixStack, pOffsetX, pOffsetY, 0, 0, 252, 140);
		
		this.setBlitOffset(200);
		fillGradient(matrixStack, 0, 0, this.guiX + textX, this.height, 0x01ffffff, 0x01ffffff);//complete left
		fillGradient(matrixStack, this.guiX + textWidth+textX, 0, this.width, this.height, 0x01ffffff, 0x01ffffff);//right side
		fillGradient(matrixStack, this.guiX + textX, 0, this.guiX +  textWidth+textX, this.guiY + textY, 0x01ffffff, 0x01ffffff);
		fillGradient(matrixStack, this.guiX + textX, this.guiY +  textHeight+textY, this.guiX +  textWidth+textX, this.height, 0x01ffffff, 0x01ffffff);
		this.setBlitOffset(0);
	}
	
	protected void drawScrollBar(PoseStack matrixStack, int mx, int my)
	{
		if(totalHeight>textHeight)
		{
			RenderSystem.setShaderTexture(0, res);
			
			int length = Math.min(141,Math.max(4, (textHeight-2) * (textHeight-2)/totalHeight ));
			int pos = (textHeight-2-length) * scrollIndex/(totalHeight-(textHeight));
			
			RenderSystem.setShaderColor(1F,1F,1F,1F);
			
			HelperGui.drawQuadWithTexture(matrixStack, res, guiX + textWidth + textX, guiY + textY + pos, 187, 1, 4, length-2, 4, length-2, 256, 256, 210);
			HelperGui.drawQuadWithTexture(matrixStack, res, guiX + textWidth + textX, guiY + textY + pos + length -2, 187, 140, 4, 2, 4, 2, 256, 256, 210);
			
			this.setBlitOffset(0);
		}
	}
}
