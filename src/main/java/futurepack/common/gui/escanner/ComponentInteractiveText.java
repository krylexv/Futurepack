package futurepack.common.gui.escanner;

import futurepack.client.research.LocalPlayerResearchHelper;
import futurepack.common.block.misc.BlockDungeonCheckpoint;
import futurepack.common.research.CustomPlayerData;
import net.minecraft.client.Minecraft;
import net.minecraft.network.chat.ClickEvent;
import net.minecraft.network.chat.ClickEvent.Action;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.network.chat.Style;

public class ComponentInteractiveText extends ComponentText
{
	private ClickEvent e;

	public ComponentInteractiveText(MutableComponent text)
	{
		super(text);
		Style s = text.getStyle();
		if(s!=null)
		{
			e = s.getClickEvent();
		}
		LocalPlayerResearchHelper.requestServerData(null);
	}

	
	@Override
	public void onClicked(int x, int y, int mouseButton, double mouseX, double mouseY, GuiScannerBase gui)
	{
		if(e!=null)
		{
			if(e.getAction()==Action.CHANGE_PAGE)
			{
				progressFPCommand(e.getValue());
			}
		}
		super.onClicked(x, y, mouseButton, mouseX, mouseY, gui);
	}
	
	private void progressFPCommand(String s)
	{
		String cmd = s.split("=")[0];
		String value = s.split("=")[1];
		
		if(cmd.equals("openresearch"))
		{
			CustomPlayerData data = CustomPlayerData.createForLocalPlayer();
			if(data.hasResearch(value))
			{
				Minecraft mc = Minecraft.getInstance();
				GuiScannerPageInfo info = (GuiScannerPageInfo) mc.screen;
				GuiScannerPageResearch screen = new GuiScannerPageResearch(value, info.getPreviosGUI());		
				screen.add(info);
				
				mc.setScreen(screen);
			}
		}
		else if(cmd.equals("opentechtree"))
		{
			Minecraft mc = Minecraft.getInstance();
			GuiScannerPageInfo info = (GuiScannerPageInfo) mc.screen;
			GuiResearchMainOverviewBase gui = info.getResearchGui();
			info.openGui(gui);
			
		}
		else if(cmd.equals("dungeon_teleport"))
		{
			BlockDungeonCheckpoint.onClientSelectTeleporter(value, Minecraft.getInstance().player);
		}
	}
}
