package futurepack.common.gui.escanner;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import net.minecraft.network.chat.TranslatableComponent;

public class ComponentTranslate extends ComponentText
{

	public ComponentTranslate(String text, Object[] params)
	{
		super(new TranslatableComponent(text, params));
	}

	public ComponentTranslate(JsonObject jo)
	{
		this(jo.get("text").getAsString(), toArray(jo.getAsJsonArray("params")));
	}
	
	private static String[] toArray(JsonArray arr)
	{
		if(arr==null)
			return null;
		String[] s = new String[arr.size()];
		for(int i=0;i<arr.size();i++)
		{
			s[i] = arr.get(i).toString();
		}
		return s;
	}
}
