package futurepack.common.gui;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.common.block.misc.TileEntityClaime;
import futurepack.common.entity.EntityDrone;
import futurepack.common.gui.inventory.ActuallyUseableContainer;
import futurepack.common.gui.inventory.ActuallyUseableContainerScreen;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.client.gui.components.Button;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ContainerListener;

public class GuiMiner extends ActuallyUseableContainerScreen<GuiMiner.ContainerMiner>
{
	ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/drohne.png");
	
	int scrollIndex;
	int scrollIndex2;
	int focused=-1;
	int focused2=-1;
	
	public GuiMiner(Player pl, EntityDrone m)
	{
		super(new ContainerMiner(pl, m), pl.getInventory(), "gui.miner");
		imageHeight=166;
	}

	@Override
	public void init() 
	{
		super.init();
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		 this.addRenderableWidget(new Button(k+5, l+141, 80, 20, new TextComponent("Working:"+inv().miner.isWorking()), b -> {
			 inv().miner.setWorking(!inv().miner.isWorking());	
				b.setMessage(new TextComponent("Working:"+inv().miner.isWorking()));
				FPPacketHandler.syncWithServer(getMenu());
		 }));
		 this.addRenderableWidget(new Button(k+5+5+80, l+141, 80, 20, new TextComponent("Repeat:"+inv().miner.isRepeat()), b -> {
			 inv().miner.setRepeat(!inv().miner.isRepeat());	
				b.setMessage(new TextComponent("Repeat:"+inv().miner.isRepeat()));
				FPPacketHandler.syncWithServer(getMenu());
		 }));
	}
	
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
		((Button)renderables.get(0)).setMessage(new TextComponent("Working:"+inv().miner.isWorking()));
		((Button)renderables.get(1)).setMessage(new TextComponent("Repeat:"+inv().miner.isRepeat()));
		
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderBg(PoseStack matrixStack, float f, int x, int y)
	{
		RenderSystem.setShaderTexture(0, res);
		RenderSystem.setShaderColor(1, 1, 1, 1);
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		blit(matrixStack, k, l, 0, 0, imageWidth, imageHeight);
		fill(matrixStack, k+7, l+141, k+169, l+159, 0xffc6c6c6);
		for(int i=0;i<6;i++)
		{
			if(i+scrollIndex<inv().claimes.length)
			{
				drawClaime(matrixStack, inv().claimes[i+scrollIndex],k+8, l+18 + i*20);
			}
		}		
		for(int i=0;i<6;i++)
		{
			if(i+scrollIndex<inv().miner.todo.size())
			{
				drawClaime(matrixStack, inv().miner.todo.get(i+scrollIndex),k+99, l+18 + i*20);
			}
		}
		RenderSystem.setShaderTexture(0, res);
		RenderSystem.setShaderColor(1, 1, 1, 1);
		if(focused>=0)
			blit(matrixStack,  k+8,l+18 + focused*20, 176, 0, 63, 18);
		if(focused2>=0)
			blit(matrixStack,  k+99,l+18 + focused2*20, 176, 0, 63, 18);
		
		
	
//		TileEntityClaime clm = EntityDrone.ClaimeData.getCurrentTile(miner);
//		if(clm!=null)
//		{
//			drawClaime(clm, k+99 ,l+18);
//		}
		if(inv().claimes.length==0)
		{
			this.font.draw(matrixStack, "No Claimes", k+12, l+84, 0xffffffff);
		}
		if(inv().miner.todo.isEmpty())
		{
			this.font.draw(matrixStack, "No Claimes", k+103, l+84, 0xffffffff);
		}
	}
	
	private void drawClaime(TileEntityClaime t, PoseStack matrixStack, int x, int y)
	{
		if(t!=null)
		{		
			String n = t.name;
			drawClaime(matrixStack, n, x, y);
		}
	}
	
	private void drawClaime(PoseStack matrixStack, String n, int x, int y)
	{
		RenderSystem.setShaderColor(1, 1, 1, 1);
		RenderSystem.setShaderTexture(0, res);
		blit(matrixStack, x, y, 176, 18, 63, 18);
		if(n.length()>9)
		{
			n = n.substring(0, 9) + ".";
		}
		boolean b = n.equals(inv().miner.getClaime());
		this.font.draw(matrixStack, n, x+3, y+6, b ? 0xffc66900 :0xffffffff);
	}
	
	@Override
	public boolean mouseReleased(double mx, double my, int b) 
	{
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		mx-=k;
		my-=l; 
		
		if(b==0)
		{
			if(hover(mx, my, 79, 26, 97, 44))
			{
				if(focused2>=0 && focused2+scrollIndex2<inv().miner.todo.size())
				{
					inv().miner.todo.remove(focused2+scrollIndex2);
					FPPacketHandler.syncWithServer(getMenu());
				}
				return true;
			}
			if(hover(mx, my, 79, 44, 97, 62))
			{
				if(focused>=0 && focused+scrollIndex<inv().claimes.length)
				{
					if(!inv().miner.todo.contains(inv().claimes[focused+scrollIndex]))
					{
						inv().miner.setClaime("");
						inv().miner.todo.add(inv().claimes[focused+scrollIndex]);
						FPPacketHandler.syncWithServer(getMenu());
					}
				}
				return true;
			}
			
			
			if(hover(mx, my, 72, 17, 78, 23))
			{
				if(scrollIndex>0)
				{
					scrollIndex--;
					if(focused<5)
						focused++;
					
				}
				return true;
			}
			
			if(hover(mx, my, 72, 131, 78, 137))
			{
				if(inv().claimes.length -scrollIndex > 6)
				{
					scrollIndex++;
					if(focused>0)
						focused--;
				}
				return true;
			}
			
			if(hover(mx, my, 163, 17, 169, 23))
			{
				if(scrollIndex2>0)
				{
					scrollIndex2--;
					if(focused2<5)
						focused2++;					
				}
				return true;
			}
			
			if(hover(mx, my, 163, 131, 169, 137))
			{
				if(inv().miner.todo.size() -scrollIndex2 > 6)
				{
					scrollIndex2++;
					if(focused2>0)
						focused2--;
				}
				return true;
			}
			
			for(int i=0;i<Math.min(6, inv().claimes.length -scrollIndex);i++)
			{
				if(hover(mx, my, 8, 18 + i*20, 71, 18 + i*20+18))
				{
					focused = i;
					return true;
				}
				else
				{
					focused = -1;
				}
			}
			for(int i=0;i<Math.min(6, inv().miner.todo.size());i++)
			{
				if(hover(mx, my, 99, 18 + i*20, 162, 18 + i*20+18))
				{
					focused2 = i;
					return true;
				}
				else
				{
					focused2 = -1;
				}
			}
			
		}
		return super.mouseReleased(mx, my, b);
	}
	
	
	private ContainerMiner inv()
	{
		return this.getMenu();
	}
	
	
	private boolean hover(double mx, double my, double x1, double y1, double x2, double y2)
	{
		return mx>=x1 && mx<=x2 && my>=y1 && my <= y2;
	}
	
	public static class ContainerMiner extends ActuallyUseableContainer implements IGuiSyncronisedContainer
	{
		Player pl;
		EntityDrone miner;
		String[] claimes;
		
		public ContainerMiner(Player pl, EntityDrone m)
		{
			this.pl = pl;
			miner = m;
			
			this.claimes = TileEntityClaime.claims.stream().map(WeakReference::get).filter(t -> t!=null && t.getLevel()==m.level && !t.isRemoved()).map(t -> t.name).toArray(String[]::new);
		}
		
		@Override
		public boolean stillValid(Player p_75145_1_)
		{			
			return true;
		}
	
		
		
		@Override
		public void addSlotListener(ContainerListener c)
		{
			super.addSlotListener(c);
			try
			{
				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				DataOutputStream out = new DataOutputStream(bytes);
				out.writeInt(claimes.length);
				for(int i=0;i<claimes.length;i++)
				{
					out.writeUTF(claimes[i]);
				}
				byte[] b = bytes.toByteArray();
				c.dataChanged(this, -1, b.length);
				for(int i=0;i<b.length;i++)
				{
					c.dataChanged(this, i, b[i]);
				}
				c.dataChanged(this, -2, b.length);
			}
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
		
		byte[] bytes;
		
		@Override
		public void setData(int id, int val)
		{
			
			if(id==-1)
			{
				bytes = new byte[val];
				return;
			}
			if(id>=0)
			{
				bytes[id] = (byte) val;
				return;
			}
			if(id == -2 && bytes != null)
			{
				if(bytes.length == val)
				{
					try
					{
						ByteArrayInputStream bin  = new ByteArrayInputStream(bytes);				
						DataInputStream in = new DataInputStream(bin);
						
						claimes = new String[in.readInt()];
						for(int i=0;i<claimes.length;i++)
						{
							claimes[i] = in.readUTF();
						}
					}
					catch (IOException e) {
						e.printStackTrace();
					}
				}
				return;
			}
			super.setData(id, val);
		}

		@Override
		public void writeToBuffer(FriendlyByteBuf buf) 
		{
			CompoundTag nbt = new CompoundTag();
			miner.addAdditionalSaveData(nbt);
			IGuiSyncronisedContainer.writeNBT(buf, nbt);
		}

		@Override
		public void readFromBuffer(FriendlyByteBuf buf) 
		{
			CompoundTag nbt = IGuiSyncronisedContainer.readNBT(buf);
			miner.readAdditionalSaveData(nbt);
			miner.setWorking(miner.isWorking());
		}

		
	}
}
