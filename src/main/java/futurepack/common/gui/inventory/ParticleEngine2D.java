package futurepack.common.gui.inventory;

import java.util.ArrayList;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.vertex.BufferBuilder;
import com.mojang.blaze3d.vertex.DefaultVertexFormat;
import com.mojang.blaze3d.vertex.Tesselator;
import com.mojang.blaze3d.vertex.VertexFormat;

import futurepack.depend.api.helper.HelperRendering;

public class ParticleEngine2D
{
	public static float zLevel = 320F;
	
	private ArrayList<Particle2D> particles = new ArrayList<ParticleEngine2D.Particle2D>();
	
	public void tick()
	{
		particles.parallelStream().forEach(Particle2D::tick);
		particles.removeIf(Particle2D::isDead);
	}
	
	public void render(float partialTicks)
	{
		Tesselator tessellator = Tesselator.getInstance();
		BufferBuilder bufferbuilder = tessellator.getBuilder();
		GlStateManager._enableBlend();
		GlStateManager._disableTexture();
		HelperRendering.glColor4f(1F, 1F, 1F, 1F);
		GlStateManager._blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA.value, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA.value, GlStateManager.SourceFactor.ONE.value, GlStateManager.DestFactor.ZERO.value);
		bufferbuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_COLOR);
		
		for(Particle2D particle : particles)
		{
			particle.render(bufferbuilder, partialTicks);
		}
		
		tessellator.end();
		GlStateManager._enableTexture();
		GlStateManager._disableBlend();
	}
	
	public void addPaticle(Particle2D particle)
	{
		particles.add(particle);
	}
	
	public static class Particle2D
	{
		public final boolean hasGravity;
		public final int maxAge;
		
		private float x,y;
		private float size = 1F;
		private int age;
		private float alpha, red, green, blue;
		
		private float motionX, motionY;
		
		public static float gravity = (9.81F / 20F) / 5F;
		
		public Particle2D(boolean hasGravity, int maxAge) 
		{
			super();
			this.hasGravity = hasGravity;
			this.maxAge = maxAge;
			
			gravity = (9.81F / 20F) / 5F;
		}

		public Particle2D setColor(int color)
		{
			alpha = (float)(color >> 24 & 255) / 255.0F;
			red = (float)(color >> 16 & 255) / 255.0F;
			green = (float)(color >> 8 & 255) / 255.0F;
			blue = (float)(color & 255) / 255.0F;
			return this;
		}
		
		public Particle2D setPostion(float x, float y)
		{
			this.x = x;
			this.y = y;
			return this;
		}
		
		public Particle2D setMotion(float x, float y)
		{
			this.motionX = x;
			this.motionY = y;
			return this;
		}

		public void tick()
		{
			age++;
			
			alpha -= Math.max(0F, (float)age/(float)maxAge - 0.5F);
			
			if(hasGravity)
			{
				motionY += gravity;
			}
			
			x += motionX;
			y += motionY;
		}
		
		public void render(BufferBuilder builder, float partialTicks)
		{
			float x0 = (x + motionX*partialTicks) - 0.5F * size;
			float x1 = (x + motionX*partialTicks) + 0.5F * size;
			float y0 = (y + motionY*partialTicks) - 0.5F * size;
			float y1 = (y + motionY*partialTicks) + 0.5F * size;
			
			builder.vertex(x0, y1, zLevel).color(red, green, blue, alpha).endVertex();
			builder.vertex(x1, y1, zLevel).color(red, green, blue, alpha).endVertex();
			builder.vertex(x1, y0, zLevel).color(red, green, blue, alpha).endVertex();
			builder.vertex(x0, y0, zLevel).color(red, green, blue, alpha).endVertex();
		}
		
		public boolean isDead()
		{
			return age >= maxAge;
		}
	}
	
}
