package futurepack.common.gui.inventory;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.common.block.inventory.TileEntityBlockBreaker;
import futurepack.common.gui.SlotUses;
import futurepack.depend.api.helper.HelperRendering;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;

public class GuiBlockBreaker extends ActuallyUseableContainerScreen<GuiBlockBreaker.ContainerBlockBreaker>
{
	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID,"textures/gui/breaker_gui.png");
		
	public GuiBlockBreaker(Player pl, TileEntityBlockBreaker tile)
	{
		super(new ContainerBlockBreaker(pl.getInventory(), tile), pl.getInventory(), tile.getGUITitle());
	}

	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderBg(PoseStack matrixStack, float var1, int var2, int var3) 
	{
		HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		RenderSystem.setShaderTexture(0, res);
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		this.blit(matrixStack, k, l, 0, 0, this.imageWidth, this.imageHeight);
	}
		
	@Override
	protected void renderLabels(PoseStack matrixStack, int p_146979_1_, int p_146979_2_)
	{
		this.font.draw(matrixStack, this.title.getString(), 8.0F, 6.0F, 4210752);
		this.font.draw(matrixStack, this.playerInventoryTitle.getString(), 8.0F, this.imageHeight - 96 + 2, 4210752);
	}
	
		
	public static class ContainerBlockBreaker extends ActuallyUseableContainer
	{
		TileEntityBlockBreaker tile;
		
		public ContainerBlockBreaker(Inventory inv, TileEntityBlockBreaker tile)
		{
			this.tile = tile;
				for (int l = 0; l < 3; ++l)
			{
				this.addSlot(new SlotUses(tile, l+1, 62 + l * 18, 30));
			}
		
			for (int l = 0; l < 3; ++l)
			{
				for (int i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
				}
			}
			
		for (int l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(inv, l, 8 + l * 18, 142));
			}
		}
		
		@Override
		public ItemStack quickMoveStack(Player pl, int sl)
		{
			if(!pl.level.isClientSide)
			{
				Slot slot= getSlot(sl);
				if(!slot.hasItem())		
					return null;
				
				if(slot.container == pl.getInventory())
				{
				this.moveItemStackTo(slot.getItem(), 0, 3, false);
				}
				else
				{
					this.moveItemStackTo(slot.getItem(), 3, this.slots.size(), false);
				}
			
				if(slot.getItem().getCount()<=0)
				{
					slot.set(ItemStack.EMPTY);
				}
			}
			this.broadcastChanges();
			return ItemStack.EMPTY;
		}

		@Override
		public boolean stillValid(Player p_75145_1_)
		{
			return true;
		}
	}
}
