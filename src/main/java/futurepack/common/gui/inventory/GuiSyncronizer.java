package futurepack.common.gui.inventory;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.common.block.logistic.TileEntitySyncronizer;
import futurepack.common.gui.SlotScrollable;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperContainerSync;
import futurepack.depend.api.helper.HelperRendering;
import futurepack.depend.api.interfaces.IContainerScrollable;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.Container;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ClickType;
import net.minecraft.world.inventory.ResultContainer;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;

public class GuiSyncronizer extends ActuallyUseableContainerScreen<GuiSyncronizer.ContainerSyncronizer>
{
	public ResourceLocation tex = new ResourceLocation(Constants.MOD_ID, "textures/gui/syncronizer.png");
	
	public GuiSyncronizer(Player pl, TileEntitySyncronizer tile)
	{
		super(new ContainerSyncronizer(pl.getInventory(), tile), pl.getInventory(), "gui.syncronizer");
		this.imageHeight = 216;
	}

	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
	{
		this.renderBackground(matrixStack);
		super.render(matrixStack, mouseX, mouseY, partialTicks);
		this.renderTooltip(matrixStack, mouseX, mouseY);
	}

	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		RenderSystem.setShaderTexture(0, tex);
		this.blit(matrixStack, this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight);

		A:
		for(int y=0;y<3;y++)
		{
			for(int x=0;x<2;x++)
			{
				int index = (container().scrollIndex+y) * 2 + x;
				int slotCount = container().slots.size() - container().invSlots;
				slotCount /= 2;
				if(index < slotCount)
				{
					this.blit(matrixStack, this.leftPos+8 + x*69, this.topPos+8 + y*40, 177, 0, 67, 38);
					
				}
				else if(index == slotCount)
				{
					this.blit(matrixStack, this.leftPos+8 + x*69, this.topPos+8 + y*40, 177, 39, 67, 38);
					
				}
				else
				{
					break A;
				}
			}
		}
	}
	
	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int mouseButton)
	{
		double mx = mouseX - leftPos;
		double my = mouseY - topPos;
		
		if(HelperComponent.isInBox(mx, my, 154, 6, 172, 13))//green button
		{
			int data = 0x4000000; // red/green button
			data |= 0x1;
			container().data = data;
			FPPacketHandler.syncWithServer(container());
		}
		else if(HelperComponent.isInBox(mx, my, 154, 33, 172, 40))//red button
		{
			int data = 0x4000000;// red/green button
			data |= 0x2;
			container().data = data;
			FPPacketHandler.syncWithServer(container());
		}
		else
		{
			boolean shift = hasShiftDown();
			boolean up = false;
			int slot = -1;
			
			A:
			for(int y=0;y<3;y++)
			{
				for(int x=0;x<2;x++)
				{
					int index = (container().scrollIndex+y) * 2 + x;
					int slotCount = container().slots.size() - container().invSlots;
					slotCount /= 2;
					if(index < slotCount)
					{
						if(HelperComponent.isInBox(mx -8 -x*69,my -8 -y*40, 10, 2, 28, 9))//plus
						{
							up = true;
							slot = index;
							break A;
						}
						else if(HelperComponent.isInBox(mx -8 -x*69,my -8 -y*40, 10, 29, 28, 36))//minus
						{
							up = false;
							slot = index;
							break A;
						}
					}
				}
			}
			if(slot>= 0)
			{
				int data = 0x8000000;//this bit indicates a +/- click;
				data |= 0xFFFFFF & slot;
				if(shift)
					data |= 0x2000000;
				if(up)
					data |= 0x1000000;
				
				container().data = data;
				FPPacketHandler.syncWithServer(container());
			}
			
		}
		
		A:
		for(int y=0;y<3;y++)
		{
			for(int x=0;x<2;x++)
			{
				int index = (container().scrollIndex+y) * 2 + x;
				int slotCount = container().slots.size() - container().invSlots;
				slotCount /= 2;
				if(index < slotCount)
				{
						if(HelperComponent.isInBox(mouseX-leftPos -8 -x*69, mouseY-topPos -8 -y*40, 2, 2, 9, 9))
						{
							container().removeSlots();
						}
				}
				else if(index == slotCount)
				{
						if(HelperComponent.isInBox(mouseX-leftPos -8 -x*69, mouseY-topPos -8 -y*40, 2, 2, 9, 9))
						{
							container().addSlots();
						}
				}
				else
				{
					break A;
				}
			}
		}

	
		return super.mouseClicked(mouseX, mouseY, mouseButton);
	}
	
	private ContainerSyncronizer container()
	{
		return this.getMenu();
	}
	
	@Override
	public boolean mouseScrolled(double mx, double my, double dw) 
	{
		int mr = container().getMaxRows();
		if(mr <= container().getRowCount())
			return false;
		
		int s0 = container().scrollIndex;
		int s1 = container().scrollIndex+container().getRowCount();
		
		if(dw > 0 && s0 > 0)
		{
			container().scrollIndex--;
			return true;
		}
		else if(dw < 0 && s1 < mr)
		{
			container().scrollIndex++;
			return true;
		}

		
		return super.mouseScrolled(mx, my, dw);
	}
	
	public static class ContainerSyncronizer extends ActuallyUseableContainer implements IContainerScrollable, IGuiSyncronisedContainer
	{
		public int data;
		private Player player;
		private TileEntitySyncronizer handler;
		private IItemHandlerModifiable filter;
		private IItemHandler storage;
		private int lastIndex = -1;
		protected final int invSlots;
		private Container buffer;
		
		public int scrollIndex;
		
		public ContainerSyncronizer(Inventory inv, TileEntitySyncronizer handler)
		{
			player = inv.player;
			this.handler = handler;
			filter = handler.getInventory();
			storage = handler.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null).orElseThrow(NullPointerException::new);
			
			HelperContainerSync.addInventorySlots(8, 134, inv, this::addSlot); // 9 * 4 = 36
			buffer = new ResultContainer();
			this.addSlot(new Slot(buffer, 0, 155, 15));
			
			
			invSlots = this.slots.size();
			
			
			if(!handler.getLevel().isClientSide)
			{
				for(int i=0;i<filter.getSlots();i++)
				{
					lastIndex = i;
					this.addSlot(new SlotScrollable(this, filter, i, (i%2) *69 +19, 19).setFakeSlot(true));
					this.addSlot(new SlotScrollable(this, storage, i, (i%2) *69 +19+36, 19));
				}
			}			
			
		}

		
		public void removeSlots()
		{
			for(int i=slots.size()-1;i>=invSlots;i--)
			{
				SlotScrollable s = (SlotScrollable) slots.get(i);
				if(!s.hasItem() && s.isFakeSlot())
				{
					slots.remove(i+1);
					slots.remove(i);
				}
					
			}
		}


		public void addSlots()
		{
			lastIndex++;
			this.addSlot(new SlotScrollable(this, filter, lastIndex, (lastIndex%2) *69 +19, 19).setFakeSlot(true));
			this.addSlot(new SlotScrollable(this, storage, lastIndex, (lastIndex%2) *69 +19+36, 19));
		}

		@Override
		public void clicked(int slotId, int dragType, ClickType mode, Player player)
		{
			if(!player.level.isClientSide)
			{
				if(slotId >= 0)//-999 => out of window click
				{
					Slot s = getSlot(slotId);
				}
				
			}
			
			if(slotId >= 0 && (mode == ClickType.PICKUP || mode == ClickType.QUICK_MOVE))
			{
				Slot s = getSlot(slotId);
				if(s instanceof SlotScrollable && ((SlotScrollable) s).isFakeSlot())
				{
					if(!getCarried().isEmpty())
					{
						ItemStack inMouseStack = getCarried().copy();
						if(mode == ClickType.PICKUP)
						{
							inMouseStack.setCount(1);
						}
						else
						{
							inMouseStack.setCount(65);
						}
						s.set(inMouseStack);
					}
					else
					{
						
						int add = (mode == ClickType.PICKUP) ? 1 : 10;
						
						if(dragType==0 && s.hasItem())
						{
							s.getItem().grow(add);
							if(s.getItem().getCount() > 64)
								s.getItem().setCount(65);
						}
						else if(dragType==1 && s.hasItem())
						{
							s.getItem().shrink(add);
							if(s.getItem().getCount()<=0)
								s.set(ItemStack.EMPTY);
						}
						
					}
					broadcastChanges();
					return;
				}
			}
			super.clicked(slotId, dragType, mode, player);
		}
		

		@Override
		public Slot getSlot(int slotId)
		{
			while(slots.size() <= slotId)
			{
				int nextSlot = slots.size() - invSlots;
				int slotIndex = nextSlot / 2;
				lastIndex = slotIndex;
				if(slotIndex * 2 == nextSlot)
				{
					this.addSlot(new SlotScrollable(this, filter, slotIndex, (slotIndex%2) *69 +19, 19).setFakeSlot(true));
				}
				else
				{
					this.addSlot(new SlotScrollable(this, storage, slotIndex, (slotIndex%2) *69 +19+36, 19));
				}
			}
			return super.getSlot(slotId);
		}
		
		@Override
		public boolean stillValid(Player playerIn)
		{
			return true;
		}

		@Override
		public ItemStack quickMoveStack(Player playerIn, int index)
		{
			ItemStack itemstack = ItemStack.EMPTY;
			Slot slot = this.slots.get(index);

//			if (slot != null && slot.getHasStack())
//			{
//				ItemStack itemstack1 = slot.getStack();
//				itemstack = itemstack1.copy();
//
//				if (index < 16)
//				{
//					if (!this.mergeItemStack(itemstack1, 16, this.inventorySlots.size(), true))
//					{
//						return ItemStack.EMPTY;
//					}
//				}
//				else if (!this.mergeItemStack(itemstack1, 0, 16, false))
//				{
//					return ItemStack.EMPTY;
//				}
//
//				if (itemstack1.isEmpty())
//				{
//					slot.putStack(ItemStack.EMPTY);
//				}
//				else
//				{
//					slot.onSlotChanged();
//				}
//			}

			return itemstack;
		}

		@Override
		public void removed(Player playerIn)
		{
			super.removed(playerIn);
			if(!buffer.isEmpty())
			{
				playerIn.drop(buffer.getItem(0), false);
				buffer.setItem(0, ItemStack.EMPTY);
			}
			
		}

		@Override
		public int getScollIndex()
		{
			return scrollIndex;
		}

		@Override
		public int getRowWidth()
		{
			return 2;
		}

		@Override
		public int getRowCount()
		{
			return 3;
		}
		
		public int getMaxRows()
		{
			int slots = this.slots.size() - invSlots;
			int rows = slots / 4;
			if(rows*4 < slots)
				rows++;
			return rows+1;
		}
		
		@Override
		public boolean isEnabled(SlotScrollable s)
		{
			int slot = s.getSlotIndex();
			int row = slot/getRowWidth() - getScollIndex();
			if(row >= 0 && row < getRowCount())
			{
				s.y = row * 40 + s.startY;
				return true;
			}
			else
			{
				return false;
			}
		}
		
		@Override
		public void writeToBuffer(FriendlyByteBuf nbt)
		{
			nbt.writeVarInt(data);
		}
		
		@Override
		public void readFromBuffer(FriendlyByteBuf nbt) 
		{
			data = nbt.readVarInt();
			
			if( (0x8000000 & data) == 0x8000000)//+/- click
			{
				int slot = data & 0xFFFFFF;
				boolean shift = (data & 0x2000000) == 0x2000000;
				boolean up = (data & 0x1000000) == 0x1000000;
				
				clicked(invSlots + slot * 2, up ? 0 : 1, shift ? ClickType.QUICK_MOVE : ClickType.PICKUP, player);
			}
			else if( (0x4000000 & data) == 0x4000000)//red / green button
			{
				if( (0x1 & data) == 0x1)//green
				{
					handler.storeFilterInItem(buffer.getItem(0));
				}
				else if((0x2 & data) == 0x2)//red
				{
					handler.loadFilterFromItem(buffer.getItem(0));
				}
			}
		}
	}
}

