package futurepack.common.gui.inventory;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.block.modification.TileEntityModificationBase;
import futurepack.common.block.modification.machines.TileEntityOptiBench;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.SlotBaseXPOutput;
import futurepack.common.modification.EnumChipType;
import futurepack.common.sync.FPGuiHandler;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperContainerSync;
import futurepack.depend.api.helper.HelperGui;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.Container;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntity;

public class GuiOptiBench extends GuiMachineSupport<TileEntityOptiBench> 
{	
	public GuiOptiBench(Player pl, TileEntityOptiBench tile)
	{
		super(new ContainerOptiBench(pl.getInventory(),tile), "opti_crafting.png", pl.getInventory());
	}
	
	@Override
	public void init() 
	{
		super.init();
		HelperGui.RestoreCursorPos();
	}
	
    @Override
	protected void renderBg(PoseStack matrixStack, float f, int mx, int my)
    {
    	super.renderBg(matrixStack, f, mx, my);     
        this.blit(matrixStack, leftPos+80, topPos+34, 176, 0, (int)(29 * (tile().getProperty(TileEntityOptiBench.FIELD_PROGRESS)/10D)), 18);
  
        //PartRenderer.renderSupport(matrixStack, guiLeft+158, guiTop+7, tile().getSupport(), mx, my);
    }
    
	@Override
	protected void renderLabels(PoseStack matrixStack, int p_146979_1_, int p_146979_2_)
	{
		super.renderLabels(matrixStack, p_146979_1_, p_146979_2_);
		//PartRenderer.renderSupportTooltip(matrixStack, guiLeft, guiTop, 158, 7, tile().getSupport(), p_146979_1_, p_146979_2_);
	}
 
    @Override
    public TileEntityOptiBench tile()
    {
    	return ((ContainerOptiBench<TileEntityOptiBench>)this.getMenu()).tile;
    }
    	
	@Override
	public boolean mouseReleased(double mx, double my, int but) 
	{
		if(HelperComponent.isInBox(mx-leftPos, my-topPos, 96, 10, 114, 28))
		{
			if(but == 0)
			{
				HelperGui.SaveCursorPos();
				FPPacketHandler.syncWithServer((IGuiSyncronisedContainer) this.getMenu());
				return true;
			}
		}
		return super.mouseReleased(mx, my, but);
	}
    
	public static class ContainerOptiBench<T extends BlockEntity & Container & ITilePropertyStorage> extends ContainerSyncBase implements IGuiSyncronisedContainer
	{
		protected final T tile;
		private Player player;
//		int lp, le, la;
		
		public ContainerOptiBench(Inventory inv, T tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			this.player = inv.player;
			
			int l;
	        int i1;
	        
	        addSlot(new SlotBaseXPOutput(inv.player, tile, 9, 115, 35, (tile instanceof TileEntityModificationBase ? () -> (double)((TileEntityModificationBase)tile).getChipPower(EnumChipType.ULTIMATE) : () -> 0D)));
	        
			for (l = 0; l < 3; ++l)
	        {
	            for (i1 = 0; i1 < 3; ++i1)
	            {
	                this.addSlot(new Slot(tile, i1 + l * 3, 26 + i1 * 18, 17 + l * 18));
	            }
	        }		
	        
	        HelperContainerSync.addInventorySlots(8, 84, inv, this::addSlot);
	        
	        this.addSlot(new Slot(tile, 10, 116, 11));
			this.addSlot(new Slot(tile, 11, 116, 59));
		}
			
		@Override
		public ItemStack quickMoveStack(Player par1EntityPlayer, int par2)
		{
			ItemStack itemstack = ItemStack.EMPTY;
	        Slot slot = this.slots.get(par2);

	        if (slot != null && slot.hasItem())
	        {
	            ItemStack itemstack1 = slot.getItem();
	            itemstack = itemstack1.copy();

	            if (par2 == 0)
	            {
	                if (!this.moveItemStackTo(itemstack1, 10, 46, true))
	                {
	                    return ItemStack.EMPTY;
	                }

	                slot.onQuickCraft(itemstack1, itemstack);
	            }
	            else if (par2 >= 10 && par2 < 37)
	            {
	                if (!this.moveItemStackTo(itemstack1, 37, 46, false))
	                {
	                    return ItemStack.EMPTY;
	                }
	            }
	            else if (par2 >= 37 && par2 < 46)
	            {
	                if (!this.moveItemStackTo(itemstack1, 10, 37, false))
	                {
	                    return ItemStack.EMPTY;
	                }
	            }
	            else if (!this.moveItemStackTo(itemstack1, 10, 46, false))
	            {
	                return ItemStack.EMPTY;
	            }

	            if (itemstack1.getCount() == 0)
	            {
	                slot.set(ItemStack.EMPTY);
	            }
	            else
	            {
	                slot.setChanged();
	            }

	            if (itemstack1.getCount() == itemstack.getCount())
	            {
	                return ItemStack.EMPTY;
	            }

	            slot.onTake(par1EntityPlayer, itemstack1);
	        }

	        return itemstack;
		}
		
		@Override
		public boolean stillValid(Player var1)
		{
			return true;
		}

		@Override
		public void writeToBuffer(FriendlyByteBuf nbt)  
		{

		}

		@Override
		public void readFromBuffer(FriendlyByteBuf nbt) 
		{
			FPGuiHandler.CONSTRUCTION_TABLE.openGui(player, tile.getBlockPos());
		}
		
//		@Override
//		public void addCraftingToCrafters(ICrafting c)
//		{
//			super.addCraftingToCrafters(c);
//			c.sendProgressBarUpdate(this, 0, (int)tile.getProgress());
//			c.sendProgressBarUpdate(this, 1, (int)tile.getPower());		
//			c.sendProgressBarUpdate(this, 2, (int)tile.getRBonus());		
//		}
//		
//		@Override
//		public void detectAndSendChanges()
//		{
//			super.detectAndSendChanges();
//			for(ICrafting c : (List<ICrafting>) this.listeners)
//			{
//				if(lp != (int)tile.getProgress())
//				{
//					c.sendProgressBarUpdate(this, 0, (int)tile.getProgress());
//				}
//				if(le != (int)tile.getPower())
//				{
//					c.sendProgressBarUpdate(this, 1, (int)tile.getPower());		
//				}
//				if(la != (int)tile.getRBonus())
//				{
//					c.sendProgressBarUpdate(this, 2, (int)tile.getRBonus());		
//				}
//				lp = (int)tile.getProgress();
//				le = (int)tile.getPower();
//				la = (int)tile.getRBonus();
//			}
//		}
//		
//		//@ TODO: OnlyIn(Dist.CLIENT)
//		@Override
//		public void updateProgressBar(int id, int val)
//		{
//			super.updateProgressBar(id, val);
//			if(id==0)
//			{
//				tile.setProgress(val);
//			}
//			if(id==1)
//			{
//				tile.setPower(val);
//			}
//			if(id==2)
//			{
//				tile.setAI(val);
//			}
//		}
		
	}
}
