package futurepack.common.gui.inventory;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.event.IndustrieSmeltEvent;
import futurepack.common.block.modification.machines.TileEntityIndustrialNeonFurnace;
import futurepack.common.gui.ContainerSyncBase;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ClickType;
import net.minecraft.world.inventory.FurnaceResultSlot;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;

public class GuiIndNeonFurnace extends GuiMachineSupport<TileEntityIndustrialNeonFurnace>
{
	public GuiIndNeonFurnace(Player pl, TileEntityIndustrialNeonFurnace tile)
	{
		super(new ContainerIndNeonFurnace(pl.getInventory(), tile), "neonindustrialfurnace.png", pl.getInventory());
	}

	
	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY) 
	{
		super.renderBg(matrixStack, partialTicks, mouseX, mouseY);
		
		int f2 = (int) (tile().getProgress() * 24);
		this.blit(matrixStack, leftPos+93, topPos+35, 176, 14, f2, 17);
		for(int i=0;i<tile().uses.length;i++)
		{
			if(tile().uses[i])
				this.blit(matrixStack, leftPos+26 + (i * 18), topPos+60, 176, 0, 17, 13);
		}
		
		//PartRenderer.renderSupport(matrixStack, guiLeft+158, guiTop+7, tile().getSupport(), mouseX, mouseY);
	}
	
	@Override
	protected void renderLabels(PoseStack matrixStack, int p_146979_1_, int p_146979_2_)
	{
		super.renderLabels(matrixStack, p_146979_1_, p_146979_2_);
		//PartRenderer.renderSupportTooltip(matrixStack, guiLeft, guiTop, 158, 7, tile().getSupport(), p_146979_1_, p_146979_2_);
	}
	
	@Override
	public TileEntityIndustrialNeonFurnace tile()
	{
		return ((ContainerIndNeonFurnace)this.getMenu()).tile;
	}

	public static class ContainerIndNeonFurnace extends ContainerSyncBase
	{
		private TileEntityIndustrialNeonFurnace tile;
//		int lp=0,lb=0;
		
		public ContainerIndNeonFurnace(Inventory pl, TileEntityIndustrialNeonFurnace tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			
			for(int i=0;i<3;i++)
			{
				this.addSlot(new Slot(this.tile, i, 26 + (18 * i), 34)); //Smelt Input
			}
			for(int i=0;i<3;i++)
			{
				this.addSlot(new FurnaceResultSlot(pl.player, this.tile, 3+i, 134, 16 + (i * 18))); //Smelt Output
			}
			int l;
			int i1;
			
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(pl, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
				}
			}
			
			for (l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(pl, l, 8 + l * 18, 142));
			}
		}
		
		@Override
		public ItemStack quickMoveStack(Player pl, int par2)
		{
			Slot slot = this.getSlot(par2);
			if(!pl.level.isClientSide && slot.hasItem())
			{
				if(slot.container == pl.getInventory())
				{
					if(tile.canPlaceItem(0, slot.getItem()))
					{
						this.moveItemStackTo(slot.getItem(), 0, 1, false);
					}
					else
					{
						this.moveItemStackTo(slot.getItem(), 1, 4, false);
					}					
				}
				else
				{
					this.moveItemStackTo(slot.getItem(), 8, this.slots.size(), false);				
				}
				
				if(slot.getItem().getCount()<=0)
				{
					slot.set(ItemStack.EMPTY);
				}
			}
			this.broadcastChanges();
			return ItemStack.EMPTY;
		}
		
		@Override
		public void clicked(int slotId, int dragType, ClickType clickTypeIn, Player player)
		{
			super.clicked(slotId, dragType, clickTypeIn, player);
			if(!getCarried().isEmpty())
			{
				MinecraftForge.EVENT_BUS.post(new IndustrieSmeltEvent(player, getCarried()));
			}
		}
		
		@Override
		public boolean stillValid(Player var1)
		{
			return true;
		}
	}
}
