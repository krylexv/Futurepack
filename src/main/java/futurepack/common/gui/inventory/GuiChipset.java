package futurepack.common.gui.inventory;

import java.text.DecimalFormat;
import java.util.ArrayList;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.common.block.modification.InventoryModificationBase;
import futurepack.common.block.modification.TileEntityModificationBase;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.SlotUses;
import futurepack.common.modification.thermodynamic.TemperatureControler;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperGui;
import futurepack.depend.api.helper.HelperRendering;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;

public class GuiChipset extends ActuallyUseableContainerScreen<GuiChipset.ContainerChipset>
{
	private TileEntityModificationBase tile;
	
	public GuiChipset(Player pl, TileEntityModificationBase tile)
	{
		super(new ContainerChipset(pl.getInventory(), tile), pl.getInventory(), "gui.chipset");
		this.tile = tile;
		tile.getInventory().startOpen(pl);
	}

	@Override
	protected void renderBg(PoseStack matrixStack, float var1, int mx, int my) 
	{
		RenderSystem.setShaderTexture(0, container().def.text);
		HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

		this.blit(matrixStack, leftPos, topPos, 0, 0, this.imageWidth, this.imageHeight);
		drawHeatSlots(matrixStack, leftPos, topPos);
		
		if(tile.getEnergyType()==EnumEnergyMode.PRODUCE)
			HelperRendering.glColor4f(0.0F, 1.0F, 0.0F, 1.0F);
		this.blit(matrixStack, leftPos-18, topPos+44, 200, 38, 9, 9);
		
		HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.blit(matrixStack, leftPos-18, topPos+58, 209, 38, 9, 9);
		
		float cp = tile.getInventory().getCorePower();
		float nc = tile.getInventory().getNeededCore();
		
		int p = (int) (nc / cp * 11);
		p = p > 14 ? 14 : p;
		
		if(!tile.getInventory().canWork())
		{
			p = 0;
			
			this.blit(matrixStack, leftPos-18, topPos+16, 191, 38, 9, 9);		
		}
		
		this.blit(matrixStack, leftPos+container().def.overclockX, topPos+container().def.overclockY, 184, 22, p * 4, 6);			
		this.blit(matrixStack, leftPos+container().def.overclockX, topPos+container().def.overclockY, 184, 29, 56, 6);
		
		int x = leftPos + container().def.overclockX -6;
		int y = topPos + container().def.overclockY -7 -4 -13;
		
		//	68 x 13
		this.blit(matrixStack, x, y, 184, 49, 68, 13);
		if(!container().tile.edit && container().tile.canWork())
		{
			this.blit(matrixStack, x+6, y+5, 190, 63, 6, 3);
		}
		if(HelperComponent.isInBox(mx-x, my-y, 55, 3, 64, 10))
		{			
			this.blit(matrixStack, x+55, y+3, 197, 63, 9, 7);
		}
		//matrixStack.pushPose();
		this.blit(matrixStack, leftPos-18, topPos+30, 182, 38, 9, 9);
		DecimalFormat df = new DecimalFormat("0.0");
		String s = df.format(tile.getHeat())+ " C";
		int w = this.font.width(s);
		this.font.draw(matrixStack, s, leftPos-18-w-2, topPos+31, tile.getHeat() > 50 ? 0xff4444: 0xffffff);
		//matrixStack.popPose();
		
		//matrixStack.pushPose();
		s = tile.getDefaultPowerUsage() + " NE";
		w = this.font.width(s);
		this.font.draw(matrixStack, s, leftPos-18-w-2, topPos+45, 0xffffff);
		//matrixStack.popPose();
		
		//matrixStack.pushPose();
		s = "RAM " + df.format(tile.getPureRam()* 5F) + "%";
		w = this.font.width(s);
		this.font.draw(matrixStack, s, leftPos-18-w-2, topPos+59, 0xffffff);
		//matrixStack.popPose();
	}

	
	
	@Override
	public boolean mouseReleased(double mx, double my, int button) 
	{
		int x = leftPos + container().def.overclockX -6;
		int y = topPos + container().def.overclockY -7 -4 -13;
		
		if(HelperComponent.isInBox(mx-x, my-y, 55, 3, 64, 10))
		{			
			FPPacketHandler.syncWithServer(container());
		}

		return super.mouseReleased(mx, my, button);
	}
	
	private void drawHeatSlots(PoseStack matrixStack, int x, int y)
	{
		TemperatureControler con = tile.getTemperatureControloer();
		
		ChipsetDefinition def = container().def;
		
		for(Integer[] ints : def.cores)
		{
			con.drawCore(matrixStack, x+ints[0]-1, y+ints[1]-1, ints[2]);
		}
		
		for(Integer[] ints : def.chips)
		{
			con.drawChip(matrixStack, x+ints[0]-1, y+ints[1]-1, ints[2]);
		}
		
		for(Integer[] ints : def.rams)
		{
			con.drawRam(matrixStack, x+ints[0]-1, y+ints[1]-1, ints[2]);
		}
//		con.drawCore(x+97,y+37, 0);
//		
//		for(int h=0;h<3;h++)
//		{
//			con.drawRam(x+151, y+7+h*18, h);
//			for(int w=0;w<3;w++)
//			{
//				con.drawChip(x+7+w*18, y+7+h*18, h*3+w);
//			}
//		}
	}
	
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks) 
	{
		this.renderBackground(matrixStack);
		super.render(matrixStack, mouseX, mouseY, partialTicks);
		this.renderTooltip(matrixStack, mouseX, mouseY);
		drawOverlay(matrixStack, mouseX, mouseY);
	}
	
	private void drawOverlay(PoseStack matrixStack, int mx, int my)
	{
		int minx = container().def.overclockX-6, miny = container().def.overclockY-7;
		int maxx = container().def.overclockX+62, maxy = container().def.overclockY+12;		
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;

		
		//rendering on/off button
		
		
		minx+=k;maxx+=k;
		miny+=l;maxy+=l;
		
		if(mx>=minx&mx<=maxx && my>=miny&my<=maxy)
		{
			float cp = tile.getInventory().getCorePower();
			float nc = tile.getInventory().getNeededCore();
			String s = nc + " / " + cp;			
			HelperRendering.disableLighting();
			HelperGui.renderText(matrixStack, mx, my, s);
			
			int h = this.font.lineHeight;
			int w = this.font.width(s);
		}
		
	}
	
	public ContainerChipset container()
	{
		return this.getMenu();
	}
	
	public static class ContainerChipset extends ContainerSyncBase implements IGuiSyncronisedContainer
	{
		public final TileEntityModificationBase tile;
		public final ChipsetDefinition def;
//		int lh, lp;
		
		public ContainerChipset(Inventory inv, TileEntityModificationBase tile)
		{
			super(tile.getInventory(), tile.getLevel().isClientSide());
			this.def = ChipsetDefinition.getDefinition(tile.getInventory());
			this.tile = tile;
			
			def.addSlots(this, tile);
			
//			for (int l = 0; l < 3; ++l)
//	        {
//	            for (int i1 = 0; i1 < 3; ++i1)
//	            {
//	                this.addSlot(new SlotUses(tile.getInventory(), i1 + l * 3, 8 + i1 * 18, 8 + l * 18));
//	            }
//	        }
//			
//			for (int l = 0; l < 3; ++l)
//	        {
//				this.addSlot(new SlotUses(tile.getInventory(), 10 + l, 152, 8 + l * 18));
//	        }
//			
//			this.addSlot(new SlotUses(tile.getInventory(), 9, 98, 38));
			
			for (int l = 0; l < 3; ++l)
	        {
	            for (int i1 = 0; i1 < 9; ++i1)
	            {
	                this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
	            }
	        }

	        for (int l = 0; l < 9; ++l)
	        {
	            this.addSlot(new Slot(inv, l, 8 + l * 18, 142));
	        }
	        if(!tile.getLevel().isClientSide)
	        	tile.getInventory().startOpen(inv.player);
		}
		
		@Override
		public Slot addSlot(Slot slotIn)
		{
			return super.addSlot(slotIn);
		}

		
		@Override
		public boolean stillValid(Player var1)
		{
			return tile.getInventory().stillValid(var1);
		}
		
		@Override
		public ItemStack quickMoveStack(Player par1EntityPlayer, int par2)
		{
			return ItemStack.EMPTY;
		}

		@Override
		public void writeToBuffer(FriendlyByteBuf nbt)
		{
			
		}

		@Override
		public void readFromBuffer(FriendlyByteBuf nbt)
		{
			tile.edit = !tile.edit;
			if(!tile.canWork())
			{	
				tile.edit = true;
			}
			broadcastChanges();
		}
		
//		@Override
//		public void addCraftingToCrafters(ICrafting c)
//		{
//			super.addCraftingToCrafters(c);
//			c.sendProgressBarUpdate(this, 0, (int)tile.heat);
//			c.sendProgressBarUpdate(this, 1, (int)tile.getPower());
//		}
//		
//		@Override
//		public void detectAndSendChanges()
//		{
//			super.detectAndSendChanges();
//			
//			for(ICrafting c : (List<ICrafting>)this.listeners)
//			{
//				if(lh != (int)tile.heat)
//				{
//					c.sendProgressBarUpdate(this, 0, (int)tile.heat);
//				}
//				if(lp != (int)tile.getPower())
//				{
//					c.sendProgressBarUpdate(this, 1, (int)tile.getPower());
//				}
//				
//				lh = (int)tile.heat;
//				lp = (int)tile.getPower();
//			}
//		}
//		
//		@Override
//		public void updateProgressBar(int id, int val)
//		{
//			super.updateProgressBar(id, val);
//			if(id==0)
//			{
//				tile.heat = val;
//			}
//			if(id==1)
//			{
//				tile.setPower(val);
//			}
//		}
		
		@Override
		public void removed(Player playerIn)
		{
			super.removed(playerIn);
			tile.getInventory().stopOpen(playerIn);
		}
	}
	
	
	public static class ChipsetDefinition
	{
		public ResourceLocation text = new ResourceLocation(Constants.MOD_ID,"textures/gui/chipset.png");
		//the display how much core power you have used
		int overclockX=78, overclockY=7;
		
		private ArrayList<Integer[]> cores = new ArrayList<Integer[]>();
		private ArrayList<Integer[]> chips = new ArrayList<Integer[]>();
		private ArrayList<Integer[]> rams = new ArrayList<Integer[]>();
		
		public void addCoreSlot(int x, int y)
		{
			addSlot(x, y, cores);	
		}
		
		public void addChipSlot(int x, int y)
		{
			addSlot(x, y, chips);	
		}
		
		public void addRamSlot(int x, int y)
		{
			addSlot(x, y, rams);	
		}
		
		private void addSlot(int x, int y, ArrayList<Integer[]> list)
		{
			Integer[] ints = new Integer[]{x,y,0};
			ints[2] = list.size();
			list.add(ints);	
		}
		
		public void addSlots(ContainerChipset con, TileEntityModificationBase base)
		{
			for(Integer[] ints : chips)
			{
				con.addSlot(new SlotUses(base.getInventory(), ints[2], ints[0], ints[1]));
			}
			for(Integer[] ints : cores)
			{
				con.addSlot(new SlotUses(base.getInventory(), ints[2]+chips.size(), ints[0], ints[1]));
			}
			for(Integer[] ints : rams)
			{
				con.addSlot(new SlotUses(base.getInventory(), ints[2]+chips.size()+cores.size(), ints[0], ints[1]));
			}
		}
		
		private static ArrayList<ChipsetDefinition> defs = new ArrayList<ChipsetDefinition>();
		
		static
		{
			ChipsetDefinition core1 = new ChipsetDefinition();
			core1.addCoreSlot(98, 38);
			
			for(int h=0;h<3;h++)
			{
				core1.addRamSlot(152, 8+h*18);
				for(int w=0;w<3;w++)
				{
					core1.addChipSlot(8+w*18, 8+h*18);
				}
			}
			register(core1);
			
			ChipsetDefinition core2 = new ChipsetDefinition();
			core2.overclockX=51;
			core2.text = new ResourceLocation(Constants.MOD_ID,"textures/gui/chipset_big.png");
			core2.addCoreSlot(50, 29);
			core2.addCoreSlot(86, 42);
			
			for(int h=0;h<3;h++)
			{
				core2.addChipSlot(8, 8+h*18);
				for(int w=0;w<2;w++)
				{
					core2.addRamSlot(134+w*18, 8+h*18);
				}
			}
			register(core2);
		}
		
		public static void register(ChipsetDefinition def)
		{
			defs.add(def);
		}
		public static ChipsetDefinition getDefinition(InventoryModificationBase base)
		{
			for(ChipsetDefinition def : defs)
			{
				if(def.chips.size() == base.getChipSlots())
				{
					if(def.cores.size() == base.getCoreSlots())
					{
						if(def.rams.size() == base.getRamSlots())
						{
							return def;
						}
					}
				}
			}
			return null;
		}
	}
	
}
