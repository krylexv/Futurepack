package futurepack.common.gui.inventory;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.block.modification.machines.TileEntityCrusher;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.SlotBaseXPOutput;
import futurepack.common.gui.SlotUses;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;

public class GuiCrusher extends GuiMachineSupport<TileEntityCrusher>
{
	public GuiCrusher(Player pl, TileEntityCrusher tile)
	{
		super(new ContainerCrusher(pl.getInventory(), tile), "super_crusher.png", pl.getInventory());
	}
	

	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY) 
	{
		super.renderBg(matrixStack, partialTicks, mouseX, mouseY);
		
		int f = (int) (tile().getProperty(TileEntityCrusher.FIELD_PROGRESS)/11F * 28F);
		this.blit(matrixStack, leftPos+78, topPos+28, 176, 6, f, 29);
	}
	
	@Override
	public TileEntityCrusher tile()
	{
		return ((ContainerCrusher)this.getMenu()).tile;
	}
	
	public static class ContainerCrusher extends ContainerSyncBase
	{
		TileEntityCrusher tile;
//		int lp, le;
		
		public ContainerCrusher(Inventory inv, TileEntityCrusher tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			
			this.addSlot(new SlotUses(tile, 0, 56, 35));//input
			this.addSlot(new SlotBaseXPOutput(inv.player, tile, 1, 116, 35));//output
			this.addSlot(new SlotUses(tile, 2, 116, 59));
			
			int l;
			int i1;
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
				}
			}
			
			for (l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(inv, l, 8 + l * 18, 142));
			}
		}
		
		@Override
		public ItemStack quickMoveStack(Player pl, int par2)
		{
			if(!pl.level.isClientSide)
			{
				Slot slot = this.slots.get(par2);
		        if(slot != null && slot.hasItem())
		        {
		        	if(slot.container == tile)
		        	{
		        		this.moveItemStackTo(slot.getItem(), 2, slots.size(), false);
		        	}
		        	else
		        	{
		        		this.moveItemStackTo(slot.getItem(), 0, 1, false);
		        	}
		        	if(slot.getItem().getCount()==0)
		        	{
		        		slot.set(ItemStack.EMPTY);
		        	}
		        }
			}
			this.broadcastChanges();
			return ItemStack.EMPTY;
		}
		
		@Override
		public boolean stillValid(Player var1)
		{
			return tile.stillValid(var1);
		}
		
//		@Override
//		public void addCraftingToCrafters(ICrafting c)
//		{
//			super.addCraftingToCrafters(c);
//			c.sendProgressBarUpdate(this, 0, (int)tile.getProgress());
//			c.sendProgressBarUpdate(this, 1, (int)tile.getPower());			
//		}
//		
//		@Override
//		public void detectAndSendChanges()
//		{
//			super.detectAndSendChanges();
//			for(ICrafting c : (List<ICrafting>) this.listeners)
//			{
//				if(lp != (int)tile.getProgress())
//				{
//					c.sendProgressBarUpdate(this, 0, (int)tile.getProgress());
//				}
//				if(le != (int)tile.getPower())
//				{
//					c.sendProgressBarUpdate(this, 1, (int)tile.getPower());		
//				}
//				
//				lp = (int)tile.getProgress();
//				le = (int)tile.getPower();
//			}
//		}
//		
//		//@ TODO: OnlyIn(Dist.CLIENT)
//		@Override
//		public void updateProgressBar(int id, int val)
//		{
//			super.updateProgressBar(id, val);
//			if(id==0)
//			{
//				tile.setProgress(val);
//			}
//			if(id==1)
//			{
//				tile.setPower(val);
//			}
//		}
	}
}
