package futurepack.common.gui.inventory;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.common.gui.SlotFakeItem;
import futurepack.common.item.tools.compositearmor.CompositeArmorInventory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.SlotItemHandler;

public class GuiCompositeArmor extends ActuallyUseableContainerScreen
{
	private final ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/modular_armor.png");

	public GuiCompositeArmor(Player pl)
	{
		super(new ContainerCompositeArmor(pl), pl.getInventory(), "gui.composite_armor");
		this.imageHeight = 181;
	}
	
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		RenderSystem.setShaderTexture(0, res);
		blit(matrixStack, this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight);
	}
	
	public static class ContainerCompositeArmor extends ActuallyUseableContainer
	{
		Player player;
		CompositeArmorInventory modularArmor;
		int slot_count;
		
		public ContainerCompositeArmor(Player pl)
		{
			player = pl;
			modularArmor = new CompositeArmorInventory(pl);
			
			if(modularArmor.head!=null)
			{	
				this.addSlot(new SlotItemHandler(modularArmor.head, 0, 107, 9));
				this.addSlot(new SlotItemHandler(modularArmor.head, 1, 107 +18, 9));
				this.addSlot(new SlotItemHandler(modularArmor.head, 2, 107 +18*2, 9));
				slot_count +=3;
			}
			
			if(modularArmor.chest!=null)
			{
				this.addSlot(new SlotItemHandler(modularArmor.chest, 0, 98, 29));
				this.addSlot(new SlotItemHandler(modularArmor.chest, 1, 98 +18, 29));
				this.addSlot(new SlotItemHandler(modularArmor.chest, 2, 98 +18*2, 29));
				this.addSlot(new SlotItemHandler(modularArmor.chest, 3, 98 +18*3, 29));
				slot_count+=4;
			}
			
			if(modularArmor.legs!=null)
			{
				this.addSlot(new SlotItemHandler(modularArmor.legs, 0, 98, 49));
				this.addSlot(new SlotItemHandler(modularArmor.legs, 1, 98 +18, 49));
				this.addSlot(new SlotItemHandler(modularArmor.legs, 2, 98 +18*2, 49));
				this.addSlot(new SlotItemHandler(modularArmor.legs, 3, 98 +18*3, 49));
				slot_count+=4;
			}
			
			if(modularArmor.feet!=null)
			{
				this.addSlot(new SlotItemHandler(modularArmor.feet, 0, 116, 69));
				this.addSlot(new SlotItemHandler(modularArmor.feet, 1, 116 +18, 69));
				slot_count+=2;
			}
			
			int l;
			int i1;
			
			for (l = 0; l < 4; ++l)
			{
				this.addSlot(new SlotFakeItem(pl.getInventory(), l + 36, 8, 9+ (3-l) * 18));
			}
			
			
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(pl.getInventory(), i1 + l * 9 + 9, 8 + i1 * 18, 99 + l * 18));
				}
			}
			
			for (l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(pl.getInventory(), l, 8 + l * 18, 157));
			}
		}
		
		@Override
		public boolean stillValid(Player playerIn)
		{
			return true;
		}
		
		@Override
		public void removed(Player playerIn)
		{
			modularArmor.saveAll();
			super.removed(playerIn);
		}
		
		@Override
		public ItemStack quickMoveStack(Player pl, int par2)
		{
			if(!pl.level.isClientSide)
			{
				Slot slot = this.slots.get(par2);
		        if(slot != null && slot.hasItem())
		        {
		        	if(par2 < slot_count)
		        	{
		        		this.moveItemStackTo(slot.getItem(), slot_count, slots.size(), false);
		        	}
		        	else
		        	{
		        		this.moveItemStackTo(slot.getItem(), 0, slot_count, false);		// 0 2	
		        	}
		        	if(slot.getItem().getCount()<=0)
		        	{
		        		slot.set(ItemStack.EMPTY);
		        	}
		        		
		        }
			}
			this.broadcastChanges();
			return ItemStack.EMPTY;
		}
		
	}
}
