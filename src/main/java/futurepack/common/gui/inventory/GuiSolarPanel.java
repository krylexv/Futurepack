package futurepack.common.gui.inventory;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.block.modification.machines.TileEntitySolarPanel;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.SlotBaseXPOutput;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;

public class GuiSolarPanel extends GuiModificationBase<TileEntitySolarPanel>
{
	public GuiSolarPanel(Player player, TileEntitySolarPanel tile)
	{
		super(new ContainerSolarPanel(player.getInventory(), tile), "solarzelle.png", player.getInventory());		
	}

	int[][][] cash = new int[14][][];
	
	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(matrixStack, partialTicks, mouseX, mouseY);
		
		renderSun(matrixStack, leftPos +67, topPos +13, tile().light);
	}

	
	private void renderSun(PoseStack matrixStack, int x, int y, int lvl)
	{
		int mr = 18; 
		if(lvl==15)
		{
			blit(matrixStack, x, y, 178, 2, 42, 42);
		}
		else if(lvl==0)
		{
			
		}
		else
		{		 
			cash[lvl-1] = null;
			if(cash[lvl-1]==null)
			{
				int r = lvl +2;
				
				int[][] points = new int[mr*2][]; 
				for(int mx = -mr; mx < mr; mx++)
				{
					int miny = 0, maxy = 0;
					for(int my= -mr; my < mr; my++)
					{
						if(mx*mx + my*my <= r*r + 3)
						{
							miny = Math.min(miny, my);
							maxy = Math.max(maxy, my);
						}
					}
					if(maxy - miny > 0)
						points[mx+mr] = new int[]{miny, maxy};
				}
				cash[lvl-1] = points;
			}
			int[][] obj = cash[lvl-1];
			
			x += 21;
			y += 21;
			for(int mx = -mr; mx < mr; mx++)
			{
				if(obj[mx+mr] != null)
				{
					int miny = obj[mx+mr][0];
					int maxy = obj[mx+mr][1];
					blit(matrixStack, x+mx, y+miny, 178 + 21 + mx, 2 + 21 + miny, 1, maxy - miny);
				}
			}
		}
	}
	
	@Override
	public TileEntitySolarPanel tile()
	{
		return ((ContainerSolarPanel)this.getMenu()).tile;
	}
	
	public static class ContainerSolarPanel extends ContainerSyncBase
	{

		protected final TileEntitySolarPanel tile;
		
		public ContainerSolarPanel(Inventory inv, TileEntitySolarPanel tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			
			addHint(this.addSlot(new SlotBaseXPOutput(inv.player, tile, 0, 26, 11)), NEON_BATTERY);
			
			 
			for (int l = 0; l < 3; ++l)
			{
				for (int i1 = 0; i1 < 9; ++i1)
	            {
	                this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
	            }
	        }
			
	        for (int l = 0; l < 9; ++l)
	        {
	            this.addSlot(new Slot(inv, l, 8 + l * 18, 142));
	        }
	      
		}
		
		@Override
		public boolean stillValid(Player var1)
		{
			return HelperResearch.isUseable(var1, tile);
		}
		
		@Override
		public ItemStack quickMoveStack(Player par1EntityPlayer, int par2)
		{
			 Slot slot = this.slots.get(par2);
			 if(slot !=null && slot.hasItem())
			 {
				 if(par2==0)
				 {
					 this.moveItemStackTo(slot.getItem(), 1, this.slots.size(), false);
				 }
				 else
				 {
					 this.moveItemStackTo(slot.getItem(), 0, 1, false);
				 }
				 
				 if(slot.getItem().getCount()<=0)
				 {
					 slot.set(ItemStack.EMPTY);
				 }
				 this.broadcastChanges();
			 }
			return ItemStack.EMPTY;
		}
		
	}
}
