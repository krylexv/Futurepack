package futurepack.common.gui.inventory;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.interfaces.IItemSupport;
import futurepack.api.interfaces.IRecyclerTool;
import futurepack.common.block.modification.machines.TileEntityRecycler;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.SlotUses;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;

public class GuiRecycler extends GuiMachineSupport<TileEntityRecycler>
{
	public GuiRecycler(Player pl, TileEntityRecycler tile)
	{
		super(new ContainerRecycler(pl.getInventory(), tile), "recycler.png", pl.getInventory());
	}

	
	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(matrixStack, partialTicks, mouseX, mouseY);
		
		int f2 = (int) (12.0f * tile().getProgress() / tile().getMaxProgress());
		this.blit(matrixStack, leftPos+68, topPos+40-f2, 176, 0, 6, f2);
		
		
		//PartRenderer.renderSupport(matrixStack, guiLeft+158, guiTop+7, tile().getSupport(), mouseX, mouseY);
	}
	
	@Override
	protected void renderLabels(PoseStack matrixStack, int p_146979_1_, int p_146979_2_)
	{
		//Removed to remove default text (inventory and gui name) super.drawGuiContainerForegroundLayer(matrixStack, p_146979_1_, p_146979_2_);
		//PartRenderer.renderSupportTooltip(matrixStack, guiLeft, guiTop, 158, 7, tile().getSupport(), p_146979_1_, p_146979_2_);
	}

	@Override
	public TileEntityRecycler tile()
	{
		return ((ContainerRecycler)this.getMenu()).tile;
	}
	
	public static class ContainerRecycler extends ContainerSyncBase
	{
		TileEntityRecycler tile;
		
		public ContainerRecycler(Inventory pl, TileEntityRecycler tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			
			this.addSlot(new SlotUses(this.tile, 0, 26, 8)); //Smelt Input		
			this.addSlot(new SlotUses(this.tile, 1, 62, 44)); //Smelt Output
			
			for (int i = 0; i < 3; ++i)
			{
				for (int j = 0; j < 3; ++j)
				{
					this.addSlot(new SlotUses(this.tile, 3*i + j + 2, 98 + j*18, 26 + i*18));
				}
			}	
			
			this.addSlot(new SlotUses(this.tile, 11, 53, 8));
			this.addSlot(new SlotUses(this.tile, 12, 53+18, 8));
			
			//this.addSlot(new SlotFurnaceOutput(pl.player, this.tile, 2, 134, 34)); //Smelt Output
			//this.addSlot(new SlotFurnaceOutput(pl.player, this.tile, 3, 125, 59)); //Smelt Output
		
			int l;
			int i1;
			
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(pl, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
				}
			}
			
			for (l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(pl, l, 8 + l * 18, 142));
			}
		}

		@Override
		public boolean stillValid(Player playerIn)
		{
			return true;
		}
		
		@Override
		public ItemStack quickMoveStack(Player pl, int index)
		{
			 Slot slot = this.slots.get(index);
			 ItemStack itemstack = ItemStack.EMPTY;
			 
			 if (slot != null && slot.hasItem())
			 {
				 ItemStack itemstack1 = slot.getItem();
				 itemstack = itemstack1.copy();
				 
				 if(index<13)
				 {
					 if (!this.moveItemStackTo(itemstack1, 13, 40, true))
					 {
						 return ItemStack.EMPTY;
					 }

					 slot.onQuickCraft(itemstack1, itemstack);
				 }
				 else
				 {
					 boolean merged = false;
					 if(itemstack1.getItem() instanceof IRecyclerTool)
					 {
						 if(this.moveItemStackTo(itemstack1, 1, 2, true))
						 {
							 merged = true;
						 }
					 }
					 
					 if(!merged && itemstack1.getItem() instanceof IItemSupport)
					 {
						 if (this.moveItemStackTo(itemstack1, 11, 13, true))
						 {
							 merged = true;
						 }
					 }
					 
					 if (!merged && !this.moveItemStackTo(itemstack1, 0, 1, true))
					 {
						 merged = true;
					 }
					 
					 if(merged)
						 slot.onQuickCraft(itemstack1, itemstack);
					 else
						 return ItemStack.EMPTY;
				 }
				 
				 
				 if (itemstack1.getCount() == 0)
				 {
					 slot.set(ItemStack.EMPTY);
				 }
				 else
				 {
					 slot.setChanged();
				 }

				 if (itemstack1.getCount() == itemstack.getCount())
				 {
					 return ItemStack.EMPTY;
				 }
			 }
			return itemstack;
		}
		
		
	}
}
