package futurepack.common;

import java.util.List;
import java.util.stream.Stream;

import com.mojang.brigadier.CommandDispatcher;

import futurepack.api.Constants;
import futurepack.api.FPApiMain;
import futurepack.api.StatsManager;
import futurepack.api.capabilities.ILogisticInterface;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.api.capabilities.ISupportStorage;
import futurepack.api.interfaces.IAirSupply;
import futurepack.api.interfaces.IChunkAtmosphere;
import futurepack.common.block.terrain.TerrainBlocks;
import futurepack.common.commands.CommandFuturepack;
import futurepack.common.commands.CommandFuturepackDebug;
import futurepack.common.entity.IPlayerData;
import futurepack.common.item.ResourceItems;
import futurepack.common.item.group.TabFB_Base;
import futurepack.common.item.group.TabFP_deco;
import futurepack.common.item.group.TabFP_items;
import futurepack.common.item.group.TabFP_maschiens;
import futurepack.common.item.group.TabFP_resources;
import futurepack.common.item.group.TabFP_tools;
import futurepack.common.item.tools.ItemGrablingHook;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.item.tools.compositearmor.CompositeArmorInventory;
import futurepack.common.item.tools.compositearmor.ItemModulShield;
import futurepack.common.modification.thermodynamic.TemperatureManager;
import futurepack.common.recipes.RecipeManagerSyncer;
import futurepack.common.recipes.airbrush.AirbrushRegistry;
import futurepack.common.research.CircularDependencyTest;
import futurepack.common.research.PlayerDataLoader;
import futurepack.common.research.ResearchLoader;
import futurepack.common.spaceships.FPPlanetRegistry;
import futurepack.common.sync.FPPacketHandler;
import futurepack.data.FPWorldData;
import futurepack.data.FuturepackWorldgenDumpReportProvider;
import futurepack.depend.api.helper.HelperOreDict;
import futurepack.extensions.CuriousIntegration.CuriosIntegration;
import futurepack.extensions.computercraft.ComputerCraftIntegration;
import futurepack.world.dimensions.atmosphere.AtmosphereManager;
import futurepack.world.protection.FPDungeonProtection;
import futurepack.world.protection.IChunkProtection;
import futurepack.world.scanning.FPChunkScanner;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.data.info.WorldgenRegistryDumpReport;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.RegisterCapabilitiesEvent;
import net.minecraftforge.event.AddReloadListenerEvent;
import net.minecraftforge.event.RegisterCommandsEvent;
import net.minecraftforge.event.server.ServerAboutToStartEvent;
import net.minecraftforge.event.server.ServerStartedEvent;
import net.minecraftforge.event.server.ServerStartingEvent;
import net.minecraftforge.event.server.ServerStoppedEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.InterModComms;
import net.minecraftforge.fml.InterModComms.IMCMessage;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLLoadCompleteEvent;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import net.minecraftforge.fml.event.lifecycle.InterModProcessEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.forge.event.lifecycle.GatherDataEvent;

@Mod(Constants.MOD_ID)
public class FuturepackMain
{
	public static FuturepackMain INSTANCE = null;
	
	public static DistExecutorBase proxy = DistExecutor.unsafeRunForDist(() -> () -> new DistExecutorClient(), () -> () -> new DistExecutorServer());
	
	public static TabFB_Base tab_deco = new TabFP_deco("futurepack.deco");
	public static TabFB_Base tab_maschiens = new TabFP_maschiens( "futurepack.machines");
	public static TabFB_Base tab_items = new TabFP_items("futurepack.items");
	public static TabFB_Base tab_resources = new TabFP_resources("futurepack.resources");
	public static TabFB_Base tab_tools = new TabFP_tools("futurepack.tools");
	
	public static DamageSource NEON_DAMAGE = new DamageSource("neonDamage");
	
	public FuturepackMain()
	{
//		FluidRegistry.enableUniversalBucket();
		
		System.out.println("FuturepackMain.FuturepackMain()");
		
		if(FuturepackMain.INSTANCE!=null)
			throw new IllegalStateException("Only one instance at once is supported");
			
		FuturepackMain.INSTANCE = this;
		
		//EVENT BUS registration
		IEventBus modBus = FMLJavaModLoadingContext.get().getModEventBus();
		
//		modBus.addListener(this::onFingerprintViolation);
		modBus.addListener(this::init);
		modBus.addListener(this::registerCapabilities);
		
		
//		MinecraftForge.EVENT_BUS.addListener(this::init);
		
		modBus.addListener(proxy::postInitClient);
		modBus.addListener(proxy::postInitServer);
		
		modBus.addListener(this::sendIMCToMods);
		modBus.addListener(this::collectIMC);
		
		modBus.addListener(this::setupFinished);
		
		MinecraftForge.EVENT_BUS.addListener(this::addReloadListener);
		MinecraftForge.EVENT_BUS.addListener(this::serverAboutToStart);
		MinecraftForge.EVENT_BUS.addListener(this::registerCommandsEvent);
		MinecraftForge.EVENT_BUS.addListener(this::serverStarting);
		MinecraftForge.EVENT_BUS.addListener(this::serverStarted);
		MinecraftForge.EVENT_BUS.addListener(this::serverStopped);
		
//		modBus.addListener(this::mappingChanged);
		
		modBus.register(FPConfig.class);
		modBus.addListener(FPEntitys::registerGlobalEntityAttributes);
		modBus.addListener(this::addDataGenerator);
		
//		MinecraftForge.EVENT_BUS.register(this);
		
		//CONFIG registration
		FPConfig.registerConfig(ModLoadingContext.get().getActiveContainer());
		
		FPRegistry.init(modBus);

		proxy.earlySetup();
		
		
		if(ModList.get().isLoaded("computercraft"))
		{
			computerCraft();
		}
		if(ModList.get().isLoaded("curios"))
		{
			CuriosIntegration.init(modBus);
		}
	}
	
	
	private static void computerCraft()
	{
		ComputerCraftIntegration.init();
	}
	
	
//	public void onFingerprintViolation(FMLFingerprintViolationEvent event)
//	{
//		System.out.println("[ERROR] Invalid fingerprint detected! The file " + event.getSource().getName() + " may have been tampered with. This version will NOT be supported by the author! Expected " + event.getExpectedFingerprint() +" but found " + Arrays.deepToString(event.getFingerprints().toArray()));
//	    
//	}
	
	public void addDataGenerator(GatherDataEvent event)
	{
		FPWorldData.init();
		
//		event.getGenerator().addProvider(new WorldgenRegistryDumpReport(event.getGenerator()));
		event.getGenerator().addProvider(new FuturepackWorldgenDumpReportProvider(event.getGenerator()));
	}
	
	public void init(FMLCommonSetupEvent event)
	{
		
		System.out.println("FuturepackMain.init()");
		
		FPPacketHandler.init();
		FPPlanetRegistry.init();
		AtmosphereManager.init();
		FPDungeonProtection.init();
		
		MinecraftForge.EVENT_BUS.register(PlayerDataLoader.instance);
		MinecraftForge.EVENT_BUS.register(ManagerGleiter.class);
		MinecraftForge.EVENT_BUS.register(MagnetActivisionHelper.class);
		MinecraftForge.EVENT_BUS.register(CompositeArmorInventory.class);
		MinecraftForge.EVENT_BUS.register(FPChunkScanner.INSTANCE);
		MinecraftForge.EVENT_BUS.addListener(ItemModulShield::onLivingDamaged);
		MinecraftForge.EVENT_BUS.register(FuturepackEventHandler.INSTANCE);
		MinecraftForge.EVENT_BUS.register(RecipeManagerSyncer.INSTANCE);
		
		HelperOreDict.Builder b = new HelperOreDict.Builder();

		b.add(ResourceItems.ingot_aluminium, new ResourceLocation("forge:ingots/aluminum"));
		b.add(ResourceItems.ingot_aluminium, new ResourceLocation("forge:ingots/aluminium"));
		b.add(ResourceItems.ingot_bioterium, new ResourceLocation("forge:ingots/bioterium"));
		b.add(ResourceItems.ingot_bitripentium, new ResourceLocation("forge:ingots/bitripentium"));
		b.add(ResourceItems.ingot_copper, new ResourceLocation("forge:ingots/copper"));
		b.add(ResourceItems.ingot_gadolinium, new ResourceLocation("forge:ingots/gadolinium"));
		b.add(ResourceItems.ingot_glowite, new ResourceLocation("forge:ingots/glowtite"));
		b.add(ResourceItems.ingot_lithium, new ResourceLocation("forge:ingots/lithium"));
		b.add(ResourceItems.ingot_magnet, new ResourceLocation("forge:ingots/magnetite"));
		b.add(ResourceItems.ingot_neodymium, new ResourceLocation("forge:ingots/neodymium"));
		b.add(ResourceItems.ingot_neon, new ResourceLocation("forge:ingots/neon"));
		b.add(ResourceItems.ingot_quantanium, new ResourceLocation("forge:ingots/quantanium"));
		b.add(ResourceItems.ingot_rare_earth, new ResourceLocation("forge:ingots/rare_earth"));
		b.add(ResourceItems.ingot_rare_earth, new ResourceLocation("forge:ingots/rareearthmetal"));
		b.add(ResourceItems.ingot_retium, new ResourceLocation("forge:ingots/retium"));
		b.add(ResourceItems.ingot_silicon, new ResourceLocation("forge:ingots/silicon"));
		b.add(ResourceItems.ingot_tin, new ResourceLocation("forge:ingots/tin"));
		b.add(ResourceItems.ingot_wakurium, new ResourceLocation("forge:ingots/wakurium"));
		b.add(ResourceItems.ingot_zinc, new ResourceLocation("forge:ingots/zinc"));
		
		b.add(TerrainBlocks.crystal_alutin, new ResourceLocation("forge:gems/aluminum"));
		b.add(TerrainBlocks.crystal_bioterium, new ResourceLocation("forge:gems/bioterium"));
		b.add(TerrainBlocks.crystal_glowtite, new ResourceLocation("forge:gems/glowtite"));
		b.add(TerrainBlocks.crystal_neon, new ResourceLocation("forge:gems/neon"));
		b.add(TerrainBlocks.crystal_retium, new ResourceLocation("forge:gems/retium"));

		WorldGenRegistry.init();
		
		HelperOreDict.FuturepackConveter = b.build();

		Thread t = new Thread(() -> FPLog.logger.info(StatsManager.sendVersionRequest("https://redsnake-games.de/futurepack/version.php")));
		t.start();

		System.out.println("FuturepackMain.init() -- Done");
	}
	
	public void registerCapabilities(RegisterCapabilitiesEvent event)
	{
		event.register(INeonEnergyStorage.class);//, new CapabilityNeon.Storage(), CapabilityNeon::new);
		event.register(ISupportStorage.class);//, new CapabilitySupport.Storage(), CapabilitySupport::new);
		event.register(ILogisticInterface.class);//, new CapabilityLogistic.Storage(), CapabilityLogistic::new);
		event.register(IPlayerData.class);//, new CapabilityPlayerData.Storage(), CapabilityPlayerData::new);
		
		event.register(IChunkProtection.class);//, new CapabilityChunkProtection.Storage(), CapabilityChunkProtection::new);
		event.register(IAirSupply.class);//, new CapabilityAirSupply.Storage(), CapabilityAirSupply::new);
		event.register(IChunkAtmosphere.class);//, new CapabilityAtmosphere.Storage(), CapabilityAtmosphere::new);
		
	}
	
	public static void reloadAllRecipes()
	{
		//recipes
		AsyncTaskManager.addTask(AsyncTaskManager.ASYNC_RESTARTABLE, AirbrushRegistry::init);
		
		AsyncTaskManager.ASYNC_RESTARTABLE.joinWithStats();
	}
	
	public void sendIMCToMods(InterModEnqueueEvent event)
	{
		System.out.println("FuturepackMain.sendIMCToMods()");
	}
	
	public void collectIMC(InterModProcessEvent event)
	{
		System.out.println("FuturepackMain.collectIMC()");
		Stream<IMCMessage> msg = InterModComms.getMessages(Constants.MOD_ID, Constants.ADD_RESEARCH_JSON::equals);
		msg.map(m -> (FPApiMain.IMCResearchPath)m.messageSupplier().get()).forEach(p -> ResearchLoader.instance.registerResearchReader(p.getModID(), p.getReader()));
		
		msg = InterModComms.getMessages(Constants.MOD_ID, Constants.DUNGEON_WHITELIST::equals);
		msg.map(m -> (ResourceLocation)m.messageSupplier().get()).forEach(FPDungeonProtection::addBlockToWhiteList);
	}
	
	public void setupFinished(FMLLoadCompleteEvent event)
	{		
		System.out.println("FuturepackMain.setupFinished()");
		proxy.setupFinished(event);
	}
	
	public void addReloadListener(AddReloadListenerEvent event) 
	{
		event.addListener(new DataFolderWalker.RecipeReloadListener());
	}
	
	public void registerCommandsEvent(RegisterCommandsEvent  event) 
	{
		CommandDispatcher<CommandSourceStack> disp = event.getDispatcher();
		CommandFuturepack.register(disp);
		CommandFuturepackDebug.register(disp);
	}

	
	public void serverAboutToStart(ServerAboutToStartEvent event) 
	{
		System.out.println("FuturepackMain.serverAboutToStart()");
		
		
		
		DataFolderWalker.setServer(event.getServer());
	}
	
	public void serverStarting(ServerStartingEvent event) 
	{
		System.out.println("FuturepackMain.serverStarting()");
		
	}
	
	
	
	public void serverStarted(ServerStartedEvent event) 
	{
		TemperatureManager.init();
		
		//tags are per world so they are now available
		reloadAllRecipes();
		//researches after IMC so other can register custom jsons
		ResearchLoader.instance.init();
		
		if(FPConfig.IS_RESEARCH_DEBUG.getAsBoolean())
		{
			Thread t = new Thread((Runnable)() -> 
			{
				try
				{
					HelperOreDict.waitForTagsToLoad();
					
					System.out.println("Starting Research Circular Dependency checker");
					List<String> s = CircularDependencyTest.checkResearchTreeDeadLocks();
					if(!s.isEmpty())
					{
						s.forEach(System.out::println);
						throw new IllegalStateException(s.get(0));
						
					}
					
					CircularDependencyTest.checkIfAllBLocksHaveLoottables();
					
					CircularDependencyTest.checkIfAllBlocksWithDropsHaveATool();
				}
				catch(Exception e)
				{
					event.getServer().execute(() -> {
						event.getServer().stopServer();
						throw e;
					});
				}
			});
			t.start();
		}		
	}
	
	public void serverStopped(ServerStoppedEvent event) 
	{
		try
		{
			FPSelectorHelper.clean();
			FPChunckManager.clean();
			((ItemGrablingHook)ToolItems.grappling_hook).clean();
			FPChunkScanner.INSTANCE.join();
		}
		catch(Throwable t)
		{
			t.printStackTrace();
		}
	}
	
//	public void mappingChanged(ModIdMappingEvent evt)
//	{
//		reloadAllRecipes();
//		
//	}

}
