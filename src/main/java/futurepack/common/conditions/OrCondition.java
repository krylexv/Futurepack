package futurepack.common.conditions;

import java.util.Set;
import java.util.function.BooleanSupplier;

import com.google.gson.JsonElement;

public class OrCondition implements BooleanSupplier
{
	private final Set<BooleanSupplier> conditions;

	public OrCondition(Set<BooleanSupplier> conditions)
	{
		this.conditions = conditions;
	}
	
	public OrCondition(BooleanSupplier...conditions)
	{
		this(Set.of(conditions));
	}

	@Override
	public boolean getAsBoolean()
	{
		for(var c : conditions)
		{
			if(c.getAsBoolean())
				return true;
		}
		return false;
	}

	public static OrCondition fromJson(JsonElement elm)
	{
		return new OrCondition(ConditionRegistry.load(elm.getAsJsonArray()));
	}
}
