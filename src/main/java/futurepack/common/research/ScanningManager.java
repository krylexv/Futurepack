package futurepack.common.research;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.Future;

import futurepack.api.Constants;
import futurepack.api.EnumScanPosition;
import futurepack.api.interfaces.IScanPart;
import futurepack.common.AsyncTaskManager;
import futurepack.common.FuturepackTags;
import futurepack.common.block.inventory.InventoryBlocks;
import futurepack.common.block.plants.PlantBlocks;
import futurepack.common.block.terrain.TerrainBlocks;
import futurepack.common.entity.living.EntityCrawler;
import futurepack.common.entity.living.EntityGehuf;
import futurepack.common.entity.living.EntityHeuler;
import futurepack.common.entity.living.EntityWolba;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageEScanner;
import futurepack.world.dimensions.Dimensions;
import net.minecraft.ChatFormatting;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.ClickEvent;
import net.minecraft.network.chat.ClickEvent.Action;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.tags.FluidTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.animal.Chicken;
import net.minecraft.world.entity.animal.Cow;
import net.minecraft.world.entity.animal.Pig;
import net.minecraft.world.entity.animal.Rabbit;
import net.minecraft.world.entity.animal.Sheep;
import net.minecraft.world.entity.animal.horse.Donkey;
import net.minecraft.world.entity.animal.horse.Horse;
import net.minecraft.world.entity.animal.horse.Llama;
import net.minecraft.world.entity.monster.CaveSpider;
import net.minecraft.world.entity.monster.Creeper;
import net.minecraft.world.entity.monster.Ghast;
import net.minecraft.world.entity.monster.MagmaCube;
import net.minecraft.world.entity.monster.Slime;
import net.minecraft.world.entity.monster.Witch;
import net.minecraft.world.entity.monster.Zombie;
import net.minecraft.world.entity.npc.Villager;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.vehicle.AbstractMinecart;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraftforge.network.NetworkDirection;

public class ScanningManager
{
	public static List<ScanEntry> list = new ArrayList<ScanEntry>();
	
	private static final ArrayList<IScanPart>[] scanList;
	
	static
	{
		scanList = new ArrayList[EnumScanPosition.values().length];
		for(int i=0;i<scanList.length;i++)
		{
			scanList[i] = new ArrayList<IScanPart>();
		}
		
		register(EnumScanPosition.HEADLINE, new ScanPartHeadline());
		register(EnumScanPosition.MAIN, new ScanPartMining());
		register(EnumScanPosition.MAIN, new ScanPartEntityAbilities());
		register(EnumScanPosition.MAIN, new ScanPartMagnetism());
		register(EnumScanPosition.MAIN, new ScanPartNeonEngine());
		register(EnumScanPosition.MAIN, new ScanPartSupport());
		register(EnumScanPosition.MAIN, new ScanPartTech());
		register(EnumScanPosition.FOOTER, new ScanPartNBTTags());
		register(EnumScanPosition.MAIN, new ScanPartAtmoshphere());
		register(EnumScanPosition.MAIN, new ScanPartNetwork());
	}
	
	static
	{
		//Geologie
		new ScanEntry(Material.STONE,	"hs.geologie");
		new ScanEntry(Material.SAND,	"hs.geologie");
		new ScanEntry(Material.DIRT,	"hs.geologie");
		new ScanEntry(Material.CLAY,	"hs.geologie");
		
		//Chemie
		new ScanEntry(Creeper.class, 	"hs.chemie");
		new ScanEntry(Blocks.TNT, 	       	"hs.chemie");
		new ScanEntry(Ghast.class, 	"hs.chemie");
		new ScanEntry(Blocks.BREWING_STAND, "hs.chemie");
		new ScanEntry(Blocks.CAULDRON, 	    "hs.chemie");
		new ScanEntry(Witch.class, 	"hs.chemie");
		new ScanEntry(Blocks.CAULDRON, 	    "hs.chemie");
		
		//Tierhaltung
		new ScanEntry(Cow.class,     		"hs.tierhaltung");
		new ScanEntry(Sheep.class,   		"hs.tierhaltung");
		new ScanEntry(Chicken.class, 		"hs.tierhaltung");
		new ScanEntry(Pig.class,     		"hs.tierhaltung");
		new ScanEntry(EntityGehuf.class,     	"hs.tierhaltung");
		new ScanEntry(Horse.class,     	"hs.tierhaltung");
		new ScanEntry(Donkey.class,     	"hs.tierhaltung");
		new ScanEntry(Rabbit.class,     	"hs.tierhaltung");
		new ScanEntry(Llama.class,     	"hs.tierhaltung");
		new ScanEntry(Blocks.ACACIA_FENCE, 		"hs.tierhaltung");
		new ScanEntry(Blocks.ACACIA_FENCE_GATE,	"hs.tierhaltung");
		new ScanEntry(Blocks.BIRCH_FENCE,		"hs.tierhaltung");
		new ScanEntry(Blocks.BIRCH_FENCE_GATE,	"hs.tierhaltung");
		new ScanEntry(Blocks.DARK_OAK_FENCE,	"hs.tierhaltung");
		new ScanEntry(Blocks.DARK_OAK_FENCE_GATE,"hs.tierhaltung");
		new ScanEntry(Blocks.JUNGLE_FENCE,		"hs.tierhaltung");
		new ScanEntry(Blocks.JUNGLE_FENCE_GATE,	"hs.tierhaltung");
		new ScanEntry(Blocks.OAK_FENCE,			"hs.tierhaltung");
		new ScanEntry(Blocks.OAK_FENCE_GATE,	"hs.tierhaltung");
		new ScanEntry(Blocks.SPRUCE_FENCE,		"hs.tierhaltung");
		new ScanEntry(Blocks.SPRUCE_FENCE_GATE,	"hs.tierhaltung");
		new ScanEntry(Blocks.HAY_BLOCK,			"hs.tierhaltung");
		
		//Logistik
		new ScanEntry(Blocks.RAIL, 	      		"hs.logistik");
		new ScanEntry(Blocks.HOPPER, 	   		"hs.logistik");
		new ScanEntry(Blocks.DROPPER, 	   		"hs.logistik");
		new ScanEntry(Blocks.DISPENSER, 	   	"hs.logistik");
		new ScanEntry(Blocks.DETECTOR_RAIL,		"hs.logistik");
		new ScanEntry(Blocks.ACTIVATOR_RAIL,	"hs.logistik");
		new ScanEntry(Blocks.POWERED_RAIL, 	   	"hs.logistik");
		new ScanEntry(Pig.class, 	      	"hs.logistik");
		new ScanEntry(Horse.class, 	    "hs.logistik");
		new ScanEntry(AbstractMinecart.class,		"hs.logistik");
		new ScanEntry(FuturepackTags.wardrobe,	"hs.logistik");
		new ScanEntry(Llama.class,		"hs.logistik");
		new ScanEntry(Blocks.DIRT_PATH, 	   	"hs.logistik");
		
		//Biologie
		new ScanEntry(Material.LEAVES, 	    "hs.biologie");
		new ScanEntry(Material.CACTUS, 	    "hs.biologie");
		new ScanEntry(Material.PLANT, 	    "hs.biologie");
		new ScanEntry(Material.REPLACEABLE_WATER_PLANT, 	"hs.biologie");
		new ScanEntry(Material.WATER_PLANT,	"hs.biologie");
		new ScanEntry(Material.REPLACEABLE_PLANT,	"hs.biologie");
		new ScanEntry(Material.BAMBOO,		"hs.biologie");
		new ScanEntry(Mob.class,		"hs.biologie");
		new ScanEntry(Blocks.SLIME_BLOCK, 	"hs.biologie");
		new ScanEntry(Blocks.COAL_ORE, 		"hs.biologie");
		new ScanEntry(Blocks.DEEPSLATE_COAL_ORE, 		"hs.biologie");
		new ScanEntry(TerrainBlocks.ore_coal_m, 		"hs.biologie");
		
		//Morphologie
		new ScanEntry(Slime.class, 			"hs.morphologie");
		new ScanEntry(MagmaCube.class,		"hs.morphologie");
		new ScanEntry(Villager.class, 		"hs.morphologie");
		new ScanEntry(Zombie.class, 			"hs.morphologie");
		new ScanEntry(Blocks.SLIME_BLOCK, 			"hs.morphologie");
		new ScanEntry(Blocks.RED_MUSHROOM_BLOCK, 	"hs.morphologie");
		new ScanEntry(Blocks.BROWN_MUSHROOM_BLOCK, 	"hs.morphologie");
		new ScanEntry(Blocks.STICKY_PISTON, 		"hs.morphologie");
		new ScanEntry(Blocks.COAL_ORE, 				"hs.morphologie");
		new ScanEntry(Blocks.DEEPSLATE_COAL_ORE, 		"hs.morphologie");
		new ScanEntry(TerrainBlocks.ore_coal_m, 		"hs.morphologie");
		
		//Metallurgie
		new ScanEntry(Blocks.IRON_ORE, 		"hs.metallurgie");
		new ScanEntry(Blocks.GOLD_ORE, 		"hs.metallurgie");
		new ScanEntry(Blocks.DEEPSLATE_IRON_ORE, 		"hs.metallurgie");
		new ScanEntry(Blocks.DEEPSLATE_GOLD_ORE, 		"hs.metallurgie");
		new ScanEntry(Material.METAL, 		"hs.metallurgie");
		new ScanEntry(FuturepackTags.bockWrapper(new ResourceLocation("forge:ores/iron")),	"hs.metallurgie");
		new ScanEntry(FuturepackTags.bockWrapper(new ResourceLocation("forge:ores/gold")),	"hs.metallurgie");
		new ScanEntry(FuturepackTags.bockWrapper(new ResourceLocation("forge:ores/copper")),	"hs.metallurgie");
		new ScanEntry(FuturepackTags.bockWrapper(new ResourceLocation("forge:ores/tin")),	"hs.metallurgie");
		new ScanEntry(FuturepackTags.bockWrapper(new ResourceLocation("forge:ores/zinc")),	"hs.metallurgie");
		new ScanEntry(FuturepackTags.bockWrapper(new ResourceLocation("forge:ores/silver")),	"hs.metallurgie");
		new ScanEntry(FuturepackTags.bockWrapper(new ResourceLocation("forge:ores/lead")),	"hs.metallurgie");
		new ScanEntry(FuturepackTags.bockWrapper(new ResourceLocation("forge:ores/nickel")),	"hs.metallurgie");
		new ScanEntry(FuturepackTags.bockWrapper(new ResourceLocation("forge:ores/cobalt")),	"hs.metallurgie");
		
		//Maschinenbau
		new ScanEntry(Blocks.PISTON, 					"hs.maschinenbau");
		new ScanEntry(Blocks.STICKY_PISTON, 			"hs.maschinenbau");
		new ScanEntry(Blocks.DISPENSER, 				"hs.maschinenbau");
		new ScanEntry(Blocks.DROPPER, 					"hs.maschinenbau");
		new ScanEntry(InventoryBlocks.block_breaker,	"hs.maschinenbau");
		new ScanEntry(Blocks.FURNACE,					"hs.maschinenbau");
		new ScanEntry(Blocks.OBSERVER,					"hs.maschinenbau");
		new ScanEntry(InventoryBlocks.techtable,		"hs.maschinenbau");
		new ScanEntry(InventoryBlocks.t0_generator,		"hs.maschinenbau");
		
		//Theromodynamik
		new ScanEntry(Blocks.FURNACE,	 	"hs.thermodynamic");
		new ScanEntry(FluidTags.LAVA, 		"hs.thermodynamic", null);
		new ScanEntry(Blocks.TORCH,  		"hs.thermodynamic");
		new ScanEntry(Blocks.WALL_TORCH,  		"hs.thermodynamic");
		new ScanEntry(Blocks.GLOWSTONE,  	"hs.thermodynamic");
		new ScanEntry(Blocks.JACK_O_LANTERN,  	"hs.thermodynamic");
		
		
		//Energieverbindungen
		new ScanEntry(Blocks.TRIPWIRE,	 		"hs.energieverbindungen");
		new ScanEntry(Blocks.TRIPWIRE_HOOK,	 	"hs.energieverbindungen");
		new ScanEntry(Blocks.REDSTONE_WIRE, 	"hs.energieverbindungen");
		new ScanEntry(Blocks.REDSTONE_BLOCK,	"hs.energieverbindungen");
		new ScanEntry(Blocks.REDSTONE_ORE,  	"hs.energieverbindungen");
		new ScanEntry(Blocks.DEEPSLATE_REDSTONE_ORE,  	"hs.energieverbindungen");
		new ScanEntry(Blocks.REDSTONE_TORCH,	"hs.energieverbindungen");
		
		//Neonenergie
		new ScanEntry(FuturepackTags.block_crystals,  	"hs.neonenergie");
		new ScanEntry(FuturepackTags.neon_sand,			"hs.neonenergie");
		new ScanEntry(InventoryBlocks.t0_generator,		"hs.neonenergie");
		new ScanEntry(InventoryBlocks.battery_box_w,	"hs.neonenergie");
		new ScanEntry(InventoryBlocks.battery_box_g,	"hs.neonenergie");
		new ScanEntry(InventoryBlocks.battery_box_b,	"hs.neonenergie");
		new ScanEntry(InventoryBlocks.modul_1_w,		"hs.neonenergie");
		new ScanEntry(InventoryBlocks.modul_1_g,		"hs.neonenergie");
		new ScanEntry(InventoryBlocks.modul_1_s,		"hs.neonenergie");
		
		//Hydraulic
		new ScanEntry(Blocks.PISTON, 		"hs.hydraulic");
		new ScanEntry(Blocks.STICKY_PISTON, "hs.hydraulic");
		
		//Menelaus Surface
		new ScanEntry(TerrainBlocks.cobblestone_m, "hs.menelaus.surface");
		new ScanEntry(TerrainBlocks.stone_m, "hs.menelaus.surface");
		new ScanEntry(TerrainBlocks.sand_m, "hs.menelaus.surface");
		new ScanEntry(TerrainBlocks.sandstone_m, "hs.menelaus.surface");
		new ScanEntry(TerrainBlocks.sandstone_chiseled_m, "hs.menelaus.surface");
		new ScanEntry(TerrainBlocks.sandstone_smooth_m, "hs.menelaus.surface");
		new ScanEntry(TerrainBlocks.dirt_m, "hs.menelaus.surface");
		new ScanEntry(TerrainBlocks.gravel_m, "hs.menelaus.surface");
		new ScanEntry(TerrainBlocks.crawler_hive, "hs.menelaus.surface");
		
		//Menelaus Mendelberry
		new ScanEntry(PlantBlocks.mendel_berry , "hs.menelaus.mendel");
		
		//Menelaus Raw Earth
		new ScanEntry(TerrainBlocks.dirt_m, "hs.menelaus.metalhaltig");
		new ScanEntry(TerrainBlocks.gravel_m, "hs.menelaus.metalhaltig");
		
		//Menelaus Crystal
		new ScanEntry(TerrainBlocks.crystal_glowtite , "hs.menelaus.crystals");
		new ScanEntry(TerrainBlocks.crystal_retium , "hs.menelaus.crystals");
		new ScanEntry(TerrainBlocks.sand_glowtite , "hs.menelaus.crystals");
		new ScanEntry(TerrainBlocks.sand_retium , "hs.menelaus.crystals");
		
		//Menelaus Mushroom	
		new ScanEntry(TerrainBlocks.log_mushroom , "hs.menelaus.tree");
		new ScanEntry(TerrainBlocks.leaves_mushroom , "hs.menelaus.tree");
		
		//Menelaus Mycel
		new ScanEntry(TerrainBlocks.huge_mycel, "hs.menelaus.mycel");
		new ScanEntry(TerrainBlocks.blueshroom, "hs.menelaus.mycel");
		new ScanEntry(TerrainBlocks.bubbleshroom, "hs.menelaus.mycel");
		new ScanEntry(TerrainBlocks.peakhead, "hs.menelaus.mycel");
		new ScanEntry(TerrainBlocks.hyticus, "hs.menelaus.mycel");
		
		//Menelaus Enity
		new ScanEntry(EntityGehuf.class, "hs.menelaus.entity");
		new ScanEntry(EntityCrawler.class, "hs.menelaus.entity");
		new ScanEntry(EntityWolba.class, "hs.menelaus.entity");
		new ScanEntry(EntityHeuler.class, "hs.menelaus.entity");
		
		//Tyros prismid hs.tyros.prismid
		new ScanEntry(TerrainBlocks.prismide, "hs.tyros.prismid");
		new ScanEntry(TerrainBlocks.prismide_stone, "hs.tyros.prismid");
		
		//Tyros hs.tyros.crystals
		new ScanEntry(TerrainBlocks.crystal_bioterium, "hs.tyros.crystals");
		new ScanEntry(TerrainBlocks.sand_bioterium, "hs.tyros.crystals");
		
		//Tyros hs.tyros.tree
		new ScanEntry(TerrainBlocks.log_tyros , "hs.tyros.tree");
		new ScanEntry(PlantBlocks.leaves_tyros , "hs.tyros.tree");
		//what about the palirie tree?
		
		//Tyros hs.tyros.topinambur
		new ScanEntry(PlantBlocks.topinambur, "hs.tyros.topinambur");
		
		//Tyros hs.tyros.entity
		new ScanEntry(CaveSpider.class, "hs.tyros.entity");
		
	}
	
	
	
	public static void register(EnumScanPosition pos, IScanPart entry)
	{
		scanList[pos.ordinal()].add(entry);
	}
	
	public static void doBlock(Level w, BlockPos pos, Player pl, BlockHitResult res)
	{
		if(!w.isClientSide)
		{
			for(ScanEntry se : list)
			{
				se.onBlock(w, pos, pl, res);
			}
			
			ExecutorService executor = AsyncTaskManager.getExecutor();
			Future<Component[]> chats = doBlock(w, pos, true, res, executor);
			Thread t = new Thread(() -> {
				MessageEScanner mes = new MessageEScanner(AsyncTaskManager.join(chats));
				FPPacketHandler.CHANNEL_FUTUREPACK.sendTo(mes, ((ServerPlayer) pl).connection.connection, NetworkDirection.PLAY_TO_CLIENT);
			});
			t.setDaemon(true);
			t.start();
		}
	}
	
	public static Future<Component[]> doBlock(Level w, BlockPos pos, boolean inGUI, BlockHitResult res, ExecutorService executor)
	{		
		ArrayList<Future<Collection<Component>>> list = new ArrayList<>();
		
		for(int i=0;i<scanList.length;i++)
		{
			for(IScanPart part : scanList[i])
			{
				Callable<Collection<Component>> asyncTexts = part.doBlockMulti(w, pos, inGUI, res);
				if(asyncTexts!=null)
				{
					list.add(executor.submit(asyncTexts));
				}
				else
				{
					throw new NullPointerException(part + " returned null instead of callable");
				}
			}
		}
		return executor.submit(mergeTexts(list));
	}
	
	private static Callable<Component[]> mergeTexts(ArrayList<Future<Collection<Component>>> list)
	{
		return () -> 
		{
			try
			{
				ArrayList<Component> texts = new ArrayList<>(list.size() * 2);
				for(Future<Collection<Component>> task : list)
				{
					Collection<Component> col = task.get();
					if(col!=null)
						texts.addAll(col);
				}
				texts.removeIf(p -> p == null);
				return texts.toArray(new Component[texts.size()]);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				ArrayList<Component> texts = new ArrayList<>();
				Throwable t = e;
				do
				{
					texts.add(new TextComponent(t.toString()));
					texts.add(new TextComponent("    " + t.getStackTrace()[0].toString()));
					if(t.getCause() == t)
						break;
					
					t = t.getCause();
				}
				while(t!=null);
				
				return texts.toArray(new Component[texts.size()]);
			}
		};
	}
	
	public static void doEntity(Level w, LivingEntity e, Player pl)
	{
		if(!w.isClientSide)
		{
			for(ScanEntry se : list)
			{
				se.onEntity(w, e, pl);
			}
//			for(ITextComponent ch : doEntity(w, e, false))
//			{
//				pl.sendMessage(ch);
//			}
			ExecutorService executor = AsyncTaskManager.getExecutor();
			Future<Component[]> chats = doEntity(w, e, true, executor);
			Thread t = new Thread(() -> 
			{
				MessageEScanner mes = new MessageEScanner(AsyncTaskManager.join(chats));
				FPPacketHandler.CHANNEL_FUTUREPACK.sendTo(mes, ((ServerPlayer) pl).connection.connection, NetworkDirection.PLAY_TO_CLIENT);
			});
			t.setDaemon(true);
			t.start();
		}
	}
	
	public static void openStart(Level w, Player pl)
	{
		if(!w.isClientSide)
		{
			List<Component> toDisplay = new ArrayList<Component>();
			
			Style blue_underlined = Style.EMPTY.withFont(Constants.unicode_font).withColor(ChatFormatting.DARK_AQUA).setUnderlined(true);
			
			Research r_get_stared = ResearchManager.getResearch("get_started");
			TextComponent msg = new TextComponent((ChatFormatting.DARK_GRAY) + "1: ");
			TranslatableComponent tool = new TranslatableComponent(r_get_stared.getTranslationKey());
			tool.setStyle(blue_underlined);
			Style open_research = Style.EMPTY.withFont(Constants.unicode_font).withClickEvent(new ClickEvent(Action.CHANGE_PAGE, "openresearch="+r_get_stared.getName()));
			msg.setStyle(open_research);
			msg.append(tool);
			toDisplay.add(msg);
			
			Research r_dungeon = ResearchManager.getResearch("story.tec_dungeon");
			msg = new TextComponent((ChatFormatting.DARK_GRAY) + "2: ");
			tool = new TranslatableComponent(r_dungeon.getTranslationKey());
			tool.setStyle(blue_underlined);
			open_research = Style.EMPTY.withFont(Constants.unicode_font).withClickEvent(new ClickEvent(Action.CHANGE_PAGE, "openresearch="+r_dungeon.getName()));
			msg.setStyle(open_research);
			msg.append(tool);
			toDisplay.add(msg);
			
			tool = new TranslatableComponent("futurepack.escanner.open_techtree");
			Style open_techtree = Style.EMPTY.withFont(Constants.unicode_font).withClickEvent(new ClickEvent(Action.CHANGE_PAGE, "opentechtree="+r_dungeon.getName()));
			tool.setStyle(open_techtree);
			toDisplay.add(tool);
			
			MessageEScanner mes = new MessageEScanner(toDisplay.toArray(new Component[toDisplay.size()]));
			FPPacketHandler.CHANNEL_FUTUREPACK.sendTo(mes, ((ServerPlayer) pl).connection.connection, NetworkDirection.PLAY_TO_CLIENT);
		}
	}
	
	public static Future<Component[]> doEntity(Level w, LivingEntity e, boolean inGUI, ExecutorService executor)
	{
		ArrayList<Future<Collection<Component>>> list = new ArrayList<>();
		for(int i=0;i<scanList.length;i++)
		{
			for(IScanPart part : scanList[i])
			{
				Callable<Collection<Component>> asyncTexts = part.doEntityMulti(w, e, inGUI);
				if(asyncTexts!=null)
				{
					list.add(executor.submit(asyncTexts));
				}
				else
				{
					throw new NullPointerException(part + " returned null instead of callable");
				}
			}
		}
		return executor.submit(mergeTexts(list));
	}
	
	private static class ScanEntry
	{
		final String reserach;
		
		Class<? extends Entity> entity;
		Block bl;
		Material m;
		TagKey<Block> ore;
		TagKey<Fluid> fluid;
		
		public ScanEntry(Class<? extends Entity> entity, String r) 
		{
			this(r);
			this.entity = entity;
		}
		
		public ScanEntry(Block b, String r) 
		{
			this(r);
			bl=b;
		}
		
		public ScanEntry(Material m, String r) 
		{
			this(r);
			this.m = m;
		}
		
		private ScanEntry(String r)
		{
			reserach=r;
			list.add(this);
		}
		
		public ScanEntry(TagKey<Fluid> fluidTag, String r, Object nullObj) 
		{
			this(r);
			fluid = fluidTag;
		}

		public ScanEntry(TagKey<Block> o, String r) 
		{
			this(r);
			this.ore = o;
		}

		public boolean onEntity(Level w, LivingEntity entity, Player user)
		{
			checkUnderground(user);
			
			if(this.entity!=null)
			{
				if(this.entity.isAssignableFrom(entity.getClass()))
				{
					add(user);	
					return true;
				}
			}
			return false;
		}
		
		public boolean onBlock(Level w, BlockPos pos, Player user, HitResult res)
		{
			checkUnderground(user);
			
			if(bl!=null)
			{
				BlockState state = w.getBlockState(pos);
				Block bl = state.getBlock();
				if(this.bl == bl)
				{
					add(user);	
					return true;
				}
			}
			if(m!=null)
			{
				BlockState state = w.getBlockState(pos);
				Block bl = state.getBlock();
				if(state.getMaterial() == m)
				{
					add(user);	
					return true;
				}
			}
			if(ore!=null)
			{
				BlockState state = w.getBlockState(pos);
				
				if(state.is(ore))
				{
					add(user);	
					return true;
				}
			}
			if(fluid!=null)
			{
				if(w.getFluidState(pos).is(fluid))
				{
					add(user);
					return true;
				}
			}
				
			
			return false;
		}
		
		private static void checkUnderground(Player pl)
		{
			if(pl.getY() < 30)
			{
				if(pl.getCommandSenderWorld().dimension().location().equals(Dimensions.MENELAUS_ID)) 
				{
					CustomPlayerData.getDataFromPlayer(pl).addResearch(ResearchManager.getResearch("hs.menelaus.caves"));
				}
				else if(pl.getCommandSenderWorld().dimension().location().equals(Dimensions.TYROS_ID))
				{
					CustomPlayerData.getDataFromPlayer(pl).addResearch(ResearchManager.getResearch("hs.tyros.caves"));
				}
			}
			else if(pl.getY() > 60)
			{
				if(pl.getCommandSenderWorld().dimension().location().equals(Dimensions.TYROS_ID))
				{
					CustomPlayerData.getDataFromPlayer(pl).addResearch(ResearchManager.getResearch("hs.tyros.surface"));
				}
			}
			
			if(pl.level.getBiome(pl.blockPosition()).is(new ResourceLocation(("futurepack:tyros_rockdesert"))))
			{
				CustomPlayerData.getDataFromPlayer(pl).addResearch(ResearchManager.getResearch("hs.tyros.deadlands"));
			}
			
		}
		
		private void add(Player pl)
		{
			CustomPlayerData.getDataFromPlayer(pl).addResearch(ResearchManager.getResearch(reserach));
		}
	}
}
