package futurepack.common.research;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Supplier;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import futurepack.api.Constants;
import futurepack.api.ItemPredicateBase;
import futurepack.api.interfaces.IGuiRenderable;
import futurepack.common.FPConfig;
import futurepack.common.FPLog;
import futurepack.common.conditions.ConditionRegistry;
import futurepack.common.gui.RenderableAspect;
import futurepack.common.gui.RenderableItem;
import futurepack.common.gui.escanner.ComponentRevision;
import futurepack.depend.api.EnumAspects;
import futurepack.depend.api.helper.HelperJSON;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.language.LanguageInfo;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.Resource;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.util.Tuple;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Blocks;

public class ResearchLoader
{
	public static final ResearchLoader instance = new ResearchLoader();
	/**Map: name of the Research , JsonObject with decription*/
	private HashMap<String, JsonObject> researchesMap;
	/**Hash of ItemStack , Research*/
	private Map<Item, Set<Research>> enables;
	private Gson gson = new Gson();
	private List<ReaderEntry> list = new ArrayList<ReaderEntry>();
	private IdentityHashMap<Research,String[]> withParents;
	
	class SimpleSupplierReader implements Supplier<Reader>
	{
		String filename;
		
		SimpleSupplierReader(String filename)
		{
			this.filename = filename;
		}
		
		@Override
		public Reader get() 
		{				
			InputStream in = ClassLoader.getSystemClassLoader().getResourceAsStream("futurepack/common/research/" + filename + ".json");
			if(in==null)
			{
				in = ResearchLoader.class.getResourceAsStream(filename + ".json");
			}
			return new InputStreamReader(in);
		}
	};
	
	public ResearchLoader()
	{
		registerResearchReader(Constants.MOD_ID, new SimpleSupplierReader("research_main"));
		registerResearchReader(Constants.MOD_ID, new SimpleSupplierReader("research_story"));
		registerResearchReader(Constants.MOD_ID, new SimpleSupplierReader("research_production"));
		registerResearchReader(Constants.MOD_ID, new SimpleSupplierReader("research_energy"));
		registerResearchReader(Constants.MOD_ID, new SimpleSupplierReader("research_logistic"));
		registerResearchReader(Constants.MOD_ID, new SimpleSupplierReader("research_chip"));
		registerResearchReader(Constants.MOD_ID, new SimpleSupplierReader("research_deco"));
		registerResearchReader(Constants.MOD_ID, new SimpleSupplierReader("research_space"));
		registerResearchReader(Constants.MOD_ID, new SimpleSupplierReader("research_tools"));
	}
	
	public synchronized void init()
	{
		FPLog.logger.info("Loading Researches");
		
		ComponentRevision.init();
		
		ResearchPage.init();
		ResearchManager.init();
		researchesMap = new HashMap<String, JsonObject>();

		if(list!=null)
		{
			withParents = new IdentityHashMap<Research, String[]>();	
			list.forEach(ReaderEntry::register);
			linkResearches();
			withParents.clear();
			withParents = null;
			calcEnableMap();
			FPLog.logger.info("added " + ResearchManager.getResearchCount() + " Researches");
			
			if(FPConfig.IS_RESEARCH_DEBUG.getAsBoolean())
			{
				ResearchManager.instance.checkResearchCorrectness();
			}
		}
	}
	
	
	public synchronized void registerResearchReader(String modID, Supplier<Reader> sup)
	{
		list.add(new ReaderEntry(modID, sup));
	}
	
	
	private void addResearchesFromReader(String modID, Reader read) throws ResearchParentNotFoundException
	{
		FPLog.logger.info("Now Loading Researches from " + read);
		
		JsonReader in = new JsonReader(read);
		JsonArray ja =  gson.fromJson(in, JsonArray.class);	
		
//		ProgressBar bar = StartupProgressManager.start("Loading Researches", ja.size());
		
		Iterator<JsonElement> iter = ja.iterator();
		while(iter.hasNext())
		{
			JsonObject ob = iter.next().getAsJsonObject();
//			bar.step(ob.get("id").getAsString());
			createResearchFromJSON(modID, ob, withParents);
		}
		try
		{
			in.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
		
//		bar.close();
	}
	
	private void linkResearches()
	{
//		ProgressBar bar = StartupProgressManager.start("Linking Researches", withParents.size());
		Iterator<Research> iter2 = withParents.keySet().iterator();
		Research parentless = null;
		try
		{			
			while(iter2.hasNext())
			{
				parentless = iter2.next();
//				bar.step(parentless.getName());
				
				String[] parents = withParents.get(parentless);
				int p=0;
				int g=0;
				for(String parent : parents)
				{
					if(parent.contains(":"))
					{
						g++;
					}
					else
					{
						p++;
					}
				}
				
				Research[] toFind = new Research[p];
				ResourceLocation[] toFind2 = new ResourceLocation[g];
				for(int i=0;i<parents.length;i++)
				{
					if(parents[i].contains(":"))
					{
						toFind2[i] = new ResourceLocation(parents[i]);
					}
					else
					{
						toFind[i] = ResearchManager.getResearch(parents[i]);
					}
					
				}
				parentless.setParents(toFind);
				parentless.setGrandparents(toFind2);
				FPLog.logger.debug("Completed "+parentless.toFullString());
			}		
		}
		catch(Exception e)
		{
			throw new ResearchParentNotFoundException(parentless, e);
		}
//		bar.close();
	}
	
	private void createResearchFromJSON(String modID, JsonObject ob, IdentityHashMap<Research, String[]> withParents)
	{
		if(!ConditionRegistry.checkCondition(ob))
			return;
		
		String name = ob.get("id").getAsString().toLowerCase();
		int x = ob.get("x").getAsInt();
		int y = ob.get("y").getAsInt();
		IGuiRenderable icon;
		if(ob.has("icon"))
		{
			icon = new RenderableItem(loadStackSingle(ob.get("icon")));
		}
		else
		{
			icon = new RenderableAspect(EnumAspects.valueOf(ob.get("picon").getAsString()));
		}	
		ResearchPage base = ResearchPage.base;
		
		if(ob.has("page"))
		{
			JsonPrimitive elm = ob.getAsJsonPrimitive("page");
			if(elm.isNumber())
			{
				base = ResearchPage.pages[elm.getAsInt()];
			}
			else if(elm.isString())
			{
				base = ResearchPage.getPageSave(elm.getAsString());
			}
		}
			
		Research r = ResearchManager.createResearch(new ResourceLocation(modID, name), x, y, icon, base);
		if(ob.has("need"))
		{
			JsonArray arr = ob.getAsJsonArray("need");
			int size = arr.size();
			List<ItemPredicateBase> it = new ArrayList<ItemPredicateBase>(size);
			for(int i=0;i<size;i++)
			{
				if(!arr.get(i).isJsonNull())
				{
					ItemPredicateBase p = HelperJSON.getItemPredicateFromJSON(arr.get(i));
					if(p.getRepresentItem() != null && !p.getRepresentItem().isEmpty())
						it.add(p);
				}
			}
			
			r.setNeeded(it.toArray(new ItemPredicateBase[it.size()]));
		}
		if(ob.has("enables"))
		{
			JsonArray arr = ob.getAsJsonArray("enables");
			List<ItemStack> list = HelperJSON.getItemFromJSON(arr, false);	
			ItemStack[] it = new ItemStack[list.size()];
			r.setEnabled(list.toArray(it));
		}
		if(ob.has("level"))
		{
			JsonPrimitive jp = ob.getAsJsonPrimitive("level");
			r.techLevel = jp.getAsInt();
		}
		if(ob.has("time"))
		{
			JsonPrimitive jp = ob.getAsJsonPrimitive("time");
			r.time = (int) (jp.getAsInt() * FPConfig.RESEARCH.time_factor.get());
		}
		if(ob.has("neon"))
		{
			JsonPrimitive jp = ob.getAsJsonPrimitive("neon");
			r.neonenergie = (int) (jp.getAsInt() * FPConfig.RESEARCH.neon_factor.get());
		}
		if(ob.has("support"))
		{
			JsonPrimitive jp = ob.getAsJsonPrimitive("support");
			r.support = (int) (jp.getAsInt() * FPConfig.RESEARCH.support_factor.get());
		}
		if(ob.has("expLvl"))
		{
			JsonPrimitive jp = ob.getAsJsonPrimitive("expLvl");
			r.expLvl = (int) (jp.getAsInt() * FPConfig.RESEARCH.xp_factor.get());
		}
		if(ob.has("aspects"))
		{
			JsonArray jr = ob.getAsJsonArray("aspects");
			if(!jr.isJsonNull())
			{
				r.aspects = EnumAspects.getAspects(jr);
			}
			
		}
		if(ob.has("visibilitylevel"))
		{
			JsonPrimitive jp = ob.getAsJsonPrimitive("visibilitylevel");
			r.visibilityLevel = jp.getAsInt();
		}
		if(ob.has("rewards"))
		{
			JsonArray arr = ob.getAsJsonArray("rewards");
			List<ItemStack> list = HelperJSON.getItemFromJSON(arr, false);	
			ItemStack[] it = new ItemStack[list.size()];
			r.setRewards(list.toArray(it));
		}
		
		FPLog.logger.debug("Add Research "+r);
		
		if(ob.has("parents"))
		{
			JsonArray ar = ob.getAsJsonArray("parents");
			if(ar.size()>0)
			{
				String[] s = new String[ar.size()];
				for(int i=0;i<ar.size();i++)
				{
					s[i] = ar.get(i).getAsString().toLowerCase();
				}
				withParents.put(r, s);
			}
		}
	}
	
	/*
	 * Achievment Map "futurepack:research.json"
	 * [
	 *   {
	 *     id:"test",
	 *     x:0,
	 *     y:0,
	 *     "parents":["achievement.mineWood"],
	 *     icon:{id:"minecraft:dirt",Damage:0,Count:1,tag:{}},
	 *     enables:[{}],
	 *     need:{id:minecraft:cobblestone,Damage:0,Count:64},
	 *     level:1,
	 *     time:20
	 *   }
	 * ]
	 * 
	 * 
	 * in lang file "futurepack:lang/en_US.lang":
	 * research.test=Test in English
	 * 
	 * 
	 * Achievment file "futurepack:lang/test.json":
	 * {
	 *   "en_US":["Test Text","New Paragraph"]
	 * }
	 * 
	 */
	
	/**
	 * @param researchId
	 * @return Tuple<researchId, domain>
	 */
	public static Tuple<String, String> getResearchAndDomain(String researchId)
	{
		researchId = researchId.toLowerCase();
		int i = researchId.indexOf(':');
		String domain = null;
		if(i>=0)
		{
			String[] ss = researchId.split(":");
			domain  = ss[0];
			researchId = ss[1];
		} 
		else
		{
			domain = ResearchManager.getResearch(researchId).getDomain();
		}
		
		return new Tuple<String, String>(researchId, domain);
	}
	
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	public JsonArray getLocalisated(String researchId)
	{
		Tuple<String, String> r = getResearchAndDomain(researchId);
		return getLocalisated(r.getB(), r.getA());
	}
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	public JsonArray getLocalisated(String domain, String o)
	{
		return getLocalisated(domain, o, Minecraft.getInstance().getLanguageManager().getSelected());
	}
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	public JsonArray getLocalisated(String domain, String o, LanguageInfo lang)
	{	
		try
		{
			JsonObject jo = null;
			if(researchesMap.containsKey(o))
			{
				jo = researchesMap.get(o);
				
				if(jo.has(lang.getCode()) || loadJson(domain, o, lang.getCode()))
				{
					return jo.getAsJsonArray(lang.getCode());
				}
				else if(jo.has("en_us"))
				{
					return jo.getAsJsonArray("en_us");
				}
				//remove this if mod finished
				else if(jo.has("de_de"))
				{
					return jo.getAsJsonArray("de_de");
				}
			}
			else
			{
				try
				{
					//Main loading -- from loacalization folder
					researchesMap.put(o, new JsonObject());
					if(!loadJson(domain, o, lang.getCode()))
					{
						//Fallback loader1 - en_US
						if(lang.getCode().equalsIgnoreCase("en_us") || !loadJson(domain, o, "en_us"))
						{
							//Fallback loader2 - de_DE -- remove this if mod finished
							if(lang.getCode().equalsIgnoreCase("de_de") || !loadJson(domain, o, "de_de"))
							{
								//Fallback loader3 -- remove this if mod finished
								ResourceLocation loc = new ResourceLocation(Constants.MOD_ID, "lang/" + o + ".json");			
								jo = ResearchLoader.loadResource(loc, JsonObject.class);
								if(jo!=null)
								{
									researchesMap.put(o, jo);
								}
							}
						}
					}

					return getLocalisated(domain, o);
				} 
				catch (IOException e) 
				{
					FPLog.logger.error(e);
				}	
			}
			}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		JsonArray arr = new JsonArray();
		arr.add(new JsonPrimitive("'" + o + "' not found, report this to the mod author."));
		return arr;
	}
	
	private boolean loadJson(String domain, String name, String lang)
	{
		try{
			ResourceLocation loc = new ResourceLocation(domain, "lang/" + lang + "/" + name + ".json");	//researches from other mods: need to search at their resource location
			JsonArray ja = ResearchLoader.loadResource(loc, JsonArray.class);
			if(ja!=null)
			{
				JsonObject jo = researchesMap.get(name);
				jo.add(lang, ja);
				return true;
			}
		}
		catch (Exception e) 
		{
			FPLog.logger.error(e);
		}
		return false;
	}
	
	private static ItemStack loadStackSingle(JsonElement elm)
	{
		List<ItemStack> list = HelperJSON.getItemFromJSON(elm, false);
		return list.isEmpty() ? new ItemStack(Blocks.DIRT) : list.get(0);
	}

	/*
	 * Achievment Map "futurepack:research.json"
	 * [
	 *   {
	 *     id:"test",
	 *     x:0,
	 *     y:0,
	 *     "parents":["achievement.mineWood"],
	 *     icon:{id:"minecraft:dirt",Damage:0,Count:1,tag:{}},
	 *     enables:[{}],
	 *     need:{id:minecraft:cobblestone,Damage:0,Count:64},
	 *     level:1,
	 *     time:20
	 *   }
	 * ]
	 * 
	 * 
	 * in lang file "futurepack:lang/en_US.lang":
	 * research.test=Test in English
	 * 
	 * 
	 * Achievment file "futurepack:lang/test.json":
	 * {
	 *   "en_US":["Test Text","New Paragraph"]
	 * }
	 * 
	 */
	
	//r can look like "futurepack:lang/en_US/basics.json"
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	protected static <T> T loadResource(ResourceLocation r, Class<T> clazz) throws IOException
	{
		ResourceManager man = Minecraft.getInstance().getResourceManager();
		InputStream stream = null;
		try
		{
			Resource res = man.getResource(r);		
			stream = res.getInputStream();
		}
		catch(FileNotFoundException e)
		{
			FPLog.logger.error(e);
			
			File f = new File("./mods/futurepack/" + r.getPath());
			stream = new FileInputStream(f);
		}
		
		Gson gson = new Gson();
		JsonReader in = new JsonReader(new InputStreamReader(stream, StandardCharsets.UTF_8));
		T t = gson.fromJson(in, clazz);
		
		in.close();
		
		return t;
	}
	
	private void run()
	{
		try
		{
			//Only Client
			researchesMap = new HashMap<String, JsonObject>();
		}
		catch(Throwable e){}
		
		for(int i=0;i<ResearchManager.getResearchCount();i++)
		{
			Research r = ResearchManager.getById(i);
			if(r.getEnables()!=null && r.getEnables().length > 0)
			{
				for(ItemStack it : r.getEnables())
				{
					if(it!=null)
					{
						Set<Research> set = enables.computeIfAbsent(it.getItem(), hash -> new TreeSet<Research>());
						set.add(r);
						if(set.size() > 1)
						{
							FPLog.logger.warn("Item %s needs multiple researchs to be unlocked: %s", it, set.toString());
						}
					}
				}
			}
			//extractLinks(r);
		}
	}
	
	private void calcEnableMap()
	{
		enables = new IdentityHashMap<Item, Set<Research>>();
		//new Thread(this,"Research Loader").start();
		run();
	}
	
	public static Set<Research> getReqiredResearch(ItemStack it)
	{
		if(instance.enables == null)
			return Collections.singleton(ResearchManager.ERROR);
		
		Set<Research> r = instance.enables.get(it.getItem());
		return r;
	}
	
	@SuppressWarnings("unused")
	private void extractLinks(Research r)
	{
		Set<LanguageInfo> allL = Minecraft.getInstance().getLanguageManager().getLanguages();
		
		for(LanguageInfo l : allL)
		{
			getLocalisated(r.getDomain(), r.getName(), l);
		}
		
		JsonObject jo = researchesMap.get(r.getName());
		if(jo!=null)
		{
			String longest = null;
			int length=0;
			HashMap<String, List<JsonObject>> map = new HashMap<>(jo.size());
			for(Entry<String, JsonElement> e : jo.entrySet())
			{
				JsonArray arr = e.getValue().getAsJsonArray();
				List<JsonObject> l = new ArrayList<>(arr.size());
				for(JsonElement elm : arr)
				{
					if(!elm.isJsonObject())
						continue;
					JsonObject joo = elm.getAsJsonObject();
					if(joo.size()==0)
						continue;		
					if(!joo.has("type"))
						continue;
					
					l.add(elm.getAsJsonObject());
				}
				
				if(!l.isEmpty())
				{
					map.put(e.getKey(), l);
					if(l.size()>length || longest==null)
					{
						length=l.size();
						longest = e.getKey();
					}
				}	
			}
			Gson pretty = new GsonBuilder().setPrettyPrinting().create();
			if(!map.isEmpty())
			{
				List<JsonObject> l = map.get(longest);
				int k=0;
				for(JsonObject parent : l)
				{
					k++;
					List<String> equals = new ArrayList<>();
					for(Entry<String, List<JsonObject>> e : map.entrySet())
					{
						if(longest.equals(e.getKey()))
						{
							equals.add(e.getKey());
							continue;
						}
						
						for(JsonObject same : e.getValue())
						{
							if(parent.equals(same))
							{
								equals.add(e.getKey());
							}
						}
					}
					if(equals.size()>1)
					{
						//creating the part
						File dir = new File("./lang/part/");
						dir.mkdirs();
						String filename = parent.get("type").getAsString()+"_"+r.getName();
						if(k>1)
							filename+=k;
						filename += ".json";
						filename = filename.toLowerCase();
						
						File partname = new File(dir, filename);
						JsonArray container =  new JsonArray();
						container.add(parent);
						
						try
						{
							JsonWriter writer = pretty.newJsonWriter(new OutputStreamWriter(new FileOutputStream(partname), StandardCharsets.UTF_8));
							pretty.toJson(container, writer);
							writer.close();
						} catch (IOException e1)
						{
							e1.printStackTrace();
						}
						
						//link the researches to the part
						JsonObject link = new JsonObject();
						link.addProperty("link", "futurepack:lang/part/" + filename);
						
						for(String lang : equals)
						{
							JsonArray baseResearch = jo.getAsJsonArray(lang);
							for(int i=0;i<baseResearch.size();i++)
							{
								if(baseResearch.get(i).isJsonObject())
								{
									JsonObject obj = baseResearch.get(i).getAsJsonObject();
									if(parent.equals(obj))
									{
										baseResearch.set(i, link); //link is inserted
										break;
									}
								}
							}
							
							try
							{
								dir = new File("./lang/"+lang+"/");
								dir.mkdir();
								File research = new File(dir, r.getName()+".json");
								JsonWriter writer = pretty.newJsonWriter(new OutputStreamWriter(new FileOutputStream(research), StandardCharsets.UTF_8));
								pretty.toJson(baseResearch, writer);
								writer.close();
							}
							catch (IOException e1)
							{
								e1.printStackTrace();
							}
						}
					}
				}
			}
		}
	}
	
	private class ReaderEntry
	{
		final String domain;
		final Supplier<Reader> reader;
		
		public ReaderEntry(String domain, Supplier<Reader> reader) 
		{
			super();
			this.domain = domain;
			this.reader = reader;
		}
		
		public void register()
		{
			addResearchesFromReader(domain, reader.get());
		}
	}
}
