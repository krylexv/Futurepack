package futurepack.common.research;

import futurepack.api.interfaces.IScanPart;
import futurepack.common.FuturepackTags;
import net.minecraft.ChatFormatting;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.BlockHitResult;

public class ScanPartMining implements IScanPart
{

	@Override
	public Component doBlock(Level w, BlockPos pos, boolean inGUI, BlockHitResult res) {
		BlockState state = w.getBlockState(pos);
		
		TextComponent msg = new TextComponent("");
		if(state.requiresCorrectToolForDrops())
		{
			TranslatableComponent tool = new TranslatableComponent("harvest.tool.needed");
			tool.setStyle((Style.EMPTY).withColor((inGUI ? ChatFormatting.DARK_GRAY : ChatFormatting.GRAY)));
			msg.append(tool);
		}
		
		TagKey<Block>[] mienables = new TagKey[]{FuturepackTags.TOOLTYPE_AXE, FuturepackTags.TOOLTYPE_PICKAXE, FuturepackTags.TOOLTYPE_SHOVEL, FuturepackTags.TOOLTYPE_HOE};
		String[] names = {"axe", "pickaxe", "shovel", "hoe"};
		
		TagKey<Block>[] materials = new TagKey[]{FuturepackTags.LEVEL_WOOD, FuturepackTags.LEVEL_GOLD, FuturepackTags.LEVEL_STONE, FuturepackTags.LEVEL_IRON, FuturepackTags.LEVEL_DIAMOND, FuturepackTags.LEVEL_NETHERITE};
		String[] namesMat = {"wood", "gold", "stone", "iron", "diamond", "nethrite"};
		
		for(int i=0;i<mienables.length;i++)
		{
			if(state.is(mienables[i]))
			{
				
				TranslatableComponent tool = new TranslatableComponent("harvest.tool." + names[i]);
				tool.setStyle((Style.EMPTY).withColor((inGUI ? ChatFormatting.DARK_GRAY : ChatFormatting.GRAY)));
				msg.append(tool);	
			}
		}
		
		for(int i=0;i<materials.length;i++)
		{
			if(state.is(materials[i]))
			{
				
				TranslatableComponent tool = new TranslatableComponent("harvest.tool.material." + namesMat[i]);
				tool.setStyle((Style.EMPTY).withColor((inGUI ? ChatFormatting.DARK_GRAY : ChatFormatting.GRAY)));
				msg.append(tool);	
			}
		}
		
		
		if(state.getMaterial() == Material.METAL)
		{
			TranslatableComponent tool = new TranslatableComponent("material.metall");
			tool.setStyle((Style.EMPTY).withColor((inGUI ? ChatFormatting.DARK_GRAY : ChatFormatting.GRAY)));
			msg.append("\n");
			msg.append(tool);
		}
		
		return msg;
	}

	@Override
	public Component doEntity(Level w, LivingEntity e, boolean inGUI) {
		return null;
	}

}
