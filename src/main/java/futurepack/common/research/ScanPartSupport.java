package futurepack.common.research;

import futurepack.api.capabilities.CapabilitySupport;
import futurepack.api.capabilities.ISupportStorage;
import futurepack.api.interfaces.IScanPart;
import net.minecraft.ChatFormatting;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.common.util.LazyOptional;

public class ScanPartSupport implements IScanPart
{

	@Override
	public Component doBlock(Level w, BlockPos pos, boolean inGUI, BlockHitResult res)
	{
		BlockEntity[] t = {null};
		w.getServer().submit(() -> {
			t[0] = w.getBlockEntity(pos);
		}).join();
		BlockEntity tile = t[0];
		
		if(tile!=null)
		{
			ISupportStorage store = null;
			LazyOptional<ISupportStorage> opt = tile.getCapability(CapabilitySupport.cap_SUPPORT, res.getDirection());
			
			if(opt==null)
			{
				throw new NullPointerException("TileEntity " + tile + " returned null for SUPPORT capability!");
			}
			
			if(!opt.isPresent())
			{
				opt = tile.getCapability(CapabilitySupport.cap_SUPPORT, res.getDirection());
				if(!opt.isPresent())
				{
					return null;
				}
			}
			store = opt.orElseThrow(NullPointerException::new);
			if(store != null)
			{
				float i = store.get();
				return new TextComponent( (inGUI ? ChatFormatting.GOLD : ChatFormatting.YELLOW)  + "Support: " + ChatFormatting.RESET + i + " SP");
			}
			
		}
		return null;
	}

	@Override
	public Component doEntity(Level w, LivingEntity e, boolean inGUI)
	{
		return null;
	}

}
