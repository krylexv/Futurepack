package futurepack.common;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.StreamSupport;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import futurepack.api.Constants;
import futurepack.common.block.terrain.TerrainBlocks;
import futurepack.common.entity.CapabilityPlayerData;
import futurepack.common.item.misc.MiscItems;
import futurepack.common.item.tools.ItemMindControllMiningHelmet;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.item.tools.compositearmor.CompositeArmorInventory;
import futurepack.common.item.tools.compositearmor.CompositeArmorPart;
import futurepack.common.potions.FPPotions;
import futurepack.common.sync.KeyManager.EnumKeyTypes;
import futurepack.common.sync.KeyManager.EventFuturepackKey;
import futurepack.depend.api.helper.HelperChunks;
import futurepack.depend.api.helper.HelperItems;
import futurepack.world.dimensions.Dimensions;
import futurepack.world.dimensions.atmosphere.FullBlockCache;
import futurepack.world.scanning.FPChunkScanner;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.AttributeModifier.Operation;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.chunk.ChunkStatus;
import net.minecraft.world.level.chunk.EmptyLevelChunk;
import net.minecraft.world.level.chunk.ImposterProtoChunk;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraft.world.level.levelgen.Heightmap.Types;
import net.minecraft.world.level.material.FluidState;
import net.minecraftforge.common.ForgeMod;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.ItemAttributeModifierEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.TickEvent.Phase;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.player.CriticalHitEvent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerWakeUpEvent;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.event.world.ChunkEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.eventbus.api.Event.Result;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class FuturepackEventHandler
{
	public static final FuturepackEventHandler INSTANCE = new FuturepackEventHandler();
	private static final UUID MICRO_GRAVITY_ID = UUID.fromString("A1B69F2A-2F7C-31EF-9022-7C4E7D5E2ABA");
	private static final AttributeModifier MICRO_GRAVITY = new AttributeModifier(MICRO_GRAVITY_ID, "MICRO GRAVITY", -0.075, AttributeModifier.Operation.ADDITION); // Add -0.07 to 0.08 so we get the vanilla default of 0.01

	private byte cooldown = 120; // 120 ticks / 20 = 60 seconds / 60 = 1 minutes
	private ArrayList<WeakReference<Mob>> mindControlled = new ArrayList<WeakReference<Mob>>();


	@SubscribeEvent
	public void onCriticalHit(CriticalHitEvent event)
	{
		if(event.getPlayer().getMainHandItem().getItem() != ToolItems.sword_neon)
		{
			return;
		}
		event.setDamageModifier(2F);

		boolean critical = event.isVanillaCritical();
		if(!critical)
		{
			critical = !event.getPlayer().isOnGround() && !event.getPlayer().isInWater() && !event.getPlayer().isPassenger();
			if(!critical)
			{
				critical = event.getPlayer().level.random.nextInt(20) == 0;
			}
			if(critical)
			{
				event.setResult(Result.ALLOW);
			}
		}

		if(critical)
		{
			if(event.getTarget() instanceof LivingEntity)
			{
				LivingEntity liv = (LivingEntity) event.getTarget();
				liv.addEffect(new MobEffectInstance(FPPotions.PARALYZED, 20*5, 0));
			}
		}
	}

	public static boolean isModdedDimension(Level type)
	{
		if(type== null)
			return false;
		else if(type.dimension().location() == null)
			return false;

		return Constants.MOD_ID.equalsIgnoreCase(type.dimension().location().getNamespace());
	}

	private List<ServerPlayer> players = new ArrayList<ServerPlayer>();

	@SubscribeEvent
	public void onDeath(LivingDeathEvent event)
	{
		if(event.getEntity() instanceof ServerPlayer && !event.getEntity().level.isClientSide && (isModdedDimension(event.getEntity().getCommandSenderWorld())))
		{
			players.add((ServerPlayer) event.getEntityLiving());
		}
	}


	@SubscribeEvent
	public void onJoin(final EntityJoinWorldEvent event)
	{
		if(event.getEntity() instanceof ServerPlayer && !event.getEntity().level.isClientSide)
		{
			final ServerPlayer pl = (ServerPlayer) event.getEntity();

			if(players.contains(pl))
			{
				if(isModdedDimension(pl.getCommandSenderWorld()))
				{
					players.remove(pl);
					//TODO: player does not respawn in spaceship

//					final BlockPos bedLocation = pl.getBedLocation();
//					final boolean foced = pl.isSpawnForced();
//					if(foced)
//					{
//						Thread t = new Thread(new Runnable()
//						{
//							@Override
//							public void run()
//							{
//								try {
//									Thread.sleep(20);
//								} catch (InterruptedException e) {
//									e.printStackTrace();
//								}
//								BlockPos c = bedLocation;
//								BlockPos h = pl.world.getHeight(Type.MOTION_BLOCKING, c);
//
//								if(c.getY() - h.getY() > 10)
//								{
//									c = h;
//								}
//
//								if(c!=null)
//									pl.setPositionAndUpdate(c.getX()+0.5,c.getY()+0.5,c.getZ()+0.5);
//								FPLog.logger.debug("Respawn Player %s at custom position",pl);
//							}
//						}, "Player Respawn Helper " + pl.getDisplayName().getString());
//						t.start();
//					}
				}
			}



		}
		else if(event.getEntity() instanceof Player && event.getEntity().level.isClientSide)
		{
			if(isModdedDimension(event.getWorld()))
			{
				if(event.getEntity().level.isClientSide)
					return;

				Thread t = new Thread(new Runnable()
				{
					@Override
					public void run()
					{
						if(event.getEntity() instanceof LocalPlayer)
						{
							try {
								Thread.sleep(200);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}

	//						World w = event.getEntity().world;

							int logReducer = 20;
							int waiting = 0;

							while(((LocalPlayer)event.getEntity()).xOld == 8.5 && ((LocalPlayer)event.getEntity()).xOld == 8.5)
							{
								if(logReducer++ >= 20)
								{
									FPLog.logger.debug("Waiting for sync ...");
									logReducer = 0 - waiting*20;
									waiting++;
								}
								try {
									Thread.sleep(200);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}

							LevelChunk c = event.getEntity().level.getChunkAt(event.getEntity().blockPosition());
							boolean output = false;

							double lx=event.getEntity().getX();
							double lz=event.getEntity().getZ();
							double ly=event.getEntity().getY();
							while(c instanceof EmptyLevelChunk)
							{
								c = event.getEntity().level.getChunkAt(event.getEntity().blockPosition());
								try {
									Thread.sleep(50);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}
							try {
								Thread.sleep(200);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}

							double ly2=event.getEntity().getY();

							if(ly2 < ly)
							{
								if(!output)
								{
									FPLog.logger.debug("Try to help Entity %s becasue the chunk is empty (was at %s,%s,%s)",event.getEntity(),lx,ly,lz);
									output = true;
								}
								event.getEntity().setPos(lx, ly, lz);
							}
						}
					}
				});
				//t.start();
			}
		}
	}



	@SubscribeEvent
	public void onPlayerWakeUp(PlayerWakeUpEvent event)
	{
		Level w = event.getPlayer().level;
		if(isModdedDimension(w) && !w.isClientSide)
		{
			System.out.println(w.getDayTime());
			long time =  w.getDayTime() + 24000L;
			((ServerLevel)w).setDayTime(time);
			System.out.println(w.getDayTime());
		}
	}

//	@SubscribeEvent
//	public void onFuturepackKeyPressed(EventFuturepackKey key)
//	{
//		CompositeArmorInventory.onCompositeArmorOpened(key);
//	}

	@SubscribeEvent
	public void onChunkLoad(ChunkEvent.Load event)
	{
		if(event.getChunk().getStatus().getChunkType() == ChunkStatus.ChunkType.LEVELCHUNK)
		{
			if(event.getWorld()!=null)
			{
				if(event.getWorld().isClientSide())
					return;

				FPChunkScanner.INSTANCE.scanChunk(event.getChunk());
				return;
			}
			else
			{
				ChunkAccess c = event.getChunk();
				if(c instanceof ImposterProtoChunk)
				{
					ImposterProtoChunk wr = (ImposterProtoChunk)c;
					LevelChunk base = wr.getWrapped();

					if(base.getLevel()!=null && !base.getLevel().isClientSide)
					{
						FPChunkScanner.INSTANCE.scanChunk(base);
						return;
					}
				}
				else
				{
					throw new IllegalArgumentException("Unknown chunk class with null world " + c.getClass());
				}
			}

		}
		else
		{
			return;
		}

	}

	public void spawnFireflies(Level w, BlockPos pos)
	{
		int range = 12;
		for(int x = pos.getX()-range;x<pos.getX()+range;x++)
		{
			for(int z = pos.getZ()-range;z<pos.getZ()+range;z++)
			{
				if(w.random.nextInt(256)==0)
				{
					BlockPos xyz = w.getHeightmapPos(Types.WORLD_SURFACE, new BlockPos(x,0,z));
					xyz = xyz.above();
					if(w.isEmptyBlock(xyz))
					{
						BlockState state = TerrainBlocks.fireflies.defaultBlockState();
						w.setBlockAndUpdate(xyz, state);
					}
				}
			}
		}
	}



	@SubscribeEvent
	public void onWorldTick(TickEvent.WorldTickEvent event)
	{
		Level w = event.world;
		if(!w.isClientSide && (w.dimension().location().equals(Dimensions.MENELAUS_ID) || w.dimension().location().equals(Dimensions.TYROS_ID)) && event.phase==Phase.START)
		{
			long time = w.getDayTime();
			if(time>13000 && w.random.nextInt(120)==0)
			{
				List<ServerPlayer> pls = ((ServerLevel)w).players();
				for(Player pl : pls)
				{
					BlockPos pos = pl.blockPosition();
					Biome b = w.getBiome(pos).value();

					ResourceLocation res = HelperItems.getRegistryName(b);
					if(res!= null && !res.getPath().contains("rockdesert"))
					{
						pos = pos.relative(w.random.nextBoolean() ? Direction.NORTH : Direction.SOUTH, w.random.nextInt(20)+ 20);
						pos = pos.relative(w.random.nextBoolean() ? Direction.EAST : Direction.WEST, w.random.nextInt(20)+ 20);
						spawnFireflies(w, pos);
					}
				}
			}
		}
		if(!w.isClientSide && (w.dimension().location().equals(Dimensions.ASTEROID_BELT_ID)))
		{
			StreamSupport.stream(((ServerLevel)event.world).getEntities().getAll().spliterator(), false)
				.filter(e -> e.getY() < event.world.getMinBuildHeight())
				.forEach(e -> e.teleportTo(e.getX(), 350, e.getZ()));

		}
		if(!w.isClientSide)
		{
			FullBlockCache.onWorldTick(w);
		}

	}

	@SubscribeEvent
	public void onServerTick(TickEvent.ServerTickEvent event)
	{
		if(event.phase == Phase.END)
		{
			if(--cooldown < 0 )
			{
				cooldown = 120;
			}

			for(int i=0;i<mindControlled.size();i++)
			{
				WeakReference<Mob> ref = mindControlled.get(i);
				if(ref == null)
				{
					mindControlled.remove(i--);
				}
				else
				{
					Mob mob = ref.get();
					if(mob == null || !mob.isAlive())
					{
						mindControlled.remove(i--);
					}
					else if(!ItemMindControllMiningHelmet.onMobArmorTick(mob))
					{
							mindControlled.remove(i--);
					}
				}
			}
		}
	}


	@SubscribeEvent
	public void onPlayerJoin(PlayerEvent.PlayerLoggedInEvent event)
	{
		if(event.getPlayer() instanceof ServerPlayer)
		{
//			FPPacketHandler.CHANNEL_FUTUREPACK.sendTo(new MessageMagnetisemUpdate(FPConfig.helmets, FPConfig.chestplates, FPConfig.leggings, FPConfig.boots), (EntityPlayerMP) event.getPlayer());

		      Player player = event.getPlayer();
//		      player.getBrain().hasMemory(p_218191_1_)
		      CompoundTag entityData = CapabilityPlayerData.getPlayerdata(player);
		      if(FPConfig.SERVER.isSpawnNoteEnabled.get() && !entityData.getBoolean("knownPlayer"))
		      {
		    	  entityData.putBoolean("knownPlayer", true);
		    	  player.getInventory().add(new ItemStack(MiscItems.spawn_note));
		      }
//		      player.getEntityData().put(Constants.MOD_ID, entityData);
		}
	}

	@SubscribeEvent
	public void onBlockUpdate(BlockEvent.NeighborNotifyEvent event)
	{
		if(event.getWorld() instanceof Level)
		{
			FPSelectorHelper.onBlockUpdate((Level)event.getWorld(), event.getPos());
			FullBlockCache.notifyBlockChange(event.getPos(), (Level)event.getWorld());
		}

	}

	@SubscribeEvent
	public void onBlockBreak(BlockEvent.BreakEvent event)
	{
		if(event.getWorld() instanceof Level)
		{
			FullBlockCache.notifyBlockChange(event.getPos(), (Level)event.getWorld());
			FPSelectorHelper.onBlockUpdate((Level)event.getWorld(), event.getPos());
		}
	}

	@SubscribeEvent
	public void onBlockPlace(BlockEvent.EntityPlaceEvent event)
	{
		if(event.getWorld() instanceof Level)
		{
			FPSelectorHelper.onBlockUpdate((Level)event.getWorld(), event.getPos());
			FullBlockCache.notifyBlockChange(event.getPos(), (Level)event.getWorld());
		}
	}

	@SubscribeEvent
	public void onBlockPlaceMulti(BlockEvent.EntityMultiPlaceEvent event)
	{
		if(event.getWorld() instanceof Level)
		{
			event.getReplacedBlockSnapshots().forEach(s -> FullBlockCache.notifyBlockChange(s.getPos(), (Level)event.getWorld()));
			FPSelectorHelper.onBlockUpdate((Level)event.getWorld(), event.getReplacedBlockSnapshots());
		}
	}

	@SubscribeEvent
	public void onWorldLoad(final WorldEvent.Load event)
	{
		if(event.getWorld() instanceof ServerLevel)
		{
			HelperChunks.getTicketKeeper((ServerLevel) event.getWorld());
		}
	}

	@SubscribeEvent
	public void onLivingUpdate(LivingEvent.LivingUpdateEvent event)
	{
		final LivingEntity liv = event.getEntityLiving();

		if(liv.isInWater())
		{
			BlockPos pos = liv.blockPosition();
			FluidState state = liv.getCommandSenderWorld().getFluidState(pos);
			if(state.is(FuturepackTags.PARALYZING))
			{
				liv.addEffect(new MobEffectInstance(FPPotions.PARALYZED, 20));
			}
		}

		if(!event.getEntity().getCommandSenderWorld().isClientSide)
		{
			boolean slowFalling = (liv.getCommandSenderWorld().dimension().location().equals(Dimensions.ASTEROID_BELT_ID));



			// 0.01 = 0.08 * (1+x)
			// 0.01/0.08 = 1+x
			// (0.01/0.08)-1 = x

			AttributeModifier GRAVITY = new AttributeModifier(MICRO_GRAVITY_ID, "MICRO GRAVITY", (0.01/0.08)-1, AttributeModifier.Operation.MULTIPLY_TOTAL);

			if(liv.getAttribute(ForgeMod.ENTITY_GRAVITY.get()).hasModifier(GRAVITY) && !slowFalling)
			{
				liv.getAttribute(ForgeMod.ENTITY_GRAVITY.get()).removeModifier(GRAVITY);
			}
			if(slowFalling && !liv.getAttribute(ForgeMod.ENTITY_GRAVITY.get()).hasModifier(GRAVITY))
			{
				liv.getAttribute(ForgeMod.ENTITY_GRAVITY.get()).addTransientModifier(GRAVITY);
			}
			if(slowFalling)
			{
				liv.fallDistance = 0F;
			}
		}

		if(cooldown == 0)
		{
			if(liv instanceof Mob)
			{
				for(ItemStack it : liv.getArmorSlots())
				{
					if(!it.isEmpty() && it.is(FuturepackTags.MINING_MINDCONTROL))
					{
						mindControlled.add(new WeakReference<Mob>((Mob) liv));
						break;
					}
				}


			}

		}
	}


	@SubscribeEvent
	public void onBiomeLoad(BiomeLoadingEvent event)
	{
		WorldGenRegistry.post(event);
	}

	@SubscribeEvent
	public void onFpKeyPress(EventFuturepackKey event)
	{
		if(event.type == EnumKeyTypes.ALL_BUTTONS)
		{
			//this is handled client side
//			FPGuiHandler.GENERIC_CHEST.openGui(event., entity);
		}
	}

	@SubscribeEvent
	public void onItemAttributeModifierEvent(ItemAttributeModifierEvent event)
	{
		if(event.getItemStack().getTagElement(CompositeArmorPart.NBT_TAG_NAME) != null)
		{
			Multimap<Attribute, AttributeModifier> mmap = HashMultimap.create();

			CompositeArmorPart part = CompositeArmorPart.getInventory(event.getItemStack());
			if(part!=null && event.getSlotType() == part.type)
			{
				mmap.putAll(part.getAttributeModifiers());
			}


			if(mmap.isEmpty())
				return;

			mmap.forEach(event::addModifier);


			Attribute[] attributes = event.getModifiers().keySet().toArray(new Attribute[event.getModifiers().keySet().size()]);
			for(Attribute attr : attributes)
			{
				AttributeModifier mod = event.getModifiers().get(attr).iterator().next();;

				UUID armorUuid = mod.getId();
				double[] amount = new double[] {0};
				ArrayList<AttributeModifier> doubled = new ArrayList<>();

				event.getModifiers().get(attr).stream().filter(m -> m.getOperation() == Operation.ADDITION).forEach(m -> {
					amount[0] += m.getAmount();
					doubled.add(m);
				});

				doubled.forEach(m -> {
					event.removeModifier(attr, m);
				});

				event.addModifier(attr, new AttributeModifier(armorUuid, mod.getName(), amount[0], Operation.ADDITION));
			}
		}
	}

	@SubscribeEvent
	public void onPlayerTick(TickEvent.PlayerTickEvent event)
	{
		for(var it : event.player.getInventory().armor)
		{
			if(it.getTagElement(CompositeArmorPart.NBT_TAG_NAME)!=null)
			{
				CompositeArmorInventory arm = new CompositeArmorInventory(event.player);
				CompositeArmorPart p;
				if((p=arm.getPart(LivingEntity.getEquipmentSlotForItem(it)))!=null)
				{
					p.onArmorTick(event.player.level, event.player, arm);
				}
			}
		}
	}

	@SubscribeEvent
	public void onTooltip(ItemTooltipEvent event)
	{
		if(event.getItemStack().getTagElement(CompositeArmorPart.NBT_TAG_NAME)!=null)
		{
			event.getToolTip().add(new TranslatableComponent("futurepack.item.is_composite_armor"));
		}
	}

	@SubscribeEvent
	public void attachTileEntityCapabilities(AttachCapabilitiesEvent<BlockEntity> event)
	{
		if(FPConfig.COMMON.enable_RF_support.get())
		{
			CapProviderRFSupport.addProvider(event);
		}

	}

	private static int counter = 50;

	@SubscribeEvent
    public void onNeighborNotify(BlockEvent.NeighborNotifyEvent event)
	{
        // UGLY HACK to prevent stack overflow in rftools dimensions with block updates - from McJty - https://github.com/McJtyMods/RFToolsDimensions/blob/1.18/src/main/java/mcjty/rftoolsdim/setup/ForgeEventHandlers.java#L49
        if (event.getWorld() instanceof ServerLevel serverLevel)
        {
        	ResourceLocation dim = serverLevel.dimension().location();
            if (isModdedDimension(serverLevel))
            {
                counter--;
                if (counter < 0) {
                    counter = 50;
                    StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
                    if (stacktrace.length > 400) {
                        FPLog.logger.warn("[Worldgen][%s] Canceled a possible stackoverflow: " + stacktrace.length, dim.toString());
                        event.setCanceled(true);
                        counter = 1;    // Force to check sooner
                    }
                }
            }
        }
    }

}
