package futurepack.common;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Pattern;

import futurepack.api.Constants;
import futurepack.common.block.deco.DecoBlocks;
import futurepack.common.block.plants.PlantBlocks;
import futurepack.common.block.terrain.TerrainBlocks;
import futurepack.world.gen.DungeonEntrancePlacement;
import futurepack.world.gen.FPFeatures;
import futurepack.world.gen.WorldGenOres;
import futurepack.world.gen.feature.CrystalBubbleConfig;
import futurepack.world.gen.feature.DungeonFeatureConfig;
import net.minecraft.core.Holder;
import net.minecraft.data.worldgen.features.FeatureUtils;
import net.minecraft.data.worldgen.features.VegetationFeatures;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.data.worldgen.placement.VegetationPlacements;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.level.biome.Biome.BiomeCategory;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.VerticalAnchor;
import net.minecraft.world.level.levelgen.GenerationStep.Decoration;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.OreConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.SimpleBlockConfiguration;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;
import net.minecraft.world.level.levelgen.placement.BiomeFilter;
import net.minecraft.world.level.levelgen.placement.HeightRangePlacement;
import net.minecraft.world.level.levelgen.placement.InSquarePlacement;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.placement.RarityFilter;
import net.minecraft.world.level.levelgen.structure.templatesystem.RuleTest;
import net.minecraft.world.level.levelgen.structure.templatesystem.TagMatchTest;
import net.minecraftforge.common.ForgeConfigSpec.ConfigValue;
import net.minecraftforge.common.ForgeConfigSpec.IntValue;
import net.minecraftforge.event.world.BiomeLoadingEvent;

public class WorldGenRegistry 
{

	private static Consumer<BiomeLoadingEvent> test;
	
	@SuppressWarnings("unchecked")
	public static void init() 
	{
		register();
		WorldGenOres.register();
		
		
		ArrayList<Consumer<BiomeLoadingEvent>> list = new ArrayList<Consumer<BiomeLoadingEvent>>();
		list.add(new CrystalCaveInserter());
		list.add(new TecDungeonInserter());
		list.add(new BedrockRiftInserter());
		list.add(new DungeonEntranceInserter());
		list.add(new ErseInserter());
		
//		list.add(b -> {
//			if(b.getRegistryName().getNamespace().equals("minecraft"))
//			{
//				b.addCarver(Carving.AIR, Biome.createCarver(BiomeTyros.LAPUTA_ISLANDS, ICarverConfig.NONE));
//			}
//		});
		
		WorldGenOres.init(list);
		
		test = merge(list.toArray(new Consumer[list.size()]));		
	}
	
	private static <T> Consumer<T> merge(Consumer<T>[] consumers)
	{
		return t -> {
			for(Consumer<T> c : consumers)
			{
				c.accept(t);
			}
		};
	}
	
	private static Holder<PlacedFeature> crystalcave_alutin, crystalcave_neon, crystalcave_retium, crystalcave_glowtite, crystalcave_bioterium;
	private static Holder<PlacedFeature>[] tec_dungeons;
	private static Holder<PlacedFeature> bedrock_rift, dungeon_entrace, erse;
	
	public static void register() 
	{
		
		//crystal bubbles
		
		CrystalBubbleConfig alutin_config = new CrystalBubbleConfig(TerrainBlocks.sand_alutin.defaultBlockState(), DecoBlocks.block_crystal_alutin.defaultBlockState());
		CrystalBubbleConfig neon_config = new CrystalBubbleConfig(TerrainBlocks.sand_neon.defaultBlockState(), DecoBlocks.block_crystal_neon.defaultBlockState());
		CrystalBubbleConfig retium_config = new CrystalBubbleConfig(TerrainBlocks.sand_retium.defaultBlockState(), DecoBlocks.block_crystal_retium.defaultBlockState());
		CrystalBubbleConfig glowtite_config = new CrystalBubbleConfig(TerrainBlocks.sand_glowtite.defaultBlockState(), DecoBlocks.block_crystal_glowtite.defaultBlockState());
		CrystalBubbleConfig bioterium_config = new CrystalBubbleConfig(TerrainBlocks.sand_bioterium.defaultBlockState(), DecoBlocks.block_crystal_bioterium.defaultBlockState());
		
		crystalcave_alutin = genFeatureCrystalCave(new ResourceLocation(Constants.MOD_ID, "crystalcave_alutin"), alutin_config, FPConfig.WORLDGEN_CAVES.hole_alutin);
		crystalcave_neon = genFeatureCrystalCave(new ResourceLocation(Constants.MOD_ID, "crystalcave_neon"), neon_config, FPConfig.WORLDGEN_CAVES.hole_neon);
		crystalcave_retium = genFeatureCrystalCave(new ResourceLocation(Constants.MOD_ID, "crystalcave_retium"), retium_config, FPConfig.WORLDGEN_CAVES.hole_retium);
		crystalcave_glowtite = genFeatureCrystalCave(new ResourceLocation(Constants.MOD_ID, "crystalcave_glowtite"), glowtite_config, FPConfig.WORLDGEN_CAVES.hole_glowtite);
		crystalcave_bioterium = genFeatureCrystalCave(new ResourceLocation(Constants.MOD_ID, "crystalcave_bioterium"), bioterium_config, FPConfig.WORLDGEN_CAVES.hole_bioterium);

		
		//tec dungeons
		
		DungeonFeatureConfig[] conf = new DungeonFeatureConfig[DyeColor.values().length];
		tec_dungeons = new Holder[conf.length];
		
		DyeColor[] tec_colors = new DyeColor[] {DyeColor.ORANGE, DyeColor.GREEN, DyeColor.LIGHT_GRAY, DyeColor.LIME, DyeColor.BROWN, DyeColor.RED, DyeColor.BLUE, DyeColor.LIGHT_BLUE, DyeColor.WHITE, DyeColor.MAGENTA};
		for(DyeColor dye : tec_colors)
		{
			Holder<PlacedFeature> tec_dungeon = genFeatureTecDungeon(new ResourceLocation(Constants.MOD_ID, "tec_dungeon_" + dye.getName()), dye, conf);
			WorldGenRegistry.tec_dungeons[dye.ordinal()] = tec_dungeon;
		}
		
		//bedrock rift
		
		Holder<ConfiguredFeature<NoneFeatureConfiguration, ?>> bedrock_rift_base = FeatureUtils.register(new ResourceLocation(Constants.MOD_ID, "configured_bedrock_rift").toString(), FPFeatures.BEDROCK_RIFT, FeatureConfiguration.NONE);
		bedrock_rift = PlacementUtils.register(new ResourceLocation(Constants.MOD_ID, "configured_bedrock_rift_placed").toString(), bedrock_rift_base, List.of(
				BiomeFilter.biome()
				));	
		
		
		//large Dungeon entrace
		
		
		DungeonFeatureConfig entrance = new DungeonFeatureConfig.Builder().addEntryAuto(1, "special_entrance_upper").build();
		var dungeon_base = FeatureUtils.register(new ResourceLocation(Constants.MOD_ID, "dungeon_entrace").toString(), FPFeatures.DUNGEON, entrance);
		dungeon_entrace = PlacementUtils.register(new ResourceLocation(Constants.MOD_ID, "dungeon_entrace_placed").toString(), dungeon_base, List.of(DungeonEntrancePlacement.INSTANCE, BiomeFilter.biome()));

		
		//erse
		
//		var erse_base = FeatureUtils.register("patch_erse", Feature.RANDOM_PATCH, FeatureUtils.simpleRandomPatchConfiguration(32, PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new SimpleBlockConfiguration(BlockStateProvider.simple(PlantBlocks.erse)))));
//		
//		erse = PlacementUtils.register("patch_erse_forest", erse_base, VegetationPlacements.worldSurfaceSquaredWithCount(2));
		
		RuleTest IS_GRASS = new TagMatchTest(FuturepackTags.erse_spawn_able);
		OreConfiguration ERSE = new OreConfiguration(IS_GRASS, PlantBlocks.erse.defaultBlockState(), 10);
		var erse_configured = FeatureUtils.register(new ResourceLocation(Constants.MOD_ID, "configured_erse").toString(), Feature.ORE, ERSE);
		
		erse = PlacementUtils.register(new ResourceLocation(Constants.MOD_ID, "configured_erse_placed").toString(), erse_configured, List.of(
				PlacementUtils.HEIGHTMAP_TOP_SOLID,
				RarityFilter.onAverageOnceEvery(2),
				InSquarePlacement.spread(),
				BiomeFilter.biome()
				));
	}

	public static Holder<ConfiguredFeature<CrystalBubbleConfig, ?>> createConfiguredCaveFeature(String name, CrystalBubbleConfig conf)
	{
		return FeatureUtils.register(name, FPFeatures.CRYSTAL_BUBBLE, conf);
	}
	
	public static Holder<ConfiguredFeature<DungeonFeatureConfig, ?>> createConfiguredTcDungeonFeature(String name, DyeColor dye, DungeonFeatureConfig[] conf)
	{	
		if(conf[dye.ordinal()] == null)
		{
			try
			{
				DungeonFeatureConfig.Builder build = new DungeonFeatureConfig.Builder().addEntryAuto(4, "new_start_dungeon_"+dye.getName());
				conf[dye.ordinal()] = build.build();
			}
			catch(NullPointerException e)
			{
				throw new IllegalArgumentException("illegal color " + dye, e);
			}		
		}
		
		return FeatureUtils.register(name, FPFeatures.DUNGEON, conf[dye.ordinal()]);
	}
	
	
	public static Holder<PlacedFeature> createPlacedCaveFeature(String name, Holder<ConfiguredFeature<CrystalBubbleConfig, ?>> cave, IntValue configFile)
	{
		float percent = configFile.get() * 0.01F;
		int chance = (int)(1F / percent);
		
		return PlacementUtils.register(name, cave, List.of(
				
				RarityFilter.onAverageOnceEvery(chance),
				InSquarePlacement.spread(),
				PlacementUtils.RANGE_8_8,
				BiomeFilter.biome()));
	}
	
	public static Holder<PlacedFeature> createPlacedTecDungeonFeature(String name, Holder<ConfiguredFeature<DungeonFeatureConfig, ?>> cave)
	{
		int chance = (int)(1.0 / FPConfig.WORLDGEN.tecdungeon_spawnrate.get());
		
		return PlacementUtils.register(name, cave, List.of(
				
				RarityFilter.onAverageOnceEvery(chance),
				InSquarePlacement.spread(),
				HeightRangePlacement.uniform(VerticalAnchor.bottom(), VerticalAnchor.absolute(60)),
				BiomeFilter.biome()));
	}
	
	
	private static Holder<PlacedFeature> genFeatureTecDungeon(ResourceLocation name, DyeColor dye, DungeonFeatureConfig[] conf) 
	{
		Holder<ConfiguredFeature<DungeonFeatureConfig, ?>> holder = createConfiguredTcDungeonFeature(name.toString(), dye, conf);
		return createPlacedTecDungeonFeature(name.toString()+"_full_height", holder);
	}

	private static Holder<PlacedFeature> genFeatureCrystalCave(ResourceLocation name, CrystalBubbleConfig config, IntValue hole) 
	{
		Holder<ConfiguredFeature<CrystalBubbleConfig, ?>> holder = createConfiguredCaveFeature(name.toString(), config);
		return createPlacedCaveFeature(name.toString()+"_full_height", holder, hole);
	}
	
	public static class CrystalCaveInserter implements Consumer<BiomeLoadingEvent>
	{
		Predicate<BiomeLoadingEvent> alutin = getRegistryMatcher(FPConfig.WORLDGEN_CAVES.wl_alutin_caves);
		Predicate<BiomeLoadingEvent> neon = getRegistryMatcher(FPConfig.WORLDGEN_CAVES.wl_neon_caves);
		Predicate<BiomeLoadingEvent> retium = getRegistryMatcher(FPConfig.WORLDGEN_CAVES.wl_retium_caves);
		Predicate<BiomeLoadingEvent> glowtite = getRegistryMatcher(FPConfig.WORLDGEN_CAVES.wl_glowtite_caves);
		Predicate<BiomeLoadingEvent> bioterium = getRegistryMatcher(FPConfig.WORLDGEN_CAVES.wl_bioterium_caves);

		
		@Override
		public void accept(BiomeLoadingEvent t) 
		{
			if(alutin.test(t))
			{
				addFeature(t, Decoration.UNDERGROUND_STRUCTURES, () -> crystalcave_alutin);
			}
			if(neon.test(t))
			{
				addFeature(t, Decoration.UNDERGROUND_STRUCTURES, () -> crystalcave_neon);
			}
			if(retium.test(t))
			{
				addFeature(t, Decoration.UNDERGROUND_STRUCTURES, () -> crystalcave_retium);
			}
			if(glowtite.test(t))
			{
				addFeature(t, Decoration.UNDERGROUND_STRUCTURES, () -> crystalcave_glowtite);
			}
			if(bioterium.test(t))
			{
				addFeature(t, Decoration.UNDERGROUND_STRUCTURES, () -> crystalcave_bioterium);
			}
		}
		
		
		
		public static Predicate<BiomeLoadingEvent> getRegistryMatcher(ConfigValue<List<? extends String>> config)
		{
			 return getRegistryMatcher(config.get());
		}
		
		public static Predicate<BiomeLoadingEvent> getRegistryMatcher(List<? extends String> list)
		{
			Predicate[] preds = list.stream().map(CrystalCaveInserter::getStringMatcher).map(CrystalCaveInserter::asRegistryMatcher).toArray(Predicate[]::new);
			return (BiomeLoadingEvent e) -> {
				for(Predicate<BiomeLoadingEvent> p : preds)
				{
					if(p.test(e))
						return true;
				}
				return false;
			}; 
		}
		
		public static Predicate<String> getStringMatcher(String regex)
		{
			 return Pattern.compile(regex).asPredicate();
		}
		
		public static Predicate<BiomeLoadingEvent> asRegistryMatcher(Predicate<String> s)
		{
			return (BiomeLoadingEvent e) -> s.test(e.getName().toString());
		}
	}
	
	public static class TecDungeonInserter implements Consumer<BiomeLoadingEvent>
	{
		DungeonFeatureConfig[] conf = new DungeonFeatureConfig[DyeColor.values().length];
		
		@Override
		public void accept(BiomeLoadingEvent t) 
		{
			List<? extends String> not_allowed_biomes = FPConfig.WORLDGEN.bl_tec_dungeons.get();
			ResourceLocation rname =  t.getName();
			if(rname==null)
				return; //broken registry
			if(not_allowed_biomes.contains(rname.toString()))
				return;
			
			DyeColor dye = getColor(t);
			if(dye == null)
				return;
			else
			{
				addFeature(t, Decoration.UNDERGROUND_STRUCTURES, () -> tec_dungeons[dye.ordinal()]);
			}
		}
		
		private static DyeColor getColor(BiomeLoadingEvent b)
		{
			if(b.getName()!=null)
			{
				switch(b.getCategory())
				{
				case SAVANNA:
				case DESERT:
				case BEACH:
					return DyeColor.ORANGE;//
				case FOREST:
				case JUNGLE:
					return DyeColor.GREEN;//
				case EXTREME_HILLS:
					return DyeColor.LIGHT_GRAY;//
				case PLAINS:
					return DyeColor.LIME;//
				case SWAMP:
				case MUSHROOM:
				case TAIGA:
					return DyeColor.BROWN;//
				case NETHER:
					return DyeColor.RED;//
				case RIVER:
					return DyeColor.BLUE;//
				case ICY:
					return DyeColor.LIGHT_BLUE;//
				case THEEND:
				case NONE:
					return DyeColor.WHITE;
				case MESA:
					return DyeColor.MAGENTA;
				default:
					return null;
				}
			}
			else
			{
				return null;
			}
		}
	}
	
	public static class BedrockRiftInserter implements Consumer<BiomeLoadingEvent>
	{
		@Override
		public void accept(BiomeLoadingEvent t) 
		{
			addFeature(t, Decoration.UNDERGROUND_STRUCTURES, () -> bedrock_rift);
		}
		
	}
	
	public static class DungeonEntranceInserter implements Consumer<BiomeLoadingEvent>
	{
		
		@Override
		public void accept(BiomeLoadingEvent t) 
		{
			if(t.getCategory() == BiomeCategory.OCEAN)
				return;
			
			addFeature(t, Decoration.SURFACE_STRUCTURES, () -> dungeon_entrace);
		}
		
	}
	
	public static class ErseInserter implements Consumer<BiomeLoadingEvent>
	{
		@Override
		public void accept(BiomeLoadingEvent t) 
		{
			if(t.getClimate().downfall > 0.1 && t.getClimate().temperature > 0.4 && t.getCategory() != BiomeCategory.DESERT)
			{
				addFeature(t, Decoration.TOP_LAYER_MODIFICATION, () -> erse);
			}
		}
	}
	
	public static void addFeature(BiomeLoadingEvent t, GenerationStep.Decoration deco, Supplier<Holder<PlacedFeature>> feature)
	{
		t.getGeneration().getFeatures(deco).add(feature.get());
	}


	public static void post(BiomeLoadingEvent event) 
	{
		test.accept(event);
	}
}
