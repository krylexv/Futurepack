package futurepack.common.recipes.recycler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import futurepack.api.ItemPredicateBase;
import futurepack.common.FPLog;
import futurepack.common.conditions.ConditionRegistry;
import futurepack.common.recipes.ISyncedRecipeManager;
import futurepack.common.recipes.assembly.AssemblyRecipe;
import futurepack.common.recipes.assembly.FPAssemblyManager;
import futurepack.depend.api.ItemPredicate;
import futurepack.depend.api.helper.HelperJSON;
import futurepack.depend.api.helper.HelperOreDict;
import futurepack.extensions.jei.FuturepackUids;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;

public class FPRecyclerShredderManager implements ISyncedRecipeManager<RecyclerShredderRecipe>
{
	public static final String NAME = "recycler_shredder";
	
	public static FPRecyclerShredderManager instance = new FPRecyclerShredderManager();
	public ArrayList<RecyclerShredderRecipe> recipes = new ArrayList<RecyclerShredderRecipe>();


	public static void addRecipe(ItemPredicateBase it, ItemStack[] singleOut, int time, float ...prob)
	{
		instance.addRecyclerRecipe(it, singleOut, time, prob);
	}
	
	public RecyclerShredderRecipe addRecyclerRecipe(ItemPredicateBase it, ItemStack[] singleOut, int time, float ...prob)
	{
		RecyclerShredderRecipe z = new RecyclerShredderRecipe(it, singleOut, time, prob);
		recipes.add(z);
		FPLog.logger.debug(z.toString());
		return z;
	}
	
	//append dynamic recipes
	public ArrayList<RecyclerShredderRecipe> getRecipesJEI()
	{
		ArrayList<RecyclerShredderRecipe> rr = new ArrayList<RecyclerShredderRecipe>(recipes);
		
		for(AssemblyRecipe ar : FPAssemblyManager.instance.recipes)
		{
			ItemPredicateBase[] preds = ar.getInput();
			ItemStack[] output = new ItemStack[preds.length];
			for(int i=0;i<preds.length;i++)
			{
				output[i] = preds[i].getRepresentItem();
			}
			rr.add(new RecyclerShredderRecipe(new ItemPredicate(ar.getOutput()), output, 20, 0.8f, 0.1f, 0.005f));
		}
		
		return rr;
	}
	
	@Override
	public Collection<RecyclerShredderRecipe> getRecipes() 
	{
		return recipes;
	}
	
	@Override
	public void addRecipe(RecyclerShredderRecipe t) 
	{
		recipes.add(t);
	}
	
	@Override
	public ResourceLocation getName() 
	{
		return FuturepackUids.RECYCLER;
	}
	
	public RecyclerShredderRecipe getMatchingRecipe(ItemStack in)
	{	
		for(RecyclerShredderRecipe rec : recipes)
		{
			if(rec.match(in))
			{
				return rec;
			}
		}
		
		for(AssemblyRecipe ar : FPAssemblyManager.instance.recipes)
		{
			if(ar.getOutput().sameItem(in))
			{
				ItemPredicateBase[] preds = ar.getInput();
				ItemStack[] output = new ItemStack[preds.length];
				for(int i=0;i<preds.length;i++)
				{
					output[i] = preds[i].getRepresentItem();
				}
				return new RecyclerShredderRecipe(new ItemPredicate(ar.getOutput()), output, 20, 0.8f, 0.1f, 0.005f);
			}
		}	
		
		return null;
	}
	
	public static void init(JsonArray recipes)
	{
		instance = new FPRecyclerShredderManager();
		FPLog.logger.info("Setup Recycler-Shredder Recipes");
		for(JsonElement elm : recipes)
		{
			setupObject(elm);
		}
	}
	
	private static void setupObject(JsonElement o)
	{
		if(o.isJsonObject())
		{
			JsonObject obj = o.getAsJsonObject();
			if(!ConditionRegistry.checkCondition(obj))
				return;
			ItemPredicateBase in = HelperJSON.getItemPredicateFromJSON(obj.get("input"));
			
			if(in.collectAcceptedItems(new ArrayList<>()).isEmpty())
			{
				FPLog.logger.warn("Broken recycler-shredder recipe with empty input %s", obj.get("input").toString());
				return;
			}
			JsonArray arr = obj.get("output").getAsJsonArray();
			List<ItemStack>[] out = new List[arr.size()];
			for(int i=0;i<arr.size();i++)
			{
				out[i] = HelperJSON.getItemFromJSON(arr.get(i), false);
				if(out[i]==null)
					return;
				if(out[i].isEmpty())
					return; //when item was not found
			}
			
			boolean empty = true;
			Main:
			for(List<ItemStack> l : out)
			{
				if(l!=null)
				{
					if(!l.isEmpty())
					{
						for(ItemStack i : l)
						{
							if(!i.isEmpty())
							{
								empty = false;
								break Main;
							}
						}
					}
				}
			}
			
			if(empty)
			{
				FPLog.logger.warn("Broken shredder recipe without output " + obj.toString());
				return;
			}
			
			ItemStack[] singleOut = new ItemStack[out.length];
			for(int i=0;i<singleOut.length;i++)
			{
				if(out[i].isEmpty())
				{
					continue;
				}
				int original = out[i].get(0).getCount();
				singleOut[i] = HelperOreDict.FuturepackConveter.getChangedItem(out[i].get(0)).copy();
				singleOut[i].setCount(original);
			}
			
			arr = obj.get("chance").getAsJsonArray();
			float[] probs = new float[arr.size()];

			for(int i=0;i<arr.size();i++)
			{
				probs[i] = arr.get(i).getAsFloat();
			}
			
			int time=20;
			if(obj.has("time"))
			{
				time = obj.get("time").getAsInt();
			}

			instance.addRecyclerRecipe(in, singleOut, time, probs);
			
		}
		else
		{
			FPLog.logger.error("Wrong JSON Type for a Recipe:  \"" + o + "\"");
		}
	}
	

	
}
