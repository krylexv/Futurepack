package futurepack.common.recipes.industrialfurnace;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import futurepack.api.ItemPredicateBase;
import futurepack.common.recipes.EnumRecipeSync;
import futurepack.common.research.CustomPlayerData;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.ItemStack;

public class IndRecipe
{
	public final String id;
	protected ItemPredicateBase[] input;
	protected ItemStack output;
			
	public IndRecipe(String id, ItemStack out, ItemPredicateBase...in) 
	{
		this.id = id;
		input = Arrays.copyOf(in, in.length);			
		output = out.copy();

	}
	
	public boolean match(ItemStack[] c, boolean[] uses)
	{
		ItemStack[] content = new ItemStack[c.length];
		for(int i=0;i<c.length;i++)
		{
			if(c[i]!=null)
				content[i] = c[i];
		}
		boolean[] use = new boolean[c.length];
		
		if(input.length <= content.length)
		{
			for(int i=0;i<input.length;i++)
			{
				if(input[i]==null)
					continue;
				boolean b = false;
				for(int j=0;j<content.length;j++)
				{
					if(content[j]==null || content[j].isEmpty())
						continue;
					if(input[i].apply(content[j], false))
					//if(input[i].getItem()== content[j].getItem() && (input[i].getItemDamage()==32767 || input[i].getItemDamage() == content[j].getItemDamage()) && content[j].getCount()>=input[i].getCount())
					{
						b=true;
						if(!use[j])
						{
							use[j] = true;
							uses[j] = true;
							content[j] = content[j].copy();
						}
						content[j].shrink(input[i].getMinimalItemCount(content[j]));
						
						break;
					}
				}
				if(!b)
				{
					return false;
				}
			}
			return true;
		}			
		return false;
	}
	
	public ItemStack getOutput()
	{
		return output.copy();
	}
	
	public ItemPredicateBase[] getInputs()
	{
		return input;
	}

	public boolean use(ItemStack[] content) 
	{
		Map<ItemStack, Integer> stacks = new HashMap<ItemStack,Integer>();
		
		for(int i=0;i<input.length;i++)
		{
			if(input[i]==null)
				continue;
			boolean b = false;
			for(int j=0;j<content.length;j++)
			{
				if(content[j]==null || content[j].isEmpty())
					continue;		
				
				if(input[i].apply(content[j], false))
				//if(input[i].getItem()== content[j].getItem() && (input[i].getItemDamage()==32767 || input[i].getItemDamage() == content[j].getItemDamage()))
				{
					Integer amount = stacks.get(content[j]);
					if(amount==null)
					{
						stacks.put(content[j], input[i].getMinimalItemCount(content[j]));
						b = true;
						break;
					}
					else
					{
						if(amount + input[i].getMinimalItemCount(content[j]) <= content[j].getCount())
						{
							stacks.put(content[j], input[i].getMinimalItemCount(content[j])+amount);
							b = true;
							break;
						}
					}					
				}
			}
			if(!b)
			{
				return false;
			}
		}
		for(Entry<ItemStack,Integer> e : stacks.entrySet())
		{
			e.getKey().shrink(e.getValue());
		}
		return true;
	}
	
	@Override
	public String toString() 
	{
		return "IndFurnace: " + id +" " +Arrays.toString(input) + " to " + output;
	}
	
	public boolean isLocalResearched()
	{
		return CustomPlayerData.createForLocalPlayer().canProduce(output);
	}
	
	public void write(FriendlyByteBuf buf)//this stuff is possible and I love it, thanks you java lambdas
	{
		buf.writeUtf(id);
		buf.writeItem(output);
		EnumRecipeSync.writePredicates(input, buf);
	}
	
	public static IndRecipe read(FriendlyByteBuf buf)
	{
		return new IndRecipe(buf.readUtf(), buf.readItem(), EnumRecipeSync.readPredicates(buf));
	}
}
