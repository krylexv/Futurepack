package futurepack.common.block.modification;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Stream;

import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.common.FPTileEntitys;
import futurepack.common.modification.EnumChipType;
import net.minecraft.core.BlockPos;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;

public class TileEntityRadar extends TileEntityModificationBase
{
	public float rotation;
	long last = 0;
	private boolean working = false;

	public TileEntityRadar(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.RADAR, pos, state);
		Random rand = new Random();
		rotation = rand.nextFloat() * 100;
	}
	
	@Override
	public void updateTile(int ticks)
	{
		if(this.getLevel() == null)
			return;

		if(!this.getLevel().isClientSide && energy.get() > 50) {
			working = false;
			if(last < 10 + (20 / getPureRam())) {
				last += ticks;
				return;
			}
			last = 0;
			AABB axisalignedbb = (new AABB(this.getBlockPos())).inflate(24.0D + 4 * getChipPower(EnumChipType.TACTIC));
			List<LivingEntity> nearbyEntites = this.getLevel().getEntitiesOfClass(LivingEntity.class, axisalignedbb);

			Stream<LivingEntity> stream = nearbyEntites.stream().filter(livingEntity ->
					!livingEntity.hasEffect(MobEffects.GLOWING) && !livingEntity.getClassification(false).isFriendly());

			stream = stream.sorted((living1, living2) -> (int) (entityDistance(living1) - entityDistance(living2)));

			Optional<LivingEntity> first = stream.findFirst();

			first.ifPresent(livingEntity -> {
				livingEntity.addEffect(new MobEffectInstance(MobEffects.GLOWING, 300));
				energy.use(50);
				working = true;
			});
			stream.close();
		}
	}


	public void updateRotation(float partialTicks) {
		rotation = (rotation + partialTicks / 20) % 360;
	}

	private double entityDistance(LivingEntity e) {
		return new Vec3(
				e.getX() - this.getBlockPos().getX(),
				e.getY() - this.getBlockPos().getY(),
				e.getZ() - this.getBlockPos().getZ()).length();
	}
	
	@Override
	public EnumEnergyMode getEnergyType() 
	{
		return EnumEnergyMode.USE;
	}
	
	@Override
	public boolean isWorking() 
	{
		return working;
	}
}
