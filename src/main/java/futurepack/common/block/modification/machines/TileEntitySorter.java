package futurepack.common.block.modification.machines;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import javax.annotation.Nullable;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.FacingUtil;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.interfaces.filter.IItemFilter;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.block.FPDirectionHelper;
import futurepack.common.filter.OrGateFilter;
import futurepack.common.modification.EnumChipType;
import futurepack.depend.api.helper.HelperInventory;
import futurepack.depend.api.helper.HelperInventory.SlotContent;
import futurepack.depend.api.helper.HelperItemFilter;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Direction.Axis;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.WorldlyContainer;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemHandlerHelper;
import net.minecraftforge.items.wrapper.SidedInvWrapper;

public class TileEntitySorter extends TileEntityMachineBase implements WorldlyContainer, ITilePropertyStorage
{
	private IItemHandler[] handler;
	private IItemHandler intern;
	
	public TileEntitySorter(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.SORTER, pos, state);
		handler = new IItemHandler[6];
		for(Direction face : FacingUtil.VALUES)
		{
			handler[face.ordinal()] = new SidedInvWrapper(this, face);
		}
		intern = new InternAccess();
	}

	@Override
	protected EnumLogisticType[] getLogisticTypes()
	{
		return new EnumLogisticType[]{EnumLogisticType.ENERGIE};
	}
	
	@Override
	public void configureLogisticStorage(LogisticStorage storage) 
	{
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.OUT, EnumLogisticType.ENERGIE);
	}
	

	@Override
	public void updateTile(int ticks)
	{
		if(level.isClientSide)
			return;
		
		ArrayList<SlotContent> out = new ArrayList<SlotContent>();
		ArrayList<SlotContent> red = new ArrayList<SlotContent>();
		ArrayList<SlotContent> green = new ArrayList<SlotContent>();
		ArrayList<SlotContent> blue = new ArrayList<SlotContent>();
		ArrayList<SlotContent> yellow = new ArrayList<SlotContent>();
		
		this.filters = null;
		
		int c = (int)( 10 * ( 1+ getChipPower(EnumChipType.INDUSTRIE)));
		c*= ticks;
		
		int offset = (int) (System.currentTimeMillis() % 9);
		
		boolean isEmpty = true;
		
		for(int j=0;j<9;j++)
		{
			int i = (j + offset) % 9; //This makes sure if one slot is not pogressable, the whole machine won't get stuck.
			
			if(items.get(i).isEmpty())
				continue;
			if(c<=0)
				break;
			
			isEmpty = false;
			
			ItemStack it = items.get(i).copy();
			int cc = Math.min(it.getCount(), c);				
			it.setCount(cc);
			c-=cc;
			
			EnumSortType t = isSortable(items.get(i));
			if(t==EnumSortType.RED)
			{			
				red.add(new SlotContent(intern, i, it, null));
			}
			else if(t==EnumSortType.GREEN)
			{			
				green.add(new SlotContent(intern, i, it, null));
			}
			else if(t==EnumSortType.BLUE)
			{			
				blue.add(new SlotContent(intern, i, it, null));
			}
			else if(t==EnumSortType.YELLOW)
			{			
				yellow.add(new SlotContent(intern, i, it, null));
			}
			else
			{			
				out.add(new SlotContent(intern, i, it, null));
			}
			
			//dont remove items here, the slot content.remove() will do this
//			items.get(i).shrink(cc);
//			if(items.get(i).getCount()<=0)
//			{
//				items.set(i, ItemStack.EMPTY);
//			}
		}
		
		if(isEmpty)
		{
			return;
		}
		
		ArrayList<SlotContent> done = new ArrayList<SlotContent>();
		ArrayList<SlotContent> rest = new ArrayList<SlotContent>();
		
		if(!out.isEmpty())
			done.addAll(doOutputForSide(getOutput(), out, null));
		if(!red.isEmpty())
			done.addAll(doOutputForSide(getRed(), red, getCallback(EnumSortType.RED)));
		if(!green.isEmpty())
			done.addAll(doOutputForSide(getGreen(), green, getCallback(EnumSortType.GREEN)));
		if(!blue.isEmpty())
			done.addAll(doOutputForSide(getBlue(), blue, getCallback(EnumSortType.BLUE)));
		if(!yellow.isEmpty())
			done.addAll(doOutputForSide(getYellow(), yellow, getCallback(EnumSortType.YELLOW)));
		
		for(SlotContent slot : done)
		{
			slot.remove();
		}	
//		rest.addAll(out);
//		rest.addAll(red);
//		rest.addAll(green);
//		rest.addAll(blue);
//		rest.addAll(yellow);
//		rest.removeAll(done);
//		
//		HelperInventory.insertItems(level, worldPosition, getInput().getOpposite(), rest);
	}
	
	private List<SlotContent> doOutputForSide(Direction face, ArrayList<SlotContent> content, @Nullable Consumer<ItemStack> transfered)
	{
		List<SlotContent> removed = HelperInventory.insertItems(level, worldPosition.relative(face.getOpposite()), face, content);
		removed.addAll(HelperInventory.ejectItemsIntoWorld(level, worldPosition.relative(face.getOpposite()), content));
		
		if(transfered!=null)
			removed.stream().map(s -> s.item).forEach(transfered);
		
		return removed;		
	}
	
	
	private IItemFilter[] filters;
	
	private EnumSortType isSortable(ItemStack item)
	{
		if(item==null || item.isEmpty())
			return EnumSortType.NONE;
		if(filters==null)
		{
			filters = new IItemFilter[EnumSortType.values().length];
			for(EnumSortType type : EnumSortType.values())
			{
				filters[type.ordinal()] = getFilter(type);
			}
		}
		
		for(int i=1;i<filters.length;i++)
		{
			if(filters[i].test(item))
			{
				return EnumSortType.values()[i];
			}
		}
		
		return EnumSortType.NONE;
	}
	
	private Consumer<ItemStack> getCallback(EnumSortType type)
	{
		if(type == EnumSortType.NONE)
			return null;
		
		if(filters!=null)
		{
			return filters[type.ordinal()]::amountTransfered;
		}
		return null;
	}
	
	@Override
 	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		return nbt;
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
	}
	
	private Direction getRed()
	{
		Direction out = getOutput();
		if(out.getAxis()==Axis.Y)
		{
			return FPDirectionHelper.rotateAround(out, Axis.X);	
		}
		return Direction.UP;
	}
	
	private Direction getGreen()
	{
		Direction out = getOutput();
		if(out.getAxis()==Axis.Y)//UP EAST,DOWN WEST
		{
			if(out== Direction.UP)
				out = Direction.DOWN;
			return FPDirectionHelper.rotateAround(out, Axis.Z);
		}
		return FPDirectionHelper.rotateAround(out, Axis.Y);
	}
	
	private Direction getBlue()
	{
		return getRed().getOpposite();	
	}
	
	private Direction getYellow()
	{
		return getGreen().getOpposite();
	}
	
	private Direction getInput()
	{
		return getBlockState().getValue(BlockRotateableTile.FACING);	
	}
	
	private Direction getOutput()
	{
		return getInput().getOpposite();
	}
	
	
	@Override
	public boolean isWorking() 
	{
		return false;
	}
	
	@Override
	public EnumEnergyMode getEnergyType() 
	{
		return EnumEnergyMode.USE;
	}

	@Override
	public int[] getSlotsForFace(Direction var1)
	{
		return new int[]{0,1,2,3,4,5,6,7,8};
	}

	@Override
	public boolean canPlaceItemThroughFace(int slot, ItemStack var2, Direction side)
	{
		if(getOutput()!=side)
		{
			return false;
		}
		return canPlaceItem(slot, var2);
	}

	@Override
	public boolean canTakeItemThroughFace(int slot, ItemStack var2, Direction side)
	{
		return false;
	}
	
	private IItemFilter getFilter(EnumSortType type)
	{
		if(type==EnumSortType.NONE)
			return null;
		
		ArrayList<IItemFilter> filters = new ArrayList<>(type.getLenght());
		for(int i=0;i<type.getLenght();i++)
		{
			if(!items.get(i+type.getSlot()).isEmpty())
			{
				filters.add(HelperItemFilter.getFilter(items.get(i+type.getSlot())));
			}
		}
		OrGateFilter filter = new OrGateFilter(filters.toArray( new IItemFilter[filters.size()]));
		return filter;
	}
	
	public enum EnumSortType
	{
		NONE(-1,-1), RED(9,4), GREEN(13,4), BLUE(17,4), YELLOW(21,4);
		
		private int slotStart;
		private int length;
		
		private EnumSortType(int slot, int length)
		{
			slotStart = slot;
			this.length = length;
		}
		
		public int getSlot()
		{
			return slotStart;
		}
		
		public int getLenght()
		{
			return length;
		}
		
		public static EnumSortType getTypeByNum(int i)
		{
			i -= 9;
			if(i>=0)
			{
				i /= 4;
				if(i==0)
					return RED;
				if(i==1)
					return GREEN;
				if(i==2)
					return BLUE;
				if(i==3)
					return YELLOW;
			}
			return NONE;
		}
	}
	
	@Override
	public int getProperty(int id) 
	{
		switch (id)
		{
		case 0:
			return this.energy.get();
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{ 
		switch (id)
		{
		case 0:
			setEnergy(value);
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 1;
	}


	@Override
	protected int getInventorySize()
	{
		return 25;
	}

	
	private class InternAccess implements IItemHandler
	{

		@Override
		public int getSlots()
		{
			return 9;
		}

		@Override
		public ItemStack getStackInSlot(int slot)
		{
			return items.get(slot);
		}

		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
		{
			return stack;
		}

		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate)
		{
			if (amount == 0)
				return null;
			if (items.get(slot).isEmpty())
				return ItemStack.EMPTY;			
			int max = Math.min(amount, items.get(slot).getMaxStackSize());		
			if (items.get(slot).getCount()<=max)
			{
				if (!simulate)			
					items.set(slot, ItemStack.EMPTY);
				
				return items.get(slot);
			}
			else
			{
				if (!simulate)				
					items.set(slot, ItemHandlerHelper.copyStackWithSize(items.get(slot), items.get(slot).getCount()-max));					
						
				return ItemHandlerHelper.copyStackWithSize(items.get(slot), max);
			}
		}

		@Override
		public int getSlotLimit(int slot)
		{
			return 0;
		}

		@Override
		public boolean isItemValid(int slot, ItemStack stack) 
		{
			return false;
		}
		
	}
}
