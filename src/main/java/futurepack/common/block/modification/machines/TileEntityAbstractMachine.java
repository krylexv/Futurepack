package futurepack.common.block.modification.machines;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.CapabilityLogistic;
import futurepack.api.capabilities.ILogisticInterface;
import futurepack.common.block.logistic.LogisticItemHandlerWrapper;
import futurepack.common.block.modification.TileEntityModificationBase;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

public abstract class TileEntityAbstractMachine extends TileEntityModificationBase 
{
	private final LazyOptional<ILogisticInterface>[] logisticOpt;
	private final LazyOptional<IItemHandler>[] itemHandlerOpt;

	@SuppressWarnings("unchecked")
	public TileEntityAbstractMachine(BlockEntityType<? extends TileEntityAbstractMachine> type, BlockPos pos, BlockState state) 
	{
		super(type, pos, state);
		
		logisticOpt = new LazyOptional[6];
		itemHandlerOpt = new LazyOptional[6];
	}

	protected void onLogisticChange(Direction face, EnumLogisticType type)
	{
		if(type == EnumLogisticType.ITEMS)
		{
			if(itemHandlerOpt[face.get3DDataValue()]!=null)
			{
				itemHandlerOpt[face.get3DDataValue()].invalidate();
				itemHandlerOpt[face.get3DDataValue()] = null;
			}
		}
	}

	public abstract IItemHandler getItemHandler(Direction face);
	
	public abstract LogisticStorage getLogisticStorage();
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side)
	{
		if(side==null)
			return super.getCapability(cap, side);
		
		if(cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(itemHandlerOpt[side.get3DDataValue()]!=null)
			{
				return (LazyOptional<T>) itemHandlerOpt[side.get3DDataValue()];
			}
			else
			{
				if(getLogisticStorage().getModeForFace(side, EnumLogisticType.ITEMS) == EnumLogisticIO.NONE)
				{
					return LazyOptional.empty();
				}
				else
				{
					itemHandlerOpt[side.get3DDataValue()] = LazyOptional.of(() -> new LogisticItemHandlerWrapper(getLogisticStorage().getInterfaceforSide(side), getItemHandler(side)));
					itemHandlerOpt[side.get3DDataValue()].addListener(p -> itemHandlerOpt[side.get3DDataValue()]=null);
					return (LazyOptional<T>) itemHandlerOpt[side.get3DDataValue()];
				}
			}
		}
		else if(cap == CapabilityLogistic.cap_LOGISTIC)
		{
			if(logisticOpt[side.get3DDataValue()] != null)
			{
				return (LazyOptional<T>) logisticOpt[side.get3DDataValue()];
			}
			else
			{
				logisticOpt[side.get3DDataValue()] = LazyOptional.of(() -> getLogisticStorage().getInterfaceforSide(side));
				logisticOpt[side.get3DDataValue()].addListener(p -> logisticOpt[side.get3DDataValue()]=null);
				return (LazyOptional<T>) logisticOpt[side.get3DDataValue()];
			}
		}
		
		return super.getCapability(cap, side);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(itemHandlerOpt);
		HelperEnergyTransfer.invalidateCaps(logisticOpt);
		super.setRemoved();
	}

}
