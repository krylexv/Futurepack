package futurepack.common.block.modification;

import futurepack.api.Constants;
import futurepack.common.FPTileEntitys;
import futurepack.common.FuturepackMain;
import futurepack.common.block.deco.DecoBlocks;
import futurepack.common.block.modification.machines.BlockCrusher;
import futurepack.common.block.modification.machines.BlockGasTurbine;
import futurepack.common.block.modification.machines.BlockImproveComponents;
import futurepack.common.block.modification.machines.BlockIndustrialNeonFurnace;
import futurepack.common.block.modification.machines.BlockInfusionGenerator;
import futurepack.common.block.modification.machines.BlockIonCollector;
import futurepack.common.block.modification.machines.BlockLifeSupportSystem;
import futurepack.common.block.modification.machines.BlockNeonFurnace;
import futurepack.common.block.modification.machines.BlockOptiAssembler;
import futurepack.common.block.modification.machines.BlockOptiBench;
import futurepack.common.block.modification.machines.BlockRecycler;
import futurepack.common.block.modification.machines.BlockSolarPanel;
import futurepack.common.block.modification.machines.BlockSorter;
import futurepack.common.block.modification.machines.BlockZentrifuge;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.material.Material;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class ModifiableBlocks 
{
	//Don't use DecoBlocks.default_white etc directly, since for maschine_white, since BlockHoldingTile will set noDrops to the Block.Properties and corrupt it for later use.
	public static final Block.Properties maschine_white = Block.Properties.copy(DecoBlocks.color_iron_white);
	public static final Block.Properties maschine_light_gray = Block.Properties.copy(DecoBlocks.color_iron_light_gray);
	public static final Block.Properties maschine_black = Block.Properties.copy(DecoBlocks.color_iron_black);
	
//	public static final Block.Properties machine_white_not_solid = Block.Properties.of(Material.METAL, DyeColor.byName("white", null)).strength(5F, 10F).sound(SoundType.METAL).harvestTool(ToolType.PICKAXE).noOcclusion();
	public static final Block.Properties machine_red_not_solid = Block.Properties.of(Material.METAL, DyeColor.byName("red", null)).strength(5F, 10F).sound(SoundType.METAL).noOcclusion();
	
	public static final Block ion_collector_w = HelperItems.setRegistryName(new BlockIonCollector(maschine_white), Constants.MOD_ID, "ion_collector_white");
	public static final Block ion_collector_g = HelperItems.setRegistryName(new BlockIonCollector(maschine_light_gray), Constants.MOD_ID, "ion_collector_gray");
	public static final Block ion_collector_b = HelperItems.setRegistryName(new BlockIonCollector(maschine_black), Constants.MOD_ID, "ion_collector_black");
	
	public static final Block opti_bench_w = HelperItems.setRegistryName(new BlockOptiBench(maschine_white), Constants.MOD_ID, "opti_bench_white");
	public static final Block opti_bench_g = HelperItems.setRegistryName(new BlockOptiBench(maschine_light_gray), Constants.MOD_ID, "opti_bench_gray");
	public static final Block opti_bench_b = HelperItems.setRegistryName(new BlockOptiBench(maschine_black), Constants.MOD_ID, "opti_bench_black");
	
	public static final Block neon_furnace_w = HelperItems.setRegistryName(new BlockNeonFurnace(maschine_white), Constants.MOD_ID, "neon_furnace_white");
	public static final Block neon_furnace_g = HelperItems.setRegistryName(new BlockNeonFurnace(maschine_light_gray), Constants.MOD_ID, "neon_furnace_gray");
	public static final Block neon_furnace_b = HelperItems.setRegistryName(new BlockNeonFurnace(maschine_black), Constants.MOD_ID, "neon_furnace_black");
	
	public static final Block solar_panel_w = HelperItems.setRegistryName(new BlockSolarPanel(maschine_white), Constants.MOD_ID, "solar_panel_white");
	public static final Block solar_panel_g = HelperItems.setRegistryName(new BlockSolarPanel(maschine_light_gray), Constants.MOD_ID, "solar_panel_gray");
	public static final Block solar_panel_b = HelperItems.setRegistryName(new BlockSolarPanel(maschine_black), Constants.MOD_ID, "solar_panel_black");
	
	public static final Block crusher_w = HelperItems.setRegistryName(new BlockCrusher(maschine_white), Constants.MOD_ID, "crusher_white");
	public static final Block crusher_g = HelperItems.setRegistryName(new BlockCrusher(maschine_light_gray), Constants.MOD_ID, "crusher_gray");
	public static final Block crusher_b = HelperItems.setRegistryName(new BlockCrusher(maschine_black), Constants.MOD_ID, "crusher_black");
	
	public static final Block sorter = HelperItems.setRegistryName(new BlockSorter(maschine_white), Constants.MOD_ID, "sorter");
	
	public static final Block infusion_generator_w = HelperItems.setRegistryName(new BlockInfusionGenerator(maschine_white), Constants.MOD_ID, "infusion_generator_white");
	public static final Block infusion_generator_g = HelperItems.setRegistryName(new BlockInfusionGenerator(maschine_light_gray), Constants.MOD_ID, "infusion_generator_gray");
	public static final Block infusion_generator_b = HelperItems.setRegistryName(new BlockInfusionGenerator(maschine_black), Constants.MOD_ID, "infusion_generator_black");
	
	public static final Block industrial_neon_furnace_w = HelperItems.setRegistryName(new BlockIndustrialNeonFurnace(maschine_white), Constants.MOD_ID, "industrial_neon_furnace_white");
	public static final Block industrial_neon_furnace_g = HelperItems.setRegistryName(new BlockIndustrialNeonFurnace(maschine_light_gray), Constants.MOD_ID, "industrial_neon_furnace_gray");
	public static final Block industrial_neon_furnace_b = HelperItems.setRegistryName(new BlockIndustrialNeonFurnace(maschine_black), Constants.MOD_ID, "industrial_neon_furnace_black");
	
	public static final Block zentrifuge_w = HelperItems.setRegistryName(new BlockZentrifuge(maschine_white), Constants.MOD_ID, "zentrifuge_white");
	public static final Block zentrifuge_g = HelperItems.setRegistryName(new BlockZentrifuge(maschine_light_gray), Constants.MOD_ID, "zentrifuge_gray");
	public static final Block zentrifuge_b = HelperItems.setRegistryName(new BlockZentrifuge(maschine_black), Constants.MOD_ID, "zentrifuge_black");
	
	public static final Block recycler_w = HelperItems.setRegistryName(new BlockRecycler(maschine_white), Constants.MOD_ID, "recycler_white");
	public static final Block recycler_g = HelperItems.setRegistryName(new BlockRecycler(maschine_light_gray), Constants.MOD_ID, "recycler_gray");
	public static final Block recycler_b = HelperItems.setRegistryName(new BlockRecycler(maschine_black), Constants.MOD_ID, "recycler_black");
	
	public static final Block opti_assembler_w = HelperItems.setRegistryName(new BlockOptiAssembler(maschine_white), Constants.MOD_ID, "opti_assembler_white");
	public static final Block opti_assembler_g = HelperItems.setRegistryName(new BlockOptiAssembler(maschine_light_gray), Constants.MOD_ID, "opti_assembler_gray");
	public static final Block opti_assembler_b = HelperItems.setRegistryName(new BlockOptiAssembler(maschine_black), Constants.MOD_ID, "opti_assembler_black");
	
	public static final Block gas_turbine_w = HelperItems.setRegistryName(new BlockGasTurbine(maschine_white), Constants.MOD_ID, "gas_turbine_white");
	public static final Block gas_turbine_g = HelperItems.setRegistryName(new BlockGasTurbine(maschine_light_gray), Constants.MOD_ID, "gas_turbine_gray");
	public static final Block gas_turbine_b = HelperItems.setRegistryName(new BlockGasTurbine(maschine_black), Constants.MOD_ID, "gas_turbine_black");
	
	public static final Block rocket_lancher = HelperItems.setRegistryName(new BlockRocketLauncher(maschine_light_gray), Constants.MOD_ID, "rocket_launcher");
	public static final Block entity_killer = HelperItems.setRegistryName(new BlockEntityLaserBase(maschine_white, () -> FPTileEntitys.ENTITY_KILLER), Constants.MOD_ID, "entity_killer");
	public static final Block entity_healer = HelperItems.setRegistryName(new BlockEntityLaserBase(maschine_white, () -> FPTileEntitys.ENTITY_HEALER), Constants.MOD_ID, "entity_healer");
	public static final Block entity_eater = HelperItems.setRegistryName(new BlockEntityLaserBase(maschine_white, () -> FPTileEntitys.ENTITY_EATER), Constants.MOD_ID, "entity_eater");
	
	public static final Block electro_magnet = HelperItems.setRegistryName(new BlockElektroMagnet(maschine_black), Constants.MOD_ID, "electro_magnet");
	
	public static final Block water_turbine_w = HelperItems.setRegistryName(new BlockWaterTurbine(maschine_white), Constants.MOD_ID, "water_turbine_white");
	public static final Block water_turbine_g = HelperItems.setRegistryName(new BlockWaterTurbine(maschine_light_gray), Constants.MOD_ID, "water_turbine_gray");
	public static final Block water_turbine_b = HelperItems.setRegistryName(new BlockWaterTurbine(maschine_black), Constants.MOD_ID, "water_turbine_black");
	
	
	public static final Block external_core = HelperItems.setRegistryName(new BlockExternalCore(machine_red_not_solid), Constants.MOD_ID, "external_core");
	public static final Block fluid_pump = HelperItems.setRegistryName(new BlockFluidPump(machine_red_not_solid), Constants.MOD_ID, "fluid_pump");
	
	public static final Block improve_components_w = HelperItems.setRegistryName(new BlockImproveComponents(maschine_white), Constants.MOD_ID, "improve_components_white");
	public static final Block improve_components_g = HelperItems.setRegistryName(new BlockImproveComponents(maschine_light_gray), Constants.MOD_ID, "improve_components_gray");
	public static final Block improve_components_b = HelperItems.setRegistryName(new BlockImproveComponents(maschine_black), Constants.MOD_ID, "improve_components_black");

	public static final Block radar = HelperItems.setRegistryName(new BlockRadar(maschine_black), Constants.MOD_ID, "radar");

	public static final Block life_support_system_w = HelperItems.setRegistryName(new BlockLifeSupportSystem(maschine_white), Constants.MOD_ID, "life_support_system_white");
	public static final Block life_support_system_g = HelperItems.setRegistryName(new BlockLifeSupportSystem(maschine_light_gray), Constants.MOD_ID, "life_support_system_gray");
	public static final Block life_support_system_b = HelperItems.setRegistryName(new BlockLifeSupportSystem(maschine_black), Constants.MOD_ID, "life_support_system_black");

	
	public static void registerBlocks(RegistryEvent.Register<Block> event)
	{
		IForgeRegistry<Block> r = event.getRegistry();
		r.registerAll(ion_collector_w, ion_collector_g, ion_collector_b);
		r.registerAll(opti_bench_w, opti_bench_g, opti_bench_b);
		r.registerAll(neon_furnace_w, neon_furnace_g, neon_furnace_b);
		r.registerAll(solar_panel_w, solar_panel_g, solar_panel_b);
		r.registerAll(crusher_w, crusher_g, crusher_b);
		r.register(sorter);
		r.registerAll(infusion_generator_w, infusion_generator_g, infusion_generator_b);
		r.registerAll(industrial_neon_furnace_w, industrial_neon_furnace_g, industrial_neon_furnace_b);
		r.registerAll(zentrifuge_w, zentrifuge_g, zentrifuge_b);
		r.registerAll(recycler_w, recycler_g, recycler_b);
		r.registerAll(opti_assembler_w, opti_assembler_g, opti_assembler_b);
		r.registerAll(gas_turbine_w, gas_turbine_g, gas_turbine_b);
		r.registerAll(rocket_lancher, entity_killer, entity_healer, entity_eater);
		r.registerAll(electro_magnet, external_core, fluid_pump);
		r.registerAll(water_turbine_w, water_turbine_g, water_turbine_b);
		r.registerAll(improve_components_w, improve_components_g, improve_components_b);
		r.registerAll(life_support_system_w, life_support_system_g, life_support_system_b);
		r.register(radar);
	}
	
	public static void registerItems(RegistryEvent.Register<Item> event)
	{
		IForgeRegistry<Item> r = event.getRegistry();
		
		r.register(item(ion_collector_w));
		r.register(item(ion_collector_g));
		r.register(item(ion_collector_b));
		r.registerAll(item(opti_bench_w), item(opti_bench_g), item(opti_bench_b));
		r.registerAll(item(neon_furnace_w), item(neon_furnace_g), item(neon_furnace_b));
		r.registerAll(item(solar_panel_w), item(solar_panel_g), item(solar_panel_b));
		r.registerAll(item(crusher_w), item(crusher_g), item(crusher_b));
		r.register(item(sorter));
		r.registerAll(item(infusion_generator_w), item(infusion_generator_g), item(infusion_generator_b));
		r.registerAll(item(industrial_neon_furnace_w), item(industrial_neon_furnace_g), item(industrial_neon_furnace_b));
		r.registerAll(item(zentrifuge_w), item(zentrifuge_g), item(zentrifuge_b));
		r.registerAll(item(recycler_w), item(recycler_g), item(recycler_b));
		r.registerAll(item(opti_assembler_w), item(opti_assembler_g), item(opti_assembler_b));
		r.registerAll(item(gas_turbine_w), item(gas_turbine_g), item(gas_turbine_b));
		r.registerAll(item(rocket_lancher), item(entity_killer), item(entity_healer), item(entity_eater));
		r.registerAll(item(electro_magnet), item(external_core), item(fluid_pump));
		r.registerAll(item(water_turbine_w), item(water_turbine_g), item(water_turbine_b));
		r.registerAll(item(improve_components_w), item(improve_components_g), item(improve_components_b));
		r.registerAll(item(life_support_system_w), item(life_support_system_g), item(life_support_system_b));
		r.register(item(radar));
	}	
	
	private final static Item item(Block bl)
	{
		return new BlockItem(bl, (new Item.Properties()).tab(FuturepackMain.tab_maschiens)).setRegistryName(HelperItems.getRegistryName(bl));
	}
}
