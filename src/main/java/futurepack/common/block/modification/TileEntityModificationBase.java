package futurepack.common.block.modification;

import java.util.List;
import java.util.Random;

import com.google.common.base.Predicate;
import com.mojang.math.Vector3f;

import futurepack.api.FacingUtil;
import futurepack.api.capabilities.CapabilityLogistic;
import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.ILogisticInterface;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.api.interfaces.tilentity.ITileClientTickable;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.block.logistic.LogisticNeonEnergyWrapper;
import futurepack.common.gui.inventory.GuiChipset;
import futurepack.common.item.ComputerItems;
import futurepack.common.modification.EnumChipType;
import futurepack.common.modification.thermodynamic.TemperatureControler;
import futurepack.common.modification.thermodynamic.TemperatureControler.ObjectContainer;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.depend.api.helper.HelperInventory;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.ParticleEngine;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.DustParticleOptions;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fml.DistExecutor;

public abstract class TileEntityModificationBase extends FPTileEntityBase implements ITileClientTickable, ITileServerTickable
{
	private final int CoolerFaktor = 5;
	
	private final InventoryModificationBase inventory;
	
//	private int passed=0;
	float heat = 0F;
	private float lastHeat = 0F;
	private int cooler = 0;
	
//	private int ticksDone = 0;
	private float ticksToDo = 0;
	
	public boolean on = true;
	public boolean edit = false;
	
	private boolean tooHot = false;
	public CapabilityNeon energy;
	
	
	public CapabilityNeon getEnergy()
	{
		return energy;
	}

	private TemperatureControler tempCon;
	private boolean isRunning=false;
	//protected float maxengine = 100F;
	
	public TileEntityModificationBase(BlockEntityType<? extends TileEntityModificationBase> type, InventoryModificationBase base, BlockPos pos, BlockState state) 
	{
		super(type, pos, state);
		energy = new CapabilityNeon(getMaxNE(), getEnergyType());
		
		inventory = base;	
		inventory.sync = this;
		
		ItemStack it = new ItemStack(ComputerItems.standart_core);
		it.setTag(new CompoundTag());
		it.getTag().putInt("core", 2);	
		inventory.core.set(0, it) ;		
		it= new ItemStack(ComputerItems.standart_ram);
		it.setTag(new CompoundTag());
		it.getTag().putInt("ram", 1);
		inventory.ram.set(0, it) ;		
		inventory.chipset.set(0, new ItemStack(ComputerItems.logic_chip));	
		
		tempCon = new TemperatureControler(this, base.getCoreSlots(), base.getRamSlots(), base.getChipSlots());
	}
	
	public abstract EnumEnergyMode getEnergyType();

	public TileEntityModificationBase(BlockEntityType<? extends TileEntityModificationBase> type, BlockPos pos, BlockState state) 
	{
		this(type, new InventoryModificationBase()
		{			
			@Override
			public int getRamSlots()
			{
				return 3;
			}
			
			@Override
			public int getCoreSlots()
			{
				return 1;
			}
			
			@Override
			public int getChipSlots()
			{
				return 9;
			}
		}, pos, state);
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		inventory.write(nbt);	
		return nbt;
	}
	
	public void writeDataSynced(CompoundTag nbt)
	{
		nbt.putFloat("heat", heat);
		nbt.putBoolean("on", this.on);
		nbt.put("energy", energy.serializeNBT());
		nbt.putBoolean("edit", edit);
	}
	
	public void readDataSynced(CompoundTag nbt)
	{
		heat = nbt.getFloat("heat");
		on = nbt.getBoolean("on");
		energy.deserializeNBT(nbt.getCompound("energy"));
		
		if(nbt.contains("edit"))
			edit = nbt.getBoolean("edit");
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		inventory.read(nbt);
	}
	

	
	
//	@Override
//	public boolean hasCapability(Capability<?> capability, EnumFacing facing) 
//	{
//		if(capability == CapabilityNeon.cap_NEON)
//		{
//			if(hasCapability(CapabilityLogistic.cap_LOGISTIC, facing))
//			{
//				ILogisticInterface log = getCapability(CapabilityLogistic.cap_LOGISTIC, facing);
//				return log.getMode(EnumLogisticType.ENERGIE) != EnumLogisticIO.NONE;
//			}
//			else
//				return true;
//		}
//		return super.hasCapability(capability, facing);
//	}
	
	@SuppressWarnings("unchecked")
	private LazyOptional<INeonEnergyStorage>[] optional = new LazyOptional[FacingUtil.VALUES.length+1];
	
	private LazyOptional<INeonEnergyStorage> getNeonCap(Direction face)
	{
		if(face == null)
		{
			if(this.optional[FacingUtil.VALUES.length]!=null)
				return this.optional[FacingUtil.VALUES.length];
			
			final LazyOptional<INeonEnergyStorage> optional;
			optional = LazyOptional.of(() -> energy);
			this.optional[FacingUtil.VALUES.length] = optional;
			return optional;
		}
		
		if(this.optional[face.get3DDataValue()] != null)
			return this.optional[face.get3DDataValue()];
		else
		{
			final LazyOptional<INeonEnergyStorage> optional;
			LazyOptional<ILogisticInterface> logistic = getCapability(CapabilityLogistic.cap_LOGISTIC, face);
			if(logistic.isPresent())
			{
				ILogisticInterface log = logistic.orElseThrow(NullPointerException::new);
				optional = LazyOptional.of( () -> new LogisticNeonEnergyWrapper(log, energy));
				logistic.addListener( l -> optional.invalidate());
			}
			else
			{
				optional = LazyOptional.of(() -> energy);
			}
			optional.addListener(o -> TileEntityModificationBase.this.optional[face.get3DDataValue()] = null); //when the object invlidates also remove this reference here;
			this.optional[face.get3DDataValue()] = optional;
			return optional;
		}
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(optional);
		super.setRemoved();
	}
	
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityNeon.cap_NEON)
		{
			return getNeonCap(facing).cast();
		}
		return super.getCapability(capability, facing);
	}
	
	public void updateNaturally(){}
	
	@Override
	public void tickServer(Level pLevel, BlockPos pPos, BlockState pState)
	{
		tick();
	}
	
	@Override
	public void tickClient(Level pLevel, BlockPos pPos, BlockState pState)
	{
		tick();
	}
	
	public final void tick() 
	{
		level.getProfiler().push("machine init");
		
		lastHeat = heat;
		
		updateEditState();
		if(edit)
		{
			level.getProfiler().pop();
			return;
		}
		
		if(getChipPower(EnumChipType.REDSTONE)>0)
		{
			on = level.getBestNeighborSignal(worldPosition)>0;
		}
		else
		{
			on = true;
		}
		Qualm();
			
		isRunning = false;
		if(canWork() && !isTooHot() && on)
		{
			//updateTile();
			int def = getDefaultPowerUsage();
			if(getEnergyType()==EnumEnergyMode.PRODUCE)
			{
				def =0;
				if(getEngine()>1 || energy.get() > 50)
				{
					level.getProfiler().push("NE transfer");
					HelperEnergyTransfer.powerLowestBlock(this);
					level.getProfiler().pop();
				}
					
			}
			if(energy.get() -  def>= 0)
			{
				float h = calcHeat();
				if(h>0 && isWorking())
				{
					if(!level.isClientSide)
						heat += h;
				}
				
				ticksToDo += this.getPureRam() /20F;
				
				
				level.getProfiler().popPush("machine tick");
				doUpdate(def, (int)ticksToDo);
			}
			isRunning = true;
		}	
		else if(level.isClientSide)
		{
			Random rand = level.random;
			
			if(rand.nextInt(20)==0)
			{
				double x=0,y=0,z=0, mx=0, my=0.1, mz=0;
				
				int side = rand.nextInt(5);
				if(side==0)
				{
					y=1.0625;
					x = rand.nextDouble();
					z = rand.nextDouble();
					mx = rand.nextDouble() * 0.05;
					mz = rand.nextDouble() * 0.05;
				}
				else
				{
					side--;
					y = rand.nextDouble();
					
					if(side>1)
					{
						side-=2;
						if(side==0)
						{
							x=-0.0625;
							mx = rand.nextDouble() * -0.05;
						}
						else
						{
							x=1.0625;
							mx = rand.nextDouble() * 0.05;
						}
						z = rand.nextDouble();
						mz = (rand.nextDouble() - rand.nextDouble()) * 0.1;
					}
					else
					{
						if(side==0)
						{
							z=-0.0625;
							mz = rand.nextDouble() * -0.05;
						}
						else
						{
							z=1.0625;
							mz = rand.nextDouble() * 0.05;
						}
						x = rand.nextDouble();
						mx = (rand.nextDouble() - rand.nextDouble()) * 0.1;
					}
						
				}
				
//				IBlockState state = world.getBlockState(pos);
				
				if(!isTooHot())
					spawnColoredFirework( worldPosition.getX()+x, worldPosition.getY()+y, worldPosition.getZ()+z, mx, my, mz);
			}
		}
		
		level.getProfiler().popPush("machine heatcalc");
		if(!level.isClientSide)
		{
			//new cooling
			float minHeat = calcMinHeat();
	
			//cool it
			if(heat>minHeat)
				heat -= Math.min( 0.5 + getChipPower(EnumChipType.DAMAGE_CONTROL), heat - minHeat);
				
			//heat it
			if(heat < minHeat)
				heat += 0.1F;
	
	
	
			/*if(heat < 0)
			{
				heat = 0;
			}*/
			
			if((lastHeat < 150 && heat >= 150) || (heat < 150 && lastHeat >= 150))
			{
				sendDataUpdatePackage(20);
			}
		}
		cooler = 0;
		level.getProfiler().popPush("machine natural tick");
		updateNaturally();
		level.getProfiler().pop();
	}
	
	private void updateEditState()
	{
		if(level.isClientSide)
			return;
		
		if(!edit)
			return;
		
		level.getProfiler().push("updateEditState");
		
		List<Player> l = level.getEntitiesOfClass(Player.class, new AABB(-10, -10, -10, 10, 10, 10).move(worldPosition), new Predicate<Player>()//getEntitiesWithinAABBExcludingEntity
		{			
			@Override
			public boolean apply(Player pl)
			{
				if(pl.containerMenu instanceof GuiChipset.ContainerChipset)
				{
					return ((GuiChipset.ContainerChipset)pl.containerMenu).tile == TileEntityModificationBase.this;
				}
				return false;
			}
		});
		edit = l.size() > 0;
		level.getProfiler().pop();
	}
	
	private final void doUpdate(int def, int ticksToDo)
	{
		int ticks;
		if(def > 0)
			ticks = Math.min(ticksToDo, energy.get() / def);
		else
			ticks = ticksToDo;
		
		if(ticks > 0)
		{
			if(energy.get() - def*ticks >= 0)
			{
				if(isWorking())
					if(!level.isClientSide)
						energy.use(def*ticks);
				
				updateTile(ticks);
				HelperInventory.doItemExtract(this);
				this.ticksToDo -= ticks;	
			}	
		}
	}
	
	public abstract void updateTile(int tickCount);
	
	public abstract boolean isWorking();
	
	public float getPureRam()
	{
		return inventory.getPureRam();
	}
	
//	public int getRamPower()
//	{
//		int ram = getPureRam();
//		ram = 20 -ram;
//		ram = ram < 0 ? 0 : ram;
//		return ram;
//	}
	
	@Deprecated
	public int getNeededCore()
	{
		return inventory.getNeededCore();
	}
	
	@Deprecated
	public int getCorePower()
	{
		return inventory.getCorePower();
	}
	
	@Deprecated
	public boolean canWork()
	{
		return inventory.canWork();
	}
	
	public boolean isTooHot()
	{
		float p = getChipPower(EnumChipType.DAMAGE_CONTROL);
		if(p>0)
		{
			if(heat > 100 * p)
			{
				tooHot = true;
			}
			else if(heat <= calcMinHeat())
			{
				tooHot = false;
			}
			return tooHot;
		}
		return false;
	}
	
	/*
	public boolean containsChip(int i)
	{
		for(ItemStack it : chipset)
		{
			if(it != null && it.getItemDamage() == i)
			{
				return true;
			}
		}
		return false;
	}*/
	
	public float getChipPower(EnumChipType i)
	{
		return inventory.getChipPower(i);
	}
	
	public void Qualm()
	{
		level.getProfiler().push("heatEffects");
		
		//if(!world.isRemote)
		{
			@SuppressWarnings("unchecked")
			ObjectContainer<ItemStack>[] cCore = new ObjectContainer[inventory.getCoreSlots()];
			for(int i=0;i<cCore.length;i++)
			{
				cCore[i] = new ObjectContainer<ItemStack>(inventory.core.get(i));
			}
			@SuppressWarnings("unchecked")
			ObjectContainer<ItemStack>[] cRam = new ObjectContainer[inventory.getRamSlots()];
			for(int i=0;i<cRam.length;i++)
			{
				cRam[i] = new ObjectContainer<ItemStack>(inventory.ram.get(i));
			}				
			@SuppressWarnings("unchecked")
			ObjectContainer<ItemStack>[] cChipset = new ObjectContainer[inventory.getChipSlots()];
			for(int i=0;i<cChipset.length;i++)
			{
				cChipset[i] = new ObjectContainer<ItemStack>(inventory.chipset.get(i));
			}
			
			tempCon.applyHeatDamage(cCore, cRam, cChipset);
			
			for(int i=0;i<cCore.length;i++)
			{
				inventory.core.set(i, cCore[i].object);
			}
			for(int i=0;i<cRam.length;i++)
			{
				inventory.ram.set(i, cRam[i].object);
			}				
			for(int i=0;i<cChipset.length;i++)
			{
				inventory.chipset.set(i, cChipset[i].object);
			}
		}
		if(heat >= 150 && level.isClientSide)
		{
			level.addParticle(ParticleTypes.LARGE_SMOKE, worldPosition.getX() + level.random.nextFloat(), worldPosition.getY()+1F, worldPosition.getZ()+ level.random.nextFloat(), 0, 0, 0);
		}
		level.getProfiler().pop();
	}
	
	public float calcHeat()
	{
		return getNeededCore() - getCorePower();// + 0.1F;
	}
	
	public float calcMinHeat()
	{
		level.getProfiler().push("calcMinHeat");
		//Biome gen = world.getBiome(pos);
		//float temp = gen.getTemperature(pos);
		//float min = temp * 25F;	
		float min = 20.0F;
		if(isRunning && isWorking())
		{
			float c = getCorePower();
			if(c>0)
				min += getNeededCore() / c * 80F;
			min -= cooler*CoolerFaktor;
		}
			
		level.getProfiler().pop();
		return min;
	}
	
	public int getDefaultPowerUsage()
	{
		int core = getCorePower();
		int need = getNeededCore();
		return (core + need) / 2;
	}
	
	public final InventoryModificationBase getInventory() 
	{
		return inventory;
	}

	public int getEngine()
	{
		double d1 = energy.get();
		double d2 = energy.getMax();
		return (int) (d1/d2 * 10D);
	}
	
	public int getMaxNE() 
	{
		return getEnergyType()==EnumEnergyMode.USE ? 1000 : (getEnergyType()==EnumEnergyMode.PRODUCE ? 4000 : 1000);
	}
	
//	@Override
//	public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newSate)
//	{
//		return (oldState.getBlock() != newSate.getBlock());
//	}

	public TemperatureControler getTemperatureControloer()
	{
		return tempCon;
	}
	
	public float getHeat()
	{
		return heat;
	}
	
	/**
	 * External cooling for machine
	 * @param amount maximum of heat-points to cool (1 point is 5�C of minheat)
	 * @param min minimum of Temperature, the cooler can work
	 * @return used heat-points; between 0 and 'amount'
	 */
	public float setHeatCool(float amount, float min)
	{
		//if(world.isRemote && inventory.guiOpen)
		//	return 0F;
		
		//Deactivate all coolers, if machine is offline
		if(!(isRunning && isWorking()))
		{
			return 0;
		}
		
		float newAmount = 0;
		
		//Current Data
		float minheat = calcMinHeat();
		//float ttt = calcMinHeat();
		
		//Can Medium cool the machine
		if(heat >= min)			
		{
			//Cooling mode
			if(minheat >= min)	
			{
				//Cold OC : affect minheat and cooler; high temperature raise on higher OC
				
				//find perfekt cool amount
				newAmount = (minheat - min) / CoolerFaktor;
				if(newAmount > amount)
					newAmount = amount;
				
				//get new minheat 
				minheat -= newAmount * CoolerFaktor;
				cooler += newAmount;
	
				//if new minheat still over min, the full amount is used
				
				//cool it -- overheating is caused here
				float delta = heat - minheat;	
				if(delta > 0)
				{	
					delta = amount > delta ? delta : amount;
					if(!level.isClientSide)
						heat -= delta;
				}
				
				//System.out.printf("Cold OC because minheat=%f using %f heatpoints!\n",ttt,newAmount);
			}
			else
			{
				//Hot OC : machine is hotter then minheat; nearly non temperature raise on higher OC
				
				//find perfekt cool amount
				newAmount = (heat - min);
				if(newAmount > amount)
					newAmount = amount;
				
				//float nmin = min - newAmount * CoolerFaktor;
				
				//cool it -- overheating is caused here
				float delta = heat - min;	
				if(delta > 0)
				{
					delta = amount > delta ? delta : amount;
					if(!level.isClientSide)
						heat -= delta;
				}
				
				//System.out.printf("Hot OC because minheat=%f using %f heatpoints!\n",ttt,newAmount);
			}
		}
		
		//return cooled amount
		return newAmount;
	}
	
	@OnlyIn(Dist.CLIENT)
	private void spawnColoredFirework(double x, double y, double z, double mx, double my, double mz)
	{
		DistExecutor.unsafeRunWhenOn(Dist.CLIENT, ()-> () -> 
		{
			ParticleEngine aman = Minecraft.getInstance().particleEngine;
			if(on)
			{
//				SimpleAnimatedParticle sp = (SimpleAnimatedParticle) new FireworkParticle.SparkFactory(null).makeParticle(ParticleTypes.FIREWORK, world, x, y, z, mx, my, mz);
////				sp.setAlphaF(0.5F);
//				sp.setColorFade(0x790c0c);
//				sp.setColor(0xd5872e);
//				sp.multipleParticleScaleBy(0.4F);
//				aman.addEffect(sp);
				
				level.addParticle(ParticleTypes.FIREWORK, x, y, z, mx, my, mz);
			}
			else
			{
//				RedstoneParticle red = (RedstoneParticle) new RedstoneParticle.Factory().makeParticle(new RedstoneParticleData(1F, 0F, 0F, 1F), world, x, y, z, 1F, 0.1F, 0.1F);
//				red.multipleParticleScaleBy(0.4F);
//				aman.addEffect(red);
				level.addParticle(new DustParticleOptions(new Vector3f(1F, 0F, 0F), 1F), x, y, z, mx, my, mz);
			}
		});
	}
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	public void setEnergy(int energy)
	{
		this.energy.set(energy);
	}

}
