package futurepack.common.block.multiblock;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import futurepack.api.interfaces.IDeepCoreLogic;
import futurepack.api.interfaces.IItemDeepCoreLens;
import futurepack.common.FPLog;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.misc.TileEntityBedrockRift;
import futurepack.common.modification.EnumChipType;
import futurepack.common.recipes.multiblock.MultiblockPatterns;
import futurepack.depend.api.helper.HelperHologram;
import futurepack.depend.api.helper.HelperInventory;
import futurepack.depend.api.helper.HelperInventory.SlotContent;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Vec3i;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.pattern.BlockInWorld;
import net.minecraft.world.level.block.state.pattern.BlockPattern;
import net.minecraft.world.level.block.state.pattern.BlockPattern.BlockPatternMatch;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;

public class DeepCoreLogic implements INBTSerializable<CompoundTag>, IDeepCoreLogic
{
	private final TileEntityDeepCoreMinerMain main;
	private final Direction direction, forwards;
	
	private BlockState[][] usedBlocks;
	private CompoundTag[][] tileData;
	
	private int chest1, chest2, chest3;	
	private float progress, durability; 
	
	public DeepCoreLogic(TileEntityDeepCoreMinerMain tileEntityDeepCoreMinerMain, CompoundTag compoundTag)
	{
		main = tileEntityDeepCoreMinerMain;
		direction = Direction.from3DDataValue(compoundTag.getByte("direction"));
		forwards = direction.getCounterClockWise();
		deserializeNBT(compoundTag);
	}

	public DeepCoreLogic(TileEntityDeepCoreMinerMain tileEntityDeepCoreMinerMain, BlockPatternMatch helper)
	{
		main = tileEntityDeepCoreMinerMain;
		forwards = helper.getForwards();
		direction = forwards.getClockWise();
		
		if(!helper.getFrontTopLeft().equals(main.getBlockPos().above(2)))
		{
			throw new RuntimeException("Not the Main Block " + main.getBlockPos() + " " + helper.getFrontTopLeft());
		}
		
		storeUsedBlocks(helper);
	}

	public void update(int ticks)
	{
		IItemDeepCoreLens lens = getLense();
		if(lens != null)
		{
			for(int i=0;i<ticks;i++)
			{
				//TODO: we need to improve this! The for loop just wastes performance
				if(lens.updateProgress(getLenseStack(), this))
				{
					damageLens();
					main.setChanged();
				}
			}
		}
		else
		{
			durability = 0F;
			ItemStack st = main.internInventory.getStackInSlot(0);
			if(!st.isEmpty())
			{
				ItemStack newLense = st.split(1);
				main.internInventory.setStackInSlot(4, newLense);
				durability = 1F;
			}
		}
	}
	
	public boolean isWorking()
	{
		boolean b =  isRiftReady() && getLense()!=null && getLense().isWorking(getLenseStack(), this);
		if(!b && !main.getLevel().isClientSide)
			setProgress(0F);
		return b;
	}

	public int getNeededEnergie()
	{
		return getLense()==null ? 0 : getLense().getNeededEnergie(getLenseStack(), this);
	}
	
	public void restoreUsedBlocks()
	{
		BlockPattern miner = MultiblockPatterns.getDeepMinerPattern();		
		
		for(int w=0;w<miner.getWidth();w++)
		{
			for(int h=0;h<miner.getHeight();h++)
			{
				if(usedBlocks[w][h] != null)
				{
					BlockPos pos = translateOffset(w, h, 0).above(2);
					BlockEntity old = main.getLevel().getBlockEntity(pos);
					
					main.getLevel().setBlockAndUpdate(pos, usedBlocks[w][h]);
					
					BlockEntity tile = main.getLevel().getBlockEntity(pos);
					if(old!=null && old==tile && old.getType() != FPTileEntitys.DEEPCORE_MAIN)
					{
						main.getLevel().removeBlockEntity(pos);
						main.getLevel().setBlockAndUpdate(pos, usedBlocks[w][h]);
						tile = main.getLevel().getBlockEntity(pos);
					}
					
					if(tile!=null)
					{
						if(tileData[w][h]!=null)
						{
							tileData[w][h].putInt("x", pos.getX());
							tileData[w][h].putInt("y", pos.getY());
							tileData[w][h].putInt("z", pos.getZ());
							tile.load(tileData[w][h]);
						}
						if(w==2) //the chests
						{					
							IItemHandler handler = HelperInventory.getHandler(tile, Direction.UP);
							if(handler!=null)
							{
								HelperInventory.transferItemStacks(main.getDeepCoreInventory(false), handler);
							}				
						}
					}
				}	
				else
				{
					FPLog.logger.warn("DeepCoreMiner had null BlockStates");
				}
			}
		}
		IItemHandler deep = main.getDeepCoreInventory(false);
		List<SlotContent> list = new ArrayList<SlotContent>(deep.getSlots());
		for(int i=0;i<deep.getSlots();i++)
		{	
			if(!deep.getStackInSlot(i).isEmpty())	
			{
				list.add(new SlotContent(deep, i));
			}
		}
		List<SlotContent> removed = HelperInventory.ejectItemsIntoWorld(main.getLevel(), main.getBlockPos().relative(direction, -1), list);
		removed.forEach(s -> s.remove());
	}
	
	 private BlockPos translateOffset(int palmOffset, int thumbOffset, int fingerOffset)
	 {
		 if (forwards != Direction.UP && forwards != Direction.DOWN)
		 {
			 Vec3i Vector3i = new Vec3i(forwards.getStepX(), forwards.getStepY(), forwards.getStepZ());
			 Vec3i Vector3i1 = new Vec3i(0, 1, 0);
			 Vec3i Vector3i2 = Vector3i.cross(Vector3i1);
			 return main.getBlockPos().offset(Vector3i2.getX() * palmOffset + Vector3i.getX() * fingerOffset, -thumbOffset + Vector3i2.getY() * palmOffset + Vector3i.getY() * fingerOffset, Vector3i2.getZ() * palmOffset + Vector3i.getZ() * fingerOffset);
		 }
		 else
		 {
			 throw new IllegalArgumentException("Invalid forwards & up combination");
		 }
	 } 
	
	public void storeUsedBlocks(BlockPatternMatch helper)
	{
		BlockPattern miner = MultiblockPatterns.getDeepMinerPattern();
		usedBlocks = new BlockState[miner.getWidth()][miner.getHeight()];
		tileData = new CompoundTag[miner.getWidth()][miner.getHeight()];
		for(int w=0;w<miner.getWidth();w++)
		{
			for(int h=0;h<miner.getHeight();h++)
			{
				BlockInWorld bws = helper.getBlock(w, h, 0);
				BlockState state = bws.getState();
				usedBlocks[w][h] = state;
				if(bws.getEntity()!=null && state.getBlock()!=MultiblockBlocks.deepcore_miner)
				{
					tileData[w][h] = bws.getEntity().saveWithFullMetadata();
					if(w==2) //the chests
					{	
						BlockEntity tile = bws.getEntity();
						LazyOptional<IItemHandler> opt = tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, direction.getOpposite());
						if(opt.isPresent())
						{
							IItemHandler handler = opt.orElse(null);
							if(handler instanceof IItemHandlerModifiable)
							{
								switch(h)
								{
								case 0:
									chest1 = handler.getSlots();
									break;
								case 1:
									chest2 = handler.getSlots();
									break;
								case 2:
									chest3 = handler.getSlots();
									break;
								default:
									break;
								}
							}
						}
					}
				}
			}
		}
	}

	@Override
	public CompoundTag serializeNBT()
	{
		CompoundTag nbt = new CompoundTag();
		nbt.putByte("direction", (byte) direction.ordinal());

		nbt.putInt("x", usedBlocks.length);
		nbt.putInt("y", usedBlocks[0].length);
		for(int i=0;i<usedBlocks.length;i++)
		{
			for(int j=0;j<usedBlocks[i].length;j++)
			{
				nbt.putString(i+"-"+j, saveBlockState(usedBlocks[i][j]));
				if(tileData[i][j] != null)
				{
					nbt.put("tile"+i+"-"+j, tileData[i][j]);
				}
			}
		}
		nbt.putIntArray("chests", new int[]{chest1, chest2, chest3});
		nbt.putFloat("progress", progress);
		
		return nbt;
	}
	
	private String saveBlockState(BlockState state)
	{
		return HelperHologram.toStateString(state);
	}

	@Override
	public void deserializeNBT(CompoundTag nbt)
	{
		usedBlocks = new BlockState[nbt.getInt("x")][nbt.getInt("y")];
		tileData = new CompoundTag[nbt.getInt("x")][nbt.getInt("y")];
		for(int i=0;i<usedBlocks.length;i++)
		{
			for(int j=0;j<usedBlocks[i].length;j++)
			{
				usedBlocks[i][j] = loadBlockState(nbt.getString(i+"-"+j));
				
				if(nbt.contains("tile"+i+"-"+j))
				{
					tileData[i][j] = nbt.getCompound("tile"+i+"-"+j);
				}
			}
		}	
		int[] chests = nbt.getIntArray("chests");
		chest1 = chests[0];
		chest2 = chests[1];
		chest3 = chests[2];
		progress = nbt.getFloat("progress");
	}
	
	private BlockState loadBlockState(String s)
	{
		return HelperHologram.fromStateString(s);
	}

	@Override
	public Direction getFacing()
	{
		return direction;
	}
	
	@Override
	public TileEntityDeepCoreMinerMain  getTileEntity()
	{
		return main;
	}
	
	public boolean needSupport()
	{
		return getLense()==null ? false : getLense().isSupportConsumer(getLenseStack(), this);
	}
	
	@Override
	@Nullable
	public TileEntityBedrockRift getRift()
	{
		BlockPos rift = main.getBlockPos();
		rift = rift.relative(direction).below();
		BlockEntity tile = main.getLevel().getBlockEntity(rift);
		if(tile==null)
			return null;
		else if(tile instanceof TileEntityBedrockRift)
			return (TileEntityBedrockRift) tile;
		
		return null;
	}
	
	@Nullable
	public IItemDeepCoreLens getLense()
	{
		ItemStack stack = getLenseStack();
		if(stack.isEmpty())
			return null;
		else if(stack.getItem() instanceof IItemDeepCoreLens)
			return (IItemDeepCoreLens) stack.getItem();
		
		return null;
	}
	
	@Nullable
	public ItemStack getLenseStack()
	{
		return main.internInventory.getStackInSlot(4);
	}

	private void damageLens()
	{
		ItemStack lense = getLenseStack();
		CompoundTag nbt = lense.getOrCreateTagElement("lense");
		int damage = nbt.getInt("damage");
		int maxDamage = getMaxDurability();		
		float ki = main.getChipPower(EnumChipType.AI) * 0.2F;
		if(main.getLevel().random.nextFloat() > ki)
		{
			damage++;
			durability = (float)(maxDamage-damage) / (float)maxDamage;
			
			if(damage>=maxDamage)
				lense.shrink(1);
			
			nbt.putInt("damage", damage);
		}
	}	
	
	@Override
	public int getTotalChestSize()
	{
		return chest1 + chest2 + chest3;
	}

	@Override
	public float getProgress()
	{
		return progress;
	}

	@Override
	public void setProgress(float f)
	{
		progress = f;
	}
	
	public int getMaxDurability()
	{
		IItemDeepCoreLens lens = getLense();
		int f = lens.getMaxDurability(getLenseStack(), this);
		return f;
	}
	
	public ItemStack getChestItem( int h)
	{
		BlockState state = usedBlocks[2][h];
		return new ItemStack(state.getBlock(), 1);
	}

	public float getDurability()
	{
		return durability;
	}

	public void setDurability(float f)
	{
		this.durability = f;
	}
}
