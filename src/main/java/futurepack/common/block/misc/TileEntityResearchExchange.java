package futurepack.common.block.misc;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.research.Research;
import futurepack.common.research.ResearchManager;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityResearchExchange extends FPTileEntityBase 
{
	private Set<Research> stored = new TreeSet<>();
	
	public TileEntityResearchExchange(BlockEntityType<? extends TileEntityResearchExchange> type, BlockPos pos, BlockState state) 
	{
		super(type, pos, state);
	}
	
	public TileEntityResearchExchange(BlockPos pos, BlockState state) 
	{
		this(FPTileEntitys.RESEARCH_EXCHANGE, pos, state);
	}

	public void onInteract(Player pl)
	{
		CustomPlayerData cd = CustomPlayerData.getDataFromPlayer(pl);	
		
		stored.forEach(cd::addResearchUnsafe);
		
		for(String rid : cd.getAllResearches())
		{
			Research r = ResearchManager.getResearchOrElse(rid, null);
			if(r!=null)
			{
				stored.add(r);
			}
		}
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag compound) 
	{
		ListTag list = new ListTag();
		for(Research r : stored)
		{
			list.add(StringTag.valueOf(r.getName()));
		}
		compound.put("researches", list);
		return super.writeDataUnsynced(compound);
	}
	
	@Override
	public void readDataUnsynced(CompoundTag compound) 
	{
		ListTag list = compound.getList("researches", StringTag.valueOf("").getId());
		ArrayList<Research> arraylist = new ArrayList<Research>(list.size());
		for(int i=0;i<list.size();i++)
		{
			arraylist.add(ResearchManager.getResearch(list.getString(i)));
		}
		stored.clear();
		stored.addAll(arraylist);
		super.readDataUnsynced(compound);
	}
}
