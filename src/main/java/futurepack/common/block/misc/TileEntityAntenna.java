package futurepack.common.block.misc;

import java.util.function.Consumer;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.INetworkUser;
import futurepack.api.interfaces.tilentity.ITileAntenne;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPBlockSelector;
import futurepack.common.FPSelectorHelper;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.network.EventWirelessFunk;
import futurepack.common.network.NetworkManager;
import futurepack.common.network.NetworkManager.Wireless;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;

public class TileEntityAntenna extends FPTileEntityBase implements ITileAntenne, INetworkUser, ITilePropertyStorage
{
	private int frequenz = 0;
	private int lastRange = -1;
	
	private final Consumer<EventWirelessFunk> method; 
	
	public TileEntityAntenna(BlockPos pos, BlockState state)
	{
		this(FPTileEntitys.ANTENNA, pos, state);
	}
	
	public TileEntityAntenna(BlockEntityType<? extends TileEntityAntenna> type, BlockPos  pos, BlockState state)
	{
		super(type, pos, state);
		method = this::onFunkEvent;
	}
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		if(hasLevel())
		{
			Wireless.registerWirelessTile(method, getLevel());
		}
	}
	
	@Override
	public void onChunkUnloaded()
	{
		Wireless.unregisterWirelessTile(method, getLevel());
		super.onChunkUnloaded();
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		nbt.putInt("frequenz", frequenz);
		return super.writeDataUnsynced(nbt);
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		frequenz = nbt.getInt("frequenz");
		super.readDataUnsynced(nbt);
	}
	
	public void onFunkEvent(EventWirelessFunk event)
	{
		if(!level.isClientSide)
		{	
			if(this.isRemoved())
			{
				Wireless.unregisterWirelessTile(method, getLevel());
				return;
			}
			if(!level.hasChunkAt(worldPosition))
			{
				Wireless.unregisterWirelessTile(method, getLevel());
				return;
			}
			
			if(event.canRecive(this))
			{
				if(event.frequenz == this.frequenz)
				{
					processMatchingPacket(event);
				}		
			}
		}
		else
		{
			Wireless.unregisterWirelessTile(method, getLevel());
		}
	}

	
	protected void processMatchingPacket(EventWirelessFunk event)
	{
		float dis = (float) Math.sqrt(event.source.distSqr(getSenderPosition()));
		if(dis < event.range + this.getRange())
		{
			NetworkManager.sendPacketUnthreaded(this, event.objPassed, event.networkDepth);
		}
	}
	
	@Override
	public boolean isNetworkAble()
	{
		return true;
	}

	@Override
	public boolean isWire()
	{
		return false;
	}

	@Override
	public int getRange()
	{
		if(level.isClientSide)
			return lastRange;
		else
		{
			FPBlockSelector sp = FPSelectorHelper.getSelector(level, getSenderPosition(), FPBlockSelector.base);//no TileEntites, so can be off thread
			return lastRange = sp.getBlocks(getSenderPosition(), Material.METAL);
		}
	}

	@Override
	public void onFunkPacket(PacketBase pkt)
	{
		Wireless.sendEvent(new EventWirelessFunk(getSenderPosition(), frequenz, getRange(), pkt, pkt.getNetworkDepth()+1), level);
	}

	@Override
	public void postPacketSend(PacketBase pkt)
	{
		
	}

	@Override
	public Level getSenderWorld()
	{
		return this.level;
	}
	
	@Override
	public BlockPos getSenderPosition()
	{
		return this.worldPosition;
	}

	@Override
	public int getProperty(int id)
	{
		switch (id) 
		{
		case 0:
			return frequenz;
		case 1:
			if(lastRange==0)
				return getRange();
			return lastRange;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			frequenz = value;
			break;
		case 1:
			lastRange = value;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount()
	{
		return 2;
	}

	public int getFrequenz()
	{
		return frequenz;
	}

	public void setFrequenz(int integer)
	{
		frequenz = integer;
	}
}
