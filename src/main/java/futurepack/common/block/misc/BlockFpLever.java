package futurepack.common.block.misc;

import net.minecraft.core.BlockPos;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.LeverBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockFpLever extends LeverBlock
{
    protected static final VoxelShape FPLEVER_NORTH_AABB = Shapes.box(0.0625D, 0.0625D, 0.75D, 0.9375D, 0.9375D, 1.0D);
    protected static final VoxelShape FPLEVER_SOUTH_AABB = Shapes.box(0.0625D, 0.0625D, 0.0D, 0.9375D, 0.9375D, 0.25D);
    protected static final VoxelShape FPLEVER_WEST_AABB = Shapes.box(0.75D, 0.0625D, 0.0625D, 1.0D, 0.9375D, 0.9375D);
    protected static final VoxelShape FPLEVER_EAST_AABB = Shapes.box(0.0D, 0.0625D, 0.0625D, 0.25D, 0.9375D, 0.9375D);
    
    protected static final VoxelShape FPLEVER_UP_AABB = Shapes.box(0.0625D, 0.0D, 0.0625D, 0.9375D, 0.25D, 0.9375D);
    protected static final VoxelShape FPLEVER_DOWN_AABB = Shapes.box(0.0625D, 0.75D, 0.0625D, 1.0D, 0.9375D, 0.9375D);

	
	public BlockFpLever(Block.Properties props)
	{
		super(props);
//		setCreativeTab(ItemGroup.REDSTONE);
//		setSoundType(SoundType.WOOD);
	}
	
	@Override
	public InteractionResult use(BlockState state, Level worldIn, BlockPos pos, Player player, InteractionHand hand, BlockHitResult hit)
	{
	      state = state.cycle(POWERED);
	      boolean flag = state.getValue(POWERED);
	      if (worldIn.isClientSide) 
	      {
	         return InteractionResult.SUCCESS;
	      } 
	      else 
	      {
	         worldIn.setBlock(pos, state, 3);
	         
	         if(flag)
	        	 worldIn.playSound((Player)null, pos, SoundEvents.FENCE_GATE_OPEN, SoundSource.BLOCKS, 0.3F, 1.5F);
	         else
	        	 worldIn.playSound((Player)null, pos, SoundEvents.FENCE_GATE_CLOSE, SoundSource.BLOCKS, 0.3F, 1.5F);
	         worldIn.updateNeighborsAt(pos, this);
	         worldIn.updateNeighborsAt(pos.relative(getConnectedDirection(state).getOpposite()), this);
	         return InteractionResult.SUCCESS;
	      }
	   }
	
//    public AxisAlignedBB getBoundingBox(IBlockState state, IWorldReader source, BlockPos pos)
//    {
//        switch ((BlockLever.EnumOrientation)state.getValue(FACING))
//        {
//            case EAST:
//            default:
//                return FPLEVER_EAST_AABB;
//            case WEST:
//                return FPLEVER_WEST_AABB;
//            case SOUTH:
//                return FPLEVER_SOUTH_AABB;
//            case NORTH:
//                return FPLEVER_NORTH_AABB;
//            case UP_Z:
//            case UP_X:
//                return FPLEVER_UP_AABB;
//            case DOWN_X:
//            case DOWN_Z:
//                return FPLEVER_DOWN_AABB;
//        }
//    }
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext sel)
	{
	      switch(state.getValue(FACE)) {
	      case FLOOR:
	         switch(state.getValue(FACING).getAxis()) {
	         case X:
	         case Z:
	         default:
	            return FPLEVER_UP_AABB;
	         }
	      case WALL:
	         switch(state.getValue(FACING)) {
	         case EAST:
	            return FPLEVER_EAST_AABB;
	         case WEST:
	            return FPLEVER_WEST_AABB;
	         case SOUTH:
	            return FPLEVER_SOUTH_AABB;
	         case NORTH:
	         default:
	            return FPLEVER_NORTH_AABB;
	         }
	      case CEILING:
	      default:
	         switch(state.getValue(FACING).getAxis()) {
	         case X:
	         case Z:
	         default:
	            return FPLEVER_DOWN_AABB;
	         }
	      }
	   }
    
}
