package futurepack.common.block.misc;

import futurepack.api.interfaces.IBlockBothSidesTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import net.minecraft.core.Direction;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class BlockGravityPulser extends BlockRotateableTile implements IBlockBothSidesTickingEntity<TileEntityPulsit>
{
	public BlockGravityPulser(Block.Properties props) 
	{
		super(props);
	}

	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		Direction face = context.getNearestLookingDirection().getOpposite();
		return super.getStateForPlacement(context).setValue(FACING, face);
	}

	@Override
	public BlockEntityType<TileEntityPulsit> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.GRAVITY_PULSER;
	}
}
