package futurepack.common.block.misc;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateable;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import futurepack.depend.api.interfaces.IItemMetaSubtypes;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;

public class BlockRFtoNEConverted extends BlockRotateable implements IItemMetaSubtypes, IBlockServerOnlyTickingEntity<TileEntityRFtoNEConverter>
{
	
	public BlockRFtoNEConverted(Block.Properties props)
	{
		super(props);
	}

	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.RF2NE_CONVERTER.openGui(pl, pos);
		}
		return InteractionResult.SUCCESS;
	}
	
	@Override
	public int getMaxMetas()
	{
		return 12;
	}

	@Override
	public String getMetaName(int meta) 
	{
		return "rf_ne_converter_"+meta/4;
	}

	@Override
	public BlockEntityType<TileEntityRFtoNEConverter> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.RF2NE_CONVERTER;
	}


}
