package futurepack.common.block.misc;

import futurepack.api.Constants;
import futurepack.common.FPTileEntitys;
import futurepack.common.FuturepackMain;
import futurepack.common.block.BlockLinkedLight;
import futurepack.common.block.deco.DecoBlocks;
import futurepack.common.block.inventory.BlockModul;
import futurepack.common.block.inventory.InventoryBlocks;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.material.MaterialColor;
import net.minecraftforge.common.PlantType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class MiscBlocks 
{
	
	public static final Block quantanium = HelperItems.setRegistryName(new BlockQuantanium(Block.Properties.of(Material.METAL, DyeColor.PURPLE).strength(3F, 5F).lightLevel(BlockQuantanium.GET_LIGHT)), Constants.MOD_ID, "quantanium");
	public static final Block extern_cooler = HelperItems.setRegistryName(new BlockExternCooler(Block.Properties.of(Material.METAL).strength(5F, 10F).noOcclusion()), Constants.MOD_ID, "extern_cooler");
	public static final Block bedrock_rift = HelperItems.setRegistryName(new BlockBedrockRift(), Constants.MOD_ID, "bedrock_rift");
	public static final Block falling_tree = HelperItems.setRegistryName(new BlockFallingTree(Block.Properties.copy(Blocks.BEDROCK).sound(SoundType.WOOD).noOcclusion().noDrops()), Constants.MOD_ID, "falling_tree");
	
	public static final Block antenna_white = HelperItems.setRegistryName(new BlockAntenna(InventoryBlocks.maschine_white), Constants.MOD_ID, "antenna_white");
	public static final Block antenna_gray = HelperItems.setRegistryName(new BlockAntenna(InventoryBlocks.maschine_light_gray), Constants.MOD_ID, "antenna_gray");
	public static final Block antenna_black = HelperItems.setRegistryName(new BlockAntenna(InventoryBlocks.maschine_black), Constants.MOD_ID, "antenna_black");
	
	public static final Block teleporter = HelperItems.setRegistryName(new BlockTeleporter(InventoryBlocks.simple_white, false, false), Constants.MOD_ID, "teleporter");
	public static final Block teleporter_up = HelperItems.setRegistryName(new BlockTeleporter(Block.Properties.of(Material.METAL).strength(3F, 5F), true, false), Constants.MOD_ID, "teleporter_up");
	public static final Block teleporter_down = HelperItems.setRegistryName(new BlockTeleporter(Block.Properties.of(Material.METAL).strength(3F, 5F), false, true), Constants.MOD_ID, "teleporter_down");
	public static final Block teleporter_both = HelperItems.setRegistryName(new BlockTeleporter(Block.Properties.of(Material.METAL).strength(3F, 5F), true, true), Constants.MOD_ID, "teleporter_both");
	
	public static final Block beam = HelperItems.setRegistryName(new BlockDungeonTeleporter(Block.Properties.of(Material.METAL, MaterialColor.COLOR_BLACK).strength(3F, 5F).noDrops(), false, false), Constants.MOD_ID, "beam");
	public static final Block beam_up = HelperItems.setRegistryName(new BlockDungeonTeleporter(Block.Properties.of(Material.METAL).strength(3F, 5F).noDrops(), true, false), Constants.MOD_ID, "beam_up");
	public static final Block beam_down = HelperItems.setRegistryName(new BlockDungeonTeleporter(Block.Properties.of(Material.METAL).strength(3F, 5F).noDrops(), false, true), Constants.MOD_ID, "beam_down");
	public static final Block beam_both = HelperItems.setRegistryName(new BlockDungeonTeleporter(Block.Properties.of(Material.METAL).strength(3F, 5F).noDrops(), true, true), Constants.MOD_ID, "beam_both");
	
	public static final Block sapling_holder_plains = HelperItems.setRegistryName(new BlockSaplingHolder(Block.Properties.of(Material.METAL, MaterialColor.PLANT).strength(3F, 5F), PlantType.PLAINS), Constants.MOD_ID, "sapling_holder_plains");
	public static final Block sapling_holder_desert = HelperItems.setRegistryName(new BlockSaplingHolder(Block.Properties.of(Material.METAL, MaterialColor.SAND).strength(3F, 5F), PlantType.DESERT), Constants.MOD_ID, "sapling_holder_desert");
	public static final Block sapling_holder_nether = HelperItems.setRegistryName(new BlockSaplingHolder(Block.Properties.of(Material.METAL, MaterialColor.COLOR_BROWN).strength(3F, 5F), PlantType.NETHER), Constants.MOD_ID, "sapling_holder_nether");
	
	public static final Block neon_engine = HelperItems.setRegistryName(new BlockNeonEngine(Block.Properties.copy(DecoBlocks.color_iron_blue).noOcclusion()), Constants.MOD_ID, "neon_engine"); 
	public static final Block gravity_pulser = HelperItems.setRegistryName(new BlockGravityPulser(Block.Properties.of(Material.STONE, MaterialColor.TERRACOTTA_LIGHT_BLUE).strength(5F, 10F)), Constants.MOD_ID, "gravity_pulser");
	public static final Block magnet = HelperItems.setRegistryName(new BlockMagnet(Block.Properties.of(Material.METAL).strength(5F, 10F)), Constants.MOD_ID, "magnet");
	public static final Block rs_timer = HelperItems.setRegistryName(new BlockRsTimer(Block.Properties.of(Material.DECORATION, MaterialColor.COLOR_LIGHT_GRAY).sound(SoundType.METAL).strength(3F, 5F)), Constants.MOD_ID, "rs_timer");
	public static final Block force_field = HelperItems.setRegistryName(new BlockForceField(Block.Properties.copy(Blocks.BEDROCK).noOcclusion().isSuffocating((state,world,pos) -> false).isViewBlocking((state,world,pos) -> false)), Constants.MOD_ID, "force_field");
	public static final Block dungeon_spawner = HelperItems.setRegistryName(new BlockDungeonSpawner(Block.Properties.copy(Blocks.BEDROCK).noOcclusion()), Constants.MOD_ID, "dungeon_spawner");
	
	public static final Block wirelessRedstoneReceiver = HelperItems.setRegistryName(new BlockWirelessRedstoneReceiver(InventoryBlocks.simple_black), Constants.MOD_ID, "wr_receiver");
	public static final Block wirelessRedstoneTransmitter = HelperItems.setRegistryName(new BlockWirelessRedstoneTransmitter(InventoryBlocks.simple_black, false), Constants.MOD_ID, "wr_transmitter");
	public static final Block wirelessRedstoneTransmitterInverted = HelperItems.setRegistryName(new BlockWirelessRedstoneTransmitter(InventoryBlocks.simple_black, true), Constants.MOD_ID, "wr_transmitter_i");
	public static final Block airlock_door = HelperItems.setRegistryName(new BlockAirlockDoor(InventoryBlocks.maschine_light_gray), Constants.MOD_ID, "airlock_door");
	public static final Block dungeon_core = HelperItems.setRegistryName(new BlockDungeonCore(Block.Properties.copy(Blocks.BEDROCK).noDrops()), Constants.MOD_ID, "dungeon_core");
	public static final Block fp_lever = HelperItems.setRegistryName(new BlockFpLever(Block.Properties.copy(DecoBlocks.color_iron_fence_white)), Constants.MOD_ID, "fp_lever");
	public static final Block fp_button = HelperItems.setRegistryName(new BlockFpButton(Block.Properties.copy(DecoBlocks.color_iron_fence_white)), Constants.MOD_ID, "button");
	
	public static final Block tyros_tree_gen = HelperItems.setRegistryName(new BlockTyrosTreeGen(), Constants.MOD_ID, "tyros_tree_gen");
	
	public static final Block rf2ne_converter_white = HelperItems.setRegistryName(new BlockRFtoNEConverted(InventoryBlocks.maschine_white), Constants.MOD_ID, "rf2ne_converter_white");
	public static final Block rf2ne_converter_gray = HelperItems.setRegistryName(new BlockRFtoNEConverted(InventoryBlocks.maschine_light_gray), Constants.MOD_ID, "rf2ne_converter_gray");
	public static final Block rf2ne_converter_black = HelperItems.setRegistryName(new BlockRFtoNEConverted(InventoryBlocks.maschine_black), Constants.MOD_ID, "rf2ne_converter_black");
	
	public static final Block modular_door = HelperItems.setRegistryName(new BlockModularDoor(Block.Properties.of(Material.METAL, MaterialColor.COLOR_LIGHT_GRAY).strength(3F, 5F).dynamicShape().noOcclusion()), Constants.MOD_ID, "modular_door");
	public static final Block fish_block = HelperItems.setRegistryName(new BlockFish(Block.Properties.of(Material.CAKE, MaterialColor.COLOR_CYAN).sound(SoundType.WOOL).strength(1.5F, 2.5F).noDrops().noOcclusion()), Constants.MOD_ID, "fish_block");
	public static final Block claime = HelperItems.setRegistryName(new BlockClaime(Block.Properties.of(Material.METAL, DyeColor.YELLOW).strength(5F, 2000F).noOcclusion()), Constants.MOD_ID, "claime");
	public static final Block linked_light = HelperItems.setRegistryName(new BlockLinkedLight(Block.Properties.of(Material.AIR).noDrops().strength(0.0F).lightLevel(state -> 15)), Constants.MOD_ID, "linked_light");
	public static final Block structure_fix_helper = HelperItems.setRegistryName(new BlockStructureFixHelper(Block.Properties.of(Material.HEAVY_METAL).strength(2000.0F).lightLevel(state -> 15)), Constants.MOD_ID, "structure_fix_helper");
	public static final Block tectern = HelperItems.setRegistryName(new BlockTectern(InventoryBlocks.maschine_black), Constants.MOD_ID, "tectern");
	public static final Block dungeon_checkpoint = HelperItems.setRegistryName(new BlockDungeonCheckpoint(InventoryBlocks.maschine_white_notsolid), Constants.MOD_ID, "dungeon_checkpoint");
	
	public static final Block door_marker = HelperItems.setRegistryName(new BlockDoorMarker(), Constants.MOD_ID, "door_marker");
	public static final Block ore_teleporter = HelperItems.setRegistryName(new BlockOreTeleporter(Block.Properties.of(Material.METAL, DyeColor.PURPLE).strength(3F, 5F)), Constants.MOD_ID, "ore_teleporter");
	public static final Block block_teleporter = HelperItems.setRegistryName(new BlockModul<>(Block.Properties.of(Material.METAL, DyeColor.PURPLE).strength(3F, 5F), () -> FPTileEntitys.BLOCK_TELEPORTER, () -> FPGuiHandler.ANTENNA), Constants.MOD_ID, "block_teleporter");
	
	public static void registerBlocks(RegistryEvent.Register<Block> event)
	{
		IForgeRegistry<Block> r = event.getRegistry();
		
		r.registerAll(quantanium, extern_cooler);
		r.registerAll(bedrock_rift, falling_tree);
		r.registerAll(antenna_white, antenna_gray, antenna_black);
		r.registerAll(teleporter, teleporter_up, teleporter_down, teleporter_both);
		r.registerAll(beam, beam_up, beam_down, beam_both);
		r.registerAll(sapling_holder_plains, sapling_holder_desert, sapling_holder_nether);
		r.registerAll(neon_engine, gravity_pulser, magnet, rs_timer, force_field, dungeon_spawner);
		r.registerAll(wirelessRedstoneReceiver, wirelessRedstoneTransmitter, wirelessRedstoneTransmitterInverted, airlock_door, dungeon_core, fp_lever, fp_button);
		r.registerAll(tyros_tree_gen);
		r.registerAll(rf2ne_converter_white, rf2ne_converter_gray, rf2ne_converter_black);
		r.registerAll(modular_door, fish_block, claime, linked_light, structure_fix_helper, tectern, dungeon_checkpoint);
		
		r.registerAll(door_marker, ore_teleporter, block_teleporter);
	}
	
	public static void registerItems(RegistryEvent.Register<Item> event)
	{
		IForgeRegistry<Item> r = event.getRegistry();
		
		r.register(item(quantanium));
		r.register(item(extern_cooler));
		r.register(item(bedrock_rift));
		r.registerAll(item(antenna_white), item(antenna_gray), item(antenna_black));
		r.registerAll(item(teleporter), item(teleporter_up), item(teleporter_down), item(teleporter_both));
		r.registerAll(item(beam), item(beam_up), item(beam_down), item(beam_both));
		r.registerAll(item(sapling_holder_plains), item(sapling_holder_desert), item(sapling_holder_nether));
		r.registerAll(item(neon_engine), item(gravity_pulser), item(magnet), item(rs_timer), item(force_field), item(dungeon_spawner));
		r.registerAll(item(wirelessRedstoneReceiver), item(wirelessRedstoneTransmitter), item(wirelessRedstoneTransmitterInverted), item(airlock_door), item(dungeon_core), item(fp_lever), item(fp_button));
		r.registerAll(item(rf2ne_converter_white), item(rf2ne_converter_gray), item(rf2ne_converter_black));
		r.registerAll(item(modular_door), item(fish_block), item(claime), itemWIP(structure_fix_helper), item(tectern), item(dungeon_checkpoint));
		
		r.registerAll(itemWIP(door_marker), itemWIP(ore_teleporter), itemWIP(block_teleporter));
	}
	
	private static final Item item(Block bl)
	{
		return new BlockItem(bl, (new Item.Properties()).tab(FuturepackMain.tab_maschiens)).setRegistryName(HelperItems.getRegistryName(bl));
	}
	
	private static final Item itemWIP(Block bl)
	{
		return new ItemWIP(bl, (new Item.Properties()).tab(FuturepackMain.tab_maschiens)).setRegistryName(HelperItems.getRegistryName(bl));
	}
}
