package futurepack.common.block.misc;

import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityTeleporter extends FPTileEntityBase 
{
	

	public TileEntityTeleporter(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.TELEPORTER, pos, state);
	}

	public CapabilityNeon energy = new CapabilityNeon(200, EnumEnergyMode.USE);
	public LazyOptional<CapabilityNeon> opt;
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		nbt.put("energy", energy.serializeNBT());
		return nbt;
	}
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		energy.deserializeNBT(nbt.getCompound("energy"));
	}
	

	

	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityNeon.cap_NEON)
		{
			if(opt==null)
			{
				opt = LazyOptional.of(()->energy);
				opt.addListener(p -> opt = null);
			}
			return (LazyOptional<T>) opt;
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(opt);
		super.setRemoved();
	}

}
