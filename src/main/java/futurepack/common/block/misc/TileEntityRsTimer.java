package futurepack.common.block.misc;

import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityRsTimer extends FPTileEntityBase implements ITilePropertyStorage
{
	final int maxMetas = 9;
	private long maxTime;
	
	private int[] delays = new int[maxMetas];
	
	public TileEntityRsTimer(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.RS_TIMER, pos, state);
		setMaxTime(20);
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		nbt.putLong("maxTime", maxTime);
		return super.writeDataUnsynced(nbt);
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		if(nbt.getLong("maxTime") > 0)
		{
			setMaxTime(nbt.getLong("maxTime"));
		}
	}
	
	public void setMaxTime(long ticks)
	{
		if(ticks == maxTime)
		{
			return;
		}
		else if(ticks<1)
		{
			throw new IndexOutOfBoundsException("Tried to set Tick time below 1!");
		}
		maxTime = ticks;	
		double waitF = (double) maxTime / (double)maxMetas;
		
		int min = (int) waitF;
		for(int i=0;i<maxMetas;i++)
		{
			delays[i] = min;
		}
		long left = ticks - min *  maxMetas;
		if(left>0)
		{
			delays[maxMetas-1]++;
			left--;
			int i=0;
			while(left>0)
			{
				delays[i]++;
				left--;
				i++;
				i %= maxMetas;
			}
		}
	}
	
	public int getDelay(int timeState)
	{		
		return delays[timeState];
	}

	public long getMaxTime()
	{
		return maxTime;
		
	}
	
//	@Override
//	public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newSate)
//	{ 
//		if(oldState.getBlock() == newSate.getBlock())
//		{
//			if(world.isRemote && getBlockState().get(BlockRsTimer.TIME) == 0)
//				world.playSound(pos.getX()+0.5F, pos.getY()+0.5F, pos.getZ()+0.5F, SoundEvents.BLOCK_LEVER_CLICK, SoundCategory.BLOCKS, 0.4F, 0.4F, false);
//			return false;
//		}
//		else
//		{
//			return true;
//		}
//		
//	}

	@Override
	public int getProperty(int id)
	{
		if(id==0)
			return (short) ((maxTime>>0) & 0xffff);
		if(id==1)
			return (short) ((maxTime>>16) & 0xffff);
		if(id==2)
			return (short) ((maxTime>>32) & 0xffff);
		if(id==3)
			return (short) ((maxTime>>48) & 0xffff);
		
		return 0;
	}

	@Override
	public void setProperty(int id, int value)
	{
		if(id==0)
			maxTime |= (value) & 0xFFFFL;
		if(id==1)
			maxTime |= ((value) & 0xFFFFL) << 16;
		if(id==2)
			maxTime |= ((value) & 0xFFFFL) << 32;
		if(id==3)
			maxTime |= ((value) & 0xFFFFL) << 48;
	}

	@Override
	public int getPropertyCount()
	{
		return 4;
	}
}
