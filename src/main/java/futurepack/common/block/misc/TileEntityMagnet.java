package futurepack.common.block.misc;

import futurepack.api.interfaces.tilentity.ITileClientTickable;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import futurepack.depend.api.helper.HelperMagnetism;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityMagnet extends FPTileEntityBase implements ITileServerTickable, ITileClientTickable
{
	int pause = 0;
	
	
	public TileEntityMagnet(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.MAGNET, pos, state);
	}

	
	public void tick(Level level, BlockPos worldPosition) 
	{
		
		pause--;
		
		if(pause>0)
			return;
		HelperMagnetism.doMagnetism(level, worldPosition);
		pause=1;
	}


	@Override
	public void tickClient(Level pLevel, BlockPos pPos, BlockState pState)
	{
		tick(pLevel, pPos);
	}


	@Override
	public void tickServer(Level pLevel, BlockPos pPos, BlockState pState)
	{
		tick(pLevel, pPos);
	}

}
