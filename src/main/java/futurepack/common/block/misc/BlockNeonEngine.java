package futurepack.common.block.misc;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockNeonEngine extends BlockRotateableTile implements IBlockServerOnlyTickingEntity<TileEntityNeonEngine>
{
	private static final VoxelShape shape_up = Shapes.or(Block.box(0, 0+1, 0, 16, 16-5, 16), Block.box(4, 0, 4, 12, 16, 12));
	private static final VoxelShape shape_down = Shapes.or(Block.box(0, 5, 0, 16, 15, 16), Block.box(4, 0, 4, 12, 16, 12));
	private static final VoxelShape shape_east = Shapes.or(Block.box(1, 0, 0, 11, 16, 16), Block.box(0, 4, 4, 16, 12, 12));
	private static final VoxelShape shape_west = Shapes.or(Block.box(5, 0, 0, 15, 16, 16), Block.box(0, 4, 4, 16, 12, 12));
	private static final VoxelShape shape_north = Shapes.or(Block.box(0, 0, 5, 16, 16, 15), Block.box(4, 4, 0, 12, 12, 16));
	private static final VoxelShape shape_south = Shapes.or(Block.box(0, 0, 1, 16, 16, 11), Block.box(4, 4, 0, 12, 12, 16));
	
	protected BlockNeonEngine(Block.Properties props) 
	{
		super(props);
//		super(Material.IRON);
//		setCreativeTab(FPMain.tab_maschiens);		
	}
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext sel)
	{
		switch (state.getValue(BlockRotateableTile.FACING)) 
		{
		case UP:
			return shape_up;
		case DOWN:
			return shape_down;
		case EAST:
			return shape_east;
		case WEST:
			return shape_west;
		case NORTH:
			return shape_north;
		case SOUTH:
			return shape_south;
		default:
			return super.getShape(state, worldIn, pos, sel);
		}
		
	}
	
	@Override
	public RenderShape getRenderShape(BlockState state)
	{
		return RenderShape.ENTITYBLOCK_ANIMATED;
	}

//	@Override
//	public void onBlockPlacedBy(World w, BlockPos pos, IBlockState state, EntityLivingBase liv, ItemStack it) 
//	{
//		int o = EnumFacing.getDirectionFromEntityLiving(pos, liv).getIndex();
//		w.setBlockState(pos, state.withProperty(FPBlocks.META(mm), o), 2);
//		super.onBlockPlacedBy(w, pos, state, liv, it);
//	}
	
	@Override
	public BlockEntityType<TileEntityNeonEngine> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.NEON_ENGINE;
	}
}
