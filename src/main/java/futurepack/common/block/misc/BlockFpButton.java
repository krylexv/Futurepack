package futurepack.common.block.misc;

import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.StoneButtonBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockFpButton extends StoneButtonBlock
{
    protected static final VoxelShape FPAABB_DOWN_OFF_Z = Shapes.box(0.25D, 0.75D, 0.28125D, 0.75D, 1.0D, 0.71875D);
    protected static final VoxelShape FPAABB_UP_OFF_Z = Shapes.box(0.25D, 0.0D, 0.28125D, 0.75D, 0.25D, 0.71875D);
    
    protected static final VoxelShape FPAABB_DOWN_OFF_X = Shapes.box(0.28125D, 0.75D, 0.25D, 0.71875D, 1.0D, 0.75D);
    protected static final VoxelShape FPAABB_UP_OFF_X = Shapes.box(0.28125D, 0.0D, 0.25D, 0.71875D, 0.25D, 0.75D);
    
    protected static final VoxelShape FPAABB_NORTH_OFF = Shapes.box(0.25D, 0.28125D, 0.75D, 0.75D, 0.71875D, 1.0D);
    protected static final VoxelShape FPAABB_SOUTH_OFF = Shapes.box(0.25D, 0.28125D, 0.0D, 0.75D, 0.71875D, 0.25D);
    protected static final VoxelShape FPAABB_WEST_OFF = Shapes.box(0.75D, 0.28125D, 0.25D, 1.0D, 0.71875D, 0.75D);
    protected static final VoxelShape FPAABB_EAST_OFF = Shapes.box(0.0D, 0.28125D, 0.25D, 0.25D, 0.71875D, 0.75D);
    protected static final VoxelShape FPAABB_DOWN_ON_Z = Shapes.box(0.25D, 0.84375D, 0.28125D, 0.75D, 1.0D, 0.71875D);
    protected static final VoxelShape FPAABB_UP_ON_Z = Shapes.box(0.25D, 0.0D, 0.28125D, 0.75D, 0.15625D, 0.71875D);
    
    protected static final VoxelShape FPAABB_DOWN_ON_X = Shapes.box(0.28125D, 0.84375D, 0.25D, 0.71875D, 1.0D, 0.75D);
    protected static final VoxelShape FPAABB_UP_ON_X = Shapes.box(0.28125D, 0.0D, 0.25D, 0.71875D, 0.15625D, 0.75D);
    
    protected static final VoxelShape FPAABB_NORTH_ON = Shapes.box(0.25D, 0.28125D, 0.84375D, 0.75D, 0.71875D, 1.0D);
    protected static final VoxelShape FPAABB_SOUTH_ON = Shapes.box(0.25D, 0.28125D, 0.0D, 0.75D, 0.71875D, 0.15625D);
    protected static final VoxelShape FPAABB_WEST_ON = Shapes.box(0.84375D, 0.28125D, 0.25D, 1.0D, 0.71875D, 0.75D);
    protected static final VoxelShape FPAABB_EAST_ON = Shapes.box(0.0D, 0.28125D, 0.25D, 0.15625D, 0.71875D, 0.75D);
	
	protected BlockFpButton(Block.Properties props) 
	{
		super(props);
//		setCreativeTab(ItemGroup.REDSTONE);
	}
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext sel)
	{
		Direction enumfacing = state.getValue(FACING);
		boolean flag = state.getValue(POWERED);
		switch(state.getValue(FACE)) 
		{
		case FLOOR:
			if (enumfacing.getAxis() == Direction.Axis.X) {
				return flag ? FPAABB_UP_ON_X : FPAABB_UP_OFF_X;
			}

			return flag ? FPAABB_UP_ON_Z : FPAABB_UP_OFF_Z;
		case WALL:
			switch(enumfacing) 
			{
			case EAST:
				return flag ? FPAABB_EAST_ON : FPAABB_EAST_OFF;
			case WEST:
				return flag ? FPAABB_WEST_ON : FPAABB_WEST_OFF;
			case SOUTH:
				return flag ? FPAABB_SOUTH_ON : FPAABB_SOUTH_OFF;
			case NORTH:
			default:
				return flag ? FPAABB_NORTH_ON : FPAABB_NORTH_OFF;
			}
		case CEILING:
		default:
			if (enumfacing.getAxis() == Direction.Axis.X)
			{
				return flag ? FPAABB_DOWN_ON_X : FPAABB_DOWN_OFF_X;
			}
			else 
			{
				return flag ? FPAABB_DOWN_ON_Z : FPAABB_DOWN_OFF_Z;
			}
		}
	}

    @Override
    protected void playSound(@Nullable Player pl, LevelAccessor w, BlockPos pos, boolean clickOn)
    {
        w.playSound(clickOn ? pl : null, pos, this.getSound(clickOn), SoundSource.BLOCKS, 0.3F, clickOn ? 1.0F : 0.9F);
     }

}
