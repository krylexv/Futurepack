package futurepack.common.block.misc;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockSelector;
import futurepack.common.FPBlockSelector;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.dim.structures.generation.BakedDungeon;
import futurepack.world.protection.FPDungeonProtection;
import futurepack.world.protection.IChunkProtection;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.material.Material;

public class TileEntityDungeonCore extends FPTileEntityBase implements Runnable
{
	private BoundingBox[] rooms = null;
	private Queue<Integer> toprotect = new LinkedList<Integer>();

	public TileEntityDungeonCore(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.DUNGEON_CORE, pos, state);
	}
	
	public void setDungeon(BakedDungeon dungeon)
	{
		if(rooms == null)
		{
			List<BoundingBox> list = dungeon.getAllStructureParts();
			rooms = list.toArray(new BoundingBox[list.size()]);
			
			for(int i = 0; i < rooms.length; i++)
			{
				toprotect.add(i);
			}
			
		}
	}
	
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		if(nbt.contains("dminX") && nbt.contains("dmaxZ"))//only checking first and last
		{
			int[] minX = nbt.getIntArray("dminX");
			int[] minY = nbt.getIntArray("dminY");
			int[] minZ = nbt.getIntArray("dminZ");
			
			int[] maxX = nbt.getIntArray("dmaxX");
			int[] maxY = nbt.getIntArray("dmaxY");
			int[] maxZ = nbt.getIntArray("dmaxZ");
			
			if(minX.length != maxZ.length)
				throw new IllegalStateException("Array lengths dont match!");
			
			rooms = new BoundingBox[minX.length];
			
			for(int i=0;i<rooms.length;i++)
			{
				rooms[i] = new BoundingBox(minX[i], minY[i], minZ[i], maxX[i], maxY[i], maxZ[i]);
				toprotect.add(i);
			}
		}
		super.readDataUnsynced(nbt);
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		if(rooms!=null)
		{
			int[] minX = new int[rooms.length];
			int[] minY = new int[rooms.length];
			int[] minZ = new int[rooms.length];
			
			int[] maxX = new int[rooms.length];
			int[] maxY = new int[rooms.length];
			int[] maxZ = new int[rooms.length];
			
			fillWithData(minX, minY, minZ, maxX, maxY, maxZ);
			
			nbt.putIntArray("dminX", minX);
			nbt.putIntArray("dminY", minY);
			nbt.putIntArray("dminZ", minZ);
			
			nbt.putIntArray("dmaxX", maxX);
			nbt.putIntArray("dmaxY", maxY);
			nbt.putIntArray("dmaxZ", maxZ);
		}
		return super.writeDataUnsynced(nbt);
	}
	
	private void fillWithData(int[] minX, int[] minY, int[] minZ, int[] maxX, int[] maxY, int[] maxZ)
	{
		for(int i=0;i<rooms.length;i++)
		{
			minX[i] = rooms[i].minX();
			minY[i] = rooms[i].minY();
			minZ[i] = rooms[i].minZ();
			
			maxX[i] = rooms[i].maxX();
			maxY[i] = rooms[i].maxY();
			maxZ[i] = rooms[i].maxZ();
		}
	}
	
	@Override
	public void onLoad()
	{
		if(!level.isClientSide)
		{
			Thread t = new Thread(new Runnable()
			{	
				@Override
				public void run()
				{
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					((ServerLevel)level).getServer().submitAsync(TileEntityDungeonCore.this);
				}
			}, "Waiting");
			t.setDaemon(true);
			t.start();
		}
	}

	@Override
	public void run()
	{
		if(rooms!=null && !toprotect.isEmpty())
		{
			Integer nextID = toprotect.poll();
			
			BoundingBox box = rooms[nextID];
			BlockPos middle = box.getCenter();
			
			/*while(!level.hasChunk(middle.getX() >> 4, middle.getZ() >> 4))
			{
				next++;
				if(next < rooms.length)
				{
					box = rooms[next];
					middle = new BlockPos( (box.x1+box.x0)/2, (box.y1+box.y0)/2, (box.z1+box.z0)/2);
				}
			}*/
				
			if(level.hasChunk(middle.getX() >> 4, middle.getZ() >> 4))
			{
				IChunkProtection prot = level.getChunkAt(middle).getCapability(FPDungeonProtection.cap_PROTECTION, null).orElseThrow(NullPointerException::new);
				if(prot.getRawProtectionState(middle) != (IChunkProtection.BLOCK_NOT_BREAKABLE | IChunkProtection.BLOCK_NOT_PLACABLE))
				{
					FPDungeonProtection.addProtection(level, box);
				}
			}
			else
			{
				toprotect.add(nextID);
			}
			
			Thread t = new Thread(new Runnable()
			{	
				@Override
				public void run()
				{
					try {
						Thread.sleep(20);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					((ServerLevel)level).getServer().submitAsync(TileEntityDungeonCore.this);
				}
			}, "Waiting");
			t.setDaemon(true);
			t.start();
		}
	}
	
	public void removeDungeonProtection()
	{
		final byte notProtected = 0;
		
		if(rooms!=null)
		{
			for(int i=0;i<rooms.length;i++)
			{
				BoundingBox box = rooms[i];
				for(int x=box.minX();x<=box.maxX();x++)
				{
					for(int y=box.minY();y<=box.maxY();y++)
					{
						for(int z=box.minZ();z<=box.maxZ();z++)
						{
							BlockPos pos = new BlockPos(x,y,z);			
							level.getChunkAt(pos).getCapability(FPDungeonProtection.cap_PROTECTION, null).ifPresent(p ->
							{
								p.setRawProtectionState(pos, notProtected);
							});
						}
					}
				}
			}
			rooms = null;
		}
		else 
		{
			IChunkProtection p = level.getChunkAt(worldPosition).getCapability(FPDungeonProtection.cap_PROTECTION, null).orElseThrow(NullPointerException::new);
			if(p.getRawProtectionState(worldPosition) == notProtected)
				return;
			
			FPBlockSelector sel = new FPBlockSelector(level, new IBlockSelector()
			{
				@Override
				public boolean isValidBlock(Level w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent)
				{
					LevelChunk c = w.getChunkAt(pos);
					IChunkProtection p = c.getCapability(FPDungeonProtection.cap_PROTECTION, null).orElse(null);
					if(p!=null)
					{
						return p.getRawProtectionState(pos) != 0;
					}
					return false;
				}
				
				@Override
				public boolean canContinue(Level w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent)
				{
					return true;
				}
			});
			sel.selectBlocks(worldPosition);
			Iterator<ParentCoords> iter = sel.getAllBlocks().iterator();
			while(iter.hasNext())
			{
				BlockPos pos = iter.next();
				p = level.getChunkAt(pos).getCapability(FPDungeonProtection.cap_PROTECTION, null).orElseThrow(NullPointerException::new);
				p.setRawProtectionState(pos, notProtected);
			}
			sel.clear();
		}
	}
}
