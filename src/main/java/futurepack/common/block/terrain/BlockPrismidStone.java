package futurepack.common.block.terrain;

import java.util.Random;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.Mth;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;

public class BlockPrismidStone extends Block 
{
	
	public BlockPrismidStone(Block.Properties props) 
	{
		super(props.randomTicks());
//		super(Material.ROCK);
//		setCreativeTab(FPMain.tab_deco);
//		setSoundType(SoundType.GLASS);
//		setTickRandomly(true);
	}
	
	@Override
	public void tick(BlockState state, ServerLevel w, BlockPos pos, Random r)
	{
		super.tick(state, w, pos, r);
		
		if(r.nextInt(100)==0 && !w.isClientSide)
		{
			 Biome b = w.getBiome(pos).value();
			
			if(w.isEmptyBlock(pos.above())) // always grow prsmide for now
			{
				w.setBlockAndUpdate(pos.above(), TerrainBlocks.prismide.defaultBlockState());
			}
		}
	}
	
	private int getAmountDropped(Random rand, int min, int max, int fortune)
	{
		int std = Mth.nextInt(rand, min, max);
		
		if(fortune > 0)
		{
	        int i = rand.nextInt(fortune + 2) - 1;
	
	        if (i < 0)
	        {
	            i = 0;
	        }

	        return std * (i + 1);
		}
		return std;
	}
	

}
