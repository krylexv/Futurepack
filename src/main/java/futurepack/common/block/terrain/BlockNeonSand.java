package futurepack.common.block.terrain;

import java.util.Random;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.FallingBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.common.PlantType;

public class BlockNeonSand extends FallingBlock
{
//	static String[] names = new String[]{"NeonGranulat", "RetiumGranulat", "GlowtitGranulat", "BioteriumGranulat", "AlutinGranulat"};
//	public static final PropertyEnum<EnumCristalType> type = BlockCristal.type;
	
	private final BlockState crystal;
	
	public BlockNeonSand(Block.Properties props, Block crystal) 
	{
		super(props);
		this.crystal = crystal.defaultBlockState();
//		setCreativeTab(FPMain.tab_deco);
//		setSoundType(SoundType.SAND);
//		setTickRandomly(true);
//		setDefaultState(this.blockState.getBaseState().withProperty(type, EnumCristalType.NEON));
	}
	
	@Override
	public void tick(BlockState state, ServerLevel w, BlockPos pos, Random r)
	{
		super.tick(state, w, pos, r);
		if(r.nextInt(100)==0 && !w.isClientSide)
		{
			if(w.isEmptyBlock(pos.above()))
			{
				w.setBlockAndUpdate(pos.above(), this.crystal);
			}
		}
	}
	
//	@Override
//	public int damageDropped(IBlockState m) 
//	{
//		return getMetaFromState(m);
//	}
//	
//	@Override
//	public void getSubBlocks(ItemGroup t, NonNullList l)
//	{
//		for(int i=0;i<EnumCristalType.getTypeCount();i++)
//		{
//			l.add(new ItemStack(this,1,i));
//		}
//	}
	
//	@Override
//	public IBlockState getStateFromMeta(int meta)
//    {
//        return this.getDefaultState().withProperty(type, EnumCristalType.fromMeta(meta));
//    }

//	@Override
//    public int getMetaFromState(IBlockState state)
//    {
//        return  state.getValue(type).getMeta();
//    }
//	
//	@Override
//    protected BlockStateContainer createBlockState()
//    {
//        return new BlockStateContainer(this, new IProperty[] {type});
//    }
	
	@Override
	public boolean canSustainPlant(BlockState state, BlockGetter world, BlockPos pos, Direction facing, IPlantable plantable)
	{
		if(plantable.getPlantType(world, pos.relative(facing)) == PlantType.DESERT)
		{
			return true; //mendel berrys can stay  here
		}
		return super.canSustainPlant(state, world, pos, facing, plantable);
	}
}
