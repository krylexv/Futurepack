package futurepack.common.block.terrain;

import java.util.Random;

import futurepack.common.FuturepackTags;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.MushroomBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.IntegerProperty;

public class BlockMenelausMushroms extends MushroomBlock
{
	public static final IntegerProperty AGE = BlockStateProperties.AGE_3;
//	public static final PropertyEnum<EnumMushroom> TYPE = PropertyEnum.create("type", EnumMushroom.class);
	
	
	public BlockMenelausMushroms(Block.Properties props)
	{
		super(props.randomTicks(), null);
//		setLightLevel(0.6F);
//		setCreativeTab(FPMain.tab_items);
	}
	
//	@Override
//	public int damageDropped(IBlockState state)
//	{
//		return state.get(TYPE).ordinal();
//	}

	
	@Override
	public void tick(BlockState state, ServerLevel w, BlockPos pos, Random rand)
	{
		if (!w.isAreaLoaded(pos, 1)) return; // prevent loading unloaded chunks when checking neighbor's light
		
		int light = w.getRawBrightness(pos, 0);
		
		if(state.getValue(AGE)<3)
		{
			float chance = 0.8F;
			chance -= light * 0.04F;
			
			if(rand.nextFloat() <= chance)
			{
				w.setBlock(pos, state.setValue(AGE, state.getValue(AGE)+1), 2);
			}
		}
		else if (rand.nextInt(10 +light*10) == 0) //only spread when dark
        {
            BlockPos blockpos1 = pos.offset(rand.nextInt(3) - 1, rand.nextInt(2) - rand.nextInt(2), rand.nextInt(3) - 1);

            for (int k = 0; k < 4; ++k)
            {
                if (w.isEmptyBlock(blockpos1) && this.canSurvive(this.defaultBlockState(), w, blockpos1))
                {
                    pos = blockpos1;
                }

                blockpos1 = pos.offset(rand.nextInt(3) - 1, rand.nextInt(2) - rand.nextInt(2), rand.nextInt(3) - 1);
            }

            if (w.isEmptyBlock(blockpos1) && this.canSurvive(this.defaultBlockState(), w, blockpos1))
            {
                w.setBlock(blockpos1, state.setValue(AGE, 0), 2);
            }
        }
		else if(rand.nextInt(10 +light/2) == 0) //tranform ground
		{
			BlockState ground = w.getBlockState(pos.below());
			
			int r = 0;
			if(ground == TerrainBlocks.sand_m.defaultBlockState())
			{
				r = 1;
			}
			else if(ground == Blocks.RED_SAND.defaultBlockState())
			{
				r = 2;
			}
			else if(ground == TerrainBlocks.dirt_m.defaultBlockState())
			{
				r = 3;
			}
			if(r > 0)
			{
				int count = 0;
				int count_g = 0;
				for(int y=-r;y<=r;y++)
				{
					for(int x=-r;x<=r;x++)
					{
						for(int z=-r;z<=r;z++)
						{
							BlockPos xyz = pos.offset(x,y,z);
							BlockState mush = w.getBlockState(xyz);
							if(mush == state)
							{
								count++;
							}
							if(mush == ground)
							{
								count_g++;
							}
							if(r == 2 && (mush == TerrainBlocks.dirt_m.defaultBlockState() || mush == Blocks.MYCELIUM.defaultBlockState()))
							{
								count_g++;
							}
							if(r == 3 && mush == Blocks.MYCELIUM.defaultBlockState())
							{
								count_g++;
							}
						}
					}
				}
				BlockState newState = null;
				if(r==1 && count>=9)
				{
					newState = Blocks.RED_SAND.defaultBlockState();
				}
				else if(r==2 && count >= 25 && count_g>=16)
				{
					newState = TerrainBlocks.dirt_m.defaultBlockState();
				}
				else if(r==3 && count >= 49 && count_g>=36)
				{
					newState = Blocks.MYCELIUM.defaultBlockState();
				}
				if(newState!=null)
				{
					w.setBlockAndUpdate(pos.below(), newState);
				}
			}
		}
	}
	
	@Override
	public boolean canSurvive(BlockState state, LevelReader worldIn, BlockPos pos)
	{
		if (pos.getY() >= 0 && pos.getY() < 256)
		{
			BlockState iblockstate = worldIn.getBlockState(pos.below());
			return iblockstate.is(FuturepackTags.MYCEL) ? true : iblockstate.getBlock().canSustainPlant(iblockstate, worldIn, pos.below(), Direction.UP, this);
		}
		else
		{
			return false;
		}
	}
	
	@Override
	public boolean isValidBonemealTarget(BlockGetter worldIn, BlockPos pos, BlockState state, boolean isClient) //bone meal for super class
	{
		return false;
	}
	
//	@Override
//	public void getSubBlocks(ItemGroup tab, NonNullList<ItemStack> list)
//	{
//		for(int i=0;i<4;i++)
//		{
//			list.add(new ItemStack(this,1,i));
//		}
//	}
//	
//	@Override
//	public int getMetaFromState(IBlockState state)
//	{
//		return state.get(AGE)*4 + state.get(TYPE).ordinal();
//	}
//	
//	@Override
//	public IBlockState getStateFromMeta(int meta)
//	{
//		IBlockState state = getDefaultState();
//		state = state.withProperty(AGE, meta / 4);
//		state = state.withProperty(TYPE, EnumMushroom.fromMeta(meta%4));
//		return state;
//	}
	
//	@Override
//	protected BlockStateContainer createBlockState()
//	{
//		return new BlockStateContainer(this, AGE, TYPE);
//	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		builder.add(AGE);
		super.createBlockStateDefinition(builder);
	}
	
	public boolean growMushroom(ServerLevel pLevel, BlockPos pPos, BlockState pState, Random pRandom) 
	{
		return false;
	}
	
//	public enum EnumMushroom implements IStringSerializable
//	{
//		rotkappe,
//		blauling,
//		blasenpilz,
//		hyticus;
//
//		@Override
//		public String getName()
//		{
//			return name().toLowerCase();
//		}
//		
//		public static EnumMushroom fromMeta(int meta)
//		{
//			return values()[meta%values().length];
//		}
//	}

//	@Override
//	public String getMetaNameSub(ItemStack is)
//	{
//		return EnumMushroom.fromMeta(is.getItemDamage()).getName();
//	}
//
//	@Override
//	public int getMaxMetas()
//	{
//		return EnumMushroom.values().length;
//	}
//
//	@Override
//	public String getMetaName(int meta)
//	{
//		return "menelaus_mushroom_"+meta;
//	}
}
