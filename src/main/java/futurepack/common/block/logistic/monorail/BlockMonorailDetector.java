package futurepack.common.block.logistic.monorail;

import java.util.List;
import java.util.Random;

import futurepack.common.entity.monocart.EntityMonocartBase;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.phys.AABB;

public class BlockMonorailDetector extends BlockMonorailBasic
{
	public static final BooleanProperty powered = BlockStateProperties.POWERED;
	
	public BlockMonorailDetector(Block.Properties props) 
	{
		super(props.randomTicks());
		this.registerDefaultState(this.defaultBlockState().setValue(powered, false));
	}
	
	@Override
	public boolean isBendable()
	{
		return false;
	}

	@Override
	public void onMonocartPasses(Level w, BlockPos pos, BlockState state, EntityMonocartBase cart)
	{
		updatePoweredState(w, pos, state);
		super.onMonocartPasses(w, pos, state, cart);
	}
	
//	@Override
//	public IBlockState getStateFromMeta(int meta)
//    {
//		if(meta>5)
//		{
//			return this.getDefaultState().with(powered, true).with(State_bendless, EnumMonorailStates.getFromMeta(meta%6));
//		}
//        return this.getDefaultState().with(powered, false).with(State_bendless, EnumMonorailStates.getFromMeta(meta));
//    }
//
//	@Override
//    public int getMetaFromState(IBlockState state)
//    {
//        return state.get(State_bendless).getMeta() + (state.get(powered)? 6 : 0);
//    }
//	
//	@Override
//    protected BlockStateContainer createBlockState()
//    {
//        return new BlockStateContainer(this, new IProperty[] {State_bendless,powered});
//    }
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		builder.add(powered);
		super.createBlockStateDefinition(builder);
	}
	
	@Override
	public boolean isSignalSource(BlockState state)
	{
		return true;
	}
	
	@Override
	public int getSignal(BlockState blockState, BlockGetter blockAccess, BlockPos pos, Direction side)
	{
		 return blockState.getValue(powered) ? 15 : 0;
	}
	
	@Override
	public int getDirectSignal(BlockState blockState, BlockGetter blockAccess, BlockPos pos, Direction side)
	{
		if(!blockState.getValue(powered))
        {
            return 0;
        }
        else
        {
            return side == Direction.UP ? 15 : 0;
        }
	}
	
	
	@Override
	public void tick(BlockState state, ServerLevel worldIn, BlockPos pos, Random rand)
    {
        if (!worldIn.isClientSide && state.getValue(powered))
        {
            this.updatePoweredState(worldIn, pos, state);
        }
    }
	
	private void updatePoweredState(Level w, BlockPos pos, BlockState state)
	{
		List<EntityMonocartBase> list = w.getEntitiesOfClass(EntityMonocartBase.class, new AABB(pos));
		
		boolean flag1 = list.size()>0;
		state = state.setValue(powered, flag1);
		w.setBlockAndUpdate(pos, state);
		//w.notifyNeighborsOfStateChange(pos, this, false);
        w.updateNeighborsAt(pos.below(), this);
        //w.HelperChunks.renderUpdate(w, pos);(pos, pos);
		if (flag1)
        {
            w.scheduleTick(new BlockPos(pos), this, 20);
        }
	}
}
