package futurepack.common.block.logistic.monorail;

import futurepack.api.capabilities.CapabilityNeon;
import futurepack.common.entity.monocart.EntityMonocartBase;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class BlockMonorailCharger extends BlockMonorailBasic implements EntityBlock
{
	public BlockMonorailCharger(Block.Properties props) 
	{
		super(props);
	}

	@Override
	public boolean isBendable()
	{
		return false;
	}

	@Override
	public void onMonocartPasses(Level w, BlockPos pos, BlockState state, EntityMonocartBase cart)
	{
		TileEntityMonorailCharger ch = (TileEntityMonorailCharger) w.getBlockEntity(pos);
		if(ch!=null)
		{
			cart.setPaused(cart.getPower()+10 < cart.getMaxPower());		
			
			if(cart.getPower()+1 < cart.getMaxPower())
			{
				ch.getCapability(CapabilityNeon.cap_NEON, Direction.UP).ifPresent(n -> {
					if(n.use(1) > 0)
					{
						cart.setPower(cart.getPower()+1);
					}
				});
			}
		}
		super.onMonocartPasses(w, pos, state, cart);
	}
	
	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new TileEntityMonorailCharger(pos, state);
	}
}
