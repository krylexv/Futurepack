package futurepack.common.block.logistic;

import futurepack.api.interfaces.tilentity.ITileNetwork;
import futurepack.common.FPTileEntitys;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class BlockWireNetwork extends BlockWireBase<TileEntityWireNetwork> 
{

	public BlockWireNetwork(Properties props) 
	{
		super(props);
	}

	@Override
	protected int getMaxNeon() 
	{
		return 500;
	}
	
	@Override
	public boolean canConnect(LevelAccessor w, BlockPos pos, BlockPos xyz, BlockState facingState, BlockEntity tile, Direction face)
	{
		if(super.canConnect(w, pos, xyz, facingState, tile, face))
			return true;
	
		if(tile!=null)
		{
			if(tile instanceof ITileNetwork)
			{
				ITileNetwork p = (ITileNetwork) tile;
				if(p.isNetworkAble())
				{	
					return true;
				}	
			}
		}
		
		return false;
	}

	@Override
	public BlockEntityType<TileEntityWireNetwork> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.WIRE_NETWORK;
	}
	
}
