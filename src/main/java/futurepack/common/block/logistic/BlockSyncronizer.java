package futurepack.common.block.logistic;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.phys.BlockHitResult;

public class BlockSyncronizer extends BlockRotateableTile implements IBlockServerOnlyTickingEntity<TileEntitySyncronizer>
{
	public static final DirectionProperty FACING = BlockRotateableTile.FACING;

	public BlockSyncronizer(Block.Properties props)
	{
		super(props);	
	}
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos , Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.SYNCRONIZER.openGui(pl, pos);
		}
		return InteractionResult.SUCCESS;
	}

	@Override
	public BlockEntityType<TileEntitySyncronizer> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.SYNCRONIZER;
	}	


}
