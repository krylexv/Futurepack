package futurepack.common.block.logistic.frames;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;

import futurepack.common.block.FPTileEntityBase;
import futurepack.common.block.misc.TileEntityFallingTree;
import futurepack.depend.api.MiniWorld;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtIo;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;

public class TileEntityWithMiniWorldBase extends FPTileEntityBase 
{

	public TileEntityWithMiniWorldBase(BlockEntityType<? extends TileEntityWithMiniWorldBase> type, BlockPos pos, BlockState state) 
	{
		super(type, pos, state);
	}
	
	public static String getSaveString(CompoundTag nbt)
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try
		{
			NbtIo.writeCompressed(nbt, out);
			out.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		String base = Base64.getEncoder().encodeToString(out.toByteArray());
		return base;
	}
	
	public static CompoundTag fromSaveString(String s)
	{		
		ByteArrayInputStream in = new ByteArrayInputStream(Base64.getDecoder().decode(s));
		try
		{
			CompoundTag tag = NbtIo.readCompressed(in);
			in.close();
			return tag;
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	

	public int ticks = 1200;
	public int maxticks = 1200;
	

	protected Level ww;
	protected MiniWorld w;

	protected CompoundTag storedMiniWorld = null;
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		if(w!=null)
		{
			CompoundTag tag = w.serializeNBT();
//			tag.remove("light");
			tag.remove("red");
			nbt.putString("mini", getSaveString(tag));
		}		
		
		nbt.putInt("ticks", ticks);
		nbt.putInt("maxticks", maxticks);
		return super.writeDataUnsynced(nbt);
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
//		System.out.println("loaded " + nbt.getAsString());
		CompoundTag tag = fromSaveString(nbt.getString("mini"));
		if(tag!=null && (level!=null || ww!=null))
			setMiniWorld(new MiniWorld(level != null ? level : ww, tag));
		else if(tag!=null)
		{
			this.storedMiniWorld = tag;
		}
		ticks = nbt.getInt("ticks");
		maxticks = nbt.getInt("maxticks");

		super.readDataUnsynced(nbt);
	}
	
	@Override
	public void writeDataSynced(CompoundTag nbt)
	{
		super.writeDataSynced(nbt);
		if(w!=null)
		{
			CompoundTag tag = w.serializeNBT();
			tag.remove("red");
			nbt.putString("mini", TileEntityFallingTree.getSaveString(tag));
		}
		nbt.putInt("ticks", ticks);
		nbt.putInt("maxticks", maxticks);
	}
	
	@Override
	public void readDataSynced(CompoundTag nbt)
	{
		super.readDataSynced(nbt);
		ticks = nbt.getInt("ticks");
		maxticks = nbt.getInt("maxticks");
	}
	
//	@Override
//	public double getViewDistance() 
//	{
//		return Math.max(super.getViewDistance(), w!=null ? Math.max(w.height, Math.max(w.depth, w.width)) * 1.5D : 0);
//	}
//	
	@Override
	public AABB getRenderBoundingBox() 
	{
		return INFINITE_EXTENT_AABB;
	}
	
	@Override
	public void setLevel(Level worldIn)
	{
//		System.out.println("Setting level and pos at " + pos + " state:" + worldIn.getBlockState(pos));
		ww = worldIn;
		
		if(storedMiniWorld!=null)
		{
			setMiniWorld(new MiniWorld(worldIn, storedMiniWorld));
		}
		super.setLevel(worldIn);
	}

	
	public MiniWorld getMiniWorld()
	{
		return w;
	}
	
	public void setMiniWorld(MiniWorld w)
	{
		this.w = w;
	}
	
}
