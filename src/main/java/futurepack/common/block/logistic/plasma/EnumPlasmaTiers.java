package futurepack.common.block.logistic.plasma;

public enum EnumPlasmaTiers 
{
	Tier0(512, 3),
	Tier1(32767L, 9),
	Tier2(2147483647L, 17),
	Tier3(140737488355327L, 33);
	
	private final long plasmaPerBlock;
	private final long tankMaxPlamsa;
	private final int innerMaxWidth;
	private final long plasmaTranferPipe;
	
	EnumPlasmaTiers(long plasmaPerBlock, int innerMaxWidth) 
	{
		this.plasmaPerBlock = plasmaPerBlock;
		this.innerMaxWidth = innerMaxWidth;
		this.tankMaxPlamsa = innerMaxWidth *innerMaxWidth*innerMaxWidth * plasmaPerBlock;
		this.plasmaTranferPipe = plasmaPerBlock / 4;
	}

	public long getPlasmaPerBlock() 
	{
		return plasmaPerBlock;
	}

	public long getTankMaxPlamsa() 
	{
		return tankMaxPlamsa;
	}

	public int getInnerMaxWidth() 
	{
		return innerMaxWidth;
	}
	
	public long getPlasmaTranferPipe() 
	{
		return plasmaTranferPipe;
	}
	
	public int getMaxBlockAmount()
	{
		return innerMaxWidth*innerMaxWidth*innerMaxWidth;
	}
}
