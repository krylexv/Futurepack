package futurepack.common.block.logistic.plasma;

import futurepack.api.interfaces.IItemNeon;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.modification.machines.TileEntitySolarPanel;
import futurepack.common.fluids.FPFluids;
import futurepack.common.modification.EnumChipType;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.ticks.TickPriority;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler.FluidAction;
import net.minecraftforge.fluids.capability.templates.FluidTank;

public class TileEntityPlasma2NeonT0 extends TileEntitySolarPanel
{
	private Boolean active = null;
	final FluidTank plasma;
	private LazyOptional<IFluidHandler> optPlasma;
	
	public TileEntityPlasma2NeonT0(BlockEntityType<? extends TileEntityPlasma2NeonT0> type, int maxTransfer, BlockPos pos, BlockState state) 
	{
		super(type, pos, state);
		plasma = new FluidTank(maxTransfer, s -> s.getFluid() == FPFluids.plasmaStill);
	}
	
	public static TileEntityPlasma2NeonT0 converterT0(BlockPos pos, BlockState state)
	{
		return new TileEntityPlasma2NeonT0(FPTileEntitys.PLASMA_2_NEON_T0, 5, pos, state);
	}
	
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		nbt.put("plasma", plasma.writeToNBT(new CompoundTag()));
		return nbt;
	}

	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		plasma.readFromNBT(nbt.getCompound("plasma"));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if (capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY)
		{
			if (optPlasma != null)
				return (LazyOptional<T>) optPlasma;
			else
			{
				optPlasma = LazyOptional.of(() -> plasma);
				optPlasma.addListener(p -> optPlasma = null);
				return (LazyOptional<T>) optPlasma;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved()
	{
		HelperEnergyTransfer.invalidateCaps(optPlasma);
		super.setRemoved();
	}

	@Override
	public void updateTile(int ticks) 
	{
		if(!level.isClientSide)
		{
			light = plasma.getFluidAmount() > 0 ? 15 : 0;
			if(light > 0)
			{
				int ne = (int) (500 * Math.min(1 + getChipPower(EnumChipType.INDUSTRIE) * 0.2F, 2F));	
				if(energy.get() + ne <= energy.getMax())
				{
					power = ne;
					energy.add(ne);
					plasma.drain(1, FluidAction.EXECUTE);
				}
			}
			
			
			if(!items.get(0).isEmpty() && energy.get() > 0)
			{		
				if(items.get(0).getItem()instanceof IItemNeon)
				{
					IItemNeon ch = (IItemNeon) items.get(0).getItem();
					ItemStack it = items.get(0);
					if(ch.isNeonable(it) && ch.getNeon(it) < ch.getMaxNeon(it))
					{
						ch.addNeon(it, energy.use(ticks));
					}					
				}		
			}
			Boolean plasma = this.plasma.getFluidAmount() > 0;
			if(plasma != active)
			{
				this.level.scheduleTick(getBlockPos(), this.getBlockState().getBlock(), 1, TickPriority.LOW);
				this.active = plasma;
			}
		}
	}
	
	@Override
	public boolean isWorking()
	{
		return power > 0;
	}
}
