package futurepack.common.block.logistic;

import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;

import futurepack.common.FPTileEntitys;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;

public class BlockPipeRedstone extends BlockPipeBase<TileEntityPipeNormal> 
{	
	public BlockPipeRedstone(Block.Properties props) 
	{
		super(props);
	}
	
	@Override
	protected EnumSide getAdditionalConnections(TileEntityPipeBase pipe, @Nullable BlockEntity otherTile, Direction face) {
		
		BlockPos jkl = pipe.getBlockPos().relative(face);
		
		BlockState b = pipe.getLevel().getBlockState(jkl);
		if(b.getBlock().canConnectRedstone(b, pipe.getLevel(), jkl, face.getOpposite()))
			return EnumSide.CABLE; // CABLE because no inv is present when this is called
		
		return EnumSide.OFF;
	}
	
	@Override
	public void appendHoverText(ItemStack stack, BlockGetter worldIn, List<Component> tooltip, TooltipFlag flagIn) 
	{
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslatableComponent("tooltip.futurepack.block.conduct.redstone"));
	}	
	
	@Override
	public boolean canConnectRedstone(BlockState state, BlockGetter world, BlockPos pos, Direction side)
	{
		if(side == null)
		{
			side = Direction.DOWN;
		}
			
		TileEntityPipeBase p = (TileEntityPipeBase) world.getBlockEntity(pos);
		if(p == null)
			return false;
		
		return (p.isSideLocked(side.getOpposite()) ? p.isIgnoreLockSub(side.getOpposite()) : true);
	}
	
	@Override
	public int getSignal(BlockState state, BlockGetter w, BlockPos pos, Direction face)
	{
		TileEntityPipeBase p = (TileEntityPipeBase) w.getBlockEntity(pos);
		
		if(p != null && p.isSideLocked(face.getOpposite()) ? p.isIgnoreLockSub(face.getOpposite()) : true)
		{
			return RedstoneSystem.getWeakPower(state, w, pos, face);
		}
		return 0;
	}
	
	@Override
	public void neighborChanged(BlockState state, Level w, BlockPos pos, Block block, BlockPos blockPos, boolean isMoving)
	{
		RedstoneSystem.neighborChanged(this, w, pos);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onPlace(BlockState state, Level w, BlockPos jkl, BlockState oldState, boolean isMoving)
	{
		super.onPlace(state, w, jkl, oldState, isMoving);
		RedstoneSystem.onBlockAdded(state, w, jkl, oldState, isMoving);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void tick(BlockState state, ServerLevel w, BlockPos jkl, Random rand)
	{
		RedstoneSystem.tick(state, w, jkl, rand);
	}
		
	@Override
	public void onRemove(BlockState state, Level worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
	      if (state.getBlock() != newState.getBlock()) {
	         super.onRemove(state, worldIn, pos, newState, isMoving);
	      }
	}

	@Override
	public boolean hasSpecial() 
	{
		return true;
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(RedstoneSystem.STATE);
	}

	@Override
	public BlockEntityType<TileEntityPipeNormal> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.PIPE_NORMAL;
	}
}
