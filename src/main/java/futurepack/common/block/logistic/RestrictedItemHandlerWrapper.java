package futurepack.common.block.logistic;

import java.util.function.BooleanSupplier;

import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.IItemHandler;

public class RestrictedItemHandlerWrapper implements IItemHandler
{
	public final BooleanSupplier insert, extract;
	public final IItemHandler base;
	
	public RestrictedItemHandlerWrapper(BooleanSupplier canInsert, BooleanSupplier canExtract, IItemHandler base)
	{
		super();
		insert = canInsert;
		extract = canExtract;
		this.base = base;
	}
	
	public RestrictedItemHandlerWrapper(boolean canInsert, boolean canExtract, IItemHandler base)
	{
		this(()->canInsert, ()->canExtract, base);
	}

	@Override
	public int getSlots()
	{
		return base.getSlots();
	}

	@Override
	public ItemStack getStackInSlot(int slot)
	{
		return base.getStackInSlot(slot);
	}

	@Override
	public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
	{
		if(insert.getAsBoolean())
			return base.insertItem(slot, stack, simulate);
		
		return stack;
	}

	@Override
	public ItemStack extractItem(int slot, int amount, boolean simulate)
	{
		if(extract.getAsBoolean())
			return base.extractItem(slot, amount, simulate);
		
		return ItemStack.EMPTY;
	}

	@Override
	public int getSlotLimit(int slot)
	{
		return base.getSlotLimit(slot);
	}

	@Override
	public boolean isItemValid(int slot, ItemStack stack) 
	{
		return base.isItemValid(slot, stack);
	}
}
