package futurepack.common.block.logistic;

import futurepack.api.EnumLogisticType;
import futurepack.api.capabilities.ILogisticInterface;
import futurepack.api.capabilities.INeonEnergyStorage;

public class LogisticNeonEnergyWrapper implements INeonEnergyStorage
{
	ILogisticInterface log;
	INeonEnergyStorage base;
	
	public LogisticNeonEnergyWrapper(ILogisticInterface log, INeonEnergyStorage base)
	{
		super();
		this.log = log;
		this.base = base;
	}

	@Override
	public int get()
	{
		return base.get();
	}

	@Override
	public int getMax()
	{
		return base.getMax();
	}

	@Override
	public int use(int used)
	{
		return base.use(used);
	}

	@Override
	public int add(int added)
	{
		return base.add(added);
	}

	@Override
	public boolean canAcceptFrom(INeonEnergyStorage other)
	{
		return base.canAcceptFrom(other) && log.getMode(EnumLogisticType.ENERGIE).canInsert();
	}

	@Override
	public boolean canTransferTo(INeonEnergyStorage other)
	{
		return base.canTransferTo(other) && log.getMode(EnumLogisticType.ENERGIE).canExtract();
	}

	@Override
	public EnumEnergyMode getType()
	{
		return base.getType();
	}
	
}
