package futurepack.common.block.logistic;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.google.common.collect.AbstractIterator;

import futurepack.api.FacingUtil;
import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockValidator;
import futurepack.api.interfaces.filter.IItemFilter;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPBlockSelector;
import futurepack.common.FPSelectorHelper;
import futurepack.common.FPTileEntitys;
import futurepack.common.filter.OrGateFilter;
import futurepack.depend.api.helper.HelperInventory;
import futurepack.depend.api.helper.HelperItemFilter;
import futurepack.depend.api.interfaces.ITileInventoryProvider;
import it.unimi.dsi.fastutil.objects.Object2BooleanOpenHashMap;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.Container;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemHandlerHelper;

public class TileEntityInsertNode extends TileEntityPipeBase implements ITileInventoryProvider, ITileServerTickable
{
	private List<WeakReference<ItemPath>> moving = new ArrayList<>(16);
	
	private int size = 10;
	private Map<Item, Boolean> filterCash = new Object2BooleanOpenHashMap<Item>();
	private byte time = 0;
	
	private SimpleContainer filter_items;
	private OrGateFilter last_item_filter;
	
	public IBlockValidator sorter = new IBlockValidator() 
	{	
		@Override
		public boolean isValidBlock(Level w, ParentCoords pos)
		{
			BlockEntity t = w.getBlockEntity(pos);
			if(!(t instanceof TileEntityPipeBase))
			{
				return true;
			}
			return false;
		}
	};
	
	public TileEntityInsertNode(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.INSERT_NODE, pos, state);
		filter_items = new SimpleContainer(3);
		filter_items.addListener(inv -> {
			last_item_filter = null;
			connectedTile = null;
		});
	}
	
	@Override
	public void tickServer(Level level, BlockPos pos, BlockState pState)
	{
		super.tickServer(level, pos, pState);;
		
		if(--time<=0)
		{
			time = 4;
			size = Math.max(size, filterCash.size());
			filterCash.clear();
			filterCash = null;
			filterCash = new Object2BooleanOpenHashMap<Item>(size);
		}
			
		ArrayList<ItemStack> stacks = getRefind();
		if(!stacks.isEmpty())
		{
			IItemHandler to = getConnectedTile();
			boolean removedStacks = false;
			if(to!=null)
			{
				for(int i=0;i<stacks.size();i++)
				{
					ItemStack toInsert = stacks.get(i);
					ItemStack notInserted = ItemHandlerHelper.insertItem(to, toInsert, false);
					if(notInserted.isEmpty())
					{
						stacks.remove(i);
						i--;
						removedStacks = true;
					}
					else if(toInsert != notInserted)
					{
						removedStacks = true;
						stacks.set(i, notInserted);
					}
				}
			}
			if(removedStacks)
				return;
		}
		IItemHandler to = getConnectedTile();
		if(to != null)
		{
			FPBlockSelector sel = FPSelectorHelper.getSelectorSave(level, pos, selector, true); //TileEntitys only in mainthread
			List<ParentCoords> list = (List<ParentCoords>) sel.getValidBlocks(sorter); //yeah this is vague
			Collections.shuffle(list);
			
			retriveItems(list);
		}
	}
	
	public Direction getSide()
	{
		return getBlockState().getValue(BlockInsertNode.FACING).getOpposite();
	}
	
	private LazyOptional<IItemHandler> itemOpt;
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(getSide() != facing)
			{
				if(itemOpt!=null)
					return (LazyOptional<T>) itemOpt;
				
				IItemHandler handler = getConnectedTile();
				if(handler!=null)
				{
					LazyOptional<IItemHandler> superH = super.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, facing);
					if(superH.isPresent())
					{
						superH.addListener(p -> itemOpt = null);
						superH.map(s -> new CombinedWrapper(handler, s));
						itemOpt = superH;
						itemOpt.addListener(p -> itemOpt = null);
						return (LazyOptional<T>) itemOpt;
					}
					
				}
					
			}
		}
		return super.getCapability(capability, facing);
	}
	
	private IItemFilter getFilter()
	{
		if(last_item_filter!=null)
			return last_item_filter;
		
		last_item_filter = HelperItemFilter.createBasicFilter(filter_items.getItem(0), filter_items.getItem(1), filter_items.getItem(2));
		if(last_item_filter.isEmpty())
		{
			last_item_filter = null;
		}
		return last_item_filter;
	}
	
	private LazyOptional<IItemHandler> connectedTile;
	
	private IItemHandler getConnectedTile()
	{
		if(connectedTile!=null && connectedTile.isPresent())
			return connectedTile.orElseThrow(NullPointerException::new);
		
		BlockEntity t = level.getBlockEntity(worldPosition.relative(getSide()));
		if(t!=null)
		{
			LazyOptional<IItemHandler> opt = t.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, getSide().getOpposite());
			if(opt.isPresent())
			{
				opt.addListener(p -> connectedTile = null);
				connectedTile = opt.lazyMap(cap -> new FilteredWrapper(cap, getFilter()));
				connectedTile.addListener(p -> connectedTile = null);
				return connectedTile.orElse(null);
			}
		}
		return null;
	}
	
	
	public Iterator<Integer> getValidSlots(IItemHandler from)
	{
		Direction side = getSide();
		BlockEntity tile = level.getBlockEntity(worldPosition.relative(side));
		if(tile != null)
		{
			IItemHandler to = getConnectedTile();
			if(to == null)
				return Collections.EMPTY_LIST.iterator();
			
			return getValidSlots(from, to);
		}
		return Collections.EMPTY_LIST.iterator();
	}
	
	private Iterator<Integer> getValidSlots(IItemHandler from, IItemHandler to)
	{
		return new AbstractIterator<Integer>()
		{
			private int i=0;
			
			@Override
			public Integer computeNext()
			{
				for(;i<from.getSlots();i++)
				{
					ItemStack stack = from.extractItem(i, 64, true);
					if(!stack.isEmpty())
					{
						if(filterCash.getOrDefault(stack.getItem(), true))
						{
							ItemStack rest = ItemHandlerHelper.insertItem(to, stack, true);
							if(rest.isEmpty())
							{
								Integer next = i;
								i++;
								return next;
							}
							else if(rest.getCount() < stack.getCount())
							{
								Integer next = i;
								i++;
								return next;
							}
							else
							{
								filterCash.put(stack.getItem(), false);
							}
						}
					}
				}
				return endOfData();
			}
		};
	}
	
	public void retriveItems(List<ParentCoords> containers)
	{
		for(ParentCoords coords : containers)
		{
			BlockEntity container = level.getBlockEntity(coords);
			if(container==null)
				continue;
			Direction face = FacingUtil.getSide(coords, coords.getParent());
			container.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, face).ifPresent(handler ->
			{
				Iterator<Integer> slots = getValidSlots(handler);
				
				while(slots.hasNext())
				{
					Integer slot = slots.next();
					if(retriveItem(handler, slot, coords))
					{
						return;
					}
				}
			});
		}
	}
	
	private boolean isItemAlreadyMoving(ItemStack stack)
	{
		Iterator<WeakReference<ItemPath>> iter = moving.iterator();
		while(iter.hasNext())
		{
			WeakReference<ItemPath> ref = iter.next();
			ItemPath path = ref.get();
			if(path==null || path.itemInPipe==null || path.itemInPipe.isEmpty())
			{
				iter.remove();
				continue;
			}
			else if(path.next<=0 && path.path.isEmpty())
			{
				iter.remove();
				continue;
			}
			
			if(ItemStack.isSame(stack, path.itemInPipe))
			{
				return true;
			}
		}
		return false;
	}
	
	private boolean retriveItem(IItemHandler from, int slot, ParentCoords container)
	{
		TileEntityPipeBase pipe = (TileEntityPipeBase) level.getBlockEntity(container.getParent());
		
		ItemStack it = from.extractItem(slot, 1, true);
		if(it.isEmpty())
		{
			return false;
		}
		if(isItemAlreadyMoving(it))
		{
			return false;
		}
		IItemHandler to = getConnectedTile();
		ItemStack rest = ItemHandlerHelper.insertItem(to, it, true);
		if(!rest.isEmpty())
			return false;
		
		it = from.extractItem(slot, 1, false);
		if(getFilter()!=null)
		{
			getFilter().amountTransfered(it);
		}
		
		LinkedList<BlockPos> pathL = new LinkedList<BlockPos>();
		ParentCoords c = container.getParent();
		while(c!=null)
		{
			pathL.addFirst(c);
			c = c.getParent();
		}
		if(!pathL.getFirst().equals(this.worldPosition))
		{
			pathL.addFirst(this.worldPosition);
		}
		
		ItemPath path = pipe.addItemPath(it, this.worldPosition.relative(getSide()), container, new ArrayList<BlockPos>(pathL));
		moving.add(new WeakReference<TileEntityPipeBase.ItemPath>(path));
		return true;
	}

	@Override
	public boolean isSideLocked(Direction side)
	{
		if(side == getSide())
			return true;
		
		return super.isSideLocked(side);
	}

	
	@Override
	public Container getInventory() 
	{
		return filter_items;
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt) 
	{
		HelperInventory.storeInventory("filters", nbt, filter_items);
		return super.writeDataUnsynced(nbt);
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt) 
	{
		HelperInventory.loadInventory("filters", nbt, filter_items);
		super.readDataUnsynced(nbt);
	}

	@Override
	public String getGUITitle() {
		return "gui.insert_node.title";
	}
}
