package futurepack.common.block.inventory;

import java.util.ArrayList;

import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.item.CraftingItems;
import futurepack.common.player.FakePlayerFactory;
import futurepack.common.player.FuturepackPlayer;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.wrapper.InvWrapper;

public class TileEntityBlockPlacer extends TileEntityInventoryBase implements ITileServerTickable
{
	private boolean last;
	private IItemHandler wrapper;
	private LazyOptional<IItemHandler> handler;
	
	private int offset;
	
	public static final int SLOT_RANGE = 9;
	
	public TileEntityBlockPlacer(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.BLOCK_PLACER, pos, state);
		
		wrapper = new InvWrapper(this);
	}
	
	@Override
	public void tickServer(Level level, BlockPos pos, BlockState state)
	{
		boolean now = level.getBestNeighborSignal(pos)>0;
		if(now && !last)
		{
			ArrayList<Integer> list = new ArrayList<Integer>(9);
			for(int i=0;i<9;i++)
			{
				if(!items.get(i).isEmpty())
				{
					list.add(i);
				}
			}
			if(list.size()>0)
			{
				int i = level.random.nextInt(list.size());
				int slot = list.get(i);
				items.set(slot, useItem(items.get(slot)));
				level.updateNeighbourForOutputSignal(pos, state.getBlock());
				if(!items.get(slot).isEmpty() && items.get(slot).getCount()<=0)
				{
					items.set(slot, ItemStack.EMPTY);
				}
			}
					
		}
		last = now;
	}

	@Override
	protected int getInventorySize()
	{
		return 10;
	}

	
	
	private ItemStack useItem(ItemStack it)
	{
		if(level instanceof ServerLevel)
		{
			BlockState state = getBlockState();
			Direction face = state.getValue(BlockRotateableTile.FACING);
			BlockPos pos = this.getBlockPos().relative(face, 1+offset);
			FuturepackPlayer pl = FakePlayerFactory.INSTANCE.getPlayer((ServerLevel) level);
			pl.setItemInHand(InteractionHand.MAIN_HAND, it); //this is important
			
			FakePlayerFactory.setupPlayer(pl, pos, face.getOpposite());
			InteractionResultHolder<ItemStack> ar = FakePlayerFactory.itemClick(level, pos, face, it, pl, InteractionHand.MAIN_HAND);
			
			if(ar.getResult() == InteractionResult.PASS || ar.getResult() == InteractionResult.FAIL)
			{
				offset++;
				offset %= getItem(SLOT_RANGE).isEmpty() ? 1 : (1 + getItem(SLOT_RANGE).getCount());
			}
			return ar.getObject();
			
		}
		return it;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(handler!=null)
				return (LazyOptional<T>) handler;
			else
			{
				handler = LazyOptional.of(() -> wrapper);
				handler.addListener(h -> handler = null);
				return (LazyOptional<T>) handler;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public boolean canPlaceItem(int slot, ItemStack it)
	{
		if(slot == SLOT_RANGE)
		{
			return it.getItem() == CraftingItems.upgrade_range;
		}
		
		return super.canPlaceItem(slot, it);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(handler);
		super.setRemoved();
	}
	
	@Override
	public String getGUITitle() 
	{
		return "block.futurepack.block_placer";
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		nbt.putInt("block_offset", offset);
		return super.writeDataUnsynced(nbt);
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		offset = nbt.getInt("block_offset");
		super.readDataUnsynced(nbt);
	}
}
