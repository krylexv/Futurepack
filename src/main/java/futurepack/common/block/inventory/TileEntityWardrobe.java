package futurepack.common.block.inventory;

import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.block.logistic.IInvWrapper;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.depend.api.interfaces.ITileInventoryProvider;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.Container;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;

public class TileEntityWardrobe extends FPTileEntityBase implements ITileInventoryProvider
{
	private ItemStackHandler itemHandler;
	private LazyOptional<IItemHandler> itemOpt;
	
	public TileEntityWardrobe(BlockEntityType<? extends TileEntityWardrobe> type, int size, BlockPos pos, BlockState state)
	{
		super(type, pos, state);
		itemHandler = new ItemStackHandler(size);
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		nbt.put("container", itemHandler.serializeNBT());
		
		return super.writeDataUnsynced(nbt);
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		if(nbt.contains("container"))
		{
			itemHandler.deserializeNBT(nbt.getCompound("container"));
		}
			
		super.readDataUnsynced(nbt);
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(itemOpt!=null)
			{
				return (LazyOptional<T>) itemOpt;
			}
			else
			{
				itemOpt = LazyOptional.of( ()-> itemHandler);
				itemOpt.addListener(p -> itemOpt = null);
				return (LazyOptional<T>) itemOpt;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public Container getInventory() 
	{
		return getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).map(IInvWrapper::new).orElse(null);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(itemOpt);
		super.setRemoved();
	}
	
	public static class Normal extends TileEntityWardrobe
	{
		public Normal(BlockPos pos, BlockState state) 
		{
			super(FPTileEntitys.WARDROBE_N, 27, pos, state);
		}
	}
	
	public static class Large extends TileEntityWardrobe
	{
		public Large(BlockPos pos, BlockState state) 
		{
			super(FPTileEntitys.WARDROBE_L, 36, pos, state);
		}
	}

	@Override
	public String getGUITitle() {
		return "gui.futurepack.wardrobe.title";
	}
}
