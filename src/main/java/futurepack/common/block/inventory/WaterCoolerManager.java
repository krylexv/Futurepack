package futurepack.common.block.inventory;

import java.util.HashMap;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.material.Fluid;
import net.minecraftforge.registries.ForgeRegistries;

public class WaterCoolerManager
{
	public static final WaterCoolerManager INSTANCE = new WaterCoolerManager();
	
	public HashMap<Fluid, CoolingEntry> recipes = new HashMap<Fluid, CoolingEntry>();
	
	public WaterCoolerManager()
	{
		//	Recalculation for IC	(all per tick)
		//	1mB Steam => 0,5EU (Steam Turbine)
		//  1mB Water => 100 mB Steam ()
		//
		//  WaterCooler: 0 - 20 CP
		//
		//	Problem: 20 * 13 mB Water => 20 * 1300 mb Steam = 26000 mb Steam => 13000 EU
		//		CP: 	Small Nuke: <500 EU  		Large Nuke: <2000 EU		3 Coalburner => Steam: ~30 EU	
		//
		//	Actual:  1 * 0.065 mB Water  = 0.065 mB Water	=> 1 * 6.5 mb Steam = 6.5 mb Steam => 3.25 EU
		//           20 * 0.065 mB Water = 1.3 mB Water		=> 20 * 6.5 mb Steam = 130 mb Steam => 65 EU
		
		
		//OK here comes some math
		//lets say each machine is 1*1*1 and out of pure copper
		//so 8920 kg
		//c from copper is = c = 4190J � kg-1K-1
		//so cooling a machine 1�C down makes 37.374800MJ
		//
		//so how much kWh are that ?  => 10.38188 kWh
		//normale STeckdose 220V * 16A = 3520W
		// 10381.88 Wh / 3520W = 2.949h = 176.964min
		//
		//to boil water we need 2257 kJ/kg
		//to heat water up to 100�C we need (from 20�C so delta T is 80K) 
		//1mB = 1dm3 so 1mb Water is = 1kg
		//c from water = 4182 J/(kg�K)
		//water from 20 to 100�C needs = 334.560kJ
		//
		//so boiling 1mb Water needs 2591.56kJ
		//
		//but we got MJ !
		// so coiling the machine 1�C down needs 
		// 37374.800KJ / 2591.56kJ
		//14,42mB of water
		// --------> ok so 1�C = 14mB water
		addEntry("minecraft:water", "steam", 100.0F, 0.14F / 2.0F, 0.14F / 2.0F);
		addEntry("minecraft:water", "mekanism:steam", 100.0F, 0.14F / 2.0F, 0.14F / 2.0F);
		//like water so...
		addEntry("ic2:distilled_water", "ic2:steam", 100.0F, 0.13F / 2.0F, 13.0F / 2.0F);
		/*
		 ok same for forestry crushed ice
		 -8C to 0C
		 c von eis = 2220 J/(kg * K)
		 so to heat it up we need  17.76kJ
		 333.5 kj/kg to schmet ice to water
		 to in total 351.26 kJ
		 37374.800KJ / 
		 -----> 1�C = 106mB crushed ice
		 */
		addEntry("forestry:ice", "minecraft:water", -8F, 106.0F / 200.0F, 106.0F / 200.0F);
		 /*
		 -------- coolant from IC 2 -------------
		 from 26�C to 926�C
		 so delta from 900
		 c from salt wate = 4.223 kJ/(gk * K)
		 so 1mB needs 3800.7 kJ
		 37374.800KJ /  3800.7
		 ---> 1�C = 9.8 mB ~10mB
		*/
		addEntry("ic2:coolant", "ic2:hot_coolant", 926.0F, 10.0F / 20.0F, 10.0F / 20.0F);
	}
	
	public void addEntry(Fluid input, Fluid output, float targetTemp, float usedAmount, float usedAmoundOutput)
	{
		if(input==null || output == null)
			return;
		
		recipes.put(input, new CoolingEntry(targetTemp, input, output, usedAmount, usedAmoundOutput));
	}
	
	
	public void addEntry(String input, String output, float targetTemp, float usedAmount, float usedAmoundOutput)
	{
		Fluid in = ForgeRegistries.FLUIDS.getValue(new ResourceLocation(input));
		Fluid out = ForgeRegistries.FLUIDS.getValue(new ResourceLocation(output));
		addEntry(in, out, targetTemp, usedAmount, usedAmoundOutput);
	}
	
	public boolean isFluidAccepted(Fluid fl)
	{
		return recipes.containsKey(fl);
	}
	
	public CoolingEntry getCooling(Fluid fl)
	{
		return recipes.get(fl);	
	}
	
	
	
	public static class CoolingEntry
	{
		/**
		 * The temperiture this machine will get at maximum
		 */
		public float targetTemp;
		
		/**
		 * the cold fluid
		 */
		public Fluid input;
		/**
		 * the hot fluid
		 */
		public Fluid output;
		/**
		 * the amount of fluid wich is needed to cool the machine 1�C down.
		 */
		public float usedAmount;
		public float usedAmountOutput;
		
		
		public CoolingEntry(float targetTemp, Fluid input, Fluid output, float usedAmount, float usedAmountOutput)
		{
			super();
			this.targetTemp = targetTemp;
			this.input = input;
			this.output = output;
			this.usedAmount = usedAmount;
			this.usedAmountOutput = usedAmountOutput;
		}
		
		
		
	}
}
