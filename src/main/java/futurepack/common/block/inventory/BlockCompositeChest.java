package futurepack.common.block.inventory;

import futurepack.common.FPTileEntitys;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.interfaces.ITileInventoryProvider;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.ChestBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;

public class BlockCompositeChest extends ChestBlock
{
	protected BlockCompositeChest(Block.Properties props)
	{
		super(props, () -> {
		      return FPTileEntitys.COMPOSITE_CHEST;
		});
	}
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if (w.isClientSide)
		{
			return InteractionResult.SUCCESS;
		}
		else
		{
			if(((ITileInventoryProvider)w.getBlockEntity(pos)).getInventory()!=null)
			{
				FPGuiHandler.GENERIC_CHEST.openGui(pl, pos);
				return InteractionResult.SUCCESS;
			}
			else
			{
				return InteractionResult.FAIL;
			}
		}
	}

	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new TileEntityCompositeChest(pos, state);
	}
}
