package futurepack.common.block.inventory;

import java.util.function.Supplier;

import futurepack.api.Constants;
import futurepack.common.FPTileEntitys;
import futurepack.common.FuturepackMain;
import futurepack.common.block.deco.DecoBlocks;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.material.Material;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class InventoryBlocks 
{
	//Don't use DecoBlocks.default_white etc directly, since for maschine_white, since BlockHoldingTile will set noDrops to the Block.Properties and corrupt it for later use.
	public static final Block.Properties maschine_white = Block.Properties.copy(DecoBlocks.color_iron_white);
	public static final Block.Properties maschine_light_gray = Block.Properties.copy(DecoBlocks.color_iron_light_gray);
	public static final Block.Properties maschine_black = Block.Properties.copy(DecoBlocks.color_iron_black);
	public static final Block.Properties maschine_white_notsolid = Block.Properties.of(Material.METAL, DyeColor.byName("white", null)).strength(5F, 10F).sound(SoundType.METAL).noOcclusion();
	
	public static final Block.Properties simple_white = Block.Properties.copy(DecoBlocks.color_iron_fence_white);
	public static final Block.Properties simple_light_gray = Block.Properties.copy(DecoBlocks.color_iron_fence_light_gray);
	public static final Block.Properties simple_black = Block.Properties.copy(DecoBlocks.color_iron_fence_black);
	
	public static final Block techtable = HelperItems.setRegistryName(new BlockTechtable(simple_light_gray), Constants.MOD_ID, "techtable");
	
	public static final Block scanner_block_w = HelperItems.setRegistryName(new BlockScanner(maschine_white), Constants.MOD_ID, "scanner_block_white");
	public static final Block scanner_block_g = HelperItems.setRegistryName(new BlockScanner(maschine_light_gray), Constants.MOD_ID, "scanner_block_gray");
	public static final Block scanner_block_b = HelperItems.setRegistryName(new BlockScanner(maschine_black), Constants.MOD_ID, "scanner_block_black");
	
	public static final Block forscher_w = HelperItems.setRegistryName(new BlockForscher(maschine_white), Constants.MOD_ID, "researcher_white");
	public static final Block forscher_g = HelperItems.setRegistryName(new BlockForscher(maschine_light_gray), Constants.MOD_ID, "researcher_gray");
	public static final Block forscher_b = HelperItems.setRegistryName(new BlockForscher(maschine_black), Constants.MOD_ID, "researcher_black");
	
	public static final Block industrial_furnace = HelperItems.setRegistryName(new BlockIndFurnace(maschine_white), Constants.MOD_ID, "industrial_furnace");
	
	public static final Block assembly_table_w = HelperItems.setRegistryName(new BlockAssemblyTable(maschine_white), Constants.MOD_ID, "assembly_table_white");
	public static final Block assembly_table_g = HelperItems.setRegistryName(new BlockAssemblyTable(maschine_light_gray), Constants.MOD_ID, "assembly_table_gray");
	public static final Block assembly_table_b = HelperItems.setRegistryName(new BlockAssemblyTable(maschine_black), Constants.MOD_ID, "assembly_table_black");
	
	public static final Block t0_generator = HelperItems.setRegistryName(new BlockBrennstoffGenerator(maschine_white_notsolid), Constants.MOD_ID, "t0_generator");
	
	public static final Block battery_box_w = HelperItems.setRegistryName(new BlockBatteryBox(maschine_white), Constants.MOD_ID, "battery_box_white");
	public static final Block battery_box_g = HelperItems.setRegistryName(new BlockBatteryBox(maschine_light_gray), Constants.MOD_ID, "battery_box_gray");
	public static final Block battery_box_b = HelperItems.setRegistryName(new BlockBatteryBox(maschine_black), Constants.MOD_ID, "battery_box_black");
	
	public static final Block composite_chest = HelperItems.setRegistryName(new BlockCompositeChest(simple_white), Constants.MOD_ID, "composite_chest");
	
	public static final Block flash_server_w = HelperItems.setRegistryName(new BlockFlashServer(maschine_white), Constants.MOD_ID, "flash_server_white");
	public static final Block flash_server_g = HelperItems.setRegistryName(new BlockFlashServer(maschine_light_gray), Constants.MOD_ID, "flash_server_gray");
	public static final Block flash_server_b = HelperItems.setRegistryName(new BlockFlashServer(maschine_black), Constants.MOD_ID, "flash_server_black");
	
	public static final Block part_press = HelperItems.setRegistryName(new BlockPartPress(simple_light_gray), Constants.MOD_ID, "part_press");
	public static final Block pusher = HelperItems.setRegistryName(new BlockPusher(simple_light_gray), Constants.MOD_ID, "pusher");
	
	public static final Block wardrobe_white_normal_1 = HelperItems.setRegistryName(new BlockWandrobe(simple_white, false), Constants.MOD_ID, "wardrobe_white_normal_1");
	public static final Block wardrobe_white_normal_2 = HelperItems.setRegistryName(new BlockWandrobe(simple_white, false), Constants.MOD_ID, "wardrobe_white_normal_2");
	public static final Block wardrobe_white_large_1 = HelperItems.setRegistryName(new BlockWandrobe(simple_white, true), Constants.MOD_ID, "wardrobe_white_large_1");
	public static final Block wardrobe_white_large_2 = HelperItems.setRegistryName(new BlockWandrobe(simple_white, true), Constants.MOD_ID, "wardrobe_white_large_2");
	public static final Block wardrobe_light_gray_normal_1 = HelperItems.setRegistryName(new BlockWandrobe(simple_light_gray, false), Constants.MOD_ID, "wardrobe_light_gray_normal_1");
	public static final Block wardrobe_light_gray_normal_2 = HelperItems.setRegistryName(new BlockWandrobe(simple_light_gray, false), Constants.MOD_ID, "wardrobe_light_gray_normal_2");
	public static final Block wardrobe_light_gray_large_1 = HelperItems.setRegistryName(new BlockWandrobe(simple_light_gray, true), Constants.MOD_ID, "wardrobe_light_gray_large_1");
	public static final Block wardrobe_light_gray_large_2 = HelperItems.setRegistryName(new BlockWandrobe(simple_light_gray, true), Constants.MOD_ID, "wardrobe_light_gray_large_2");
	public static final Block wardrobe_black_normal_1 = HelperItems.setRegistryName(new BlockWandrobe(simple_black, false), Constants.MOD_ID, "wardrobe_black_normal_1");
	public static final Block wardrobe_black_normal_2 = HelperItems.setRegistryName(new BlockWandrobe(simple_black, false), Constants.MOD_ID, "wardrobe_black_normal_2");
	public static final Block wardrobe_black_large_1 = HelperItems.setRegistryName(new BlockWandrobe(simple_black, true), Constants.MOD_ID, "wardrobe_black_large_1");
	public static final Block wardrobe_black_large_2 = HelperItems.setRegistryName(new BlockWandrobe(simple_black, true), Constants.MOD_ID, "wardrobe_black_large_2");
	
	public static final Block block_breaker = HelperItems.setRegistryName(new BlockBreaker(simple_light_gray), Constants.MOD_ID, "block_breaker");
	public static final Block block_placer = HelperItems.setRegistryName(new BlockPlacer(simple_white), Constants.MOD_ID, "block_placer");
	public static final Block fuel_cell = HelperItems.setRegistryName(new BlockFuelCell(simple_light_gray), Constants.MOD_ID, "fuel_cell");
	public static final Block drone_station = HelperItems.setRegistryName(new BlockDroneStation(maschine_light_gray), Constants.MOD_ID, "drone_station");
	
	public static final Block modul_1_w = HelperItems.setRegistryName(new BlockModul<TileEntityModulT1>(maschine_white, () -> FPTileEntitys.MODUL_T1, () -> FPGuiHandler.MODUL_1), Constants.MOD_ID, "modul_1_white");
	public static final Block modul_1_g = HelperItems.setRegistryName(new BlockModul<TileEntityModulT1>(maschine_light_gray, () -> FPTileEntitys.MODUL_T1, () -> FPGuiHandler.MODUL_1), Constants.MOD_ID, "modul_1_gray");
	public static final Block modul_1_s = HelperItems.setRegistryName(new BlockModul<TileEntityModulT1>(maschine_black, () -> FPTileEntitys.MODUL_T1, () -> FPGuiHandler.MODUL_1), Constants.MOD_ID, "modul_1_black");
	
	public static final Block modul_2_w = HelperItems.setRegistryName(new BlockModul<>(maschine_white, () -> FPTileEntitys.MODUL_T2, () -> FPGuiHandler.MODUL_2), Constants.MOD_ID, "modul_2_white");
	public static final Block modul_2_g = HelperItems.setRegistryName(new BlockModul<>(maschine_light_gray, () -> FPTileEntitys.MODUL_T2, () -> FPGuiHandler.MODUL_2), Constants.MOD_ID, "modul_2_gray");
	public static final Block modul_2_s = HelperItems.setRegistryName(new BlockModul<>(maschine_black, () -> FPTileEntitys.MODUL_T2, () -> FPGuiHandler.MODUL_2), Constants.MOD_ID, "modul_2_black");
	
	public static final Block modul_3_w = HelperItems.setRegistryName(new BlockModul<>(maschine_white, () -> FPTileEntitys.MODUL_T3, () -> FPGuiHandler.MODUL_3), Constants.MOD_ID, "modul_3_white");
	public static final Block modul_3_g = HelperItems.setRegistryName(new BlockModul<>(maschine_light_gray, () -> FPTileEntitys.MODUL_T3, () -> FPGuiHandler.MODUL_3), Constants.MOD_ID, "modul_3_gray");
	public static final Block modul_3_s = HelperItems.setRegistryName(new BlockModul<>(maschine_black, () -> FPTileEntitys.MODUL_T3, () -> FPGuiHandler.MODUL_3), Constants.MOD_ID, "modul_3_black");
	
	public static final Block modul_1_calc_w = HelperItems.setRegistryName(new BlockModulClientTicking<>(maschine_white, () -> FPTileEntitys.MODUL_T1_CALC, () -> FPGuiHandler.MODUL_1_CALC), Constants.MOD_ID, "modul_1_calculation_white");
	public static final Block modul_1_calc_g = HelperItems.setRegistryName(new BlockModulClientTicking<>(maschine_light_gray, () -> FPTileEntitys.MODUL_T1_CALC, () -> FPGuiHandler.MODUL_1_CALC), Constants.MOD_ID, "modul_1_calculation_gray");
	public static final Block modul_1_calc_s = HelperItems.setRegistryName(new BlockModulClientTicking<>(maschine_black, () -> FPTileEntitys.MODUL_T1_CALC, () -> FPGuiHandler.MODUL_1_CALC), Constants.MOD_ID, "modul_1_calculation_black");
	
	public static final Block board_computer_w = HelperItems.setRegistryName(new BlockBoardComputer(maschine_white), Constants.MOD_ID, "board_computer_white");
	public static final Block board_computer_g = HelperItems.setRegistryName(new BlockBoardComputer(maschine_light_gray), Constants.MOD_ID, "board_computer_gray");
	public static final Block board_computer_s = HelperItems.setRegistryName(new BlockBoardComputer(maschine_black), Constants.MOD_ID, "board_computer_black");
	
	public static final Block advanced_board_computer_w = HelperItems.setRegistryName(new BlockAdvancedBoardcomputer(maschine_white), Constants.MOD_ID, "advanced_board_computer_white");
	public static final Block advanced_board_computer_g = HelperItems.setRegistryName(new BlockAdvancedBoardcomputer(maschine_light_gray), Constants.MOD_ID, "advanced_board_computer_gray");
	public static final Block advanced_board_computer_s = HelperItems.setRegistryName(new BlockAdvancedBoardcomputer(maschine_black), Constants.MOD_ID, "advanced_board_computer_black");
	
	public static final Block water_cooler = HelperItems.setRegistryName(new BlockWaterCooler(DecoBlocks.default_light_gray_notsolid), Constants.MOD_ID, "water_cooler");
	public static final Block fermentation_barrel = HelperItems.setRegistryName(new BlockFermentationBarrel(maschine_white), Constants.MOD_ID, "fermentation_barrel");
	
	public static final Block shared_researcher_w = HelperItems.setRegistryName(new BlockSharedResearcher(maschine_white), Constants.MOD_ID, "shared_researcher_white");
	public static final Block shared_researcher_g = HelperItems.setRegistryName(new BlockSharedResearcher(maschine_light_gray), Constants.MOD_ID, "shared_researcher_gray");
	public static final Block shared_researcher_s = HelperItems.setRegistryName(new BlockSharedResearcher(maschine_black), Constants.MOD_ID, "shared_researcher_black");
	
	public static final Block logistic_chest = HelperItems.setRegistryName(new BlockLogisticChest(maschine_white), Constants.MOD_ID, "logistic_chest");
	
	public static final Block optibench_crafting_module_w = HelperItems.setRegistryName(new BlockOptibenchCraftingModule(maschine_white), Constants.MOD_ID, "optibench_crafting_module_white");
	public static final Block pusher_ticking = HelperItems.setRegistryName(new BlockPusher(simple_light_gray, (Supplier<BlockEntityType<? extends TileEntityPusher>>)() -> FPTileEntitys.PUSHER_TICKING), Constants.MOD_ID, "pusher_ticking");
	
	
	public static void registerBlocks(RegistryEvent.Register<Block> event)
	{
		IForgeRegistry<Block> r = event.getRegistry();
		
		r.register(techtable);
		r.registerAll(scanner_block_w, scanner_block_g, scanner_block_b);
		r.registerAll(forscher_w, forscher_g, forscher_b);
		r.register(industrial_furnace);
		r.registerAll(assembly_table_w, assembly_table_g, assembly_table_b);
		r.register(t0_generator);
		r.registerAll(battery_box_w, battery_box_g, battery_box_b);
		r.register(composite_chest);
		r.registerAll(flash_server_w, flash_server_g, flash_server_b);
		r.registerAll(part_press, pusher);
		r.registerAll(wardrobe_white_normal_1, wardrobe_white_normal_2, wardrobe_white_large_1, wardrobe_white_large_2);
		r.registerAll(wardrobe_light_gray_normal_1, wardrobe_light_gray_normal_2, wardrobe_light_gray_large_1, wardrobe_light_gray_large_2);
		r.registerAll(wardrobe_black_normal_1, wardrobe_black_normal_2, wardrobe_black_large_1, wardrobe_black_large_2);
		r.registerAll(block_breaker, block_placer, fuel_cell, drone_station);
		r.registerAll(modul_1_w, modul_1_g, modul_1_s);
		r.registerAll(modul_2_w, modul_2_g, modul_2_s);
		r.registerAll(modul_3_w, modul_3_g, modul_3_s);
		r.registerAll(modul_1_calc_w, modul_1_calc_g, modul_1_calc_s);
		r.registerAll(board_computer_w, board_computer_g, board_computer_s);
		r.registerAll(advanced_board_computer_w, advanced_board_computer_g, advanced_board_computer_s);
		r.registerAll(shared_researcher_w, shared_researcher_g, shared_researcher_s);
		r.registerAll(water_cooler, fermentation_barrel, logistic_chest, optibench_crafting_module_w, pusher_ticking);
	}
	
	public static void registerItems(RegistryEvent.Register<Item> event)
	{
		IForgeRegistry<Item> r = event.getRegistry();
		
		r.register( item(techtable));
		r.register( item(scanner_block_w));
		r.register( item(scanner_block_g));
		r.register( item(scanner_block_b));
		r.register( item(forscher_w));
		r.register( item(forscher_g));
		r.register( item(forscher_b));
		r.register(item(industrial_furnace));
		r.registerAll(item(assembly_table_w), item(assembly_table_g), item(assembly_table_b));
		r.register(item(t0_generator));
		r.registerAll(item(battery_box_w), item(battery_box_g), item(battery_box_b));
		r.register(item(composite_chest));
		r.registerAll(item(flash_server_w), item(flash_server_g), item(flash_server_b));
		r.registerAll(item(part_press), item(pusher));
		r.registerAll(item(wardrobe_white_normal_1), item(wardrobe_white_normal_2), item(wardrobe_white_large_1), item(wardrobe_white_large_2));
		r.registerAll(item(wardrobe_light_gray_normal_1), item(wardrobe_light_gray_normal_2), item(wardrobe_light_gray_large_1), item(wardrobe_light_gray_large_2));
		r.registerAll(item(wardrobe_black_normal_1), item(wardrobe_black_normal_2), item(wardrobe_black_large_1), item(wardrobe_black_large_2));
		r.registerAll(item(block_breaker), item(block_placer), item(fuel_cell), item(drone_station));
		r.registerAll(item(modul_1_w), item(modul_1_g), item(modul_1_s));
		r.registerAll(item(modul_2_w), item(modul_2_g), item(modul_2_s));
		r.registerAll(item(modul_3_w), item(modul_3_g), item(modul_3_s));
		r.registerAll(item(modul_1_calc_w), item(modul_1_calc_g), item(modul_1_calc_s));
		r.registerAll(item(board_computer_w), item(board_computer_g), item(board_computer_s));
		r.registerAll(item(advanced_board_computer_w), item(advanced_board_computer_g), item(advanced_board_computer_s));
		r.registerAll(item(shared_researcher_w),item(shared_researcher_g),item(shared_researcher_s));
		r.registerAll(item(water_cooler), item(fermentation_barrel), item(logistic_chest), item(optibench_crafting_module_w), item(pusher_ticking));
	}
	
	private final static Item item(Block bl)
	{
		return new BlockItem(bl, (new Item.Properties()).tab(FuturepackMain.tab_maschiens)).setRegistryName(HelperItems.getRegistryName(bl));
	}
}
