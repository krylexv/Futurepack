package futurepack.common.block.inventory;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.FacingUtil;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.CapabilityLogistic;
import futurepack.api.capabilities.ILogisticInterface;
import futurepack.api.interfaces.IFluidTankInfo.FluidTankInfo;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.FuturepackTags;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.block.logistic.LogisticFluidWrapper;
import futurepack.common.block.logistic.LogisticItemHandlerWrapper;
import futurepack.common.block.misc.MiscBlocks;
import futurepack.common.block.misc.TileEntityExternCooler;
import futurepack.common.fluids.FPFluids;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Fluid;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.FluidActionResult;
import net.minecraftforge.fluids.FluidAttributes;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler.FluidAction;
import net.minecraftforge.fluids.capability.templates.FluidTank;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.ForgeRegistry;

public class TileEntityFermentationBarrel extends FPTileEntityBase implements ITilePropertyStorage, ITileServerTickable
{
	private LogisticStorage storage;
	private FluidTank gas;
	
	private ItemContainer handler;
	
	private int progress;
	
	private LazyOptional<ILogisticInterface>[] logOpt;
	private LazyOptional<IFluidHandler>[] fluidOpt;
	private LazyOptional<IItemHandler>[] itemOpt;
	
	public TileEntityFermentationBarrel(BlockPos pos, BlockState state)
	{
		this(FPTileEntitys.FERMENTATION_BARREL, pos, state);
	}
	
	@SuppressWarnings("unchecked")
	public TileEntityFermentationBarrel(BlockEntityType<? extends TileEntityFermentationBarrel> type, BlockPos pos, BlockState state)
	{
		super(type, pos, state);
		
		storage = new LogisticStorage(this, this::onLogisticChange, EnumLogisticType.ITEMS, EnumLogisticType.FLUIDS);
		storage.setDefaut(EnumLogisticIO.INOUT, EnumLogisticType.ITEMS);
		storage.setDefaut(EnumLogisticIO.OUT, EnumLogisticType.FLUIDS);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.FLUIDS);
		storage.removeState(EnumLogisticIO.IN, EnumLogisticType.FLUIDS);
		
		gas = new FluidTank(FluidAttributes.BUCKET_VOLUME*8);
//		gas.setCanDrain(true); logistic manage should handle only output IO.
//		gas.setCanFill(false);

		progress = 0;
		
		handler = new ItemContainer();
		
		itemOpt = new LazyOptional[6];
		fluidOpt = new LazyOptional[6];
		logOpt = new LazyOptional[6];
		
	}
	
	protected void onLogisticChange(Direction face, EnumLogisticType type)
	{
		if(type == EnumLogisticType.FLUIDS)
		{
			if(fluidOpt[face.get3DDataValue()]!=null)
			{
				fluidOpt[face.get3DDataValue()].invalidate();
				fluidOpt[face.get3DDataValue()] = null;
			}
		}
		if(type == EnumLogisticType.ITEMS)
		{
			if(itemOpt[face.get3DDataValue()]!=null)
			{
				itemOpt[face.get3DDataValue()].invalidate();
				itemOpt[face.get3DDataValue()] = null;
			}
		}
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(fluidOpt);
		HelperEnergyTransfer.invalidateCaps(logOpt);
		HelperEnergyTransfer.invalidateCaps(itemOpt);
		super.setRemoved();
	}
	
	@Override
	public void tickServer(Level level, BlockPos worldPosition, BlockState pState)
	{

			progress++;
			
			if(!handler.getStackInSlot(0).isEmpty())
			{				
				int time = (int) (24.0f - Math.sqrt(handler.getStackInSlot(0).getCount()*4)) * 3;
				
				if(progress >= time)
				{
					progress = 0;
				
					if(!handler.getStackInSlot(0).isEmpty() && handler.insertItemInternal(1, new ItemStack(Blocks.DIRT), true).isEmpty())
					{

						if(handler.getStackInSlot(0).is(FuturepackTags.COMPOST))
						{		
							for(Direction f : FacingUtil.VALUES)//TODO @Wugi why is only at the start the external cooler important?
							{
								BlockPos p = worldPosition.relative(f);
								if(level.getBlockState(p).getBlock() == MiscBlocks.extern_cooler)
								{
									TileEntityExternCooler t = (TileEntityExternCooler) level.getBlockEntity(p);
									
									if(t==null)
										continue;
										
									int temp = Math.max((int)(t.f - 1), 0);
										
									switch(temp)
									{
									case 0: progress += 2; break;
									case 1: progress += 3; break;
									case 2: progress += 1; break;
									};
								}
							}
								
								
							int amount = 125;
								
							if(amount + gas.getFluidAmount() <= gas.getCapacity())
							{
								gas.fill(new FluidStack(FPFluids.biogasFluidStill, amount), FluidAction.EXECUTE);
								
								handler.extractItem(0, 1, false, true);
										
								if(level.random.nextInt(10) == 0)
								{
									handler.insertItemInternal(1, new ItemStack(Blocks.DIRT), false);
								}
									
								updateData();
							}
						}
					}
					
				}
			}
			else
			{
				progress = 0;
			}

			if(gas.getFluidAmount() > 0)
			{
				BlockEntity te = level.getBlockEntity(worldPosition.above());
				
				if(te != null)
				{
					te.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, Direction.DOWN).ifPresent(handler ->{
						int res = handler.fill(new FluidStack(gas.getFluid(), Math.min(gas.getFluidAmount(), 125)), FluidAction.EXECUTE);
						if(res > 0)
						{
							gas.drain(res, FluidAction.EXECUTE);
							
							updateData();
						}
					});
				}
			}
			
			if(!handler.getStackInSlot(2).isEmpty())
			{
				FluidActionResult stack = FluidUtil.tryFillContainer(handler.getStackInSlot(2), gas, gas.getFluidAmount(), null, false);
				if(stack.success)
				{
					if(handler.insertItemInternal(3, stack.getResult(), true).isEmpty())
					{
						stack = FluidUtil.tryFillContainer(handler.getStackInSlot(2), gas, gas.getFluidAmount(), null, true);
						handler.insertItemInternal(3, stack.getResult(), false);
						
						handler.extractItem(2, stack.getResult().getCount(), false, true);	
						
						updateData();
					}
				}
			}
	}

	@Override
	public int getProperty(int id) 
	{
		switch (id)
		{
		case 0:
			if(!gas.getFluid().isEmpty())
				return ((ForgeRegistry<Fluid>)ForgeRegistries.FLUIDS).getID(gas.getFluid().getFluid());
			return -1;
		case 1:
			return gas.getFluidAmount();
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			if(value==-1)
				gas.setFluid(FluidStack.EMPTY);
			else
			{
				gas.setFluid(new FluidStack(((ForgeRegistry<Fluid>)ForgeRegistries.FLUIDS).getValue(value), 1));
			}
			break;
		case 1:
			if(gas.getFluid()!=null && !gas.isEmpty())
				gas.getFluid().setAmount(value);
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 2;
	}


	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		gas.readFromNBT(nbt.getCompound("gas"));
		storage.read(nbt);
		handler.deserializeNBT(nbt.getCompound("items"));
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		CompoundTag tag = new CompoundTag();
		gas.writeToNBT(tag);
		nbt.put("gas", tag);
	
		storage.write(nbt);
		
		CompoundTag items = handler.serializeNBT();
		nbt.put("items", items);

		return nbt;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(facing==null)
			return super.getCapability(capability, facing);
		
		if(capability==CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY)
		{
			return HelperEnergyTransfer.getFluidCap(fluidOpt, facing, this::getLogisticStorage, () -> new LogisticFluidWrapper(storage.getInterfaceforSide(facing), gas));
		}
		else if(capability==CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(itemOpt[facing.get3DDataValue()] != null)
			{
				return (LazyOptional<T>) itemOpt[facing.get3DDataValue()];
			}
			else
			{
				if(storage.getModeForFace(facing, EnumLogisticType.ITEMS) == EnumLogisticIO.NONE)
				{
					return LazyOptional.empty();
				}
				else
				{
					itemOpt[facing.get3DDataValue()] = LazyOptional.of(() -> new LogisticItemHandlerWrapper(storage.getInterfaceforSide(facing), handler));
					itemOpt[facing.get3DDataValue()].addListener(p -> itemOpt[facing.get3DDataValue()] = null);
					return (LazyOptional<T>) itemOpt[facing.get3DDataValue()];
				}
			}	
		}
		else if(capability == CapabilityLogistic.cap_LOGISTIC)
		{
			if(logOpt[facing.get3DDataValue()]!=null)
			{
				return (LazyOptional<T>) logOpt[facing.get3DDataValue()];
			}
			else
			{
				logOpt[facing.get3DDataValue()] = LazyOptional.of(() -> getLogisticStorage().getInterfaceforSide(facing));
				logOpt[facing.get3DDataValue()].addListener(p -> logOpt[facing.get3DDataValue()] = null);
				return (LazyOptional<T>) logOpt[facing.get3DDataValue()];
			}
		}
		return super.getCapability(capability, facing);
	}
	
	private LogisticStorage getLogisticStorage()
	{
		return storage;
	}

	public FluidTankInfo getGas() 
	{
		return new FluidTankInfo(gas, 0);
	}
	
	private class ItemContainer extends ItemStackHandlerGuis
	{

		public ItemContainer()
		{
			super(4);
		}
		
		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate, boolean gui)
		{
			validateSlotIndex(slot);
			
			if(isItemValid(slot, stack, gui))		
				return super.insertItem(slot, stack, simulate, gui);
			
			return stack;
		}
		
		public ItemStack insertItemInternal(int slot, ItemStack stack, boolean simulate)
		{
			validateSlotIndex(slot);			
			return super.insertItemInternal(slot, stack, simulate);
		}
		
		@Override
		public boolean isItemValid(int slot, ItemStack stack, boolean gui)
		{
			if(slot == 0)
	    	{
				return stack.is(FuturepackTags.COMPOST);
	    	}
			else if(slot == 2)
			{
				FluidTank tank = new FluidTank(4000);
				tank.setFluid(new FluidStack(FPFluids.biogasFluidStill, 4000));
				return FluidUtil.tryFillContainer(stack, tank, 4000, null, false).isSuccess();
			}
			
			return false;
		}
		
		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate, boolean gui)
		{
			validateSlotIndex(slot);
			
			if(slot==1 || gui)
				return super.extractItem(slot, amount, simulate, gui);
			return ItemStack.EMPTY;
		}
		
		@Override
		public int getSlotLimit(int slot)
		{
			return 64;
		}
	}

	public IItemHandler getGui()
	{
		return new ItemHandlerGuiOverride(handler);
	}
	

	
//	private int last_state = 0;
	
	public void updateData()
	{
//		int now = gas.getFluidAmount() / 1000;
//		if(now != last_state)
//		{
//			last_state = now;
//		
//			final SUpdateTileEntityPacket pack = getUpdatePacket();
//			world.getEntitiesWithinAABB(ServerPlayerEntity.class, new AxisAlignedBB(pos.getX()-20, pos.getY()-20, pos.getZ()-20, pos.getX()+20, pos.getY()+20, pos.getZ()+20), new Predicate<ServerPlayerEntity>() //getEntitiesWithinAABBExcludingEntity
//			{			
//				@Override
//				public boolean apply(ServerPlayerEntity var1)
//				{
//					var1.connection.sendPacket(pack);
//					return false;
//				}
//			});
//		}
//		
		//markDirty();
		
		BlockState state = getBlockState();
		int fillLevel;
		if(gas.isEmpty())
			fillLevel = 0;
		else
		{
			fillLevel = (int) Math.min(gas.getFluidAmount() / 1000F * 7F, 7F);
		}
		
		if(state.getValue(BlockFermentationBarrel.FILL) != fillLevel)
		{
			level.setBlockAndUpdate(getBlockPos(), state.setValue(BlockFermentationBarrel.FILL, fillLevel));
		}
		
	}
	

	

	
}
