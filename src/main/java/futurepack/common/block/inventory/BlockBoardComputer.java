package futurepack.common.block.inventory;

import futurepack.api.interfaces.IBlockBothSidesTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateable;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;

public class BlockBoardComputer extends BlockRotateable implements IBlockBothSidesTickingEntity<TileEntityBoardComputer>
{

	public BlockBoardComputer(Block.Properties props) 
	{
		super(props);
	}

	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		TileEntityBoardComputer comp = (TileEntityBoardComputer) w.getBlockEntity(pos);
		comp.searchForShip();
		
		if(HelperResearch.canOpen(pl, state))
		{
			if(comp.isOwner(pl))
			{
				FPGuiHandler.BOARD_COMPUTER.openGui(pl, pos);
			}
		}
		return InteractionResult.SUCCESS;
	}

	@Override
	public BlockEntityType<TileEntityBoardComputer> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.BOARD_COMPUTER;
	};
	
}
