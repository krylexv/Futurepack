package futurepack.common.block.inventory;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import futurepack.api.ItemPredicateBase;
import futurepack.api.PacketBase;
import futurepack.api.interfaces.INetworkUser;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.api.interfaces.tilentity.ITileWithOwner;
import futurepack.common.FPConfig;
import futurepack.common.FPTileEntitys;
import futurepack.common.item.misc.ItemResearchBlueprint;
import futurepack.common.item.misc.MiscItems;
import futurepack.common.network.FunkPacketCalcSolution;
import futurepack.common.network.FunkPacketCalculation;
import futurepack.common.network.FunkPacketCalculation.EnumCalculationType;
import futurepack.common.network.FunkPacketExperience;
import futurepack.common.network.FunkPacketGetResearch;
import futurepack.common.network.FunkPacketNeon;
import futurepack.common.network.FunkPacketSupport;
import futurepack.common.network.NetworkManager;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.research.Research;
import futurepack.common.research.ResearchManager;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.Container;
import net.minecraft.world.WorldlyContainer;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityForscher extends TileEntityInventoryBase implements WorldlyContainer, ITileNetwork, INetworkUser, ITilePropertyStorage, ITileWithOwner, ITileServerTickable
{
	public static final List<WeakReference<TileEntityForscher>> researchers = Collections.synchronizedList(new ArrayList<>());
	
	
	public TileEntityForscher(BlockPos pos, BlockState state) 
	{
		this(FPTileEntitys.FORSCHER, pos, state);
	}
	
	public TileEntityForscher(BlockEntityType<? extends TileEntityForscher> type, BlockPos pos, BlockState state) 
	{
		super(type, pos, state);
		researchers.add(new WeakReference<TileEntityForscher>(this));
	}

	private Research research;
	private int stored_neon,stored_support,stored_xp;
	private int neon, support, xp, time;
	private int neon_calcmodules;
//	private int neonbuff = 9;
	private boolean[] slots = new boolean[4];
	private UUID user;
	private String invname = "";
	
	private long last = 0;
	
	private RewardInventory researchRewards = new RewardInventory();
	//sync for client
	public boolean hasRewards = false;
	
	//slots 0...3: Search ; 4:Research SLot
	
	public RewardInventory getResearchRewards() {
		return researchRewards;
	}

	@Override
	public void tickServer(Level level, BlockPos worldPosition, BlockState pState)
	{
		if(checkResources())
		{
			//time, neon, support, xp;
			if(xp - stored_xp > 0)
			{
				NetworkManager.sendPacketThreaded(this, new FunkPacketExperience(worldPosition, this, xp - stored_xp));
			}
			if(support - stored_support > 0)
			{
				NetworkManager.sendPacketThreaded(this, new FunkPacketSupport(worldPosition, this, support - stored_support));
			}
			if(neon - stored_neon > 0)
			{
				NetworkManager.sendPacketThreaded(this, new FunkPacketNeon(worldPosition, this, neon - stored_neon));
			}
			
			if(System.currentTimeMillis() -last >= 1000)
			{
				if(xp>0)
				{
					NetworkManager.sendPacketThreaded(this, new FunkPacketCalculation(worldPosition, this,EnumCalculationType.EXP, xp/2));
				}
				if(support>0)
				{
					NetworkManager.sendPacketThreaded(this, new FunkPacketCalculation(worldPosition, this,EnumCalculationType.SUPPORT, support/2));
				}
				if(neon>0)
				{
					NetworkManager.sendPacketThreaded(this, new FunkPacketCalculation(worldPosition, this,EnumCalculationType.NEON, neon/2));
				}
				last = System.currentTimeMillis();
			}
			
			if(slots[3] && xp>0)
			{
				if(stored_xp>0)
				{
					stored_xp--;
					xp--;
				}		
			}
			if(slots[2] && support>0)
			{
				if(stored_support>0)
				{
					support--;
					stored_support--;
				}
			}
			if(slots[1] && neon>0)
			{
				if(stored_neon>0)
				{
//					int red = Math.min(neonbuff+1, Math.min(stored_neon, neon));			
//					stored_neon-=red;
//					neon-=red;
					stored_neon--;
					neon--;
				}
			}
			if(slots[0] && time > 0)
			{
				time--;
			}
			
			if(xp<=0 && support<=0 && neon<=0 && time<=0)
			{
				if(slots[0] & slots[1] && slots[2] && slots[3])
				{
					finishResearch();
				}
			}
		}
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		if(user!=null)
		{
			nbt.putLong("uuid1", user.getMostSignificantBits());
			nbt.putLong("uuid2", user.getLeastSignificantBits());
		}
		if(research!=null)
			nbt.putString("researchN", research.getName());
		nbt.putInt("neon", neon);
		nbt.putInt("support", support);
		nbt.putInt("xp", xp);
		nbt.putInt("time", time);
		nbt.putInt("sneon", stored_neon);
		nbt.putInt("ssupport", stored_support);
		nbt.putInt("sxp", stored_xp);
		nbt.putString("invname", invname);
//		nbt.putInt("neonbuff", neonbuff);
		
		researchRewards.write(nbt);
		
		return nbt;
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		user = new UUID(nbt.getLong("uuid1"), nbt.getLong("uuid2"));
		if(nbt.contains("research"))
		{
			research = ResearchManager.getById(nbt.getInt("research"));
		}
		else if(nbt.contains("researchN"))
		{
			research = ResearchManager.getResearch(nbt.getString("researchN"));
		}
		neon = nbt.getInt("neon");
		stored_neon = nbt.getInt("sneon");
		time = nbt.getInt("time");
		support = nbt.getInt("support");
		stored_support = nbt.getInt("ssupport");
		xp = nbt.getInt("xp");
		stored_xp = nbt.getInt("sxp");
		invname = nbt.getString("invname");
//		neonbuff = nbt.getInteger("neonbuff");
		
		researchRewards.read(nbt);
		
	}
	
	private boolean checkResources()
	{
		if(research!=null)
		{
			ItemPredicateBase[] need = research.getNeeded();
			
			if(need!=null && need.length<=4)
			{
				boolean any = false;
				Arrays.fill(slots, true);
				for(int i=0;i<need.length;i++)
				{
					if(this.items.get(i).isEmpty())
					{
						slots[i] = false;
					}
					else if(!need[i].apply(items.get(i), false))
					{
						slots[i] = false;
					}
					else
					{
						slots[i] = true;
						any = true;
					}
				}
				return any;
			}		
		}
		
		return false;
	}
	
	private boolean setResearch(Research r)
	{
		ServerLevel ws = (ServerLevel) level;
		CustomPlayerData data = CustomPlayerData.getDataFromUUID(this.user, ws.getServer());
		
		if(data.canResearch(r) || FPConfig.RESEARCH.disable_researching.get())
		{
			research = r;
			neon = r.getNeonenergie();
			support = r.getSupport();
			xp = r.getExpLvl();
//			neonbuff =  0;
			
			time = r.getTime();
			return true;
		}
		return false;
	}
	
	public boolean startResearch()
	{
		System.out.println("Research start");
		
		if(!this.items.get(4).isEmpty())
		{
			Research r = ItemResearchBlueprint.getResearchFromItem(this.items.get(4));
			if(r!=null)
			{
				if(setResearch(r))
				{
					items.set(4, ItemStack.EMPTY);
				}
			}
		}
		return false;
	}
	
	public void downloadReserch()
	{
		if(this.items.get(4).isEmpty())
			NetworkManager.sendPacketThreaded(this, new FunkPacketGetResearch(worldPosition, this));
	}
	
	public void deleteResearch()
	{
		if(research!=null)
		{
			stored_neon += research.getNeonenergie()-neon;
			stored_support += research.getSupport()-support;
			stored_xp += research.getExpLvl()-xp;
						
			ItemStack it = ItemResearchBlueprint.getItemForResearch(research);
			research = null;
			if(this.items.get(4).isEmpty())
			{
				this.items.set(4, it);
			}

			neon = -1;
			support = -1;
			xp = -1;
			time = -1;
		}
		
	}
	
	protected void finishResearch()
	{
		ServerLevel ws = (ServerLevel) level;
		CustomPlayerData data = CustomPlayerData.getDataFromUUID(this.user, ws.getServer());	
		boolean finishedFirst = ! data.hasResearch(this.research);
		finishedFirst = data.addResearch(this.research) && finishedFirst;//only works if the parent are discovered
		ItemPredicateBase[] need = research.getNeeded();
		if(need!=null && need.length<=4)
		{
			for(int i=0;i<need.length;i++)
			{
				if(this.items.get(i).isEmpty())
				{
					slots[i] = false;
					continue;
				}
				else if(!(need[i].apply(items.get(i), false)))
				{
					slots[i] = false;
					continue;
				}
				int sts = getMinValidStackSize(items.get(i), need[i]);
				if(sts==-1)
					continue;
				
				items.get(i).shrink(sts);
				if(items.get(i).getCount()==0)
				{
					items.set(i, ItemStack.EMPTY);
				}
			}
		}		
		
		if(finishedFirst)
			researchRewards.appendAll(research.getRewards());	

		research = null;
		neon = -1;
		support = -1;
		xp = -1;
		time = -1;
//		neonbuff =  0;
	}
	
	private int getMinValidStackSize(ItemStack it, ItemPredicateBase pred)
	{
		return pred.getMinimalItemCount(it);
//		
//		it = it.copy();
//		int min = 1;
//		for(int j=min;j<=it.getMaxStackSize();j++)
//		{
//			it.setCount(j);
//			if(pred.apply(it))
//			{
//				return j;
//			}
//		}
//		return -1;
	}

	@Override
	public boolean isNetworkAble()
	{
		return true;
	}

	@Override
	public boolean isWire()
	{
		return false;
	}

	@Override
	public void onFunkPacket(PacketBase pkt)
	{
		if(pkt instanceof FunkPacketCalcSolution)
		{
			FunkPacketCalcSolution pcs = (FunkPacketCalcSolution) pkt;
			if(worldPosition.equals(pcs.receiver))
			{
				int red = 0;
				switch(pcs.type)
				{
				case NEON:
					red = Math.min(pcs.solutionAmout, Math.min(stored_neon, neon));			
					stored_neon-=red;
					neon-=red;
					break;
				case EXP:
					red = Math.min(pcs.solutionAmout, Math.min(stored_xp, xp));			
					stored_xp-=red;
					xp-=red;
					break;
				case SUPPORT:
					red = Math.min(pcs.solutionAmout, Math.min(stored_support, support));			
					stored_support-=red;
					support-=red;
					break;
				}
			}
		}
	}

	
	@Override
	protected int getInventorySize()
	{
		return 5;
	}
	
	@Override
	public int[] getSlotsForFace(Direction side)
	{
		return new int[]{};
	}

	@Override
	public boolean canPlaceItemThroughFace(int index, ItemStack itemStackIn, Direction direction)
	{
		return false;
	}

	@Override
	public boolean canTakeItemThroughFace(int index, ItemStack stack, Direction direction)
	{
		return false;
	}

	@Override
	public boolean canPlaceItem(int var1, ItemStack var2)
	{
		if(var1==4)
		{
			if(research!=null)
			{
				return false;
			}
			if(var2!=null)
			{
				return var2.getItem() == MiscItems.RESEARCH_BLUEPRINT;
			}
		}
		return super.canPlaceItem(var1, var2);
	}
	
	
	@Override
	public void postPacketSend(PacketBase pkt)
	{
		if(pkt instanceof FunkPacketExperience)
		{
			FunkPacketExperience exp = (FunkPacketExperience) pkt;
			stored_xp += exp.collected;			
		}
		else if(pkt instanceof FunkPacketSupport)
		{
			FunkPacketSupport sup = (FunkPacketSupport) pkt;
			stored_support += sup.collected;
		}
		else if(pkt instanceof FunkPacketNeon)
		{
			FunkPacketNeon neon = (FunkPacketNeon) pkt;
			stored_neon += neon.collected;
//			neonbuff = ((FunkPacketNeon) pkt).speedmodifier;
		}
		else if(pkt instanceof FunkPacketGetResearch)
		{
			FunkPacketGetResearch res = (FunkPacketGetResearch) pkt;
			if(res.research!=null)
			{
				this.items.set(4, ItemResearchBlueprint.getItemForResearch(res.research));
			}
		}
		else if(pkt instanceof FunkPacketCalculation)
		{
			FunkPacketCalculation calcs = (FunkPacketCalculation) pkt;
			if(calcs.type==EnumCalculationType.NEON)
			{
				this.neon_calcmodules = calcs.getWorkingCalculationModules();
			}
		}
	}

	public float getProgess(int bar)
	{
		if(research==null)
		{
			return 0;
		}
			
		switch(bar)
		{
		case 0: return this.time / (float) research.getTime();
		case 1: return this.neon / (float) research.getNeonenergie();
		case 2: return this.support / (float) research.getSupport();
		case 3: return this.xp / (float) research.getExpLvl();
		default: return 0;
		}
	}
	
	public boolean isBlocked(int bar)
	{
		if(research==null)
		{
			return false;
		}
		
		switch(bar)
		{
		case 0: return false;
		case 1: return this.neon>0 && this.stored_neon<=0;
		case 2: return this.support>0 && this.stored_support<=0;
		case 3: return this.xp>0 && this.stored_xp<=0;
		case 4:	
		case 5:
		case 6:
		case 7:
			return !slots[bar-4];
		default: return true;
		}
	}
	
	@Override
	public int getPropertyCount()
	{
		return 11;
	}
	
	@Override
	public int getProperty(int id)
	{
		switch(id)
		{
		case 0:return research==null?-1 : research.id;
		case 1:return (int) (Short.MAX_VALUE * getProgess(0));
		case 2:return (int) (Short.MAX_VALUE * getProgess(1));
		case 3:return (int) (Short.MAX_VALUE * getProgess(2));
		case 4:return (int) (Short.MAX_VALUE * getProgess(3));
		case 5:return stored_neon;
		case 6:return stored_support;
		case 7:return stored_xp;
		case 8: return TileEntityScannerBlock.booleanToInt(slots);
		case 9:return neon_calcmodules;
		case 10: return researchRewards.isEmpty() ? 0 : 1;
		default: return 0;
		}
	}
	
	@Override
	public void setProperty(int id, int value)
	{
		switch(id)
		{
		case 0:research = (value==-1 ? null : ResearchManager.getById(value));break;
		case 1:time=(int) (research!=null ? value/32767F*research.getTime() : 0);break;
		case 2:neon=(int) (research!=null ? value/32767F*research.getNeonenergie(): 0);break;
		case 3:support=(int) (research!=null ? value/32767F*research.getSupport(): 0);break;
		case 4:xp=(int) (research!=null ? value/32767F*research.getExpLvl(): 0);break;		
		case 5:stored_neon=value;break;
		case 6:stored_support=value;break;
		case 7:stored_xp=value;break;
		case 8:slots = TileEntityScannerBlock.intToBool(slots, value);break;
		case 9:neon_calcmodules = value;break;
		case 10:hasRewards = value > 0;break;
		}
	}
	
	@Override
	public void setOwner(Player liv)
	{
		this.user = liv.getGameProfile().getId();
		this.invname = liv.getGameProfile().getName();
	}

	@Override
	public Level getSenderWorld()
	{
		return this.level;
	}
	
	@Override
	public BlockPos getSenderPosition()
	{
		return this.worldPosition;
	}
	
	public ItemStack[] getHoloItems()
	{
		if(research!=null)
		{
			ItemPredicateBase[] preds = research.getNeeded();
			ItemStack[] its = new ItemStack[preds.length]; 
			for(int i=0;i<preds.length;i++)
			{
				its[i] = preds[i].getRepresentItem();
			}
			return its;
		}
		else
			return null;
	}

	@Override
	public boolean isOwner(Player pl)
	{
		if(user==null)
			return true;
		return isOwner(pl.getGameProfile().getId());
	}
	
	@Override
	public boolean isOwner(UUID pl)
	{
		if(user==null)
			return true;
		return this.user.equals(pl);
	}

	public UUID getOwnerID()
	{
		return this.user;
	}
	
	public Research getResearch() 
	{
		return this.research;
	}

	public String getUsername() 
	{
		return invname;
	}
	
	@Override
	public String getGUITitle() 
	{
		return "gui.futurepack.researcher.title";
	}
	
	
	
	public static class RewardInventory implements Container
	{
		protected ArrayList<ItemStack> items = new ArrayList<ItemStack>();
		
		public RewardInventory()
		{
			
		}
		
		public CompoundTag write(CompoundTag nbt)
		{
			ListTag l = new ListTag();
			for(int i=0;i<items.size();i++)
			{
				if(!items.get(i).isEmpty())
				{
					CompoundTag tag = new CompoundTag();
					items.get(i).save(tag);
					l.add(tag);
				}
			}
			nbt.put("Rewards", l);
			return nbt;
		}
		
		public void read(CompoundTag nbt)
		{
			items.clear();
			ListTag l = nbt.getList("Rewards", 10);
			for(int i=0;i<l.size();i++)
			{
				CompoundTag tag = l.getCompound(i);
				ItemStack is = ItemStack.of(tag);
				items.add(is);
				//setInventorySlotContents(tag.getInt("slot"), is);
			}
		}
		
		@Override
		public int getContainerSize() 
		{
			return items.size();
		}

		@Override
		public ItemStack getItem(int var1) 
		{
			if(var1<items.size())
			{
				return items.get(var1);
			}
			return ItemStack.EMPTY;
		}

		@Override
		public ItemStack removeItem(int slot, int amount)
	    {
	        if (this.items.get(slot) != null)
	        {
	            ItemStack itemstack;

	            if (this.items.get(slot).getCount() <= amount)
	            {
	                itemstack = this.items.get(slot);
	                this.items.set(slot, ItemStack.EMPTY);
	                this.setChanged();
	                return itemstack;
	            }
	            else
	            {
	                itemstack = this.items.get(slot).split(amount);

	                if (this.items.get(slot).getCount() == 0)
	                {
	                	this.items.set(slot, ItemStack.EMPTY);
	                }

	                this.setChanged();
	                return itemstack;
	            }
	        }
	        else
	        {
	            return ItemStack.EMPTY;
	        }
	    }

		@Override
		public ItemStack removeItemNoUpdate(int index)
		{
			if(getItem(index)!=null)
			{
				ItemStack stack = items.get(index);
				items.remove(index);
				return stack;
			}
			return null;
		}
		
		@Override
		public void setItem(int var1, ItemStack var2) 
		{
			for(int i = items.size(); i <= var1; i++)
				items.add(ItemStack.EMPTY);
			
			items.set(var1, var2);
			//this.markDirty();
		}

		@Override
		public int getMaxStackSize() 
		{
			return 64;
		}


		@Override
		public void startOpen(Player pl) {}

		@Override
		public void stopOpen(Player pl) {}

		@Override
		public boolean canPlaceItem(int var1, ItemStack var2) 
		{
			return false;
		}

		@Override
		public void clearContent() { }
	
		
		@Override
		public boolean isEmpty()
		{
			for(int i = 0; i < items.size(); i++)
			{
				if(!items.get(i).isEmpty())
					return false;
			}
			
			return true;
		}

		@Override
		public void setChanged() 
		{
			// TODO Auto-generated method stub
		}

		@Override
		public boolean stillValid(Player player) 
		{
			return true;
		}
		
		public void appendAll(ItemStack[] its)
		{
			for(ItemStack it: its)
				items.add(it.copy());
		}

		public void compress() 
		{
		    while (items.contains(ItemStack.EMPTY)) 
		    {
		    	items.remove(ItemStack.EMPTY);
		    }
		}
	}
	
}
