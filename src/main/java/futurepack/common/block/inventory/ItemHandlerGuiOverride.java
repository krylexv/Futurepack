package futurepack.common.block.inventory;

import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.IItemHandlerModifiable;

public class ItemHandlerGuiOverride implements IItemHandlerModifiable
{
	private final ItemStackHandlerGuis handler;


	public ItemHandlerGuiOverride(ItemStackHandlerGuis inventory) 
	{
		this.handler = inventory;
	}

	@Override
	public int getSlots()
	{
		return this.handler.getSlots();
	}

	@Override
	public ItemStack getStackInSlot(int slot)
	{
		return this.handler.getStackInSlot(slot);
	}

	@Override
	public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
	{
		return this.handler.insertItem(slot, stack, simulate, true);
	}

	@Override
	public ItemStack extractItem(int slot, int amount, boolean simulate)
	{
		return this.handler.extractItem(slot, amount, simulate, true);
	}

	@Override
	public void setStackInSlot(int slot, ItemStack stack)
	{
		this.handler.setStackInSlot(slot, stack);
	}

	@Override
	public int getSlotLimit(int slot)
	{
		return this.handler.getSlotLimit(slot);
	}
	
	@Override
	public boolean isItemValid(int slot, ItemStack stack) 
	{
		return this.handler.isItemValid(slot, stack, true);
	}
}
