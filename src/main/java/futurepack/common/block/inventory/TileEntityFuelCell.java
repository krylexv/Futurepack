package futurepack.common.block.inventory;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.interfaces.tilentity.ITileFuelProvider;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.item.CraftingItems;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityFuelCell extends  TileEntityInventoryLogistics implements ITilePropertyStorage, ITileFuelProvider, ITileServerTickable
{
	private int fuel=0;
	private int maxfuel = 8000;
	
	public TileEntityFuelCell(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.FUEL_CELL, pos, state);
	}
	
	@Override
	public void configureLogisticStorage(LogisticStorage storage) 
	{
		storage.setDefaut(EnumLogisticIO.INOUT, EnumLogisticType.ITEMS);
	}

	
	@Override
	public void tickServer(Level pLevel, BlockPos pPos, BlockState pState)
	{
		if(fuel <= maxfuel -1000)
		{
			for(int i=0;i<9;i++)
			{
				if(!items.get(i).isEmpty() && items.get(9).getCount()<64)
				{
					boolean flag = items.get(i).getItem()==CraftingItems.fuel_rods; //ersezen durch vergleich der Items
					if(flag)
					{
						fuel += 1000;
						items.get(i).shrink(1);
						if(level.random.nextInt(2)==0 && !level.isClientSide)
						{
							if(items.get(9).isEmpty())
							{
								items.set(9, new ItemStack(Items.BLAZE_POWDER));
							}
							else if(items.get(9).getItem() == Items.BLAZE_POWDER)
							{
								items.get(9).grow(1);
							}
							
						}
						break;
					}
				}
			}

		}
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		nbt.putInt("fuel", fuel);
		storage.write(nbt);
		return nbt;
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		fuel = nbt.getInt("fuel");
		storage.read(nbt);
	}

	@Override
	public int getProperty(int id)
	{
		switch (id)
		{
		case 0:
			return fuel;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			fuel = value;
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 1;
	}

	@Override
	public int[] getSlotsForFace(Direction side)
	{
		int[] slots = new int[getInventorySize()];
		for(int i=0;i<slots.length;i++)
		{
			slots[i]=i;
		}
		return slots;
	}
	
	@Override
	public void setItem(int var1, ItemStack var2)
	{
		if(var1>10)
		{
			if(var2.getItem()== Items.BLAZE_POWDER)
			{
				if(getItem(9).isEmpty())
				{
					super.setItem(9, var2);
				}
				else
				{
					getItem(9).grow(var2.getCount());
				}
			}
			else if(var2.getItem()== CraftingItems.fuel_rods)
			{
				if(getItem(0).isEmpty())
				{
					super.setItem(0, var2);
				}
				else
				{
					getItem(0).grow(var2.getCount());
				}
			}
		}
		else
			super.setItem(var1, var2);
	}
	
	@Override
	public boolean canPlaceItem(int slot, ItemStack item)
	{
		return slot<9 && items.get(slot).isEmpty() && item.getItem()== CraftingItems.fuel_rods;
	}

	@Override
	public boolean canPlaceItemThroughFace(int index, ItemStack it, Direction direction)
	{
		return storage.canInsert(direction, EnumLogisticType.ITEMS);
	}

	@Override
	public boolean canTakeItemThroughFace(int index, ItemStack stack, Direction direction)
	{
		boolean flag = !items.get(index).isEmpty() && items.get(index).getItem()==Items.BLAZE_POWDER;
		return storage.canExtract(direction, EnumLogisticType.ITEMS) && flag;
	}

	@Override
	protected int getInventorySize()
	{
		return 10;
	}

	@Override
	public int getMaxStackSize()
	{
		return 64;
	}
	
	public float getFluid()
	{
		return (float)fuel / (float)maxfuel;
	}

	@Override
	public int getFuel()
	{
		return fuel;
	}

	@Override
	public int getMaxFuel()
	{
		return maxfuel;
	}

	@Override
	public boolean useFuel(int amount)
	{
		if(fuel>=amount)
		{
			fuel-=amount;
			return true;
		}
		return false;
	}

	@Override
	public boolean addFuel(int amount)
	{
		if(amount+fuel<=getMaxFuel())
		{
			fuel+=amount;
			return true;
					
		}
		return false;
	}
	
	@Override
	public String getGUITitle() {
		return "gui.futurepack.fuelcell.title";
	}
	
}
