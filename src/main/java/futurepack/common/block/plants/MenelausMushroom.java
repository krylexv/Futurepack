package futurepack.common.block.plants;

import java.util.Random;

import futurepack.common.block.terrain.TerrainBlocks;
import futurepack.world.gen.FPFeatures;
import futurepack.world.gen.feature.BigMushroomFeatureConfig;
import net.minecraft.core.Holder;
import net.minecraft.data.worldgen.features.FeatureUtils;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;

public class MenelausMushroom extends BasicNotTree<BigMushroomFeatureConfig>
{

	BigMushroomFeatureConfig config = new BigMushroomFeatureConfig(TerrainBlocks.log_mushroom.defaultBlockState(), TerrainBlocks.leaves_mushroom.defaultBlockState(), 5, 10);
	Holder<ConfiguredFeature<BigMushroomFeatureConfig, ?>> tree = FeatureUtils.register("menealus_muchroom_tree", FPFeatures.MUSHROOM_TREE, config);
	
	@Override
	protected Holder<ConfiguredFeature<BigMushroomFeatureConfig, ?>> getTree(Random random, boolean par2) 
	{
		return tree;
	}

}
