package futurepack.common.block.plants;

import java.util.Random;

import futurepack.world.gen.FPFeatures;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.data.worldgen.features.FeatureUtils;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;

public class PalirieTree extends BasicNotTree<NoneFeatureConfiguration> 
{
	private Holder<ConfiguredFeature<NoneFeatureConfiguration, ?>> tree = FeatureUtils.register("palirie_tree", FPFeatures.PALIRIE_TREE, NoneFeatureConfiguration.INSTANCE);
	
	@Override
	protected Holder<ConfiguredFeature<NoneFeatureConfiguration, ?>> getTree(Random randomIn, boolean largeHive) 
	{
		return tree;
	}
	
	
	@Override
	public boolean growTree(ServerLevel world, ChunkGenerator chunkGenerator, BlockPos pos, BlockState state, Random rand)
	{
		world.setBlock(pos, Blocks.AIR.defaultBlockState(), 4);
//		tree.config.forcePlacement();
		if (tree.value().place(world, chunkGenerator, rand, pos)) {
			return true;
		} else {
			world.setBlock(pos, state, 4);
			return false;
		}
	}
}
