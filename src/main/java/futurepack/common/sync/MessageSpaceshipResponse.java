package futurepack.common.sync;

import java.util.UUID;
import java.util.function.Supplier;

import futurepack.common.spaceships.SpaceshipCashClient;
import futurepack.common.spaceships.SpaceshipHasher;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.network.NetworkEvent;

/** Responge to client to send spaceship data*/
public class MessageSpaceshipResponse
{
	private BlockState[][][] blocks;
	private UUID uid;
	
	private MessageSpaceshipResponse()
	{
		
	}
	
	public MessageSpaceshipResponse(UUID uid, BlockState[][][] states)
	{
		blocks = states;
		this.uid = uid;
	}
	
	private void progress()
	{
		SpaceshipCashClient.addSpaceship(uid, blocks);
	}
	
	public static MessageSpaceshipResponse decode(FriendlyByteBuf buf) 
	{
		MessageSpaceshipResponse m = new MessageSpaceshipResponse();
		m.uid = new UUID(buf.readLong(), buf.readLong());
		int l = buf.readVarInt();
		byte[] compressed = new byte[l];
		buf.readBytes(compressed);
		byte[] decomp = SpaceshipHasher.decompress(compressed);
		m.blocks = SpaceshipHasher.fromBytes(decomp);
		
		return m;
	}
	
	public static void encode(MessageSpaceshipResponse msg, FriendlyByteBuf buf) 
	{
		buf.writeLong(msg.uid.getMostSignificantBits());
		buf.writeLong(msg.uid.getLeastSignificantBits());
		byte[] bytes = SpaceshipHasher.compress(SpaceshipHasher.asBytes(msg.blocks));
		buf.writeVarInt(bytes.length);
		buf.writeBytes(bytes);
	}
	
	public static void consume(MessageSpaceshipResponse message, Supplier<NetworkEvent.Context> ctx) 
	{
		DistExecutor.runWhenOn(Dist.CLIENT, () -> message::progress);
		ctx.get().setPacketHandled(true);
	}
	
}
