package futurepack.common.entity.living;

import javax.annotation.Nullable;

import futurepack.common.entity.SubInventory;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.world.Container;
import net.minecraft.world.ContainerListener;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.AgeableMob;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.PlayerRideableJumping;
import net.minecraft.world.entity.Saddleable;
import net.minecraft.world.entity.TamableAnimal;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ChestMenu;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public abstract class EntityRideableAnimalBase extends TamableAnimal implements ContainerListener, PlayerRideableJumping, Saddleable, MenuProvider
{
	private static final EntityDataAccessor<Byte> STATUS = SynchedEntityData.defineId(EntityRideableAnimalBase.class, EntityDataSerializers.BYTE);

	public static final int BOOL_TAME = 2;
	public static final int BOOL_SADDLED = 4;
	public static final int BOOL_BREEDING = 8;
	public static final int BOOL_EATING_HAYSTACK = 16;
	public static final int BOOL_REARING = 32;
	public static final int BOOL_CHEST = 64;

	
	private int jumpRearingCounter;
	public int sprintCounter;
	protected boolean horseJumping;
	protected SimpleContainer horseChest;
	/**
	 * The higher this value, the more likely the horse is to be tamed next time a
	 * player rides it.
	 */
	protected float jumpPower;
	private boolean allowStandSliding;
	private float headLean;
	private float prevHeadLean;
	private float rearingAmount;
	private float prevRearingAmount;
	private float mouthOpenness;
	private float prevMouthOpenness;
	protected boolean canGallop = true;
	/** Used to determine the sound that the horse should make when it steps */
	protected int gallopTime;

	protected EntityRideableAnimalBase(EntityType<? extends EntityRideableAnimalBase> type, Level worldIn)
	{
		super(type, worldIn);
		initAnimalChest();
	}

	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();
		this.getEntityData().define(STATUS, (byte) 0);
	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);

		if (!this.horseChest.getItem(0).isEmpty())
		{
			compound.put("SaddleItem", this.horseChest.getItem(0).save(new CompoundTag()));
		}
	}

	@Override
	public void readAdditionalSaveData(CompoundTag compound)
	{
		super.readAdditionalSaveData(compound);

		if (compound.contains("SaddleItem", 10))
		{
			ItemStack itemstack = ItemStack.of(compound.getCompound("SaddleItem"));
			if (itemstack.getItem() == Items.SADDLE)
			{
				this.horseChest.setItem(0, itemstack);
			}
		}
	}

	// From Abstarct Horse

	/**
	 * For vehicles, the first passenger is generally considered the controller and
	 * "drives" the vehicle. For example, Pigs, Horses, and Boats are generally
	 * "steered" by the controlling passenger.
	 */
	@Nullable
	public Entity getControllingPassenger()
	{
		return this.getPassengers().isEmpty() ? null : this.getPassengers().get(0);
	}

	/**
	 * returns true if all the conditions for steering the entity are met. For pigs,
	 * this is true if it is being ridden by a player and the player is holding a
	 * carrot-on-a-stick
	 */
	public boolean canBeControlledByRider()
	{
		return this.getControllingPassenger() instanceof LivingEntity;
	}

	@OnlyIn(Dist.CLIENT)
	public float getRearingAmount(float p_110223_1_)
	{
		return Mth.lerp(p_110223_1_, this.prevRearingAmount, this.rearingAmount);
	}

	@OnlyIn(Dist.CLIENT)
	public float getMouthOpennessAngle(float p_110201_1_)
	{
		return Mth.lerp(p_110201_1_, this.prevMouthOpenness, this.mouthOpenness);
	}

	public boolean canJump()
	{
		return this.isSaddled();
	}

	public void handleStartJump(int jumpPower)
	{
		this.allowStandSliding = true;
		this.makeHorseRear();
		this.playJumpSound();
	}

	public void handleStopJump()
	{
	}

	/**
	 * can equip saddle
	 */
	@Override
	public boolean isSaddleable()
	{
		return this.isAlive() && !this.isBaby() && this.isTame();
	}

	@Override
	public void equipSaddle(SoundSource p_230266_1_)
	{
		this.horseChest.setItem(0, new ItemStack(Items.SADDLE));
		if (p_230266_1_ != null)
		{
			this.level.playSound((Player) null, this, SoundEvents.HORSE_SADDLE, p_230266_1_, 0.5F, 1.0F);
		}
	}

	@Override
	public boolean isSaddled()
	{
		return this.getWatchableBoolean(BOOL_SADDLED);
	}

	@Override
	public void onPlayerJump(int jumpPowerIn)
	{
		if (this.isSaddled())
		{
			if (jumpPowerIn < 0)
			{
				jumpPowerIn = 0;
			} else
			{
				this.allowStandSliding = true;
				this.makeHorseRear();
			}

			if (jumpPowerIn >= 90)
			{
				this.jumpPower = 1.0F;
			} else
			{
				this.jumpPower = 0.4F + 0.4F * (float) jumpPowerIn / 90.0F;
			}
		}
	}

	protected void initAnimalChest()
	{
		SimpleContainer inventory = this.horseChest;
		this.horseChest = new SimpleContainer(this.getInventorySize());
		if (inventory != null)
		{
			inventory.removeListener(this);
			int i = Math.min(inventory.getContainerSize(), this.horseChest.getContainerSize());

			for (int j = 0; j < i; ++j)
			{
				ItemStack itemstack = inventory.getItem(j);
				if (!itemstack.isEmpty())
				{
					this.horseChest.setItem(j, itemstack.copy());
				}
			}
		}

		this.horseChest.addListener(this);
		this.updateSaddledStatus();
		this.itemHandler = net.minecraftforge.common.util.LazyOptional
				.of(() -> new net.minecraftforge.items.wrapper.InvWrapper(this.horseChest));
	}

	protected void updateSaddledStatus()
	{
		if (!this.level.isClientSide)
		{
			this.setWatchableBoolean(BOOL_SADDLED, !this.horseChest.getItem(0).isEmpty());
			this.setWatchableBoolean(BOOL_CHEST, hasChest());
		}
	}

	public void containerChanged(Container invBasic)
	{
		boolean flag = this.isSaddled();
		this.updateSaddledStatus();
		if (this.tickCount > 20 && !flag && this.isSaddled())
		{
			this.playSound(SoundEvents.HORSE_SADDLE, 0.5F, 1.0F);
		}

	}

	@Override
	public AgeableMob getBreedOffspring(ServerLevel p_241840_1_, AgeableMob p_241840_2_)
	{
		return null;
	}

	public void travel(Vec3 travelVector)
	{
		if (this.isAlive())
		{
			if (this.isVehicle() && this.canBeControlledByRider() && this.isSaddled())
			{
				LivingEntity livingentity = (LivingEntity) this.getControllingPassenger();
				this.setYRot(livingentity.getYRot());
				this.yRotO = this.getYRot();
				this.setXRot(livingentity.getXRot() * 0.5F);
				this.setRot(this.getYRot(), this.getXRot());
				this.yBodyRot = this.getYRot();
				this.yHeadRot = this.yBodyRot;
				float f = livingentity.xxa * 0.5F;
				float f1 = livingentity.zza;
				if (f1 <= 0.0F)
				{
					f1 *= 0.25F;
					this.gallopTime = 0;
				}

				if (this.onGround && this.jumpPower == 0.0F && this.isRearing() && !this.allowStandSliding)
				{
					f = 0.0F;
					f1 = 0.0F;
				}

				if (this.jumpPower > 0.0F && !this.isHorseJumping() && this.onGround)
				{
					double d0 = this.getHorseJumpStrength() * (double) this.jumpPower * (double) this.getBlockJumpFactor();
					double d1;
					if (this.hasEffect(MobEffects.JUMP))
					{
						d1 = d0 + (double) ((float) (this.getEffect(MobEffects.JUMP).getAmplifier() + 1)
								* 0.1F);
					} else
					{
						d1 = d0;
					}

					Vec3 vector3d = this.getDeltaMovement();
					this.setDeltaMovement(vector3d.x, d1, vector3d.z);
					this.setHorseJumping(true);
					this.hasImpulse = true;
					net.minecraftforge.common.ForgeHooks.onLivingJump(this);
					if (f1 > 0.0F)
					{
						float f2 = Mth.sin(this.getYRot() * ((float) Math.PI / 180F));
						float f3 = Mth.cos(this.getYRot() * ((float) Math.PI / 180F));
						this.setDeltaMovement(this.getDeltaMovement().add((double) (-0.4F * f2 * this.jumpPower), 0.0D,
								(double) (0.4F * f3 * this.jumpPower)));
					}

					this.jumpPower = 0.0F;
				}

				this.flyingSpeed = this.getSpeed() * 0.1F;
				if (this.isControlledByLocalInstance())
				{
					this.setSpeed((float) this.getAttributeValue(Attributes.MOVEMENT_SPEED));
					super.travel(new Vec3((double) f, travelVector.y, (double) f1));
				} else if (livingentity instanceof Player)
				{
					this.setDeltaMovement(Vec3.ZERO);
				}

				if (this.onGround)
				{
					this.jumpPower = 0.0F;
					this.setHorseJumping(false);
				}

				this.calculateEntityAnimation(this, false);
			} else
			{
				this.flyingSpeed = 0.02F;
				super.travel(travelVector);
			}
		}
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	public void tick()
	{

		super.tick();

		if ((this.isControlledByLocalInstance() || this.isEffectiveAi()) && this.jumpRearingCounter > 0
				&& ++this.jumpRearingCounter > 20)
		{
			this.jumpRearingCounter = 0;
			this.setRearing(false);
		}

		if (this.sprintCounter > 0)
		{
			++this.sprintCounter;
			if (this.sprintCounter > 300)
			{
				this.sprintCounter = 0;
			}
		}

		this.prevHeadLean = this.headLean;
		if (this.isEatingHaystack())
		{
			this.headLean += (1.0F - this.headLean) * 0.4F + 0.05F;
			if (this.headLean > 1.0F)
			{
				this.headLean = 1.0F;
			}
		} else
		{
			this.headLean += (0.0F - this.headLean) * 0.4F - 0.05F;
			if (this.headLean < 0.0F)
			{
				this.headLean = 0.0F;
			}
		}

		this.prevRearingAmount = this.rearingAmount;
		if (this.isRearing())
		{
			this.headLean = 0.0F;
			this.prevHeadLean = this.headLean;
			this.rearingAmount += (1.0F - this.rearingAmount) * 0.4F + 0.05F;
			if (this.rearingAmount > 1.0F)
			{
				this.rearingAmount = 1.0F;
			}
		} else
		{
			this.allowStandSliding = false;
			this.rearingAmount += (0.8F * this.rearingAmount * this.rearingAmount * this.rearingAmount
					- this.rearingAmount) * 0.6F - 0.05F;
			if (this.rearingAmount < 0.0F)
			{
				this.rearingAmount = 0.0F;
			}
		}

		this.prevMouthOpenness = this.mouthOpenness;
		if (this.getWatchableBoolean(64))
		{
			this.mouthOpenness += (1.0F - this.mouthOpenness) * 0.7F + 0.05F;
			if (this.mouthOpenness > 1.0F)
			{
				this.mouthOpenness = 1.0F;
			}
		} else
		{
			this.mouthOpenness += (0.0F - this.mouthOpenness) * 0.7F - 0.05F;
			if (this.mouthOpenness < 0.0F)
			{
				this.mouthOpenness = 0.0F;
			}
		}

	}

	protected void dropEquipment()
	{
		super.dropEquipment();
		if (this.horseChest != null)
		{
			for (int i = 0; i < this.horseChest.getContainerSize(); ++i)
			{
				ItemStack itemstack = this.horseChest.getItem(i);
				if (!itemstack.isEmpty() && !EnchantmentHelper.hasVanishingCurse(itemstack))
				{
					this.spawnAtLocation(itemstack);
				}
			}

		}
	}

	public boolean isPushable()
	{
		return !this.isVehicle();
	}

	protected boolean getWatchableBoolean(int p_110233_1_)
	{
		return (this.entityData.get(STATUS) & p_110233_1_) != 0;
	}

	protected void setWatchableBoolean(int p_110208_1_, boolean p_110208_2_)
	{
		byte b0 = this.entityData.get(STATUS);
		if (p_110208_2_)
		{
			this.entityData.set(STATUS, (byte) (b0 | p_110208_1_));
		} else
		{
			this.entityData.set(STATUS, (byte) (b0 & ~p_110208_1_));
		}

	}
	
	public boolean isTame()
	{
		return true; // this.getWatchableBoolean(BOOL_TAME);
	}

	public boolean isHorseJumping()
	{
		return this.horseJumping;
	}

	public void setHorseTamed(boolean tamed)
	{
		this.setWatchableBoolean(BOOL_TAME, tamed);
	}

	public void setHorseJumping(boolean jumping)
	{
		this.horseJumping = jumping;
	}

	protected void onLeashDistance(float distance)
	{
		if (distance > 6.0F && this.isEatingHaystack())
		{
		}

	}

	public boolean isEatingHaystack()
	{
		return this.getWatchableBoolean(BOOL_EATING_HAYSTACK);
	}

	public boolean isRearing()
	{
		return this.getWatchableBoolean(BOOL_REARING);
	}

	public boolean isBreeding()
	{
		return this.getWatchableBoolean(BOOL_BREEDING);
	}

	public void setBreeding(boolean breeding)
	{
		this.setWatchableBoolean(BOOL_BREEDING, breeding);
	}

	public double getHorseJumpStrength()
	{
		return this.getAttributeValue(Attributes.JUMP_STRENGTH);
	}

	public void setRearing(boolean rearing)
	{
		if (rearing)
		{
		}

		this.setWatchableBoolean(BOOL_REARING, rearing);
	}

	private void makeHorseRear()
	{
		if (this.isControlledByLocalInstance() || this.isEffectiveAi())
		{
			this.jumpRearingCounter = 1;
			this.setRearing(true);
		}
	}

	protected void playJumpSound()
	{
		this.playSound(SoundEvents.HORSE_JUMP, 0.4F, 1.0F);
	}
	
	private net.minecraftforge.common.util.LazyOptional<?> itemHandler = null;

	@Override
	public <T> net.minecraftforge.common.util.LazyOptional<T> getCapability(net.minecraftforge.common.capabilities.Capability<T> capability, @Nullable net.minecraft.core.Direction facing) 
	{
		if (this.isAlive() && capability == net.minecraftforge.items.CapabilityItemHandler.ITEM_HANDLER_CAPABILITY && itemHandler != null)
			return itemHandler.cast();
		return super.getCapability(capability, facing);
	}

	@Override
	public void invalidateCaps() 
	{
		super.invalidateCaps();
		if (itemHandler != null) {
			itemHandler.invalidate();
			itemHandler = null;
		}
	}
	
	public boolean hasChest()
	{
		if(level.isClientSide)
		{
			return getWatchableBoolean(BOOL_CHEST);
		}
		else
		{	
			if(horseChest==null)
				return false;
			return !this.horseChest.getItem(1).isEmpty() && this.horseChest.getItem(1).getItem() == Blocks.CHEST.asItem();
		}
	}
	
	protected int getInventorySize()
	{
		return 2 + (hasChest() ? 3*9 : 0);
	}
	
	protected void mountTo(Player player) 
	{
		this.setInSittingPose(false);
		this.setRearing(false);
		if (!this.level.isClientSide) 
		{
			player.setYRot(this.getYRot());
			player.setXRot(this.getXRot());
			player.startRiding(this);
		}
	}
	
	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inv, Player pl) 
	{
		if(this.isTame() && this.hasChest())
		{
			initAnimalChest();
			return ChestMenu.threeRows(id, inv, new SubInventory(horseChest, 2));
		}
		else
		{
			return null;
		}
		
	}
}
