package futurepack.common.entity.living;

import futurepack.common.entity.Navigator;
import futurepack.common.entity.NavigatorWallCrawler;
import futurepack.common.entity.ai.AIFindHiveMind;
import futurepack.common.entity.ai.AIRandomWalkAtCeiling;
import net.minecraft.core.Direction;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier.Builder;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.LeapAtTargetGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;

public class EntityDungeonSpider extends Monster
{
	/**
	 * 0b00000111 = attached Direction
	 * 0b00111000 = facing Direction
	 * 0b11000000 =
	 */
	private static final EntityDataAccessor<Byte> STATUS_FLAGS = SynchedEntityData.defineId(EntityDungeonSpider.class, EntityDataSerializers.BYTE);
	
	protected final Navigator navi;

	public EntityDungeonSpider(EntityType<? extends Monster> type, Level w) 
	{
		super(type, w);
		navi = new NavigatorWallCrawler(this);
		
	}
	
	protected void registerGoals() 
	{
		this.goalSelector.addGoal(3, new LeapAtTargetGoal(this, 0.4F));
		this.goalSelector.addGoal(3, new MeleeAttackGoal(this, 1.0D, true));
		
//		this.goalSelector.addGoal(4, new AIHiveMind(this));
		this.goalSelector.addGoal(4, new AIRandomWalkAtCeiling(this));
		this.goalSelector.addGoal(5, new AIFindHiveMind(this, this::isHiveMindBlock, 0.45F, 0.001F));
//		this.goalSelector.addGoal(5, new WaterAvoidingRandomWalkingGoal(this, 0.8D));
		this.goalSelector.addGoal(6, new LookAtPlayerGoal(this, Player.class, 8.0F));
		this.goalSelector.addGoal(6, new RandomLookAroundGoal(this));
		this.targetSelector.addGoal(1, new HurtByTargetGoal(this));
		this.targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(this, Player.class, 10, true, true, this::shouldAttackTarget));
	}

	public static Builder registerAttributes() 
	{
		return Monster.createLivingAttributes().add(Attributes.MAX_HEALTH, 12.0).add(Attributes.MOVEMENT_SPEED, 0.3).add(Attributes.FOLLOW_RANGE, 35.0D);
	}
	
	protected boolean isHiveMindBlock(BlockEntity tile)
	{
		return tile.getType() == BlockEntityType.CHEST;
	}
	
	@Override
	protected void defineSynchedData() 
	{
		super.defineSynchedData();
		this.entityData.define(STATUS_FLAGS, (byte)0);
	}
	
	@Override
	public void baseTick() //things will get complicated here because it needs damage and stuff
	{
		
		if(isAttachedToWallOrCeiling())
		{
//			this.addPotionEffect(new EffectInstance(Effects.SLOW_FALLING, 20));
			
			if(!navi.isFinished())
			{
				navi.move(0.05F);
			}
			
//			SpiderEntity
		}
		
		
		super.baseTick();
	}
	
	@Override
	public void setTarget(LivingEntity entitylivingbaseIn) 
	{
		super.setTarget(entitylivingbaseIn);
		detachFromWall();
	}
	
	@Override
	public boolean requiresCustomPersistence() 
	{
		return true;
	}
	
	public boolean isAttachedToWallOrCeiling()
	{
		byte b = (byte) (this.entityData.get(STATUS_FLAGS) & 0b111);
		return b > 0;
	}
	
	public Direction getAttachedDirection()
	{
		byte b = (byte) (this.entityData.get(STATUS_FLAGS) & 0b111);
		if(b==0)
			return null;
		return Direction.from3DDataValue(b-1);
	}
	
	public void setAttachedDirection(Direction dir)
	{
		byte state = (byte) (this.entityData.get(STATUS_FLAGS) & 0b11111000);
		byte b = (byte) ((1+dir.get3DDataValue()) & 0b111);//add 1 to the index so we have 0 as not detached and 1 as attached to ground
		state = (byte) (state | b);
		this.entityData.set(STATUS_FLAGS, state);
	}
	
	public Direction getFacingDirection()
	{
		byte b = (byte) (this.entityData.get(STATUS_FLAGS) & 0b111000);
		b = (byte) ((b >> 3) & 0b111);
		if(b==0)
			return null;
		return Direction.from3DDataValue(b-1);
	}
	
	public void setFacingDirection(Direction dir)
	{
		byte state = (byte) (this.entityData.get(STATUS_FLAGS) & 0b11000111);
		byte b = (byte) ((1+dir.get3DDataValue()) & 0b111);//add 1 to the index so we have 0 as not detached and 1 as attached to ground
		b = (byte) ((b << 3) & 0b111000);
		state = (byte) (state | b);
		this.entityData.set(STATUS_FLAGS, state);
	}
	
	public void detachFromWall() 
	{
		byte state = (byte) (this.entityData.get(STATUS_FLAGS) & 0b11000000);
		this.entityData.set(STATUS_FLAGS, state);
	}
	
	protected boolean shouldAttackTarget(LivingEntity liv)
	{
		return true;
	}
	
	public Navigator getNavi() 
	{
		return navi;
	}
	
	@Override
	public boolean causeFallDamage(float distance, float damageMultiplier, DamageSource source) 
	{
		if(isAttachedToWallOrCeiling())
		{
			return false;
		}
		return super.causeFallDamage(distance, damageMultiplier, source);
	}
	
	@Override
	public boolean hurt(DamageSource source, float amount) 
	{
		if(source == DamageSource.IN_WALL)
		{
			if(isAttachedToWallOrCeiling())
			{
				return false;
			}
		}
		return super.hurt(source, amount);
	}
}
