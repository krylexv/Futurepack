package futurepack.common.entity.living;

import java.util.List;
import java.util.UUID;

import javax.annotation.Nullable;

import com.google.common.base.Predicate;

import futurepack.api.interfaces.IItemNeon;
import futurepack.common.FPEntitys;
import futurepack.common.FuturepackMain;
import futurepack.common.block.deco.DecoBlocks;
import futurepack.common.entity.EntityForceField;
import net.minecraft.core.particles.BlockParticleOption;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.AttributeModifier.Operation;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier.Builder;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.RandomStrollGoal;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;

public class EntityEvilRobot extends Monster
{
	private static final EntityDataAccessor<Integer> BOT_STATE = SynchedEntityData.defineId(EntityEvilRobot.class, EntityDataSerializers.INT);

	/*
	 * Operations von den AttributeModifier
	 * 0: wert + basis wert
	 * 1: wert * modifizewrter basis wert + basiwert
	 * 2: (wert+1 ) * doppelt modiferter basiswert
	 */
	
	private static final AttributeModifier ARMOR_ARMOR_MODIEFIER     = new AttributeModifier(UUID.fromString("67576589-7686-5682-ADFF-33434456FEA6"), "Armor boost", 8.0D, Operation.ADDITION);
	private static final AttributeModifier ARMOR_KNOCKBACK_RESISTENCE_MODIFIER  = new AttributeModifier(UUID.fromString("67576589-7686-5682-ADFF-33434456FEB7"), "Armor knockback resistence boost", 2.0D, Operation.ADDITION);
	
	private static final AttributeModifier ARMORLESS_SPEED_MODIEFIER = new AttributeModifier(UUID.fromString("67576589-7686-5682-ADFF-3346346CAE3B"), "Armorless speed boost", 0.5D, Operation.MULTIPLY_BASE);
	private static final AttributeModifier ARMORLESS_ATTACK_MODIFIER = new AttributeModifier(UUID.fromString("67576589-7686-5682-ADFF-3A8900BDFE3F"), "Armorless attack boost", 0.75D, Operation.MULTIPLY_TOTAL);
	
	private static final AttributeModifier REGENERATION_SPEED_MODIEFIER     = new AttributeModifier(UUID.fromString("67576589-7686-5682-ADFF-33434456FEA6"), "Regeneration speed slowdown", -0.75D, Operation.MULTIPLY_TOTAL);
	
	public static enum EnumBotState
	{
		Base(true, true, false),
		Damaged(false, false, false),
		Regenerating(false, false, true);
		
		public final boolean absorbEnergie;
		public final boolean hasArmor;
		public final boolean regenerating;
		
		private EnumBotState(boolean absorbEnergie, boolean hasArmor, boolean regenerating)
		{
			this.absorbEnergie = absorbEnergie;
			this.hasArmor = hasArmor;
			this.regenerating = regenerating;
		}	
	}
	
	public EntityEvilRobot(Level worldIn)
	{
		super(FPEntitys.EVIL_ROBOT, worldIn);
		
	}
	
	public EntityEvilRobot(EntityType<EntityEvilRobot> type, Level worldIn)
	{
		super(type, worldIn);
		
	}
	
	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(1, new FloatGoal(this));
		this.goalSelector.addGoal(2, new MeleeAttackGoal(this, 1.1D, false));
		this.goalSelector.addGoal(5, new RandomStrollGoal(this, 1.0D));
		this.goalSelector.addGoal(6, new LookAtPlayerGoal(this, Player.class, 8.0F));
		this.goalSelector.addGoal(6, new RandomLookAroundGoal(this));
		this.targetSelector.addGoal(1, new HurtByTargetGoal(this, new Class[0]));
		this.targetSelector.addGoal(2, new NearestAttackableTargetGoal(this, Player.class, true));
	}
	
	
	
	@Override
	protected void defineSynchedData()
    {
        super.defineSynchedData();
        this.getEntityData().define(BOT_STATE, EnumBotState.Base.ordinal());        
    }
	
	public static Builder registerAttributes()
    {
		return Monster.createMonsterAttributes()
				.add(Attributes.MOVEMENT_SPEED, 0.23D)
				.add(Attributes.MAX_HEALTH, 50F)
				.add(Attributes.ATTACK_DAMAGE, 5D)
				.add(Attributes.ARMOR, 2.0D);
    }
	
	public void setState(EnumBotState state)
	{
		getEntityData().set(BOT_STATE, state.ordinal());
		updateModifiers();
	}
	
	public EnumBotState getState()
	{
		return EnumBotState.values()[getEntityData().get(BOT_STATE)];
	}
	
	private void updateModifiers()
	{
		this.getAttribute(Attributes.ARMOR).removeModifier(ARMOR_ARMOR_MODIEFIER);
		this.getAttribute(Attributes.KNOCKBACK_RESISTANCE).removeModifier(ARMOR_KNOCKBACK_RESISTENCE_MODIFIER);
		this.getAttribute(Attributes.MOVEMENT_SPEED).removeModifier(REGENERATION_SPEED_MODIEFIER);
		this.getAttribute(Attributes.MOVEMENT_SPEED).removeModifier(ARMORLESS_SPEED_MODIEFIER);	
		this.getAttribute(Attributes.ATTACK_DAMAGE).removeModifier(ARMORLESS_ATTACK_MODIFIER);
		
		EnumBotState state = getState();
		if(state.hasArmor)
		{
			this.getAttribute(Attributes.ARMOR).addTransientModifier(ARMOR_ARMOR_MODIEFIER);
			this.getAttribute(Attributes.KNOCKBACK_RESISTANCE).addTransientModifier(ARMOR_KNOCKBACK_RESISTENCE_MODIFIER);
		}
		else
		{
			this.getAttribute(Attributes.MOVEMENT_SPEED).addTransientModifier(ARMORLESS_SPEED_MODIEFIER);	
			this.getAttribute(Attributes.ATTACK_DAMAGE).addTransientModifier(ARMORLESS_ATTACK_MODIFIER);
		}
		
		if(state.regenerating)
		{
			this.getAttribute(Attributes.MOVEMENT_SPEED).addTransientModifier(REGENERATION_SPEED_MODIEFIER);
		}
	}
	
	private EnumBotState last;
	
	@Override
	public void baseTick()
	{
		if(getHealth() < getMaxHealth() * 0.5F)
		{
			if(getHealth() < getMaxHealth() * 0.2F)
			{
				if(getState()!=EnumBotState.Regenerating)
				{
					setState(EnumBotState.Regenerating);				
				}
			}
			else if(getState()==EnumBotState.Base)
			{
				setState(EnumBotState.Damaged);
				if(!level.isClientSide)
				{
					ServerLevel serv = (ServerLevel) level;
					BlockParticleOption data = new BlockParticleOption(ParticleTypes.BLOCK, DecoBlocks.color_iron_white.defaultBlockState());
					serv.sendParticles(data, getX(), getY()+getEyeHeight(), getZ(), 40, 0, 0.1, 0, 0.2);
					serv.sendParticles(data, getX(), getY()+getEyeHeight()/2, getZ(), 40, 0, 0.1, 0, 0.2);
					serv.sendParticles(data, getX(), getY(), getZ(), 40, 0, 0.1, 0, 0.2);
				}
			}
		}
		else
		{
			setState(EnumBotState.Base);
		}
		
		EnumBotState state = getState();
		if(state!=last && level.isClientSide)
		{
			onSyncedDataUpdated(BOT_STATE);
		}
			
		if(state.regenerating)
		{
			this.heal(0.1F);
			if(level.isClientSide)
			{
				addParticleTypes(ParticleTypes.HEART, 0.1F);
			}
		}
		
		last = state;
		super.baseTick();
	}
	
	@Override
	public void onSyncedDataUpdated(EntityDataAccessor<?> key)
	{
		if(key==BOT_STATE)
		{
			if(getState()==EnumBotState.Regenerating)
			{
				createForceField();
			}		
		}
		super.onSyncedDataUpdated(key);
	}
	
	@Override
	public boolean hurt(DamageSource source, float amount)
	{
		if(getState().absorbEnergie)
		{
			if(source == FuturepackMain.NEON_DAMAGE)
			{
				addParticleTypes(ParticleTypes.HEART, 1F);
				heal(amount);
				return true;
			}
			else if(source.getEntity() instanceof LivingEntity)
			{
				LivingEntity base = (LivingEntity) source.getEntity();
				ItemStack it = base.getMainHandItem();
				if(it!=null && it.getItem() instanceof IItemNeon)
				{
					IItemNeon neon = (IItemNeon) it.getItem();
					int min = (int) Math.min(neon.getNeon(it), amount);
					neon.addNeon(it, -min);
					heal(min);
					addParticleTypes(ParticleTypes.HEART, min);
				}
			}
		}
		return super.hurt(source, amount);
	}
	
	@Override
	public boolean addEffect(MobEffectInstance potioneffectIn, @Nullable Entity cause)
	{
		if(potioneffectIn.getEffect() == MobEffects.POISON)
			return false;
				
		return super.addEffect(potioneffectIn, cause);
	}
	
	private void addParticleTypes(ParticleOptions heart, float speed)
	{
		AABB bb = this.getBoundingBox();
		while(speed>0)
		{	
			if(level.random.nextFloat() <= speed)
			{			
				double x = bb.minX +(bb.maxX-bb.minX)*level.random.nextFloat();
				double y = bb.minY +(bb.maxY-bb.minY)*level.random.nextFloat();
				double z = bb.minZ +(bb.maxZ-bb.minZ)*level.random.nextFloat();
				level.addParticle(heart, x, y, z, 0D, 0D, 0D);		
			}
			speed--;
		}
	}
	
	private void createForceField()
	{
		setDeltaMovement(Vec3.ZERO);
		
		if(level.isClientSide)
		{
			AABB bb = new AABB(getX()-5, getY(), getZ()-5, getX()+5, getY()+5, getZ()+5);
			List<LivingEntity> base = level.getEntitiesOfClass(LivingEntity.class, bb, new Predicate<LivingEntity>()
			{
				@Override
				public boolean apply(LivingEntity input)
				{				
					return !EntityEvilRobot.class.isAssignableFrom(input.getClass());
				}
			});
			
			base.forEach(e ->
			{
				Vec3 vec = e.position();
				Vec3 pos = position();
				Vec3 mot = vec.subtract(pos).add(0, 0.2, 0);
				double dis = mot.length();
				mot = mot.scale((5-dis) / dis );
				 
				e.push(mot.x, mot.y, mot.z);
			});
		}
		else
		{
			EntityForceField.createFromEntity(this);
		}
		
	}
}
