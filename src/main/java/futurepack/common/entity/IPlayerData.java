package futurepack.common.entity;

import net.minecraft.nbt.CompoundTag;

public interface IPlayerData
{
	public CompoundTag getTag();
	
	public void addAll(CompoundTag nbt);
}
