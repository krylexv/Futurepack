package futurepack.common.entity.ai;

import java.util.EnumSet;

import futurepack.common.block.terrain.TerrainBlocks;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;

public class AIEatFireflyes extends Goal
{
	private final Animal parentEntity;
	
	private BlockPos firefliyes; 
	protected BlockState toFind;
	
	public AIEatFireflyes(Animal ghast)
	{
		this.parentEntity = ghast;
		//this.setMutexFlags(EnumSet.of(Goal.Flag.MOVE));
		this.setFlags(EnumSet.of(Goal.Flag.JUMP));
		toFind = TerrainBlocks.fireflies.defaultBlockState();
	}
	
	@Override
	public boolean canUse()
	{
		if(this.parentEntity.level.isDay())
			return false;
		firefliyes = new BlockPos(this.parentEntity.position());
		Level w = this.parentEntity.level;
		return w.getBlockState(firefliyes) == toFind;
	}
	
	@Override
	public void start()
	{	
		if(firefliyes!=null)
		{
			if(this.parentEntity.isBaby())
			{
				parentEntity.ageUp((int)(-parentEntity.getAge() / 20 * 0.1F), true);
			}
			else
			{
				
				this.parentEntity.setInLove(null);

				CompoundTag nbt = new CompoundTag();
				parentEntity.addAdditionalSaveData(nbt);
				int inLove = nbt.getInt("InLove");
				inLove = Math.max(inLove, 1200);//1 minutie
				nbt.putInt("InLove", inLove + 600);
				parentEntity.readAdditionalSaveData(nbt);
			}
			
			this.parentEntity.level.removeBlock(firefliyes, false);
			if(parentEntity.level.random.nextInt(4)==0)
			{
				parentEntity.spawnAtLocation(Items.GLOWSTONE_DUST, 1);
			}
			firefliyes = null;
		}		
	}
	
	@Override
	public void stop()
	{
		firefliyes = null;
	}
}
