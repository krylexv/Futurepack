package futurepack.common.entity.ai;

import java.util.EnumSet;
import java.util.Random;

import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.control.MoveControl;
import net.minecraft.world.entity.ai.goal.Goal;

public class AIRandomFly extends Goal
{
	private final Mob parentEntity;

	public AIRandomFly(Mob ghast)
	{
		this.parentEntity = ghast;
		this.setFlags(EnumSet.of(Goal.Flag.MOVE));
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean canUse()
	{
		MoveControl entitymovehelper = this.parentEntity.getMoveControl();

		if (!entitymovehelper.hasWanted())
		{
			return true;
		}
		else
		{
			double d0 = entitymovehelper.getWantedX() - this.parentEntity.getX();
			double d1 = entitymovehelper.getWantedY() - this.parentEntity.getY();
			double d2 = entitymovehelper.getWantedZ() - this.parentEntity.getZ();
			double d3 = d0 * d0 + d1 * d1 + d2 * d2;
			return d3 < 1.0D || d3 > 3600.0D;
		}
	}

	/**
	 * Returns whether an in-progress EntityAIBase should continue executing
	 */
	@Override
	public boolean canContinueToUse()
	{
		return false;
	}

	
	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	@Override
	public void start()
	{
		Random random = this.parentEntity.getRandom();
		double d0 = this.parentEntity.getX() + (random.nextFloat() * 2.0F - 1.0F) * 16.0F;
		double d1 = this.parentEntity.getY() + (random.nextFloat() * 2.0F - 1.0F) * 16.0F;
		double d2 = this.parentEntity.getZ() + (random.nextFloat() * 2.0F - 1.0F) * 16.0F;
		
		d1 = Math.min(160, d1);
		
		this.parentEntity.getMoveControl().setWantedPosition(d0, d1, d2, 1.0D);
	}
}
