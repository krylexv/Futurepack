package futurepack.common.entity.drones;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Queue;

import net.minecraft.core.BlockPos;

/**
 * An abstract Drone Station that can handle multitasking with multiple Drones on multiple Claims
 * @author Wugi
 *
 */
public abstract class DroneControllerBase 
{
	/**
	 * List of all Zones (Claims) assigned to this Station
	 */
	ArrayList<ZoneBase> zones;
	
	/**
	 * List of Task with an assigned Worker
	 */
	Queue<WeakReference<TaskBase>> working;
	
	/**
	 * List of Task without an assigned Worker
	 */
	Queue<WeakReference<TaskBase>> ready;
	
	/**
	 * List of spawned Workers without an Task (idle)
	 */
	Queue<IWorker> workers;
	
	/**
	 * Query the maximum number of workers (Drones) for this station
	 * @return total number of worker
	 */
	abstract int getWorkerCount();
	
	/**
	 * Callback to spawn an Worker (idr. a Drone) that can work for this station
	 * @return Interface of the worker
	 */
	abstract IWorker spawnWorker();
	
	/**
	 * Callback to destroy an Worker (idr. a Drone) that is no longer needed.
	 * Important: always reduce getWorkerCount() and not killing an worker yourself
	 */
	abstract void destroyWorker(IWorker worker);
	
	abstract BlockPos getLandingPos();
	
	/**
	 * TileEnity tick of the Drone Station
	 */
	void tick()
	{
		//Process Zones
		for(ZoneBase zone : zones)
		{
			if(zone.isDirty() && zone.isEnabled())
			{
				zone.generateTasks();
				zone.tasks.stream().map(WeakReference::new).forEach(ready::add);
			}
		}
		
		int workerCount = getWorkerCount();
		
		//Kill not exisiting Workers
		for(int i = workerCount - 1; i >= working.size(); i--)
		{
			WeakReference<TaskBase> wtask = working.remove();
			TaskBase task = wtask.get();
			if(task != null)
			{
				destroyWorker(task.worker);
				task.worker = null;
			}
			
			ready.add(wtask);
		}
		
		//Work on all Active Tasks
		for(WeakReference<TaskBase> wtask : working)
		{
			TaskBase task = wtask.get();
			if(task.step())
			{
				working.remove(wtask);
				setWorkerIdle(task.worker);
				task.worker = null;
				
				if(task.isReapeatable())
				{
					ready.add(wtask);
				}
			}
		}
		
		//Check ready Tasks
		if(working.size() < workerCount)
		{
			WeakReference<TaskBase> wtask = ready.poll();
			TaskBase task = wtask.get();
			
			if(task != null)
			{
				if(task.isReady())
				{
					IWorker w = null;
					if(workers.isEmpty())
					{
						w = spawnWorker();
					}
					else
					{
						w = workers.poll();
					}
						
					working.add(wtask);
					
				}
				else
				{
					ready.add(wtask);
				}
			}			
		}
		
		//Check Workers at home
		for(IWorker worker : workers)
		{
			if(worker.isFinised() && getLandingPos().equals(worker.getPosition()))
			{
				destroyWorker(worker);
			}
		}
	}
	
	void setWorkerIdle(IWorker worker)
	{
		if(worker != null)
		{
			workers.add(worker);
			worker.moveOrTeleport(getLandingPos());
		}
	}
	
}
