package futurepack.common.entity.drones;

import net.minecraft.core.BlockPos;
import net.minecraft.core.BlockPos.MutableBlockPos;
import net.minecraft.core.Vec3i;

public class ZoneMiner extends ZoneBase {

	@Override
	boolean isDirty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	private static int DivRoundUp(int num, int divisor) 
	{
	    return (num + divisor - 1) / divisor;
	}
	
	@Override
	void generateTasks() 
	{
		tasks.clear();
		
		//Split [min, max] into Pieces of at least 8� Block Volume cuboid; limit amout of pieces to 8 per dimension (512 in total)
		Vec3i size = new Vec3i(max.getX() - min.getX() + 1, max.getY() - min.getY() + 1, max.getZ() - min.getZ() + 1);
		MutableBlockPos step = new MutableBlockPos(8,8,8);
		
		if(size.getX() > 8*8)
		{
			step.setX(size.getX() / 8);
		}
		
		if(size.getY() > 8*8)
		{
			step.setY(size.getY() / 8);
		}
		
		if(size.getZ() > 8*8)
		{
			step.setZ(size.getZ() / 8);
		}
		
		
		for(int x = DivRoundUp(size.getX(), step.getX()) - 1; x >= 0 ; x--)
		{
			int taskMinX = min.getX() + (x-1)*step.getX();
			int taskMaxX = min.getX() + (x)*step.getX();
			
			if(x == 0)
			{
				taskMinX = min.getX();			
			}
			
			for(int y = DivRoundUp(size.getY(), step.getY()) - 1; y >= 0 ; y--)
			{
				int taskMinY = min.getY() + (y-1)*step.getY();
				int taskMaxY = min.getY() + (y)*step.getY();
				
				if(y == 0)
				{
					taskMinY = min.getY();			
				}
				
				for(int z = DivRoundUp(size.getZ(), step.getZ()) - 1; z >= 0 ; z--)
				{
					int taskMinZ = min.getZ() + (z-1)*step.getZ();
					int taskMaxZ = min.getZ() + (z)*step.getZ();
					
					if(z == 0)
					{
						taskMinZ = min.getZ();			
					}
					
					tasks.add(new TaskMiner(new BlockPos(taskMinX, taskMinY, taskMinZ), new BlockPos(taskMaxX, taskMaxY, taskMaxZ)));
				}
			}
		}
		
		
	}

}
