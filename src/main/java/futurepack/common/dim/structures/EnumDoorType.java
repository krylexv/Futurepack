package futurepack.common.dim.structures;

public enum EnumDoorType 
{
	NORMAL,
	WATER;

	public static EnumDoorType fromName(String asString) 
	{
		try
		{
			return EnumDoorType.valueOf(asString.toUpperCase());
		}
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
			return NORMAL;
		}
	}
	
}
