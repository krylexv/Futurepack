package futurepack.common.dim.structures;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Function;

import futurepack.common.dim.structures.generation.DungeonGeneratorBase;
import futurepack.common.dim.structures.generation.EnumGenerationType;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;

public class ManagerDungeonStructures
{
	static
	{
		transformers = new TreeSet<>((a,b) -> b.hashCode() - a.hashCode());
		addTransformer((path,rot,base) -> 
		{
			if(path.startsWith("dungeon/new_start_dungeon_"))
			{
				return new StructureTecDungeon(base);
			}
			else if(path.startsWith("dungeon/checkpoint"))
			{
				return new StructureCheckpoint(base);
			}
			else
			{
				return base;
			}
		});
	}
	
	
	public static void init(DungeonGeneratorBase base, boolean spawnEntrace)
	{
		//Corridor
		addEntryAuto(base, 2, "corridor_I", EnumGenerationType.NORMAL, 6);
		addEntryAuto(base, 4, "corridor_L", EnumGenerationType.NORMAL, 2);
		addEntryAuto(base, 4, "corridor_T", EnumGenerationType.NORMAL, 6);
		addEntryAuto(base, 1, "corridor_X_1", EnumGenerationType.NORMAL, 1);
		addEntryAuto(base, 1, "corridor_X_2", EnumGenerationType.NORMAL, 1);		
		addEntryManual(base, 4, "corridor_stairs", EnumGenerationType.NORMAL, 4);
		addEntryAuto(base, 4, "corridor_end_1", EnumGenerationType.NORMAL, 1);
		addEntryAuto(base, 4, "corridor_end_2", EnumGenerationType.NORMAL, 1);
		addEntryManual(base, 4, "corridor_ladder_flat", EnumGenerationType.NORMAL, 4);
		addEntryManual(base, 4, "corridor_ladder", EnumGenerationType.NORMAL, 2);
		addEntryManual(base, 4, "corridor_laddermulti", EnumGenerationType.NORMAL, 3);
		
		addEntryAuto(base, 4, "corner_large_water", EnumGenerationType.NORMAL, 4);
		addEntryAuto(base, 4, "corner_water", EnumGenerationType.NORMAL, 3);
		addEntryAuto(base, 4, "corridor_water", EnumGenerationType.NORMAL, 4);
		addEntryAuto(base, 4, "crashed_water_water_clear", EnumGenerationType.NORMAL, 2);
		addEntryAuto(base, 4, "crossing_t_water", EnumGenerationType.NORMAL, 4);
		addEntryAuto(base, 4, "crossing_water", EnumGenerationType.NORMAL, 4);
		addEntryAuto(base, 4, "height_cross_big_water_clear", EnumGenerationType.NORMAL, 4);
		addEntryAuto(base, 4, "height_large_water", EnumGenerationType.NORMAL, 4);
		addEntryAuto(base, 4, "height_large_water_clear", EnumGenerationType.NORMAL, 4);
		addEntryAuto(base, 4, "height_small_water", EnumGenerationType.NORMAL, 4);
		addEntryAuto(base, 4, "height_start_water", EnumGenerationType.NORMAL, 4);
		addEntryAuto(base, 4, "height_water", EnumGenerationType.NORMAL, 4);
		addEntryAuto(base, 4, "height_water_clear", EnumGenerationType.NORMAL, 4);
		
		addEntryAutoWater(base, 2, "corridor_I", EnumGenerationType.NORMAL, 6);
		addEntryAutoWater(base, 4, "corridor_L", EnumGenerationType.NORMAL, 2);
		addEntryAutoWater(base, 4, "corridor_T", EnumGenerationType.NORMAL, 6);
		addEntryAutoWater(base, 1, "corridor_X_1", EnumGenerationType.NORMAL, 1);
		addEntryAutoWater(base, 1, "corridor_X_2", EnumGenerationType.NORMAL, 1);		
		addEntryManualWater(base, 4, "corridor_stairs", EnumGenerationType.NORMAL, 4);
		addEntryAutoWater(base, 4, "corridor_end_1", EnumGenerationType.NORMAL, 1);
		addEntryAutoWater(base, 4, "corridor_end_2", EnumGenerationType.NORMAL, 1);
		addEntryManualWater(base, 4, "corridor_ladder_flat", EnumGenerationType.NORMAL, 4);
		addEntryManualWater(base, 4, "corridor_ladder", EnumGenerationType.NORMAL, 2);
		addEntryManualWater(base, 4, "corridor_laddermulti", EnumGenerationType.NORMAL, 3);
		
		//Deco
		addEntryAuto(base, 4, "deco_end_1", EnumGenerationType.NORMAL, 8);
		addEntryManual(base, 4, "deco_end_2", EnumGenerationType.NORMAL, 8);
		//addEntryAuto(base, 4, "doors", EnumGenerationType.NORMAL, 2); //REMOVED: Messes up Dungeon Generation (uncompletable)
		addEntryAuto(base, 4, "reactor_water", EnumGenerationType.NORMAL, 2);
		
		//Loot
		addEntryManual(base, 4, "loot_end_1", EnumGenerationType.NORMAL, 4);
		addEntryManualWater(base, 4, "loot_end_1", EnumGenerationType.NORMAL, 4);
		addEntryAuto(base, 4, "loot_end_crystal_bio", EnumGenerationType.NORMAL, 1);
		addEntryAuto(base, 4, "loot_end_crystal_neon", EnumGenerationType.NORMAL, 6);
		addEntryManual(base, 4, "loot_jmpnrun_1", EnumGenerationType.NORMAL, 4);
		addEntryManual(base, 4, "loot_end_flat", EnumGenerationType.NORMAL, 4);
		addEntryManual(base, 4, "loot_end_simple", EnumGenerationType.NORMAL, 3);
		addEntryManualWater(base, 4, "loot_end_flat", EnumGenerationType.NORMAL, 4);
		addEntryManualWater(base, 4, "loot_end_simple", EnumGenerationType.NORMAL, 3);
		addEntryManual(base, 4, "loot_jmpnrun_2", EnumGenerationType.NORMAL, 1);
		
		addEntryAuto(base, 4, "loot_end_tectern", EnumGenerationType.NORMAL, 1);//TODO: fix spacing 
		//addEntryAuto(base, 4, "game_cart_clear", EnumGenerationType.NORMAL, 1);		//REMOVED: Minigame in this part is unplayable since Gravity Pulsar change
		addEntryAuto(base, 4, "height_with_chest_clear", EnumGenerationType.NORMAL, 2);
		
		//checkpoint / teleporter
		addEntryAuto(base, 4, "checkpoint_1", EnumGenerationType.NORMAL, 4);//TODO: fix spacing 
		addEntryAutoWater(base, 4, "checkpoint_1", EnumGenerationType.NORMAL, 4);
		//addEntryAuto(base, 4, "checkpoint_forcefield_clear", EnumGenerationType.NORMAL, 2);	//REMOVED: Messes up Dungeon Generation (uncompletable)
		
		//Boss -- TODO more boss rooms @Wugi
		addEntryManual(base, 4, "special_boss_1", EnumGenerationType.BOSS, 100);	
		addEntryManualWater(base, 4, "special_boss_1", EnumGenerationType.BOSS, 100);
		
		
		//addEntryManual(base, 4, "special_boss_2", EnumGenerationType.BOSS, 100);
		addEntryAuto(base, 4, "special_teclock_1", EnumGenerationType.TECLOCK, 100);
		addEntryAutoWater(base, 4, "special_teclock_1", EnumGenerationType.TECLOCK, 100);
		
		
		//Entrace -- TODO more entraces ? @Wugi
		if(spawnEntrace)
			addEntryAuto(base, 1, "special_entrance_bottom", EnumGenerationType.ENTRACE, 100);
		else
			base.addEntry(get("special_entrance_bottom", 0), EnumGenerationType.ENTRACE, 100);
		
		addEntryManual(base, 4, "special_endroom_1", EnumGenerationType.ENDROOM, 100);
		addEntryAuto(base, 1, "special_endroom_2", EnumGenerationType.ENDROOM, 100);
		/*
		addEntryAuto(base, 4, "large_entrance_T", EnumGenerationType.NORMAL, 1);
		addEntryAuto(base, 1, "large_X", EnumGenerationType.NORMAL, 5);
		addEntryAuto(base, 4, "large_T", EnumGenerationType.NORMAL, 5);
		addEntryAuto(base, 4, "large_corner", EnumGenerationType.NORMAL, 5);*/
		
		addEntryAuto(base, 4, "loot_deepcoreminer", EnumGenerationType.NORMAL, 1);
	}
	
	
	public static void addEntryAuto(DungeonGeneratorBase base, int rotations, String path, EnumGenerationType type, int weight)
	{
		
		for(int i=0; i<rotations; i++)
		{
			if(type==EnumGenerationType.BOSS)
			{
				base.addEntry(new StructureBoss(get(path, i)), type, weight*4/rotations);
			}
			else if(type==EnumGenerationType.TECLOCK)
			{
				base.addEntry(new StructureTecLock(get(path, i)), type, weight*4/rotations);
			}	
			else if(type==EnumGenerationType.ENTRACE)
			{
				base.addEntry(new StructureEntrance(get(path, i)), type, weight*4/rotations);
			}
			else if(type==EnumGenerationType.ENDROOM)
			{
				base.addEntry(new StructureEndRoom(get(path,i)), type, weight*4/rotations);
			}
			else
			{
				base.addEntry(get(path, i), type, weight*4/rotations);
			}
		}
	}
	
	public static void addEntryAutoWater(DungeonGeneratorBase base, int rotations, String path, EnumGenerationType type, int weight)
	{
		for(int i=0; i<rotations; i++)
		{
			StructureBase blocks = waterFy(get(path, i));
			
			if(type==EnumGenerationType.BOSS)
			{
				base.addEntry(new StructureBoss(blocks), type, weight*4/rotations);
			}
			else if(type==EnumGenerationType.TECLOCK)
			{
				base.addEntry(new StructureTecLock(blocks), type, weight*4/rotations);
			}	
			else if(type==EnumGenerationType.ENTRACE)
			{
				base.addEntry(new StructureEntrance(blocks), type, weight*4/rotations);
			}
			else if(type==EnumGenerationType.ENDROOM)
			{
				base.addEntry(new StructureEndRoom(blocks), type, weight*4/rotations);
			}
			else
			{
				base.addEntry(blocks, type, weight*4/rotations);
			}
		}
	}
	
	public static void addEntryManual(DungeonGeneratorBase base, int rotations, String path, EnumGenerationType type, int weight)
	{
		for(int i=0; i<rotations; i++)
		{
			StructureBase blocks = waterFy(get(path+"_R"+i, 0));
			
			if(type==EnumGenerationType.BOSS)
			{
				base.addEntry(new StructureBoss(blocks), type, weight*4/rotations);
			}
			else if(type==EnumGenerationType.TECLOCK)
			{
				base.addEntry(new StructureTecLock(blocks), type, weight*4/rotations);
			}
			else if(type==EnumGenerationType.ENDROOM)
			{
				base.addEntry(new StructureEndRoom(blocks), type, weight*4/rotations);
			}
			else
			{
				base.addEntry(blocks, type, weight*4/rotations);
			}
		}	
	}
	
	public static void addEntryManualWater(DungeonGeneratorBase base, int rotations, String path, EnumGenerationType type, int weight)
	{
		for(int i=0; i<rotations; i++)
		{
			if(type==EnumGenerationType.BOSS)
			{
				base.addEntry(new StructureBoss(get(path+"_R"+i, 0)), type, weight*4/rotations);
			}
			else if(type==EnumGenerationType.TECLOCK)
			{
				base.addEntry(new StructureTecLock(get(path+"_R"+i, 0)), type, weight*4/rotations);
			}
			else if(type==EnumGenerationType.ENDROOM)
			{
				base.addEntry(new StructureEndRoom(get(path+"_R"+i, 0)), type, weight*4/rotations);
			}
			else
			{
				base.addEntry(get(path+"_R"+i, 0), type, weight*4/rotations);
			}
		}	
	}
	
	public static StructureBase waterFy(StructureBase input)
	{
		changeBlocks(input, ManagerDungeonStructures::aquafier);
		
		for(OpenDoor door : input.doors)
		{
			door.type = EnumDoorType.WATER;
		}
		return input;
	}
	
	public static StructureBase changeBlocks(StructureBase input, Function<BlockState, BlockState> replaceFunc)
	{
		for(BlockState[][] yz : input.getBlocks())
		{
			for(BlockState[] z : yz)
			{
				for(int i=0;i<z.length;i++)
				{
					z[i] = replaceFunc.apply(z[i]);
				}
			}
		}
		return input;
	}
	
	public static BlockState aquafier(BlockState state)
	{
		if(state==null)
			return null;
		else if(state.getBlock()==Blocks.AIR || state.isAir())
			return Blocks.WATER.defaultBlockState();
		else if(state.hasProperty(BlockStateProperties.WATERLOGGED))
		{
			return state.setValue(BlockStateProperties.WATERLOGGED, true);
		}
		else
		{
			return state;
		}
	}
	
	private static InputStream get(String name)
	{
		InputStream in = ClassLoader.getSystemClassLoader().getResourceAsStream("futurepack/common/dim/structures/" + name);
		if(in==null)
		{
			in = LoaderStructures.class.getResourceAsStream("./" + name);
		}
		return in;
	}
	

	@FunctionalInterface
	public static interface IStructureTransformer
	{
		public StructureBase transform(String path, int rotation, StructureBase base);
	}
	
	private final static Set<IStructureTransformer> transformers;
	
	public static void addTransformer(IStructureTransformer transformer)
	{
		transformers.add(transformer);
	}
	
	public static StructureBase transform(String path, int rotation, StructureBase base) 
	{
		for(IStructureTransformer f : transformers)
		{
			base = f.transform(path, rotation, base);
		}
		return base;
	}
	
	public static StructureBase get(String name, int rot)
	{
		name = "dungeon/"+name+".json";
		StructureBase base = LoaderStructures.getFromStream(get(name), rot, name);
		return transform(name, rot, base);
	}
}
