package futurepack.common.dim.structures;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mojang.serialization.JsonOps;

import futurepack.depend.api.helper.HelperHologram;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtOps;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.state.BlockState;

public class LoaderStructures
{
	/**
	 * rotated counter clockwise
	 */
	public static Object[][] rotateAtY(Object[][] blocks)
	{
		int oldX = blocks.length;
		int oldZ = blocks[0].length;
		int newX = oldZ;
		int newZ = oldX;
		
		Object[][] array = new Object[newX][newZ];
		
		for(int x=0;x<oldX;x++)
		{
			for(int z=0;z<oldZ;z++)
			{
				Object obj = blocks[x][z];
				array[z][newZ-1-x] = obj;
			}
		}
		return array;
	}
	
	/**
	 * rotate counter clockwise
	 */
	public static BlockState[][][] rotateAtY(BlockState[][][] blocks)
	{
		int oldX = blocks.length;
		int oldZ = blocks[0][0].length;
		int sizeY = blocks[0].length;
		int newX = oldZ;
		int newZ = oldX;
		
		BlockState[][][] array = new BlockState[newX][sizeY][newZ];
		for(int x=0;x<oldX;x++)
		{
			for(int y=0;y<sizeY;y++)
			{
				for(int z=0;z<oldZ;z++)
				{				
					BlockState obj = blocks[x][y][z];
					array[z][y][newZ-1-x] = rotateState(obj, Rotation.COUNTERCLOCKWISE_90);
				}
			}
		}	
		return array;
	}
	
	public static void rotateAtY(int[] ints, int w, int h, int d)
	{
		int x=ints[0];
		//int y=ints[1];
		int z=ints[2];
		
		ints[0]=z;
		//ints[1]=y;
		ints[2]=w-1-x;
		
	}
	
	public static BlockState rotateState(BlockState state, Rotation rot)
	{
		if(state == null)
			return null;
		
		return state.rotate(rot);
//		try
//		{
//			return state.rotate(null, null, rot);
//		}
//		catch (NullPointerException e)
//		{
//			e.printStackTrace();
//			return state.rotate(rot);
//		}
	}
	
	/**
	 * @param obj is the JsonObject containing 
	 * @param rotation is the amount of rotations for 90degres counterclockwise
	 */
	public static StructureBase getFromJson(JsonObject obj, int rotation, String name)
	{
		JsonObject data = obj.getAsJsonObject("data");
		ArrayList<Object[]> list = new ArrayList<Object[]>(data.entrySet().size());
		Iterator<Entry<String,JsonElement>> iter = data.entrySet().iterator();
		int w=0,h=0,d=0;
		Map<String, BlockState> stateMap = new TreeMap<String, BlockState>();
		if(obj.has("stateMap"))
		{
			JsonObject base = obj.getAsJsonObject("stateMap");
			for(Entry<String, JsonElement> e : base.entrySet())
			{
				stateMap.put(e.getKey(), HelperHologram.fromStateString(e.getValue().getAsString()));
			}
		}
		while(iter.hasNext())
		{
			Entry<String,JsonElement> e = iter.next();
			String[] parts = e.getKey().split("-");
			int x=Integer.valueOf(parts[0]);
			int y=Integer.valueOf(parts[1]);
			int z=Integer.valueOf(parts[2]);
			w=Math.max(w, x);
			h=Math.max(h, y);
			d=Math.max(d, z);
			
			String blst = e.getValue().getAsString();
			BlockState state = stateMap.computeIfAbsent(blst, HelperHologram::fromStateString);
			if(state == null)
			{
				throw new IllegalArgumentException("Unsupported BlockState " + blst); 
			}
			list.add(new Object[]{x,y,z,state});
		}
		stateMap = null;
		w++;
		d++;
		h++;
		BlockState[][][] blocks = new BlockState[w][h][d];
		Iterator<Object[]> ite = list.iterator();
		while(ite.hasNext())
		{
			Object[] pars = ite.next();
			int x=(Integer) pars[0];
			int y=(Integer) pars[1];
			int z=(Integer) pars[2];
			blocks[x][y][z] = (BlockState) pars[3];
		}
		for(int i=0;i<rotation;i++)
		{
			blocks = rotateAtY(blocks);
		}
		StructureBase base = new StructureBase(name + "R" + rotation, blocks);
		if(obj.has("inventorys"))
		{
			JsonArray arr = obj.get("inventorys").getAsJsonArray();
			DungeonChest[] pos = new DungeonChest[arr.size()];
			for(int i=0;i<arr.size();i++)
			{			
				JsonElement elm = arr.get(i);
				if(elm.isJsonPrimitive())
				{
					String s = elm.getAsString();
					int[] xyz = getInts(s.split("-"));
					int rw=w,rd=d;
					for(int j=0;j<rotation;j++)
					{
						rotateAtY(xyz, rw,h,rd);
						int buf=rw;
						rw=rd;
						rd=buf;
					}
					pos[i] = new DungeonChest(new BlockPos(xyz[0],xyz[1],xyz[2]), base);
				}
				else if(elm.isJsonObject())
				{
					JsonObject ent = elm.getAsJsonObject();
					String s = ent.get("pos").getAsString();
					
					int[] xyz = getInts(s.split("-"));
					int rw=w,rd=d;
					for(int j=0;j<rotation;j++)
					{
						rotateAtY(xyz, rw,h,rd);
						int buf=rw;
						rw=rd;
						rd=buf;
					}
					
					ResourceLocation res = new ResourceLocation(ent.get("loot").getAsString());
					pos[i] = new DungeonChest(new BlockPos(xyz[0],xyz[1],xyz[2]), res, base);
					
				}	
			}
			base.setChests(pos);
		}
		if(obj.has("doors"))
		{
			JsonArray arr = obj.getAsJsonArray("doors");
			OpenDoor[] doors = new OpenDoor[arr.size()];
			for(int i=0;i<doors.length;i++)
			{
				JsonObject doorJ = arr.get(i).getAsJsonObject();			
				Direction face = Direction.from3DDataValue(doorJ.get("facing").getAsInt());
				int dw = doorJ.get("w").getAsInt();
				int dh = doorJ.get("h").getAsInt();
				int dd = doorJ.get("d").getAsInt();
				
				String[] s = doorJ.get("start").getAsString().split(",");
				int[] xyz = new int[]{Integer.valueOf(s[0]), Integer.valueOf(s[1]), Integer.valueOf(s[2])};
				int rw=w,rd=d;
				for(int j=0;j<rotation;j++)
				{
					int buf = dw;
					dw = dd;
					dd = buf;
					
					rotateAtY(xyz, rw, h, rd);
					
					buf=rw;
					rw=rd;
					rd=buf;
					
					xyz[2] += 1 - dd;
					
					if(face!= Direction.DOWN && face != Direction.UP)
						face = face.getCounterClockWise();
				}
				BlockPos pos = new BlockPos(xyz[0],xyz[1],xyz[2]);
				
				EnumDoorType doorType;
				if(doorJ.has("type"))
				{
					doorType = EnumDoorType.fromName(doorJ.get("type").getAsString());
				}
				else
				{
					doorType = EnumDoorType.NORMAL;
				}
				
				doors[i] = new OpenDoor(pos, face, dw, dh, dd, doorType);								
			}		
			base.setOpenDoors(doors);
		}
		if(obj.has("nbt"))
		{
			JsonArray arr = obj.get("nbt").getAsJsonArray();
			Map<BlockPos, CompoundTag> tilEntityData = new HashMap<BlockPos, CompoundTag>(arr.size());
			for(int i=0;i<arr.size();i++)
			{			
				JsonElement elm = arr.get(i);
				if(elm.isJsonObject())
				{
					JsonObject ent = elm.getAsJsonObject();
					String s = ent.get("pos").getAsString();
					
					int[] xyz = getInts(s.split("-"));
					int rw=w,rd=d;
					for(int j=0;j<rotation;j++)
					{
						rotateAtY(xyz, rw,h,rd);
						int buf=rw;
						rw=rd;
						rd=buf;
					}				
					CompoundTag nbt = (CompoundTag) JsonOps.INSTANCE.convertTo(NbtOps.INSTANCE, ent.get("nbt"));
					tilEntityData.put(new BlockPos(xyz[0],xyz[1],xyz[2]), nbt);
				}	
			}
			base.setTileData(tilEntityData);
		}
		return base;
	}
	
	public static StructureBase getFromStream(InputStream in, int rot, String name)
	{
		if(in==null)
		{
			throw new NullPointerException("InputStream is null! in StructureLoader");
		}
		Gson gson = new Gson();
		JsonObject obj = gson.fromJson(new InputStreamReader(in), JsonObject.class);
		return getFromJson(obj, rot, name);
	}
	
	private static int[] getInts(String... args)
	{
		int[] xyz = new int[args.length];
		for(int i=0;i<args.length;i++)
		{
			xyz[i] = Integer.valueOf(args[i]);
		}
		return xyz;
	}	
}
