package futurepack.common.dim.structures;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.TreeMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.mojang.serialization.JsonOps;

import futurepack.common.block.misc.BlockQuantanium;
import futurepack.common.block.misc.MiscBlocks;
import futurepack.depend.api.helper.HelperHologram;
import futurepack.depend.api.interfaces.ITileInventoryProvider;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtOps;
import net.minecraft.world.Container;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.DirectionalBlock;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.items.CapabilityItemHandler;

/**
 * THis will create a Json String of a Strucutre
 * it saves even air so use bedrock if you dont want to copy any blocks
 * saves all inventorys for auto loot generating
 */
public class StructureToJSON
{
	final Level w;
	public String fileName;
	
	public StructureToJSON(Level w)
	{
		this.w=w;
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd_kk-mm-S");
		fileName = sdf.format(new Date());
	}
	
	public void generate(BlockPos start, BlockPos end)
	{
		int sx = Math.min(start.getX(), end.getX());
		int sy = Math.min(start.getY(), end.getY());
		int sz = Math.min(start.getZ(), end.getZ());
		int ex = Math.max(start.getX(), end.getX());
		int ey = Math.max(start.getY(), end.getY());
		int ez = Math.max(start.getZ(), end.getZ());
		
		JsonObject data = new JsonObject();
		JsonArray invs = new JsonArray();
		
		ArrayList<BlockPos> doors = new ArrayList<BlockPos>();
		int[] next = new int[]{0};
		Map<BlockState, String> map = new IdentityHashMap<>();
		Map<String, String> stateMap = new TreeMap<>();
		JsonArray tileMap = new JsonArray();
		
		for(int x=sx;x<=ex;x++)
		{
			for(int y=sy;y<=ey;y++)
			{
				for(int z=sz;z<=ez;z++)
				{
					BlockPos xyz = new BlockPos(x,y,z);
					BlockState state = w.getBlockState(xyz);
					if(state.getBlock() == Blocks.BEDROCK)
					{
						continue;
					}
					if(state.getBlock() == Blocks.END_ROD || state.getBlock() == MiscBlocks.door_marker)
					{
						doors.add(xyz);
						state = Blocks.AIR.defaultBlockState();
//						continue;
					}
					if(state.getBlock() == MiscBlocks.quantanium)
					{
						state = state.setValue(BlockQuantanium.glowing, false);
					}
					String pos = String.format("%s-%s-%s", x-sx,y-sy,z-sz);
					
					String hex = map.computeIfAbsent(state, st -> {
						String h = Integer.toHexString(next[0]++);
						stateMap.put(h, save(st));
						return h;
					});
					
					data.add(pos, new JsonPrimitive(hex));
					BlockEntity tile = w.getBlockEntity(xyz);
					if(tile!=null)
					{
						if( (tile instanceof Container || tile instanceof ITileInventoryProvider || tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, Direction.UP).isPresent()))
						{
							invs.add(new JsonPrimitive(pos));
						}
						
						JsonObject entry = new JsonObject();
						entry.addProperty("pos",pos);

						
						BlockEntity tile2 = ((EntityBlock)state.getBlock()).newBlockEntity(xyz, state);
						if(tile2 == null)
						{
							entry.add("nbt", NbtOps.INSTANCE.convertTo(JsonOps.INSTANCE, tile.saveWithFullMetadata()));
							tileMap.add(entry);
						}
						else
						{
							tile2.setLevel(w);
							CompoundTag tag1 =  tile.saveWithFullMetadata();
							CompoundTag tag2 =  tile2.saveWithFullMetadata();
							
							if(!tag1.equals(tag2))//only if actuall stuff is saved and not defualt values
							{
								entry.add("nbt", NbtOps.INSTANCE.convertTo(JsonOps.INSTANCE, tile.saveWithFullMetadata()));
								tileMap.add(entry);
							}
						}
						
					}
				}
			}
		}
		
		JsonArray doorsJ = new JsonArray();
		BlockPos XYZ = null;
		while(!doors.isEmpty())
		{
			BlockPos pos = doors.remove(0);
			BlockState state = w.getBlockState(pos);
			Direction face = state.getValue(DirectionalBlock.FACING);
			Block doorBlock = state.getBlock();
			
			//testing in negative direction
			int[][] first = new int[][]{{-1,0,0},{0,-1,0},{0,0,-1}};
			int index = 0;
			while(index < first.length)
			{
				BlockPos poz = pos.offset(first[index][0],first[index][1],first[index][2]);
				state = w.getBlockState(poz);
				if(state.getBlock() != doorBlock || state.getValue(DirectionalBlock.FACING)!=face)
				{
					index++;
				}
				else
				{
					pos = poz;
					doors.remove(pos);
				}
			}
			
			BlockPos minXYZ = pos.offset(-sx,-sy,-sz);
			if(minXYZ.equals(XYZ))
			{
				continue;
			}
			XYZ = minXYZ;
			
			System.out.println(minXYZ);
			
			//testing in positive direction
			index=0;
			first = new int[][]{{1,0,0},{0,1,0},{0,0,1}};
			int[] whd = new int[]{1,1,1};
			while(index < first.length)
			{
				BlockPos poz = pos.offset(first[index][0],first[index][1],first[index][2]);
				state = w.getBlockState(poz);
				if(state.getBlock() != doorBlock || state.getValue(DirectionalBlock.FACING)!=face)
				{
					index++;
				}
				else
				{
					pos = poz;
					whd[index]++;
				}
			}
			for(int x=0;x<whd[0];x++)
			{
				for(int y=0;y<whd[1];y++)
				{
					for(int z=0;z<whd[1];z++)
					{
						BlockPos rem = new BlockPos(sx+x+minXYZ.getX(),sy+y+minXYZ.getY(),sz+z+minXYZ.getZ());
						doors.remove(rem);
					}	
				}
			}
				
			JsonObject door = new JsonObject();
			door.addProperty("facing", face.get3DDataValue());
			door.addProperty("w", whd[0]);
			door.addProperty("h", whd[1]);
			door.addProperty("d", whd[2]);
			door.addProperty("start", String.format("%s,%s,%s", minXYZ.getX(), minXYZ.getY(), minXYZ.getZ()));
			
			EnumDoorType type = null;
			if(doorBlock == MiscBlocks.door_marker)
			{
				type = EnumDoorType.WATER;
			}
			else if(doorBlock == Blocks.END_GATEWAY)
			{
				type = EnumDoorType.NORMAL;
			}
			
			if(type!=null)
			{
				door.addProperty("type", type.name().toLowerCase());
			}
			
			doorsJ.add(door);
			
			System.out.println("Door " + door);
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		
		
		JsonObject obj = new JsonObject();
		obj.add("data", data);
		obj.add("inventorys", invs);
		obj.add("doors", doorsJ);
		obj.add("stateMap", gson.toJsonTree(stateMap));
		obj.add("nbt", tileMap);
		
		try
		{	
			FileOutputStream out =  new FileOutputStream(fileName+".json");
		    String prettyJson = gson.toJson(obj);
			
			out.write(prettyJson.toString().getBytes());
			out.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
			System.out.println(obj.toString());
		}
		
		
	}
	
	private String save(BlockState state)
	{
		return HelperHologram.toStateString(state);
	}
}
