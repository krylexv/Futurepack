package futurepack.common.dim.structures.generation;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;

public interface IDungeonEventListener
{
	/**
	 * Called when every structure is placed & protection has been applied
	 * 
	 * @param w the World where this dungeon is
	 * @param dungeon the baked dungeon instance
	 * @param pos the start position of the 1st room. SO most likely somewhere in the center of the dungeon.
	 */
	public void onDungeonFinished(ServerLevel w, BakedDungeon dungeon, BlockPos pos);
}
