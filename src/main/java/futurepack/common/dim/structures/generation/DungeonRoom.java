package futurepack.common.dim.structures.generation;

import futurepack.common.dim.structures.StructureBase;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;

public class DungeonRoom
{
	public final StructureBase structure;
	public final BlockPos pos;
	public final CompoundTag extra;
	
	public DungeonRoom(StructureBase structure, BlockPos pos, CompoundTag extra)
	{
		super();
		this.structure = structure;
		this.pos = pos;
		this.extra = extra;
	}
}
