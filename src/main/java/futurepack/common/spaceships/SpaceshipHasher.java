package futurepack.common.spaceships;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.UUID;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import futurepack.api.ParentCoords;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;

public class SpaceshipHasher
{
	public static UUID hash(BlockState[][][] blocks)
	{
		return hash(asBytes(blocks));
	}
	
	public static UUID hash(byte[] bytes)
	{
		return UUID.nameUUIDFromBytes(bytes);
	}
	
	public static UUID hash(String s)
	{
		return hash(s.getBytes(StandardCharsets.UTF_8));
	}
	
	private static int asInt(BlockState state)
	{
		if(state==null)
			return 0;
		
		return Block.getId(state);
	}
	
	private static BlockState asBlockState(int d)
	{
		return Block.stateById(d);
	}
	
	public static byte[] asBytes(BlockState[][][] blocks)
	{
		int w = blocks.length;
		int h = blocks[0].length;
		int d = blocks[0][0].length;
		
		ByteBuffer buf = ByteBuffer.allocate(4 * w * h * d + 4*3);
		buf.order(ByteOrder.BIG_ENDIAN);
		buf.putInt(w);
		buf.putInt(h);
		buf.putInt(d);
		for(int x=0;x<w;x++)
		{
			for(int y=0;y<h;y++)
			{
				for(int z=0;z<d;z++)
				{
					buf.putInt(asInt(blocks[x][y][z]));
				}
			}
		}
		return buf.array();
	}
	
	public static BlockState[][][] fromBytes(byte[] bytes)
	{
		ByteBuffer buf = ByteBuffer.wrap(bytes);
		buf.order(ByteOrder.BIG_ENDIAN);
		int w = buf.getInt();
		int h = buf.getInt();
		int d = buf.getInt();
		
		BlockState[][][] states = new BlockState[w][h][d];
		for(int x=0;x<w;x++)
		{
			for(int y=0;y<h;y++)
			{
				for(int z=0;z<d;z++)
				{
					states[x][y][z] = asBlockState(buf.getInt());
				}
			}
		}
		return states;
	}
	
	public static BlockState[][][] getAllBlocks(Level world, Collection<ParentCoords> blocks)
	{
		blocks.iterator().next();
		int sx,sy,sz;
		int ex,ey,ez;
		sx=sy=sz=Integer.MAX_VALUE;
		ex=ey=ez=Integer.MIN_VALUE;
		
		for(BlockPos c : blocks)
		{
			sx = Math.min(sx, c.getX());
			sy = Math.min(sy, c.getY());
			sz = Math.min(sz, c.getZ());
			
			ex = Math.max(ex, c.getX());
			ey = Math.max(ey, c.getY());
			ez = Math.max(ez, c.getZ());
		}
		ex++;ey++;ez++;
		
		int w = ex-sx;
		int h = ey-sy;
		int d = ez-sz;
		
		BlockState[][][] states = new BlockState[w][h][d];
		for(BlockPos c : blocks)
		{
			BlockState st = world.getBlockState(c);
			int x = c.getX()-sx;
			int y = c.getY()-sy;
			int z = c.getZ()-sz;
			
			states[x][y][z] = st;
		}
		
		return states;
	}
	
	public static byte[] compress(byte[] compress)
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream(compress.length);
		try
		{
			GZIPOutputStream gout = new GZIPOutputStream(out);
			gout.write(compress);
			gout.close();
			out.close();
			return out.toByteArray();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
			return null;
		}
		
	}
	
	public static byte[] decompress(byte[] decompress)
	{
		ByteArrayInputStream in = new ByteArrayInputStream(decompress);
		ByteArrayOutputStream out = new ByteArrayOutputStream(decompress.length);
		try
		{
			GZIPInputStream gin = new GZIPInputStream(in);
			byte[] buf = new byte[1024 * 1024];
			while(gin.available()>0)
			{
				int read = gin.read(buf, 0 , buf.length);
				if(read > 0)
					out.write(buf, 0, read);
			}
			gin.close();
			out.close();
			return out.toByteArray();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}
}
