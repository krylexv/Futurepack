package futurepack.common.spaceships;

import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import futurepack.api.interfaces.IPlanet;
import futurepack.common.DataFolderWalker;
import futurepack.common.conditions.ConditionRegistry;
import net.minecraft.resources.ResourceLocation;

public class Json2PlanetReader 
{
	public static IPlanet readerPlanet(ResourceLocation id, JsonObject obj)
	{
		testProperty(obj, "texture");
		testProperty(obj, "name");
		ResourceLocation tex = new ResourceLocation(obj.get("texture").getAsString());
		String name = obj.get("name").getAsString();
		String[] upgrades;
		if(obj.has("upgrades"))
		{
			upgrades = DataFolderWalker.gson.fromJson(obj.get("upgrades"), String[].class);
		}
		else
		{
			upgrades = new String[0];
		}
		PlanetBase base = new PlanetBase(id, tex, name, upgrades);
		
		if(obj.has("breathable_atmosphere"))
		{
			base.setBreathableAtmosphere(obj.get("breathable_atmosphere").getAsBoolean());
		}
		if(obj.has("oxygen_gravity_velocity"))
		{
			base.setOxygenProprties(base.getSpreadVelocity(), obj.get("oxygen_gravity_velocity").getAsFloat());
		}
		if(obj.has("oxygen_spread_velocity"))
		{
			base.setOxygenProprties(obj.get("oxygen_spread_velocity").getAsFloat(), base.getGravityVelocity());
		}
		
		return base;
	}
	
	public static void readerJsons(ResourceLocation id, JsonObject obj)
	{
		if(!ConditionRegistry.checkCondition(obj))
			return;
		
		FPPlanetRegistry.instance.registerPlanetOptionalOverrite(readerPlanet(id, obj));
	}
	
	public static void testProperty(JsonObject obj, String name)
	{
		if(!obj.has(name))
			throw new JsonSyntaxException("Json is missing " + name + " entry!");
	}
}
