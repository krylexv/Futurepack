package futurepack.common.item;

import java.util.List;

import futurepack.api.interfaces.IItemWithRandom;
import futurepack.common.modification.IPartCore;
import futurepack.common.modification.thermodynamic.TemperatureManager;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class ItemCore extends Item implements IItemWithRandom
{
//	String[] iconname = new String[]{"Standart-Core","A1-Core","P2-Core","TCT-Core","Master-Core","Non-Core","Dungon-Core","Univ-Core","Zombie-Core","Entronium-Core"};
//	static Integer[] basecore = new Integer[]{1, 2, 3, 4, 5, 7, 7, 8, 0, 10};
//	IIcon[] icons = new IIcon[iconname.length];
	
	private final int corepower;
	
	public ItemCore(Properties props, int corepower) 
	{
		super(props);
		this.corepower = corepower;
//		this.setCreativeTab(FPMain.tab_items);
//		this.setHasSubtypes(true);
	}
	
	
	
//	@Override
//	public void registerIcons(IIconRegister reg) 
//	{
//		for(int i=0;i<iconname.length;i++)
//		{
//			icons[i] = reg.registerIcon("futurepack:" + iconname[i]);
//		}
//	}
//	
//	@Override
//	public IIcon getIconFromDamage(int par1) 
//	{
//		if(par1 < icons.length)
//		{
//			return icons[par1];
//		}
//		return super.getIconFromDamage(par1);
//	}
	
//	@Override
//	public String getTranslationKey(ItemStack it) 
//	{
//		if(it.getItemDamage() < iconname.length)
//		{
//			return "item." + iconname[it.getItemDamage()];
//		}
//		return super.getTranslationKey(it);
//	}
	
	public static IPartCore getCore(ItemStack it)
	{
		if(it != null && it.getItem() instanceof ItemCore)
		{
			ItemCore core = (ItemCore) it.getItem();
			final int p = core.getCorePower(it);
			final float temp = TemperatureManager.getTemp(it);
			
			return new IPartCore()
			{	
				@Override
				public int getCorePower() 
				{
					return p;
				}

				@Override
				public float getMaximumTemperature() 
				{
					return temp;
				}
			};
		}
		return null;
	}
	
	public int getCorePower(ItemStack it)
	{
		int base = corepower;
		if(it.hasTag() && it.getTag().contains("core"))
		{
			base += it.getTag().getInt("core");
		}
		return base;
	}
	
	@Override
	public void appendHoverText(ItemStack it, Level w, List<Component> list, TooltipFlag par4) 
	{
		list.add(new TranslatableComponent("tooltip.futurepack.item.core_power", getCorePower(it)));
		list.add(new TranslatableComponent("tooltip.futurepack.item.max_temp", TemperatureManager.getTemp(it)));
		super.appendHoverText(it, w, list, par4);
	}
	
//	@Override
//	public void getSubItems(ItemGroup t, NonNullList l) 
//	{
//		if(isInCreativeTab(t))
//		{
//			for(int i=0;i<iconname.length;i++)
//			{
//				l.add(new ItemStack(this,1,i));
//			}
//		}
//	}

	@Override
	public void setRandomNBT(ItemStack it, int random) 
	{
		if(!it.hasTag())
		{
			it.setTag(new CompoundTag());
		}
			
		if(this == ComputerItems.torus_core)
		{
			random += it.getTag().getInt("corebase");
		}
		
		it.getTag().putInt("core", random);
	}

//	@Override
//	public int getMaxMetas() 
//	{
//		return iconname.length;
//	}
//
//	@Override
//	public String getMetaName(int meta) 
//	{
//		return iconname[meta];
//	}
	
	public static ItemStack getFromToasted(ItemStack it)
	{
		if(!it.isEmpty() && it.getItem() == ComputerItems.toasted_core && it.hasTag())
		{
			CompoundTag nbt = it.getTag();
			return ItemStack.of(nbt);
		}
		return null;
	}
}
