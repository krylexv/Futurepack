package futurepack.common.item.tools;

import net.minecraft.world.item.Item;
import net.minecraft.world.item.PickaxeItem;
import net.minecraft.world.item.Tier;

public class ItemFpPickaxe extends PickaxeItem
{
	public ItemFpPickaxe(Tier tier, int attackDamageIn, float attackSpeedIn, Item.Properties builder) 
	{
		super(tier, attackDamageIn, attackSpeedIn, builder);
	}	
}
