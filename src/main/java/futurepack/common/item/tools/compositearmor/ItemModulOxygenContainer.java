package futurepack.common.item.tools.compositearmor;

import java.util.List;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public abstract class ItemModulOxygenContainer extends ItemModulArmorBase
{
	private final int defaultMaxOxygen;
	
	public ItemModulOxygenContainer(EquipmentSlot slot, int maxOxygen, Item.Properties props)
	{
		super(slot, props);
		defaultMaxOxygen = maxOxygen;	
	}
	
	@Override
	public int getBarColor(ItemStack stack)
	{
		return Mth.hsvToRgb(0.62F, 1.0F, (0.5F + (float)getOxygen(stack) / (float)getMaxOxygen(stack) * 0.5F));
	}
	
	@Override
	public int getBarWidth(ItemStack stack)
	{
		return (int) (13 * ( ((double)getOxygen(stack) / (double)getMaxOxygen(stack))));
	}
	
	@Override
	public boolean isBarVisible(ItemStack stack)
	{
		return getOxygen(stack) < getMaxOxygen(stack);
	}
	

	public static int addOxygen(ItemStack it, int i)
	{
		int ox = getOxygen(it) + i;
		int max = getMaxOxygen(it);
		
		if(ox > max)
		{
			i = max - ox + i;
			ox = max;
		}
			
		CompoundTag nbt = it.getTagElement("oxygen");
		if(nbt==null)
		{
			nbt = constructNBT(it);	
		}
		nbt.putInt("amount", ox);
		it.addTagElement("oxygen", nbt);
		
		return i;
	}

	public static int getMaxOxygen(ItemStack it)
	{
		CompoundTag nbt = it.getTagElement("oxygen");
		if(nbt==null)
		{
			nbt = constructNBT(it);	
			return ((ItemModulOxygenContainer)it.getItem()).defaultMaxOxygen;
		}
		return nbt.getInt("max");
	}
	
	public static int getOxygen(ItemStack it)
	{
		CompoundTag nbt = it.getTagElement("oxygen");
		if(nbt==null)
		{
			nbt = constructNBT(it);	
			return 0;
		}
		return nbt.getInt("amount");
	}

	private static CompoundTag constructNBT(ItemStack it)
	{
		CompoundTag nbt = new CompoundTag();
		nbt.putInt("max", ((ItemModulOxygenContainer)(it.getItem())).defaultMaxOxygen);
		nbt.putInt("amount", 0);
		it.addTagElement("oxygen", nbt);
		return nbt;
	}
	
	@Override
	public void appendHoverText(ItemStack it, Level w, List<Component> l, TooltipFlag p_77624_4_) 
	{
		l.add(new TextComponent(getOxygen(it) + "/" + getMaxOxygen(it) + " O\u2082"));
		super.appendHoverText(it, w, l, p_77624_4_);
	}

}
