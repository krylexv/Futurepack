package futurepack.common.item.tools.compositearmor;

import java.util.WeakHashMap;

import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;

public class ItemModulDynamo extends ItemModulNeonContainer
{
	private static final WeakHashMap<Player, Vec3> last_pos = new WeakHashMap<Player, Vec3>();
	
	public ItemModulDynamo(Item.Properties props) 
	{
		super(EquipmentSlot.LEGS, 1000, props);
	}

	@Override
	public void onArmorTick(Level world, Player player, ItemStack it, CompositeArmorInventory inv)
	{	
		pushEnergy(inv, it);
		
		Vec3 last = last_pos.get(player);
		
		Vec3 pos = player.position();
		
		last_pos.put(player, pos);
		
		if(last == null)
			return;
		
		if(player.isPassenger() || !player.isOnGround())
			return;
		
		float speed = (float) last.distanceTo(player.position());
			
		if(speed <= 0.05)
			return;
		
		if(world.random.nextFloat() < speed)
		{
			if(getNeon(it) < getMaxNeon(it))
			{
				addNeon(it, 1);	
				player.causeFoodExhaustion(0.05F);
			}
		}
	}
	
	@Override
	public boolean isEnergyProvider()
	{
		return true;
	}
	
}
