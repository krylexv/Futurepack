package futurepack.common.item.tools.compositearmor;

import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ItemModulFireShield extends ItemModulShield
{

	public ItemModulFireShield(Properties props)
	{
		super(props);
		initDamage *= 2;
	}
	
	@Override
	public boolean canBlockDamage(Level world, Player player, ItemStack it, CompositeArmorPart inv, DamageSource src, float amount)
	{
		return src.isFire();
	}

	@Override
	public float applyDamageReduction(Level world, Player player, ItemStack it, CompositeArmorPart inv, DamageSource src, float amount)
	{
		return amount * 0.5F;
	}
	
	@Override
	public boolean isSlotFitting(ItemStack stack, EquipmentSlot type, CompositeArmorPart armorPart)
	{
		return super.isSlotFitting(stack, type, armorPart) || type == EquipmentSlot.LEGS || type == EquipmentSlot.FEET;
	}
}
