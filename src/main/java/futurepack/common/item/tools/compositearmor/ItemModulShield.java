package futurepack.common.item.tools.compositearmor;

import java.util.List;

import futurepack.common.FPSounds;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.event.entity.living.LivingAttackEvent;

public class ItemModulShield extends ItemModulNeonContainer
{
	protected float initDamage=2F;
	protected int initStatus=20;
	
	public ItemModulShield(Item.Properties props)
	{
		super(EquipmentSlot.CHEST, 100, props);
	}
	
	public ItemModulShield(Item.Properties props, int neStorage)
	{
		super(EquipmentSlot.CHEST, neStorage, props);
	}

	@Override
	public void onArmorTick(Level world, Player player, ItemStack it, CompositeArmorInventory armor)
	{
		pullEnergy(armor, it);
		
		CompoundTag nbt = it.getTag();
		if(nbt==null)
		{
			nbt=new CompoundTag();
			it.setTag(nbt);
		}
		
		if(getNeon(it)>0)
		{
			int st = nbt.getInt("status");
			if(st < nbt.getInt("maxstatus"))
			{
				st++;
				addNeon(it, -1);
				nbt.putInt("status", st);
			}
		}
	}
	
	
	public boolean canBlockDamage(Level world, Player player, ItemStack it, CompositeArmorPart inv, DamageSource src, float amount)
	{
		CompoundTag nbt = it.getTag();
		if(nbt==null)
		{
			nbt=new CompoundTag();
			it.setTag(nbt);
		}
		
		String[] type;
		
		if(nbt.contains("damage_types"))
		{
			type = nbt.getString("damage_types").toLowerCase().split("\\+");
		}
		else
		{
			type = default_type;
		}
		
		for(String s : type)
		{
			if(src.msgId.toLowerCase().contains(s))
			{
				return true;
			}
		}
		return false;
	}
	
	public float applyDamageReduction(Level world, Player player, ItemStack it, CompositeArmorPart inv, DamageSource src, float amount)
	{
		return amount;
	}
	
	/**
	 * @return true if the event should get canceled
	 */
	private static final String[] default_type = {"mob", "player", "arrow", "fireball", "thrown", "indirectMagic", "explosion", "lightningBolt", "cactus", "magic", "fallingBlock", "dragonBreath", "fireworks", "neonDamage", "bee.hive", "bee.aggressive", "sting", "sweetBerryBush", "trident"};
	
	public boolean onWearerDamaged(Level world, Player player, ItemStack it, CompositeArmorPart inv, DamageSource src, float amount)
	{
		CompoundTag nbt = it.getTag();
		if(nbt==null)
		{
			nbt=new CompoundTag();
			it.setTag(nbt);
		}
		boolean block = canBlockDamage(world, player, it, inv, src, amount);
		
		if(!block)
			return false;
		
		float state = (float)nbt.getInt("status") / (float)getMaxStatus(nbt);
		
		if(state>0)
		{
			float dmg = getDamage(nbt) * state;
			if(amount >= dmg)
			{
				nbt.putInt("status", 0);
				amount -= dmg;
				
				player.hurt(src, amount);
			}
			else
			{
				float f = amount / dmg;
				f = state -f;
				
				nbt.putInt("status", (int) (getMaxStatus(nbt) * f));		
			}
			
			if(!world.isClientSide)
				world.playSound(null, player.getX(), player.getY(), player.getZ(), FPSounds.SHIELD_HIT, SoundSource.PLAYERS, 0.2F, 1.6F);
			
			return true;
		}	
		
		return false;
	}
	
	protected float getDamage(CompoundTag nbt)
	{
		if(nbt.contains("damage"))
		{
			return nbt.getFloat("damage");
		}
		nbt.putFloat("damage", initDamage);
		return initDamage;
	}
	
	protected int getMaxStatus(CompoundTag nbt)
	{
		if(nbt.contains("maxstatus"))
		{
			return nbt.getInt("maxstatus");
		}
		nbt.putInt("maxstatus", initStatus);
		return initStatus;
	}
	
	public static void onLivingDamaged(LivingAttackEvent event)
	{
		if(event.isCanceled())
			return;
		
		if(event.getEntityLiving() instanceof Player)
		{
			Player pl = (Player) event.getEntityLiving();
			for(int slot=0;slot<pl.getInventory().armor.size();slot++)
			{
				ItemStack armor = pl.getInventory().armor.get(slot);
				if(armor.isEmpty())
					continue;
				CompositeArmorPart armorPart = CompositeArmorPart.getInventory(armor);
				if(armorPart!=null)
				{
					for(int i=0;i<armorPart.getSlots();i++)
					{
						ItemStack sub = armorPart.getStackInSlot(i);
						if(sub.isEmpty())
							continue;
						if(sub.getItem() instanceof ItemModulShield)
						{
							if(((ItemModulShield)sub.getItem()).onWearerDamaged(event.getEntity().level, pl, sub, armorPart, event.getSource(), event.getAmount()))
							{
								event.setCanceled(true);
								return;
							}
						}
					}
				}
			}
		}
	}

	@Override
	public void fillItemCategory(CreativeModeTab tab, NonNullList<ItemStack> subItems)
	{
		if(allowdedIn(tab))
		{
			ItemStack stack = new ItemStack(this);
			stack.setTag(new CompoundTag());
			
			stack.getTag().putInt("maxstatus", initStatus);
			stack.getTag().putFloat("damage", initDamage);
			
			subItems.add(stack);
		}
	}
	
	@Override
	public void appendHoverText(ItemStack it, Level w, List<Component> l, TooltipFlag p_77624_4_)
	{
		super.appendHoverText(it, w, l, p_77624_4_);
		if(it.getTag()!=null)
		{
			CompoundTag nbt = it.getTag();
			l.add(new TextComponent(nbt.getInt("status") +  "/" + nbt.getInt("maxstatus")));
		}
	}
	
	@Override
	public boolean isEnergyConsumer()
	{
		return true;
	}
}
