package futurepack.common.item.tools;

import java.util.List;

import futurepack.api.interfaces.IItemNeon;
import futurepack.common.entity.throwable.EntityLaser;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.nbt.ListTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.BowItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.EnchantmentCategory;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.Level;

public class ItemLaserBow extends BowItem implements IItemNeon
{

	public ItemLaserBow(Item.Properties props)
	{
		super(props);
//		setMaxDamage(250);
//		setCreativeTab(FPMain.tab_items);
//		setFull3D();
	}
	
	@Override
	public int getMaxDamage(ItemStack stack)
	{
		int lvl = EnchantmentHelper.getItemEnchantmentLevel(Enchantments.UNBREAKING, stack);
		return super.getMaxDamage(stack) +(100 * lvl);
	}
	
	@Override
	public boolean isNeonable(ItemStack it)
	{
		return it.getDamageValue()>0;
	}

	@Override
	public void addNeon(ItemStack it, int i)
	{
		it.setDamageValue(it.getDamageValue() - i);
	}

	@Override
	public int getMaxNeon(ItemStack it) 
	{
		return it.getMaxDamage();
	}

	@Override
	public int getNeon(ItemStack it) 
	{
		return getMaxNeon(it) - it.getDamageValue();
	}

	@Override
	public void releaseUsing(ItemStack it, Level w, LivingEntity pl, int timeLeft)
    {
        int j = this.getUseDuration(it) - timeLeft;

		if(w.isClientSide)
			return;
			
//		if(pl instanceof EntityPlayer)
//			((EntityPlayer)pl).addStat(FPAchievements.laserbow);
		
		float f = j / getCount(it);
		f = (f * f + f * 2.0F) / 3.0F;

		if (f < 0.1D)
		{
			return;
		}

		if (f > 1.0F)
		{
			f = 1.0F;
		}
			
		EntityLaser ball = new EntityLaser(w, pl, f + EnchantmentHelper.getItemEnchantmentLevel(Enchantments.PUNCH_ARROWS, it));
		ball.shootFromRotation(pl, pl.getXRot(), pl.getYRot(), 0F, 5F, 0);
		ball.setExplosionPower((byte) EnchantmentHelper.getItemEnchantmentLevel(Enchantments.FLAMING_ARROWS, it));
		if (f == 1.0F)
		{
			ball.setIsCritical(true);
		}
			
		w.addFreshEntity(ball);
		it.hurtAndBreak(getPowereNeeded(it), pl, p -> {
			p.broadcastBreakEvent(p.getUsedItemHand());
		});
		
		return;
	}
	
	@Override
	public InteractionResultHolder<ItemStack> use(Level w, Player pl, InteractionHand hand)
    {
		ItemStack it = pl.getItemInHand(hand);
        if(getNeon(it)>=getPowereNeeded(it))
		{
        	pl.startUsingItem(hand);
            return new InteractionResultHolder<ItemStack>(InteractionResult.SUCCESS, it);
		}
        return InteractionResultHolder.pass(it);
    }
	
	
	public int getPowereNeeded(ItemStack it)
	{
		int i = 1;
		ListTag nbttaglist = it.getEnchantmentTags();
        if (nbttaglist != null)
        {
        	for (int j = 0; j < nbttaglist.size(); ++j)
            {
        		short short1 = nbttaglist.getCompound(j).getShort("id");
        		if(Enchantment.byId(short1).category== EnchantmentCategory.BOW)
        		{
        			
        		}
        		short short2 = nbttaglist.getCompound(j).getShort("lvl");
                i += short2;
            }
        }
        if(EnchantmentHelper.getItemEnchantmentLevel(Enchantments.INFINITY_ARROWS, it)>0)
        {
        	i /= 2;
        }
		return i;	
	}
	
//	@Override
//	public IIcon getIcon(ItemStack stack, int renderPass, EntityPlayer player, ItemStack usingItem, int useRemaining)
//	{
//		if(useRemaining>0)
//		{
//			int j = this.getMaxItemUseDuration(usingItem) - useRemaining;
//			float f = (float)j / getCount(stack);
//	        f = (f * f + f * 2.0F);
//	        if(f<0.3F)
//	        	return this.itemIcon;
//	       
//	        if(f>3.0F)
//	        	f=3.0F;
//	        f-=1;
//	        if(f<0)
//	        	f=0;
//	        
//			return getItemIconForUseDuration((int)f);
//		}
//		return this.itemIcon;
//	}
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	@Override
	public void appendHoverText(ItemStack it, Level w, 	List<Component> l, TooltipFlag p_77624_4_) 
	{
		l.add(HelperItems.getTooltip(it, this));
		super.appendHoverText(it, w, l, p_77624_4_);
	}
	
	private float getCount(ItemStack it)
	{
		return 20F / (1 + EnchantmentHelper.getItemEnchantmentLevel(Enchantments.POWER_ARROWS, it));
	}
	
	@Override
	public int getUseDuration(ItemStack it)
    {
        return 72000 / (1 + EnchantmentHelper.getItemEnchantmentLevel(Enchantments.POWER_ARROWS, it));
    }
	
//	
//	@Override
//	public ModelResourceLocation getModel(ItemStack stack, EntityPlayer player, int useRemaining)
//	{
//		if(useRemaining==0)
//			return null;
//		
//		int j = this.getMaxItemUseDuration(stack) - useRemaining;
//		float f = (float)j / getCount(stack);
//        f = (f * f + f * 2.0F);
//        if(f<0.3F)
//        	return null;
//       
//        if(f>3.0F)
//        	f=3.0F;
//        f-=1;
//        if(f<0)
//        	f=0;
//		
//        int i = (int) f;
////		if(i==0)
////			return null;
////        
////		i--;
//		
//		ModelResourceLocation model = new ModelResourceLocation(Constants.MOD_ID +":items/laser_bow_pulling_" + i, "inventory");
//		return model;
//	}
}
