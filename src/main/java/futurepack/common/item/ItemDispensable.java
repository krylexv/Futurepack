package futurepack.common.item;

import net.minecraft.core.BlockSource;
import net.minecraft.core.Direction;
import net.minecraft.core.Position;
import net.minecraft.core.dispenser.DispenseItemBehavior;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.DispenserBlock;

public abstract class ItemDispensable extends Item implements DispenseItemBehavior
{

	public ItemDispensable(Properties properties) 
	{
		super(properties);
		DispenserBlock.registerBehavior(this, this);
	}

	@Override
	public ItemStack dispense(BlockSource src, ItemStack it) 
	{
		Direction enumfacing = src.getBlockState().getValue(DispenserBlock.FACING);
		Position pos = DispenserBlock.getDispensePosition(src);
		src.getLevel().levelEvent(1000, src.getPos(), 0);//souns
		int f = enumfacing.getStepX() + 1 + (enumfacing.getStepZ() + 1) * 3;
		src.getLevel().levelEvent(2000, src.getPos(), f);//ParticleTypes
		
		return dispense(src, it, pos, enumfacing);
	}
	
	public abstract ItemStack dispense(BlockSource src, ItemStack it, Position pos, Direction enumfacing);

}
