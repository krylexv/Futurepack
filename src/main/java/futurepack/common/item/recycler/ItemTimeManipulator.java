package futurepack.common.item.recycler;

import java.util.ArrayList;
import java.util.Random;

import futurepack.api.EnumLogisticIO;
import futurepack.api.interfaces.IRecyclerRecipe;
import futurepack.api.interfaces.IRecyclerTool;
import futurepack.api.interfaces.tilentity.ITileRecycler;
import futurepack.common.block.modification.machines.TileEntityRecycler;
import futurepack.common.item.tools.compositearmor.ItemCompositeArmor;
import futurepack.common.modification.EnumChipType;
import futurepack.common.modification.IModificationPart;
import futurepack.common.modification.IModificationPart.EnumCorePowerType;
import futurepack.common.modification.PartsManager;
import futurepack.common.recipes.recycler.FPRecyclerTimeManipulatorManager;
import futurepack.common.recipes.recycler.RecyclerTimeManipulatorRecipe;
import futurepack.depend.api.ItemNBTPredicate;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;

public class ItemTimeManipulator extends Item implements IRecyclerTool 
{

	public ItemTimeManipulator(Item.Properties props)
	{
		super(props);
//		setCreativeTab(FPMain.tab_items);
//		setMaxDamage(8192);
//		setMaxStackSize(1);
	}

	@Override
	public EnumLogisticIO getSupportMode() 
	{
		return EnumLogisticIO.IN;
	}

	@Override
	public boolean craftComplete(ITileRecycler tile, IRecyclerRecipe recipe, ItemStack tool, ItemStack in) 
	{		
		int damage = tool.getDamageValue();
		
		if(damage + 1 >= tool.getMaxDamage())
		{
			tool.shrink(1);
		}
		else
		{
			tool.hurt(1, tile.getTileLevel().random, null);
		}
		
		RecyclerTimeManipulatorRecipe rec = (RecyclerTimeManipulatorRecipe)recipe;
		
		tile.getSupport().use(rec.getSupport(in));
		
		if(rec.isRepairRecipe())
		{
			int dmg = in.getDamageValue();
			int delta = Math.min(dmg,25);
			in.setDamageValue(dmg-delta);
			
			if(in.getItem() instanceof ItemCompositeArmor) {
				in.getOrCreateTag().putBoolean("broken", false);
			}
			
			return in.getDamageValue() == 0;
		}

		return true;
	}

	@Override
	public ArrayList<ItemStack> getOutput(ITileRecycler tile, IRecyclerRecipe recipe, ItemStack tool, ItemStack in, Random random) 
	{
		RecyclerTimeManipulatorRecipe rec = (RecyclerTimeManipulatorRecipe) recipe;
		
		ArrayList<ItemStack> res = new ArrayList<ItemStack>();
		
		if(rec.isRepairRecipe())
		{
			if(in.getDamageValue() <= 25)
			{
				ItemStack neu = in.copy();
				neu.setDamageValue(0);
				res.add(neu);
				return res;
			}
			return res;
		}
		
		res.add((rec.getMaxOutput()[0]).copy());			
		return res;
	}

	@Override
	public IRecyclerRecipe updateRecipe(ITileRecycler tile, ItemStack tool, ItemStack in) 
	{	
		RecyclerTimeManipulatorRecipe rec = FPRecyclerTimeManipulatorManager.instance.getMatchingRecipe(in);
		
		if(rec == null)
		{
			ItemStack in1 = in.copy();
			ItemStack in2 = TileEntityRecycler.getUntoastedItemStack(in1);
			
			if(in1 != in2)
			{
				in1.setCount(1);
				in2.setCount(1);
				
				float value = 0;
				
				IModificationPart part = PartsManager.getPartFromItem(in2);
				
				if(part != null)
				{
				
					if(part.isCore())
					{
						value += part.getCorePower(EnumCorePowerType.PROVIDED) * 2F;
					}
					else if(part.isRam())
					{
						value += part.getRamSpeed() * 2F;
					}
					else if(part.isChip())
					{
						for(EnumChipType chip : EnumChipType.values())
						{
							float v = part.getChipPower(chip) - 1F;
							if(v >= 0)
								value += v * 5.0f + 1.0f;
						}
					}
				
					rec = new RecyclerTimeManipulatorRecipe(new ItemNBTPredicate(in1), in2, (int)value * 2, (int)value * 10);
					
				}
				
			}
		}
		
		return rec;
	}

	@Override
	public void tick(ITileRecycler tile, IRecyclerRecipe recipe, ItemStack tool, ItemStack in) 
	{
	}

	@Override
	public int getMaxProgress(ITileRecycler tile, IRecyclerRecipe recipe, ItemStack tool, ItemStack in)
	{
		return Math.max(2, ((RecyclerTimeManipulatorRecipe)recipe).getMaxProgress());
	}

	@Override
	public boolean canWork(ITileRecycler tile, IRecyclerRecipe recipe, ItemStack tool, ItemStack in, int ticks) 
	{
		return tile.getSupport().get() >= ticks * ((RecyclerTimeManipulatorRecipe)recipe).getSupport(in);
	}
	
}
