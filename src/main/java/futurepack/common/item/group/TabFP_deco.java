package futurepack.common.item.group;

import futurepack.common.block.deco.DecoBlocks;
import net.minecraft.world.item.ItemStack;

public class TabFP_deco extends TabFB_Base
{

	public TabFP_deco(String label) 
	{
		super(label);
	}

	@Override
	public ItemStack makeIcon()
	{
		return new ItemStack(DecoBlocks.color_iron_orange);
	}
}
	
