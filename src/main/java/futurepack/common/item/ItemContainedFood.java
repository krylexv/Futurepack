package futurepack.common.item;

import java.util.List;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class ItemContainedFood extends Item
{
	private final ItemStack remaining;
	private final int use_duration;
	
	public ItemContainedFood(Properties properties, ItemStack remain, int use_duration) 
	{
		super(properties);
		this.remaining = remain;
		this.use_duration = use_duration;
	}

	public ItemContainedFood(Properties properties, ItemStack remain) 
	{
		this(properties, remain, -1);
	}
	
	@Override
	public int getUseDuration(ItemStack stack) 
	{
		if(use_duration == -1)
			return super.getUseDuration(stack);
		
		return use_duration;
	}
	
	@Override
	public ItemStack finishUsingItem(ItemStack stack, Level worldIn, LivingEntity entityLiving)
	{
		ItemStack remains = remaining;
		
		if(remains == null)
			return super.finishUsingItem(stack, worldIn, entityLiving);
	    	
	    	
		super.finishUsingItem(stack.copy(), worldIn, entityLiving);

		if(entityLiving instanceof Player)
		{
			if(!((Player)entityLiving).addItem(remains.copy()))
			{
				if(stack.getCount() <= 1)
				{
					return remains.copy();
	        		}
				else
				{
					((Player)entityLiving).drop(remains.copy(), true);
				}
			}
		}
		stack.shrink(1);
		return stack;
	}
	
	@Override
	public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn)
	{
		tooltip.add(new TranslatableComponent(getDescriptionId() + ".tooltip"));
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
	}
}
