package futurepack.common.item.misc;

import java.util.ArrayList;
import java.util.List;

import futurepack.api.interfaces.IPlanet;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.spaceships.FPPlanetRegistry;
import futurepack.world.dimensions.Dimensions;
import net.minecraft.ChatFormatting;
import net.minecraft.Util;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.RecordItem;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class ItemFpRecord extends RecordItem
{

	protected ItemFpRecord(SoundEvent soundIn, Properties builder) 
	{
		super(15, soundIn, builder);
	}
	
	   public InteractionResultHolder<ItemStack> use(Level worldIn, Player playerIn, InteractionHand handIn) 
	   {
		   if(!worldIn.isClientSide)
		   {
				CustomPlayerData cd = CustomPlayerData.getDataFromPlayer(playerIn);	
				
				ItemStack itDisc = playerIn.getItemInHand(handIn);
					
				ResourceLocation dim = null;
					
				if(itDisc.getItem() == MiscItems.record_menelaus && cd.hasResearch("story.nav.menelaus"))
				{
					playerIn.sendMessage(new TranslatableComponent("research.planet.discover", "Menelaus"), Util.NIL_UUID);				
					dim = Dimensions.MENELAUS_ID;		
				}
				else if(itDisc.getItem() == MiscItems.record_envia && cd.hasResearch("story.nav.envia"))
				{
					playerIn.sendMessage(new TranslatableComponent("research.planet.discover", "Envia"), Util.NIL_UUID);				
					dim = Dimensions.ENVIA_ID;		
				}
				else if(itDisc.getItem() == MiscItems.record_tyros && cd.hasResearch("story.nav.tyros"))
				{
					playerIn.sendMessage(new TranslatableComponent("research.planet.discover", "Tyros"), Util.NIL_UUID);				
					dim = Dimensions.TYROS_ID;		
				}
				else if(itDisc.getItem() == MiscItems.record_lyrara && cd.hasResearch("story.nav.lyrara"))
				{
					playerIn.sendMessage(new TranslatableComponent("research.planet.discover", "Lyrara"), Util.NIL_UUID);				
					dim = Dimensions.LYRARA_ID;		
				}
				else if(itDisc.getItem() == MiscItems.record_entros && cd.hasResearch("story.nav.entros"))
				{
					playerIn.sendMessage(new TranslatableComponent("research.planet.discover", "Entros"), Util.NIL_UUID);				
					dim = Dimensions.ENTROS_ID;		
				}
				else if(itDisc.getItem() == MiscItems.record_unknown && cd.hasResearch("story.nav.asteroid"))
				{
					playerIn.sendMessage(new TranslatableComponent("research.planet.discover", "Asteroid"), Util.NIL_UUID);				
					dim = Dimensions.ASTEROID_BELT_ID;		
				}
				else if(itDisc.getItem() == MiscItems.record_futurepack && cd.hasResearch("story.nav.entros"))
				{
					playerIn.sendMessage(new TranslatableComponent("research.planet.discover", "Mincra"), Util.NIL_UUID);
					
					ItemStack itCoord = FPPlanetRegistry.instance.getItemFromPlanet(FPPlanetRegistry.instance.MINECRAFT.get());
					ItemEntity ie = new ItemEntity(worldIn, playerIn.getX(), playerIn.getY(), playerIn.getZ(), itCoord);
					worldIn.addFreshEntity(ie);
					return InteractionResultHolder.success(itDisc);
				}
				
				if(dim != null)
				{	
					ArrayList<ResourceLocation> disabled = new ArrayList<ResourceLocation>();
					
					disabled.add(Dimensions.ENVIA_ID);
					disabled.add(Dimensions.LYRARA_ID);
					disabled.add(Dimensions.ENTROS_ID);
//					disabled.add(Dimensions.ASTEROID_BELT_ID);
					
					IPlanet pl = FPPlanetRegistry.instance.getPlanetByDimension(dim);
					if(pl == null || disabled.contains(dim))
					{
						playerIn.sendMessage(new TranslatableComponent("research.record.sorry"), Util.NIL_UUID);
					}
					else
					{
						ItemStack itCoord = FPPlanetRegistry.instance.getItemFromPlanet(pl);
						ItemEntity ie = new ItemEntity(worldIn, playerIn.getX(), playerIn.getY(), playerIn.getZ(), itCoord);
						worldIn.addFreshEntity(ie);
					}
				}
				else
				{
					playerIn.sendMessage(new TranslatableComponent("research.record.unresearched"), Util.NIL_UUID);		
				}
				
				
				playerIn.getCooldowns().addCooldown(this, 200);
				
				return InteractionResultHolder.success(itDisc);
		   }
		   
		   return super.use(worldIn, playerIn, handIn);			
	   }
	   
		@Override
		public void appendHoverText(ItemStack stack, Level w, List<Component> tooltip, TooltipFlag advanced)
		{
			super.appendHoverText(stack, w, tooltip, advanced);
			tooltip.add(new TranslatableComponent("research.record.tooltip").setStyle(Style.EMPTY.withColor(ChatFormatting.DARK_RED)));		
		}

}
