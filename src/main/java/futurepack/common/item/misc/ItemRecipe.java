package futurepack.common.item.misc;

import java.util.List;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class ItemRecipe extends Item 
{
//	private IIcon empty;
	
	public ItemRecipe(Item.Properties props) 
	{
		super(props);
		
	}
	
//	@Override
//	public void registerIcons(IIconRegister r) 
//	{
//		empty = r.registerIcon(getIconString() + "_empty");
////		System.out.println(r + " " + r.getClass());
////		System.out.println(empty + " " + empty.getClass());
//		super.registerIcons(r);
//	}
//	
//	@Override
//	public IIcon getIconIndex(ItemStack it) 
//	{
//		if(it.hasTagCompound() && it.getTag().hasKey("recipe"))
//		{
//			return super.getIconIndex(it);
//		}
//		return empty;
//	}
	
	@Override
	public void appendHoverText(ItemStack it, Level w, List<Component> l, TooltipFlag par4) 
	{
		if(it.hasTag() && it.getTag().contains("recipe"))
		{
			CompoundTag nbt = it.getTagElement("recipe");
			l.add(new TranslatableComponent(nbt.getString("output")));
//			l.add("ignore Metadata: " + nbt.getBoolean("ignoremetas"));
		}
		if(it.getTagElement("assemblyRecipe")!=null)
		{
			CompoundTag nbt = it.getTagElement("assemblyRecipe");
			l.add(new TranslatableComponent(nbt.getString("output")));
//			l.add("ignore Metadata: " + nbt.getBoolean("ignoremetas"));
		}
		super.appendHoverText(it, w, l, par4);
	}
}
