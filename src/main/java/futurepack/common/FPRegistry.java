package futurepack.common;


import com.mojang.serialization.Codec;

import futurepack.api.Constants;
import futurepack.common.block.deco.DecoBlocks;
import futurepack.common.block.inventory.InventoryBlocks;
import futurepack.common.block.logistic.LogisticBlocks;
import futurepack.common.block.misc.MiscBlocks;
import futurepack.common.block.modification.ModifiableBlocks;
import futurepack.common.block.multiblock.MultiblockBlocks;
import futurepack.common.block.plants.PlantBlocks;
import futurepack.common.block.terrain.TerrainBlocks;
import futurepack.common.fluids.FPFluids;
import futurepack.common.item.ComputerItems;
import futurepack.common.item.CraftingItems;
import futurepack.common.item.FoodItems;
import futurepack.common.item.ResourceItems;
import futurepack.common.item.SpawnEggItems;
import futurepack.common.item.misc.MiscItems;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.potions.FPPotions;
import futurepack.common.recipes.crafting.FPSerializers;
import futurepack.world.dimensions.ChunkGeneratorAsteroidBelt;
import futurepack.world.dimensions.biomes.FPBiomes;
import futurepack.world.dimensions.biomes.RemappedBiomeProvider;
import futurepack.world.gen.FPFeatures;
import futurepack.world.gen.FPPlacementModifiers;
import net.minecraft.core.Registry;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeSource;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.carver.WorldCarver;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.storage.loot.functions.LootItemFunctionType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.DeferredRegister;

@Mod.EventBusSubscriber(modid = Constants.MOD_ID, bus = Bus.MOD)
public class FPRegistry
{
	public static final DeferredRegister<Codec<? extends ChunkGenerator>> CHUNK_GENERATOR = DeferredRegister.create(Registry.CHUNK_GENERATOR_REGISTRY, Constants.MOD_ID);
	public static final DeferredRegister<Codec<? extends BiomeSource>> BIOME_SOURCE = DeferredRegister.create(Registry.BIOME_SOURCE_REGISTRY, Constants.MOD_ID);


	public static void init(IEventBus bus)
	{
		CHUNK_GENERATOR.register(bus);
		BIOME_SOURCE.register(bus);
		FPPlacementModifiers.init(bus);
		FPLootFunctions.init(bus);


		CHUNK_GENERATOR.register("asteroids", () -> ChunkGeneratorAsteroidBelt.CODEC);

		BIOME_SOURCE.register("remapped", () -> RemappedBiomeProvider.CODEC);

	}

	@SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event)
	{
		DecoBlocks.registerBlocks(event);
		TerrainBlocks.registerBlocks(event);
		ModifiableBlocks.registerBlocks(event);
		InventoryBlocks.registerBlocks(event);
		MiscBlocks.registerBlocks(event);
		PlantBlocks.registerBlocks(event);
		LogisticBlocks.registerBlocks(event);
		MultiblockBlocks.registerBlocks(event);

		FPFluids.registerBlocks(event);

		System.out.println("Registering Blocks");
	}

	@SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event)
	{
		ComputerItems.register(event);
		ToolItems.register(event);
		MiscItems.register(event);
		ResourceItems.register(event);
		CraftingItems.register(event);
		FoodItems.register(event);
		SpawnEggItems.registerItems(event);

		DecoBlocks.registerItems(event);
		TerrainBlocks.registerItems(event);
		ModifiableBlocks.registerItems(event);
		InventoryBlocks.registerItems(event);
		MiscBlocks.registerItems(event);
		PlantBlocks.registerItems(event);
		LogisticBlocks.registerItems(event);
		MultiblockBlocks.registerItems(event);

		FPFluids.registerItems(event);

		System.out.println("Registering Items");
	}

	@SubscribeEvent
    public static void registerBiomes(RegistryEvent.Register<Biome> event)
	{
//		FPBiomes.register(event);

//		FPBiomes.registerSurfaceBuilder(Forgereg);
//		FPBiomes.registerCarvers();
//		FPBiomes.registerFeatures();
	}

//	@SubscribeEvent
//    public static void registerSurfaceBuilder(RegistryEvent.Register<SurfaceBuilder<?>> event)
//	{
//		FPBiomes.registerSurfaceBuilder(event.getRegistry()::register); // called
//	}

	@SubscribeEvent
    public static void registerFeatures(RegistryEvent.Register<Feature<?>> event)
	{
		FPFeatures.registerFeatures(event.getRegistry()::register); //called
	}

	@SubscribeEvent
    public static void registerCavers(RegistryEvent.Register<WorldCarver<?>> event)
	{
		FPBiomes.registerCarvers(event.getRegistry()::register);//this is called
	}

//	@SubscribeEvent
//    public static void registerPlacement(RegistryEvent.Register<PlacementModifierType<?>> event)
//	{
//		FPPlacementModifiers.registerPlacement(null);
//		FPBiomes.registerPlacement(event.getRegistry()::register); //called
//	}

	@SubscribeEvent
    public static void registerPotions(RegistryEvent.Register<MobEffect> event)
	{
		FPPotions.register(event);
	}

	@SubscribeEvent
    public static void registerSounds(RegistryEvent.Register<SoundEvent> event)
	{
		FPSounds.register(event);
	}

//	@SubscribeEvent
//	public static void registerDimension(RegistryEvent.Register<ModDimension> event)
//	{
//		Dimensions.register(event);
//	}

	@SubscribeEvent
	public static void registerTileEntityTypes(RegistryEvent.Register<BlockEntityType<?>> event)
	{
		FPTileEntitys.registerTileEntitys(event);
	}

	@SubscribeEvent
	public static void registerEntityTypes(RegistryEvent.Register<EntityType<?>> event)
	{
		FPEntitys.registerEntitys(event);
	}

	@SubscribeEvent
	public static void registerSerializers(RegistryEvent.Register<RecipeSerializer<?>> event)
	{
		FPSerializers.init(event);
	}

	@SubscribeEvent
	public static void registerFluids(RegistryEvent.Register<Fluid> event)
	{
		FPFluids.register(event);
	}

}
