package futurepack.common.filter;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.Map.Entry;

import javax.script.Bindings;
import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import javax.script.SimpleBindings;

import org.openjdk.nashorn.api.scripting.NashornScriptEngineFactory;
import org.openjdk.nashorn.api.scripting.ScriptObjectMirror;

import automatic.version.Version;
import futurepack.api.helper.HelperTags;
import net.minecraft.nbt.CompoundTag;
import net.minecraftforge.fml.ModList;

public class JSFilterFactory 
{
	private static ScriptEngine JS_engine;
	
	public static ScriptEngine createJSEngine()
	{
		if(JS_engine == null)
		{
			//core mods need a had ref on the nashorn engine as well.
			NashornScriptEngineFactory fac = new NashornScriptEngineFactory();
			JS_engine = fac.getScriptEngine(JSFilterFactory::isClassAllowed);
			Bindings env = getGlobalJSBindings();
			JS_engine.setBindings(env, ScriptContext.GLOBAL_SCOPE);
		}
		return JS_engine;
	}
	
	private static Bindings getGlobalJSBindings()
	{
		SimpleBindings bindings = new SimpleBindings();
		bindings.clear();
		bindings.put("game_version", Version.minecraft);
		bindings.put("futurepack_version", Version.version);
		bindings.put("game_name", "minecraft");
		
		if(ModList.get()!=null)
			bindings.put("modlist", ModList.get().getMods());
		
		bindings.put("helper_tags", HelperTags.INSTANCE);
		
		return bindings;
	}
	
	private static boolean isClassAllowed(String s)
	{
		return false;
	}
	
	public static CompiledScript compile(Reader read) throws ScriptException
	{
		Compilable comp = (Compilable) createJSEngine();
		CompiledScript exe = comp.compile(read);
		return exe;
	}
	
	public static void main(String[] args) 
	{
		InputStreamReader read = new InputStreamReader(JSFilterFactory.class.getResourceAsStream("test.js"));
		CompiledScript script;
		try {
			script = compile(read);
			SimpleBindings bindings = new SimpleBindings();
			bindings.put("item", new ItemImpl(null));
			CompoundTag nbt = new CompoundTag();
			nbt.putInt("burntime", 45);
			bindings.put("nbt", new NBTWrapper(nbt));
			Object o = script.eval(bindings);
			System.out.println("###############################################");
			System.out.println(o);
			printBindings(0, bindings);
			
			Bindings globalVars = (Bindings) bindings.get("nashorn.global");
			if(globalVars.containsKey("filterItem"))
			{
				ScriptObjectMirror scriptObj = (ScriptObjectMirror) globalVars.get("filterItem");
				Object result = scriptObj.call(bindings, new ItemImpl(null));
				System.out.println(result);
			}
			else
			{
				throw new ScriptException("no function 'filterItem' was defined");
			}
		}
		catch (ScriptException e) {
			e.printStackTrace();
		}
	}
	
	private static void printBindings(int i, Bindings b)
	{
		char[] c = new char[i];
		Arrays.fill(c, '\t');
		String s = new String(c);
		for(Entry<String, Object> e : b.entrySet())
		{
			System.out.println(s + e.getKey() +" = "+ e.getValue()+ " , " +e.getValue().getClass());
			if(e.getValue() instanceof Bindings)
			{
				printBindings(i+1, (Bindings) e.getValue());
			}
		}
	}
}
