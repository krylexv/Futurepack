package futurepack.common.commands;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Consumer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mojang.brigadier.Command;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import futurepack.depend.api.helper.HelperHologram;
import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockUpdatePacket;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.FluidState;
import net.minecraftforge.registries.ForgeRegistries;

public class StructurePortingUtil implements Runnable
{
	private static Gson gson = new GsonBuilder().setPrettyPrinting().create();
	
	private static StructurePortingUtil currentProcess;
	
	public static int doOreScan(CommandContext<CommandSourceStack> src) throws CommandSyntaxException
	{
		if(currentProcess!=null)
		{
			if(currentProcess.thread.isAlive())
			{
				src.getSource().sendFailure(new TextComponent("Structure porting already in progress. Killing that"));
				currentProcess.thread.interrupt();
				return 0;
			}
			else
			{
				currentProcess = null;
			}
		}
		File structures = new File(".", "structures");
		structures.mkdirs();
		
		File old = new File(structures, "old");
		old.mkdir();
		File converted = new File(structures, "converted");
		converted.mkdir();
		
		File mappings = new File("./..", "mapping.json").getAbsoluteFile();
		
		currentProcess = new StructurePortingUtil(mappings, old, converted, src.getSource());
		
		//starts thread, will look at "./old" and converts these strructure jsons to "converted" . Will make a mapping.json were all maping from old to new blocks are stored
		return Command.SINGLE_SUCCESS;
	}

	private File mappings, old, converted;
	private Map<String, String> map;
	private InteractivCommandUtil interact;
	
	private boolean isMapDirty = false;
	private final Consumer<Packet<ClientGamePacketListener>> connection;
	
	public final Thread thread;
	
	public StructurePortingUtil(File mappings, File old, File converted, CommandSourceStack interact) 
	{
		super();
		this.mappings = mappings;
		this.old = old;
		this.converted = converted;
		if(interact.getEntity() instanceof ServerPlayer)
		{
			connection = ((ServerPlayer)interact.getEntity()).connection::send;
		}
		else
		{
			connection = null;
		}
		
		thread = new Thread(this, "Structure Porting");
		thread.setDaemon(true);
		thread.start();
		
		this.interact = new InteractivCommandUtil(interact);
	}

	public String getMappedState(String old)
	{
		try
		{
			if(!mappings.exists())
			{
				map = new HashMap<>();
				FileWriter wr = new FileWriter(mappings);
				gson.toJson(map, Map.class, wr);
				wr.close();
			}
			if(map==null)
			{
				FileReader re = new FileReader(mappings);
				map = gson.fromJson(re, Map.class);
				re.close();
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
		String mapping = map.computeIfAbsent(old, this::askForMapping);
		if(isMapDirty)
		{
			try
			{
				FileWriter wr = new FileWriter(mappings);
				gson.toJson(map, Map.class, wr);
				wr.close();
				isMapDirty = false;
			}	
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
		return mapping;
	}
	
	private String askForMapping(String old)
	{
		while(true)
		{
			String[] s = old.split(":");
			if(s.length==3)
			{
				ResourceLocation res = new ResourceLocation(s[0], s[1]);
				int i = Integer.valueOf(s[2]);
				if(i==0)
				{
					Block bl = ForgeRegistries.BLOCKS.getValue(res);
					if(bl!=null && bl!=Blocks.AIR)
					{
						String out = HelperHologram.toStateString(bl.defaultBlockState());
						interact.sendString(old + " is " + out + "(fpdebug yes/no) ?");
						boolean b = interact.waitForCommand(boolean.class);
						if(b)
						{
							isMapDirty = true;
							return out;
						}
					}
				}
			}
			
			interact.sendString("What is " + old + " (use fpdebug structures select)");
			if(s.length==3)
			{
				int i = Integer.valueOf(s[2]);
				String name = DyeColor.byId(i).getName();
				interact.sendString("Meta " + i + " could be color " + name);
				if(i < 6)
				{
					Direction f = Direction.from3DDataValue(i);
					interact.sendString("Meta " + i + " could be facing " + f.getSerializedName());
				}
			}
			BlockPos pos = interact.waitForCommand(BlockPos.class);
			BlockState state = interact.getWorld().getBlockState(pos);
			String out = HelperHologram.toStateString(state);
			interact.sendString(old + " is " + out + "(fpdebug yes/no) ?");
			boolean b = interact.waitForCommand(boolean.class);
			if(b)
			{
				isMapDirty = true;
				return out;
			}
		}
	}
	
	private void tryConverting(File input, File output)
	{
		final String sponge = HelperHologram.toStateString(Blocks.SPONGE.defaultBlockState());
		try
		{
			FileReader read = new FileReader(input);
			JsonObject obj = gson.fromJson(read, JsonObject.class);
			read.close();
			
			int i = 0;
			Map<Integer, String> stateMap = new Int2ObjectArrayMap<>();
			Map<String, Integer> inversStateMap = new TreeMap<>( (s1,s2)->s1.compareTo(s2));
			JsonObject data = obj.getAsJsonObject("data");
			Map<String, Integer> converted = new TreeMap<>();
			
			for(Entry<String, JsonElement> e : data.entrySet())
			{
				String pos = e.getKey();
				String oldState = e.getValue().getAsString();
				Integer newState = inversStateMap.getOrDefault(oldState, null);
				if(newState == null)
				{
					sendBlock(pos, sponge);
					String newStateString = getMappedState(oldState);
					stateMap.put(i, newStateString);
					inversStateMap.put(oldState, i);
					newState = i;
					i++;
				}
				converted.put(pos, newState);
				sendBlock(pos, stateMap.get(newState));
			}
			
			obj.remove("data");
			obj.add("data", gson.toJsonTree(converted));
			obj.add("stateMap", gson.toJsonTree(stateMap));
			
			FileWriter wr = new FileWriter(output);
			String pretty = gson.toJson(obj);
			wr.write(pretty);
			wr.close();
			clearClientBlocks();
			interact.sendString("Converted " + input.getName());
		}
		catch(IOException e)
		{
			interact.sendErrorMessage(new TextComponent(e.toString()));
			e.printStackTrace();
		}
	}
	
	private Set<BlockPos> sendBlocks = new TreeSet<>();
	
	private void sendBlock(BlockPos pos, BlockState state)
	{
		if(connection!=null)
		{
			ClientboundBlockUpdatePacket pkt = new ClientboundBlockUpdatePacket(new BlockGetter() {
				
				@Override
				public BlockEntity getBlockEntity(BlockPos pos) 
				{
					return null;
				}
				
				@Override
				public FluidState getFluidState(BlockPos pos) 
				{
					return null;
				}
				
				@Override
				public BlockState getBlockState(BlockPos pos)
				{
					return state;
				}

				@Override
				public int getHeight() {
					return 0;
				}

				@Override
				public int getMinBuildHeight() {
					return 0;
				}
			}, pos);
			sendBlocks.add(pos);
			connection.accept(pkt);
		}
	}

	private BlockPos origin = null;
	
	private void sendBlock(String relPos, String stateString)
	{
		if(origin==null)
		{
			origin = new BlockPos(interact.getPos()).above(2);
		}
		BlockState state = HelperHologram.fromStateString(stateString);
		String[] parts = relPos.split("-");
		int x=Integer.valueOf(parts[0]);
		int y=Integer.valueOf(parts[1]);
		int z=Integer.valueOf(parts[2]);
		BlockPos pos = origin.offset(x, y, z);
		sendBlock(pos, state);
	}
	
	
	private void clearClientBlocks()
	{
		if(connection!=null)
		{
			sendBlocks.stream().map(p -> new ClientboundBlockUpdatePacket(interact.getWorld(), p)).forEach(connection);
		}
		//stuff
		sendBlocks.clear();
		origin =  null;
	}
	
	
	
	@Override
	public void run() 
	{
		interact.sendString("Begin Convertig");
		for(String oldStructure : old.list())
		{
			if(oldStructure.endsWith(".json"))
			{
				File newStructure = new File(converted, oldStructure);
				if(newStructure.exists())
				{
					interact.sendString("Skipped " + oldStructure + ", already converted.");
					continue;
				}
				else
				{
					tryConverting(new File(old, oldStructure), newStructure);
				}
			}
			if(interact.isInterrupted())
				break;
		}
		interact.sendString("Done Convertig");
		
		currentProcess = null;
	}
	
}
