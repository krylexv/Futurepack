package futurepack.common.commands;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

public class JSONArray2Obj 
{
	
	public static void array2obj()
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		try {
			FileReader read = new FileReader(new File("./__constants_old.json").getAbsoluteFile());
			JsonArray arr = gson.fromJson(read, JsonArray.class);
			JsonObject result = new JsonObject();
			read.close();
			
			for(JsonElement elm : arr)
			{
				JsonObject data = elm.getAsJsonObject();
				String name = data.remove("name").getAsString();
				if(result.has(name))
				{
					throw new IllegalArgumentException("name "  + name + " already exists!");
				}
				else
				{
					result.add(name, data);
				}
			}
			
			FileWriter write = new FileWriter(new File("_constants_fix.json"));
			String out = gson.toJson(result);
			write.append(out);
			write.close();
			
			writeJson(gson, result, new File("."), "_constants_fix.json");
			
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void writeJson(Gson gson, JsonElement result, File folder, String name) throws IOException
	{
		FileWriter write = new FileWriter(new File(folder, name));
		String out = gson.toJson(result);
		write.append(out);
		write.close();
	}
	
	private static void applyConstants(JsonObject constants, JsonElement toChange)
	{
		if(toChange.isJsonObject())
		{
			JsonObject obj = toChange.getAsJsonObject();
			for(Entry<String, JsonElement> e : obj.entrySet())
			{
				JsonElement elm = e.getValue();
				if(elm.isJsonObject())
				{
					applyConstants(constants, elm);
				}
				else if(elm.isJsonArray())
				{
					applyConstants(constants, elm);
				}
			}
			if(obj.has("constant"))
			{
				String con = obj.remove("constant").getAsString().replaceFirst("futurepack:", "");
				JsonObject replace = constants.getAsJsonObject(con);
				
				if(replace == null)
				{
					System.out.println("constant " + con + " is null");
					return;
				}
				
				if(replace.has("item"))
				{
					if(replace.get("item").isJsonObject())
					{
						JsonObject o = replace.getAsJsonObject("item");
						constants.add(con, o);
						replace = o;
					}
				}
				
				replace.entrySet().stream().forEach(e -> obj.add(e.getKey(), e.getValue()));
			}
			
		}
		else if(toChange.isJsonArray())
		{
			for(JsonElement elm : toChange.getAsJsonArray())
			{
				applyConstants(constants, elm);
			}
		}
	}
	
	public static void reimplementConstants(File recipesIn, File recipesOut)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		
		try
		{
			FileReader read = new FileReader(new File(recipesIn, "_constants.json").getAbsoluteFile());
			JsonObject con = gson.fromJson(read, JsonObject.class);
			read.close();
			
			for(File sub : recipesIn.listFiles())
			{
				if(sub.isFile())
				{
					if(sub.getName().startsWith("_"))
						continue;
					
					read = new FileReader(sub);
					JsonObject arr = gson.fromJson(read, JsonObject.class);
					read.close();
					applyConstants(con, arr);
					
					writeJson(gson, arr, recipesOut, sub.getName());
					
				}
			}
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) 
	{
		File recipeIn = new File("E:/CODE/mc1.14.4/futurepack/src/main/resources/data/futurepack/recipes_with_constants");
		File recipeOut = new File("E:/CODE/mc1.14.4/futurepack/src/main/resources/data/futurepack/recipes");
		reimplementConstants(recipeIn, recipeOut);
	}
	
}
