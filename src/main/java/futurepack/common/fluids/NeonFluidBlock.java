package futurepack.common.fluids;

import futurepack.common.potions.FPPotions;
import net.minecraft.core.BlockPos;
import net.minecraft.core.HolderSet;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.material.FluidState;
import net.minecraftforge.fluids.ForgeFlowingFluid;

public abstract class NeonFluidBlock extends ForgeFlowingFluid 
{
	
	protected NeonFluidBlock(Properties properties) 
	{
		super(properties);
	}

	@Override
	public boolean isEntityInside(FluidState state, LevelReader world, BlockPos pos, Entity entity, double yToTest, HolderSet<Fluid> tag, boolean testingHead) 
	{
		boolean result =  super.isEntityInside(state, world, pos, entity, yToTest, tag, testingHead);
		if(result && entity instanceof LivingEntity)
		{
			((LivingEntity)entity).addEffect(new MobEffectInstance(FPPotions.PARALYZED, 60, 1));
		}
		return result;
	}
	
	public static class Flowing extends NeonFluidBlock
    {
        public Flowing(Properties properties)
        {
            super(properties);
            registerDefaultState(getStateDefinition().any().setValue(LEVEL, 7));
        }

        protected void createFluidStateDefinition(StateDefinition.Builder<Fluid, FluidState> builder) {
            super.createFluidStateDefinition(builder);
            builder.add(LEVEL);
        }

        public int getAmount(FluidState state) {
            return state.getValue(LEVEL);
        }

        public boolean isSource(FluidState state) {
            return false;
        }
    }

    public static class Source extends NeonFluidBlock
    {
        public Source(Properties properties)
        {
            super(properties);
        }

        public int getAmount(FluidState state) {
            return 8;
        }

        public boolean isSource(FluidState state) {
            return true;
        }
    }
}
