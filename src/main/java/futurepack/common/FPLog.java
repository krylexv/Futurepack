package futurepack.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import automatic.version.Version;
import futurepack.api.Constants;

public class FPLog 
{
	
	public static final Logger logger;
	static
	{
		SavedMessageFactory fact = null;
		try
		{
			fact = new SavedMessageFactory(new PrintStream(new File("logs/FP-Latest.log")));
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		if(fact!=null)
		{
			logger = LogManager.getLogger(Constants.MOD_ID, fact);
		}
		else
		{
			logger = LogManager.getLogger(Constants.MOD_ID);
		}
		
		
		initLog();
		
	}
	public static void initLog()
	{
		logger.info("Starting %s %s", Constants.MOD_ID, Version.version);
		logger.info("Debug Enabled: %s",logger.isDebugEnabled());
	}
	

	
}
