package futurepack.world.dimensions;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.stream.IntStream;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.LevelHeightAccessor;
import net.minecraft.world.level.levelgen.LegacyRandomSource;
import net.minecraft.world.level.levelgen.WorldgenRandom;
import net.minecraft.world.level.levelgen.synth.PerlinSimplexNoise;

public class AsteroidLayer
{
	public static Codec<AsteroidLayer> CODEC = RecordCodecBuilder.<AsteroidLayer>create((b) -> 
	{
		return b.group(
				Codec.INT_STREAM.fieldOf("shape_noise").forGetter(l -> IntStream.of(l.noiseA)),
				Codec.INT_STREAM.fieldOf("roundness_noise").forGetter(l -> IntStream.of(l.noiseB)),
				Codec.FLOAT.fieldOf("scale").forGetter(l -> l.scale),
				Codec.INT.fieldOf("base_height").forGetter(l -> l.baseHeight),
				Codec.INT.fieldOf("height_variation").forGetter(l -> l.heightVariation),
				Codec.LONG.fieldOf("seed").forGetter(l -> l.seed)
				)
				.apply(b, AsteroidLayer::new);
	   }).stable();
	
	private final int[] noiseA, noiseB;
	private final PerlinSimplexNoise perlinA, perlinB;
	private final float scale;
	private final int baseHeight, heightVariation;
	private final long seed;
	
	public AsteroidLayer(IntStream perlinA, IntStream perlinB, Float scale, Integer baseHeight, Integer heightVariation, Long seed) 
	{
		this(perlinA.toArray(), perlinB.toArray(), scale.floatValue(), baseHeight.intValue(), heightVariation.intValue(), seed.longValue());
	}
	
	public AsteroidLayer(int[] perlinA, int[] perlinB, float scale, int baseHeight, int heightVariation, long seed) 
	{
		noiseA = perlinA;
		noiseB = perlinB;
		
		this.perlinA = new PerlinSimplexNoise(new WorldgenRandom(new LegacyRandomSource(seed ^ 679786098609L)), Arrays.stream(noiseA).boxed().toList());
		this.perlinB = new PerlinSimplexNoise(new WorldgenRandom(new LegacyRandomSource(seed ^ 12345654323456L)), Arrays.stream(noiseB).boxed().toList());
		this.scale = scale;
		this.baseHeight = baseHeight;
		this.heightVariation = heightVariation;
		this.seed = seed;
	}

	void generatePillarAt(int x, int z, BlockPos.MutableBlockPos mut,  Consumer<BlockPos.MutableBlockPos> callback, int min_y, int height)
	{
		int min = min_y + 1;
		int max = height + min_y -6;
		
		double d = perlinB.getValue(x /200D, z /200D, false);
		if(d > 0.1D)
		{
			d -= 0.1;
			d *= 4;
			d = d > 1 ? 1 : d;
			double e = perlinA.getValue(x /(200D/scale), z /(200D/scale), false);
			
			int roundness = (int) (Math.sqrt(e * d * 50) * (5/scale));
			
			double offset = seed * 0.1D;
			double x1 = Math.cos((x/800D)*2*Math.PI + offset) +  Math.cos((x/400D)*2*Math.PI + offset*2)*0.5D + Math.cos((x/1800D)*2*Math.PI + offset*4)*0.75;
			double z1 = Math.sin((z/800D)*2*Math.PI + offset) +  Math.sin((z/400D)*2*Math.PI + offset*2)*0.5D + Math.sin((z/1800D)*2*Math.PI + offset*4)*0.75;
			double xz = (1D + (x1+z1)) / 2D;
			
			int h = (int) (baseHeight+(xz)*heightVariation);
			int lower = h - roundness;
			int upper = h  + roundness;
			lower = lower < min ? min : lower;
			upper = upper > max ? max : upper;
			if(lower < upper)
			{
				for(int y =lower;y < upper;y++)
				{
					callback.accept(mut.set(x, y, z));
				}
			}
		}
	}
	
	public AsteroidLayer withSeed(long seed)
	{
		return new AsteroidLayer(IntStream.of(noiseA), IntStream.of(noiseA), scale, baseHeight, heightVariation, seed);
	}
}
