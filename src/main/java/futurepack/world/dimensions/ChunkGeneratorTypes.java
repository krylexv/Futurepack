//package futurepack.world.dimensions;
//
//import futurepack.api.Constants;
//import futurepack.world.dimensions.envia.ChunkGeneratorEnvia;
//import futurepack.world.dimensions.envia.EnviaSettings;
//import futurepack.world.dimensions.menelaus.ChunkGeneratorMenelaus;
//import futurepack.world.dimensions.menelaus.MenelausSettings;
//import futurepack.world.dimensions.tyros.ChunkGeneratorTyros;
//import futurepack.world.dimensions.tyros.TyrosSettings;
//import net.minecraft.world.gen.ChunkGeneratorType;
//import net.minecraft.world.gen.OverworldGenSettings;
//import net.minecraftforge.common.world.ForgeWorldType.IChunkGeneratorFactory;
//import net.minecraftforge.event.RegistryEvent;
//
//public class ChunkGeneratorTypes
//{
//	//public static final ChunkGeneratorType.Settings<BelowBedrockSettings> BELOW_BEDROCK_SETTINGS;
//	public static final ChunkGeneratorType<OverworldGenSettings, ChunkGeneratorMenelaus> MENLAUS_GEN;
//	public static final ChunkGeneratorType<OverworldGenSettings, ChunkGeneratorTyros> TYROS_GEN;
//	public static final ChunkGeneratorType<OverworldGenSettings, ChunkGeneratorEnvia> ENVIA_GEN;
//	
//	static
//	{
//		
//		IChunkGeneratorFactory<OverworldGenSettings, ChunkGeneratorMenelaus> factoryIn2 = ChunkGeneratorMenelaus::new;
//		MENLAUS_GEN = new ChunkGeneratorType(factoryIn2, true, MenelausSettings::new);
//		MENLAUS_GEN.setRegistryName(Constants.MOD_ID, "menelaus");
//		
//		IChunkGeneratorFactory<OverworldGenSettings, ChunkGeneratorTyros> factoryIn3 = ChunkGeneratorTyros::new;
//		TYROS_GEN = new ChunkGeneratorType(factoryIn3, true, TyrosSettings::new);
//		TYROS_GEN.setRegistryName(Constants.MOD_ID, "tyros");
//		
//		IChunkGeneratorFactory<OverworldGenSettings, ChunkGeneratorEnvia> factoryIn4 = ChunkGeneratorEnvia::new;
//		ENVIA_GEN = new ChunkGeneratorType(factoryIn4, true, EnviaSettings::new);
//		ENVIA_GEN.setRegistryName(Constants.MOD_ID, "envia");
//	}
//	
//	
//	public static void register(RegistryEvent.Register<ChunkGeneratorType<?,?>> event)
//	{
//		event.getRegistry().registerAll(MENLAUS_GEN, TYROS_GEN, ENVIA_GEN);
//	}
//}
