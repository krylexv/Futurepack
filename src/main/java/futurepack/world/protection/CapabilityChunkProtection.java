package futurepack.world.protection;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import it.unimi.dsi.fastutil.ints.Int2ByteOpenHashMap;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.ByteArrayTag;
import net.minecraftforge.common.util.INBTSerializable;

public class CapabilityChunkProtection implements INBTSerializable<ByteArrayTag>, IChunkProtection
{		

	Map<Integer, Byte> map;
	
	
	public CapabilityChunkProtection()
	{
		map = new Int2ByteOpenHashMap();
	}
	
	public void addProtection(Map<Integer, Byte> queed)
	{
		if(queed!=null)
		{	
			map.putAll(queed);
		}	
	}

	@Deprecated
	@Override
	public ByteArrayTag serializeNBT()
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream();		
		try
		{
			GZIPOutputStream zip = new GZIPOutputStream(out);
			DataOutputStream data = new DataOutputStream(zip);
			
			map.entrySet().removeIf(e -> {return e.getValue()==null || e.getValue()==0;}); //removing 'wrong' states
			
			data.writeInt(map.entrySet().size());
			 
			for(Entry<Integer, Byte> e : map.entrySet())
			{
				data.writeInt(e.getKey());
				data.writeByte(e.getValue());
			}
			
			data.close();
		}
		catch (IOException e)
		{		
			e.printStackTrace();
		}		
		return new ByteArrayTag(out.toByteArray());
	}

	@Deprecated
	@Override
	public void deserializeNBT(ByteArrayTag nbt)
	{
		ByteArrayInputStream in = new ByteArrayInputStream(nbt.getAsByteArray());
		try
		{
			GZIPInputStream zip = new GZIPInputStream(in);
			DataInputStream data = new DataInputStream(zip);
			
			int s=data.readInt();
			for(int i=0;i<s;i++)
			{
				int pos = data.readInt();
				byte state = data.readByte();
				if(state!=0)
					map.put(pos, state);
			}
			data.close();
		}
		catch (IOException e)
		{		
			e.printStackTrace();
		}	
	}				
	
	public void setState(int pos, byte state)
	{
		if(state==0)
			map.remove(pos);
		else
			map.put(pos, state);
	}
	
	public byte getState(int pos)
	{
		return map.getOrDefault(pos, (byte) 0);
	}
	
	public boolean hasProtection()
	{
		for(Byte b : map.values())
		{
			if(b!=null && b!=0)
				return true;
		};
		return false;
	}

	@Override
	public byte getRawProtectionState(BlockPos pos)
	{
		return getState(IChunkProtection.getPosition(pos));
	}

	@Override
	public void setRawProtectionState(BlockPos pos, byte state)
	{
		setState(IChunkProtection.getPosition(pos), state);
	}

	@Override
	public boolean hasChunkProtection()
	{
		return hasProtection();
	}
}
