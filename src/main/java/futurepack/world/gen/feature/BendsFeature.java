package futurepack.world.gen.feature;

import java.util.Random;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.DoubleUnaryOperator;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.google.common.collect.AbstractIterator;
import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.util.Mth;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.Heightmap.Types;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;

public class BendsFeature extends Feature<BendsFeatureConfig>
{

	public BendsFeature(Codec<BendsFeatureConfig> configFactoryIn) 
	{
		super(configFactoryIn);
	}

	private static Stream<BendInstance> getCurrentBends(BendsFeatureConfig config, Random rand, BlockPos pos, final long seed)
	{
		ChunkPos base = new ChunkPos(pos);
		int x = base.x;
		int z = base.z;
		
		int dw = config.getMaxWidthInChunk();
		
		Stream<ChunkPos> stream = StreamSupport.stream(Spliterators.spliteratorUnknownSize(new AbstractIterator<ChunkPos>()
		{
			private int dx = -dw;
			private int dz = -dw;
			
			@Override
			protected ChunkPos computeNext() 
			{
				ChunkPos pos = new ChunkPos(x+dx, z+dz);
				if(dx <= dw)
				{
					dx++;
				}
				else
				{
					if(dz <= dw)
					{
						dz++;
						dx = -dw;
					}
					else
					{
						endOfData();
					}
				}
				return pos;
			}
			
		}, Spliterator.ORDERED), false);
		Stream<BendInstance> bends = stream.map(ch -> getBend(config, ch, seed)).filter(t -> t != null).filter(b -> b.canGenerateIn(base));
		return bends;
	}
	

	private static BendInstance getBend(BendsFeatureConfig config, ChunkPos pos, long seed)
	{
		long bendSeed = seed ^ pos.toLong();
		Random r = new Random(bendSeed);
		
		if(r.nextFloat() < config.chance_per_chunk)
		{
			float a = config.min_a + r.nextFloat() * (config.max_a - config.min_a);
			float height = config.max_height + r.nextFloat() * (config.max_height - config.min_height);
			float thickness = config.min_thickness + r.nextFloat() * (config.max_thickness - config.min_thickness);
			
			int x = r.nextInt(16);
			int z = r.nextInt(16);
			int y = 60;
			
			BendInstance bend = new BendInstance(a, height, new BlockPos(pos.x*16 + x, y, pos.z*16 + z), (float) (r.nextFloat() * 2 * Math.PI), thickness);
			return bend;
		}
		else
		{
			return null;
		}
		
	}
	
	public static class BendInstance
	{		
		private float thickness, anstieg;
		private DoubleUnaryOperator baseLine, parabel;
		
		private final BlockPos start;
		
		private boolean XPosi, ZPosi;
		private final int chunkX, chunkZ;
		
		public BendInstance(float a, float height, BlockPos start, float degree, float thickness) 
		{
			//y = -a(x-c)^2 + b; a,b input; c = 
			// 0 = y, b = ax^2; b/a = x^2; x = sqrt(b/a) = c
			
			this.start = start;
			float offset = (float) Math.sqrt(height/a);
			float width = 2*offset;
			
			double x = Math.cos(degree);
			double z = Math.sin(degree);
			XPosi = x > 0;
			ZPosi = z > 0;
			
			chunkX = start.getX() >> 4;
			chunkZ = start.getZ() >> 4;
			
			this.thickness = thickness;
			
			double anstieg = z/x;
			this.anstieg = (float) anstieg;
	
			baseLine = d -> (d - start.getX()) * anstieg + start.getZ();
			
			parabel = d -> -a * (d-offset) * (d-offset) + height;
		}
		
		public boolean canGenerateIn(ChunkPos base) 
		{
			if(base.x == chunkX && base.z == chunkZ)
				return true;
			
			int dx = base.x - chunkX;
			int dz = base.z - chunkZ;
			
			if((dx>0) != XPosi || (dz>0) !=ZPosi)
				return false;
			
			int x = base.x * 16;
			int z = base.z * 16;
			
			int z0 = Mth.floor(baseLine.applyAsDouble(x));
			int z1 = Mth.floor(baseLine.applyAsDouble(x+16));
			
			if(z <= z0 && z0 <= z+16)
				return true;
			if(z <= z1 && z1 <= z+16)
				return true;
			if(z0 < z && z1 > z+16)
				return true;
			
			return false;
		}
		
		public void placeBlocks(LevelAccessor worldIn, BlockPos pos, BendsFeatureConfig config)
		{
			BlockPos.MutableBlockPos mut = new BlockPos.MutableBlockPos();
			mut.set(pos);
			
			int last_z = Mth.floor(baseLine.applyAsDouble(pos.getX()-1));
			int last_y = -1;
			for(int x=pos.getX();x<pos.getX()+16;x++)
			{
				mut.setX(x);
				int z0 = Mth.floor(baseLine.applyAsDouble(x));
				
				int dzz = last_z < z0 ? 1 : -1;
				for(int z = last_z; z==z0 || (dzz>0 ? z<z0 : z>z0); z+=dzz)//DIESE ZEILE
				{
//				int z = last_z;
					if( z >= pos.getZ() && z <= pos.getZ() + 15) 
					{
							
						float dx = start.getX() -x;
						float dz = start.getZ() -z;
						double dis = Math.sqrt(dx*dx + dz*dz);
						double y = parabel.applyAsDouble(dis) + start.getY();
							
							
						mut.setZ(z);
						int height = worldIn.getHeight(Types.OCEAN_FLOOR_WG, mut.getX(), mut.getZ());
							
						if(y>= height)
						{
							mut.setY(Mth.floor(y));
							if(last_y!=-1 && Math.abs(mut.getY() - last_y) > 1)
							{
								int yy = Mth.floor(y);
								mut.setY(last_y);
								int dy = yy - last_y;
								while(mut.getY() != yy)
								{
									mut.move(0, dy>0?1:-1, 0);
									placeBendPart(worldIn, mut, config);
								}
							}
							else
							{
								placeBendPart(worldIn, mut, config);
							}
								
								
							last_y = mut.getY();
						}
					}
				}
				last_z = z0;
			}
		}
		
		public void placeBendPart(LevelAccessor worldIn, BlockPos.MutableBlockPos pos, BendsFeatureConfig config)
		{
//			worldIn.setBlockState(pos, config.fillerblock, 3);
			
			double r = thickness /2D;
			double r3 = r*r;
			
			for(int x=0;x<thickness+1;x++)
			{
				for(int z=0;z<thickness+1;z++)
				{
					BlockState state = config.topblock;
					for(int y=(int) (thickness);y>=0;y--)
					{
						double dis = (x-r)*(x-r) + (y-r)*(y-r) + (z-r)*(z-r);
						if(dis <= r3)
						{
							worldIn.setBlock(pos.offset(x, y, z), state, 3);
							state = config.fillerblock;
						}
					}
				}
			}
		}
	}

	@Override
	public boolean place(FeaturePlaceContext<BendsFeatureConfig> pContext) 
	{
		Stream<BendInstance> s = getCurrentBends(pContext.config(), pContext.random(), pContext.origin(), pContext.level().getSeed());
		s.forEach( b -> b.placeBlocks(pContext.level(), pContext.origin(), pContext.config()));
		
		return true;
	}
}
