package futurepack.world.gen.feature;

import java.util.Random;

import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.LevelSimulatedRW;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;
import net.minecraft.world.level.material.Material;

public class TyrosDeadTreeFeature extends Feature<NoneFeatureConfiguration>
{
	
	public static BlockState colors[] = {Blocks.LIGHT_GRAY_TERRACOTTA.defaultBlockState(), Blocks.CYAN_TERRACOTTA.defaultBlockState(), Blocks.BLUE_TERRACOTTA.defaultBlockState(), Blocks.LIGHT_BLUE_TERRACOTTA.defaultBlockState()};
	
	public TyrosDeadTreeFeature(Codec<NoneFeatureConfiguration> serialiser)
	{
		super(serialiser);
	}
	
	private boolean canStand(LevelSimulatedRW w, BlockPos pos)
	{
		if(isGround(w, pos.below()))
		{
			if(isGround(w, pos.offset(-2,-1,-2)) && isGround(w, pos.offset(2,-1,2)))
			{
				if(isGround(w, pos.offset(2,-1,-2)) && isGround(w, pos.offset(-2,-1,2)))
				{
					return true;
				}
			}
		}
		
		return false;
	}
	
	private boolean isGround(LevelSimulatedRW w, BlockPos pos)
	{
		return w.isStateAtPosition(pos, s -> s.getMaterial() == Material.STONE);
	}
	
	/**
	 * generates the Tree stem
	 * @return the high of the Tree
	 */
	private void genTreeStemm(LevelSimulatedRW w, BlockPos pos, int h, Random rand)
	{	
		BlockPos.MutableBlockPos xyz = new BlockPos.MutableBlockPos();
		for(int i=0;i<h;i++)
		{
			double r = (1.0 - i/(double)h)*1.5 +1.5;
			for(int x=(int) -r;x<r;x++)
			{
				for(int z=(int) -r;z<r;z++)
				{
					if(x*x + z*z < r*r)
					{
						xyz.set(pos).move(x, i, z);				
						//w.setBlockState(xyz, colors[Math.min(Math.max(Math.abs(x + (rand.nextInt(2) - 1)), Math.abs(z + (rand.nextInt(2) - 1))), colors.length - 1)]);
						//w.setBlockState(xyz, colors[(int) Math.min(Math.max(Math.sqrt(x*x+z*z) + 0.5, 0), colors.length - 1)]);
						BlockState state = colors[(int) Math.min(Math.max(Math.sqrt(x*x+z*z) + rand.nextFloat(), 0), colors.length - 1)];
						this.setBlock(w, xyz, state);
					}
				}
			}
		}
		
		final BlockState ore = Blocks.IRON_ORE.defaultBlockState();
		
		for(int i = -3; i < 0; i++)
		{
			double r = (1.0 - i/(double)3)*1.5 +1.5;
			for(int x=(int) -r;x<r;x++)
			{
				for(int z=(int) -r;z<r;z++)
				{
					if(x*x + z*z < r*r)
					{
						xyz.set(pos).move(x, i, z);		
						if(rand.nextInt(8) <= 4)
							this.setBlock(w, xyz, ore);
						
					}
				}
			}
		} 
	}

	public boolean place(WorldGenLevel w, ChunkGenerator generator, Random rand, BlockPos pos, NoneFeatureConfiguration config) 
	{
		if(!canStand(w, pos))
			return false;
		
		int height = 10  + rand.nextInt(20);
		
		genTreeStemm(w, pos, height, rand);
		
		return true;
	}

	@Override
	public boolean place(FeaturePlaceContext<NoneFeatureConfiguration> pContext) 
	{
		return place(pContext.level(), pContext.chunkGenerator(), pContext.random(), pContext.origin(), pContext.config());
	}

}
