package futurepack.world.gen;

import java.util.function.Consumer;

import com.mojang.serialization.Codec;

import futurepack.api.Constants;
import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.biome.BiomeSource;
import net.minecraft.world.level.levelgen.placement.PlacementModifier;
import net.minecraft.world.level.levelgen.placement.PlacementModifierType;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.RegistryObject;

public class FPPlacementModifiers 
{
	
	public static final DeferredRegister<PlacementModifierType<?>> PLACEMENT_MODIFIER = DeferredRegister.create(Registry.PLACEMENT_MODIFIER_REGISTRY, Constants.MOD_ID);
	

	public static RegistryObject<PlacementModifierType<DungeonEntrancePlacement>> DUNGEON_ENTRANE = PLACEMENT_MODIFIER.register("dungeon_entrance", () -> () -> DungeonEntrancePlacement.CODEC);
	public static RegistryObject<PlacementModifierType<GroupPlacementAtSurface>> GROUP_AT_SURFACE = PLACEMENT_MODIFIER.register("group_at_surface", () -> () -> GroupPlacementAtSurface.CODEC);

	public static void init(IEventBus bus)
	{
		PLACEMENT_MODIFIER.register(bus);
	}	
	
}
