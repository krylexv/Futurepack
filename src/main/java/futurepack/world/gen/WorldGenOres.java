package futurepack.world.gen;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import futurepack.api.Constants;
import futurepack.common.FPConfig;
import futurepack.common.FuturepackTags;
import futurepack.common.WorldGenRegistry;
import futurepack.common.block.terrain.TerrainBlocks;
import net.minecraft.core.Holder;
import net.minecraft.core.Registry;
import net.minecraft.data.BuiltinRegistries;
import net.minecraft.data.worldgen.features.FeatureUtils;
import net.minecraft.data.worldgen.features.OreFeatures;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.VerticalAnchor;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.configurations.OreConfiguration;
import net.minecraft.world.level.levelgen.placement.BiomeFilter;
import net.minecraft.world.level.levelgen.placement.CountPlacement;
import net.minecraft.world.level.levelgen.placement.HeightRangePlacement;
import net.minecraft.world.level.levelgen.placement.InSquarePlacement;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.structure.templatesystem.RuleTest;
import net.minecraft.world.level.levelgen.structure.templatesystem.TagMatchTest;
import net.minecraftforge.event.world.BiomeLoadingEvent;

public class WorldGenOres
{
	public static final int SIZE_DIAMOND = 8;
	public static final int SIZE_IRON = 9;
	
	public static final int SIZE_DAS_IST_JA_NUR_EXTRA = 6; //Zitaat: WUgand, 21:23Uhr 23.01.2019. Man muss ja f�r Zitate die genau urhzeit nenenenenenenenenenenenenenen. 
	public static final int SIZE_QUARTZ_KLUMPEN = 12; //ZItat WUgand, zeit sihe oben
	
	public static final RuleTest IS_ROCK_MENELAUS = new TagMatchTest(FuturepackTags.stone_menelaus);
	
	public static Holder<PlacedFeature> tin, copper, zinc, bauxite, magnetite, copper_m, quartz_m, coal_m;
	
	public static void register()
	{
//		Registry<ConfiguredFeature<?, ?>> registry = BuiltinRegistries.CONFIGURED_FEATURE;
		
		tin = addOre(new ResourceLocation(Constants.MOD_ID, "ore_tin"), TerrainBlocks.ore_tin.defaultBlockState(), TerrainBlocks.ore_tin_deepslate.defaultBlockState(), SIZE_IRON, FPConfig.WORLDGEN_ORES.tinOre.get(), 0, 64);//
		copper = addOre(new ResourceLocation(Constants.MOD_ID, "ore_copper"), TerrainBlocks.ore_copper.defaultBlockState(), SIZE_IRON, FPConfig.WORLDGEN_ORES.copperOre.get(), 0, 64);//
		zinc = addOre(new ResourceLocation(Constants.MOD_ID, "ore_zinc"), TerrainBlocks.ore_zinc.defaultBlockState(), TerrainBlocks.ore_zinc_deepslate.defaultBlockState(), SIZE_IRON, FPConfig.WORLDGEN_ORES.zincOre.get(), 0, 64);//
	
		bauxite = addOre( new ResourceLocation(Constants.MOD_ID, "ore_magnetite"), TerrainBlocks.ore_bauxite.defaultBlockState(), TerrainBlocks.ore_bauxite_deepslate.defaultBlockState(), SIZE_DIAMOND, FPConfig.WORLDGEN_ORES.bauxiteOre.get(), 0, 64);
		magnetite = addOre(new ResourceLocation(Constants.MOD_ID, "ore_bauxite"), TerrainBlocks.ore_magnetite.defaultBlockState(), TerrainBlocks.ore_magnetite_deepslate.defaultBlockState(), SIZE_DIAMOND, FPConfig.WORLDGEN_ORES.magnetiteOre.get(), 0, 64);
		
		copper_m = addOreM(new ResourceLocation(Constants.MOD_ID, "ore_copper_m"), TerrainBlocks.ore_copper_m.defaultBlockState(), SIZE_DAS_IST_JA_NUR_EXTRA, FPConfig.WORLDGEN_ORES.copperOreM.get(), 50, 50+64);//
		quartz_m = addOreM(new ResourceLocation(Constants.MOD_ID, "ore_quartz_m"), TerrainBlocks.ore_quartz_m.defaultBlockState(), SIZE_QUARTZ_KLUMPEN, FPConfig.WORLDGEN_ORES.quartzOreM.get(), 50, 50+64);//in tags
		coal_m = addOreM(new ResourceLocation(Constants.MOD_ID, "ore_coal_m"), TerrainBlocks.ore_coal_m.defaultBlockState(), SIZE_DIAMOND, FPConfig.WORLDGEN_ORES.coalOreM.get(), 50, 50+64);//
		
	}
	
	public static void init(ArrayList<Consumer<BiomeLoadingEvent>> list)
	{
		list.add(WorldGenOres::registerOres);
	}
	
	public static void registerOres(BiomeLoadingEvent b)
	{
		WorldGenRegistry.addFeature(b, GenerationStep.Decoration.UNDERGROUND_ORES, () -> tin);
		WorldGenRegistry.addFeature(b, GenerationStep.Decoration.UNDERGROUND_ORES, () -> copper);
		WorldGenRegistry.addFeature(b, GenerationStep.Decoration.UNDERGROUND_ORES, () -> zinc);
		
		WorldGenRegistry.addFeature(b, GenerationStep.Decoration.UNDERGROUND_ORES, () -> bauxite);
		WorldGenRegistry.addFeature(b, GenerationStep.Decoration.UNDERGROUND_ORES, () -> magnetite);
		
		WorldGenRegistry.addFeature(b, GenerationStep.Decoration.UNDERGROUND_ORES, ()-> copper_m);
		WorldGenRegistry.addFeature(b, GenerationStep.Decoration.UNDERGROUND_ORES, ()-> quartz_m);
		WorldGenRegistry.addFeature(b, GenerationStep.Decoration.UNDERGROUND_ORES, ()-> coal_m);
	}
	
	public static Holder<ConfiguredFeature<OreConfiguration, ?>> createConfiguredOreFeature(String name, RuleTest canPlace, BlockState state, int cluster_size)
	{
		return FeatureUtils.register(name, Feature.ORE, new OreConfiguration(canPlace, state, cluster_size));
	}
	
	public static Holder<ConfiguredFeature<OreConfiguration, ?>> createConfiguredOreFeature(String name, List<OreConfiguration.TargetBlockState> stateList, int cluster_size)
	{
		return FeatureUtils.register(name, Feature.ORE, new OreConfiguration(stateList, cluster_size));
	}
	
	public static Holder<PlacedFeature> createPlacedOreFeature(String name, Holder<ConfiguredFeature<OreConfiguration, ?>> ore, int count_per_chunk, int min_height, int max_height)
	{
		return PlacementUtils.register(name, ore, List.of(
				CountPlacement.of(count_per_chunk),
				InSquarePlacement.spread(),
				HeightRangePlacement.uniform(VerticalAnchor.aboveBottom(min_height), VerticalAnchor.absolute(max_height)),
				BiomeFilter.biome()));
	}

	
	private static Holder<PlacedFeature> addOre(ResourceLocation oreName, BlockState state, BlockState state_deepslate, int cluster_size, int count_per_chunk, int min_height, int max_height)
	{
		return createPlacedOreFeature(oreName.toString()+"_middle", createConfiguredOreFeature(oreName.toString(), List.of(OreConfiguration.target(OreFeatures.STONE_ORE_REPLACEABLES, state), OreConfiguration.target(OreFeatures.DEEPSLATE_ORE_REPLACEABLES, state_deepslate)), cluster_size), count_per_chunk, min_height, max_height);
	}
	
	private static Holder<PlacedFeature> addOre(ResourceLocation oreName, BlockState state, int cluster_size, int count_per_chunk, int min_height, int max_height)
	{
		return createPlacedOreFeature(oreName.toString()+"_middle", createConfiguredOreFeature(oreName.toString(), OreFeatures.STONE_ORE_REPLACEABLES, state, cluster_size), count_per_chunk, min_height, max_height);
	}
	
	private static Holder<PlacedFeature> addOreM(ResourceLocation oreName, BlockState state, int cluster_size, int count_per_chunk, int min_height, int max_height)
	{
		return createPlacedOreFeature(oreName.toString()+"_middle", createConfiguredOreFeature(oreName.toString(), IS_ROCK_MENELAUS, state, cluster_size), count_per_chunk, min_height, max_height);
		
	}
}
