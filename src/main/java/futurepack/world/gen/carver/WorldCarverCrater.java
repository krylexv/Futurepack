package futurepack.world.gen.carver;

import java.util.BitSet;
import java.util.List;
import java.util.Map;
import java.util.Random;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.CarvingMask;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.levelgen.LegacyRandomSource;
import net.minecraft.world.level.levelgen.carver.CaveCarverConfiguration;
import net.minecraft.world.level.levelgen.synth.SimplexNoise;

public class WorldCarverCrater extends WorldCaverMultiChunkStructures<CaveCarverConfiguration, WorldCarverCrater.Crater>
{
	public final int minRadius;
	
	public static final BlockState AIR = Blocks.AIR.defaultBlockState();
	public static final BlockState STONE = Blocks.STONE.defaultBlockState();
	
	public WorldCarverCrater(int maxRadius, int minRadius)
	{
		super(CaveCarverConfiguration.CODEC);
		this.maxRadius = maxRadius;
		this.minRadius = minRadius;
		
		chancePerChunk = 1F / (maxRadius);
		
		minY = 85;
		maxY = 105;
	}
	
	/**
	 * Simple quadratic gradient, defined from 0 to radius
	 * Used as the large base crater
	 * @param x
	 * @param r the max radius
	 * @param h the max height
	 * @return the function value, 0 to h
	 */
	public static double craterBase(double x, double r, double h)
	{
		double a = -2.0 * (h-r) / (r*r*r);
		double b = 1.0/r + 3.0*(h-r)/(r*r);
		return a * x*x*x + b*x*x;
	}
	
	/**
	 * Smooth gradient, defined from 0 to radius
	 * Used for the inner small Mountain
	 * 
	 * @param x
	 * @param r the max radius
	 * @param h the max height
	 * @return the function value, h to 0
	 */
	public static double innerMountain(double x, double r, double h)
	{
		return h * (1.0 + Math.cos(Math.PI / r * x));
	}
	
	/**
	 * Inverse gradient, used for the crater border
	 * 
	 * @param x
	 * @param r the radius
	 * @param h the max height
	 * @return the function value, h to 0
	 */
	public static double outerCrater(double x, double r, double h)
	{
		double a = (r-2.0*h)/(r*r*r);
		double b = 1.0/(2.0*r) - 3.0/2.0 * ( (r-2.0*h)/(r*r) );
		double c = -1;
		double d = h;
		return a*x*x*x + b * x*x + c*x + d;
	}
	
	/**
	 * 
	 * @param x
	 * @param r_inner radius of the inner little mountain
	 * @param borderLength
	 * @param craterRadius
	 * @param h_inner height of the inner little mountain
	 * @param borderHeight
	 * @param craterDepth
	 * @return
	 */
	public static double craterHeight(double x, double r_inner, double borderLength, double craterRadius, double h_inner, double borderHeight, double craterDepth)
	{
		if(x<0)
			throw new IllegalArgumentException("x >= 0 !");
		
		double y = 0;

		if(x < craterRadius)
		{
			y = craterBase(x, craterRadius, craterDepth) - craterDepth + borderHeight;
		}	
		else if(x - craterRadius < borderLength)
		{
			y = outerCrater(x - craterRadius, borderLength, borderHeight);
		}
		else
			return 0;
		
		if(x < r_inner)
			y += innerMountain(x, r_inner, h_inner);
		
		return y;
	}

//	private static int getMaskPos(int x, int y, int z)
//	{
//		x &= 15;
//		z &= 15;
//		return x | z << 4 | y << 8;
//	}
	
	@Override
	protected boolean recursiveGenerate(ChunkAccess w, int chunkX, int chunkZ, Map<Integer, Crater> map, CarvingMask mask, CaveCarverConfiguration config)
	{
		int r = maxRadius / 16 + 2;
		List<Crater> list = gatherStructures(w, chunkX, chunkZ, r, 12L, map);//TODO: somehow get world seed and replace the 12L
		if(list.isEmpty())
			return false;
		
		BlockPos.MutableBlockPos pos = new BlockPos.MutableBlockPos();
		
		for(int x=0;x<16;x++)
		{
			for(int z=0;z<16;z++)
			{
				Double min = null;
				
				pos.set(chunkX*16 +x, 0, chunkZ*16 + z);
				
				EnumChange change = EnumChange.NOTHING;
				for(Crater c : list)
				{
					double h = c.getHeight(pos);
					if(c.change == EnumChange.NOTHING)
						continue;
					change = change.combine(c.change);
					min = change.mix(min, h);					
				}
				if(min==null)
					continue;
				
				if(min < 2)
					min = 2D;
				
				pos.setY(min.intValue());
				
				if(!mask.get(x, min.intValue(), z))
				{
					if(w.getBlockState(pos) == AIR && change.up)
					{
						for(int y=min.intValue();y>0;y--)
						{
							pos.setY(y);
							if(w.getBlockState(pos) != AIR)
								break;
							
							mask.set(x, y, z);
							w.setBlockState(pos, STONE, false);
						}
					}
					else if(change.down)
					{
						for(int y=min.intValue();y<255;y++)
						{
							pos.setY(y);
							if(w.getBlockState(pos) == AIR)
								break;
							
							mask.set(x,y, z);
							w.setBlockState(pos, AIR, false);
						}
					}
				}
			}
		}
		return true;
	}
	
	@Override
	public Crater getStructureBase(BlockGetter w, int chunkX, int chunkZ, long worldSeed, Map<Integer, Crater> map, Random r)
	{
		int x = r.nextInt(16);
		int z = r.nextInt(16);
		int y = minY + r.nextInt(maxY-minY);
		BlockPos pos = new BlockPos(chunkX * 16 + x, y, chunkZ*16 + z);
		//problem: getHeight functioniert nich wenn wenn chunk nicht generiert ist.
		
		int radius = r.nextInt(maxRadius-minRadius)+minRadius;
		int height = radius/3 + r.nextInt(radius/3);
		Crater c = new Crater(worldSeed, pos, radius, height);
		
		c.innerHeightFactor  = jitter(r, 0F, 1/15F);
		c.innerRadiusFactor  = jitter(r, 3F, 8F);
		c.craterBorderRadius = jitter(r, 0.5F, 0.9F);
		c.borderHeightFactor *= 1F + jitter(r, -0.1F, 0.1F);
		c.roundnessFactor = 1 + r.nextInt(4);
		return c;
	}

	
	protected class Crater
	{
		private BlockPos middle;
		private int radius;
		private int height;
		SimplexNoise simplex;
		
		public double innerHeightFactor = 1D/12D;
		public double innerRadiusFactor = 6D;
		
		public double craterBorderRadius = 5D/6D;
		
		public double borderHeightFactor = 5D;
		public int roundnessFactor = 6;
		
		
		public EnumChange change;
		
		public Crater(long seed, BlockPos middle, int radius, int height)
		{
			super();
			this.middle = middle;
			this.radius = radius;
			this.height = height;
			simplex = new SimplexNoise(new LegacyRandomSource(seed ^ middle.hashCode()));
		}

		public double getHeight(BlockPos pos)
		{
			double dis = dis(pos);
			
			change = EnumChange.NOTHING;
			if(dis > radius)
				return 0;
			int dx = pos.getX() - middle.getX();
			int dz = pos.getZ() - middle.getZ();
			double angle = Math.atan2(dx, dz);
			
			double s1,s2;
			synchronized (this)
			{
				s1 = simplex.getValue(pos.getX(), pos.getZ());
				s2 = simplex.getValue(pos.getZ(), pos.getX());
				s1 += 0.1 * radius * Math.sin(roundnessFactor * angle) * simplex.getValue(0, angle);
			}
			
			//s1 = s1 * 0.1 * radius;
			//s2 = s2 * 0.05 * height;

			double h = craterHeight(dis, radius + s1, height + s2);
			h += middle.getY();
			
//			double h1 = rand.nextDouble() - 0.5;
//			h1 = 0.1 * height;
			//h += h1;
			
			if(h<0)
				h=0;
			
			return h;
		}
		
		private double dis(BlockPos p1)
		{
			int x = p1.getX() - middle.getX();
			int z = p1.getZ() - middle.getZ();
			return Math.sqrt(x*x + z*z);
		}
		
		public double craterHeight(double x, double r, double h)
		{
			double h_inner = h * innerHeightFactor;
			double r_inner = r / innerRadiusFactor;
			double borderLength = r * (1D - craterBorderRadius);
			double craterRadius = r * craterBorderRadius;
			double craterDepth = h;
			double borderHeight = h / borderHeightFactor;
			
			if(x < craterDepth/2)
				change = EnumChange.ONLY_DOWN;
			else if(x < craterRadius)
				change = EnumChange.BOTH;
			else if(x - craterRadius < borderLength)
			{
				change = EnumChange.ONLY_UP;
			}
			
			return WorldCarverCrater.craterHeight(x, r_inner, borderLength, craterRadius, h_inner, borderHeight, craterDepth);
		}
	}
	
	private static enum EnumChange
	{
		NOTHING(false, false),
		ONLY_UP(true, false),
		ONLY_DOWN(false, true),
		BOTH(true, true);
		
		public final boolean up, down;
		
		private EnumChange(boolean up, boolean down)
		{
			this.up = up;
			this.down = down;
		}
		
		public Double mix(Double min, Double h)
		{
			if(this==EnumChange.NOTHING)
				return null;
			
			if(min==null && h==null)
				return null;
			if(min==null && h!=null)
				return h;
			if(min!=null && h==null)
				return min;
			
			if(this==ONLY_UP)
				return Math.max(min.doubleValue(), h.doubleValue());
			else
				return Math.min(min.doubleValue(), h.doubleValue());
		}

		public EnumChange combine(EnumChange other)
		{
			boolean u = this.up || other.up;
			boolean d = this.down || other.down;
			
			return get(u, d);
		}
		
		public static EnumChange get(boolean up, boolean down)
		{
			if(up & down)
				return BOTH;
			else if(up && !down)
				return EnumChange.ONLY_UP;
			else if (!up && down)
				return ONLY_DOWN;
			else
				return EnumChange.NOTHING;
		}
	}


	@Override
	public boolean isStartChunk(CaveCarverConfiguration pConfig, Random pRandom)
	{
		return true;
	}
}
