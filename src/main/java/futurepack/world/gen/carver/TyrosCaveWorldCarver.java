package futurepack.world.gen.carver;

import net.minecraft.world.level.levelgen.carver.WorldCarver;


public class TyrosCaveWorldCarver extends ExtendedCaveWorldCaver
{
	public TyrosCaveWorldCarver() 
	{
		super();
	}

	public static final CarveConfig TYROS = new CarveConfig(22, WorldCarver.LAVA);
	
    @Override
	public CarveConfig getConfig() 
    {
    	return TYROS;
    }
}
