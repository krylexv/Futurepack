package futurepack.world.gen.carver;

import java.util.BitSet;
import java.util.function.Function;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.chunk.CarvingMask;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.levelgen.Aquifer;
import net.minecraft.world.level.levelgen.carver.CanyonCarverConfiguration;
import net.minecraft.world.level.levelgen.carver.CarvingContext;
import net.minecraft.world.level.levelgen.carver.WorldCarver;

public class LargeCanyonWorldCaver extends ExtendedCanyonWorldCaver
{
	protected int LAVA_LEVEL = 22;
	protected float scaleYRange = 1.2F;
	protected float scaleplacementYBound = 3.5F;
	protected float scaleplacementXZBound = 1.75F;
	protected boolean closeTop = true;

	@Override
	protected boolean carveEllipsoid(CarvingContext pContext, CanyonCarverConfiguration pConfig, ChunkAccess pChunk, Function<BlockPos, Holder<Biome>> pBiomeAccessor, Aquifer pAquifer, double pX, double pY, double pZ, double placementXZBound, double placementYBound, CarvingMask pCarvingMask, WorldCarver.CarveSkipChecker pSkipChecker) 
	{
		pY *= scaleYRange;
		placementYBound *= scaleplacementYBound;
		placementXZBound *= scaleplacementXZBound;
		return super.carveEllipsoid(pContext, pConfig, pChunk, pBiomeAccessor, pAquifer, pX, pY, pZ, placementXZBound, placementYBound, pCarvingMask, pSkipChecker);
	}
}
