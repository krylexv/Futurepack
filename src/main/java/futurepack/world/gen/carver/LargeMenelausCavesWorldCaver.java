package futurepack.world.gen.carver;

import java.util.Set;
import java.util.function.Function;

import com.google.common.collect.ImmutableSet;

import futurepack.common.block.terrain.TerrainBlocks;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.CarvingMask;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.levelgen.Aquifer;
import net.minecraft.world.level.levelgen.carver.CarvingContext;
import net.minecraft.world.level.levelgen.carver.CaveCarverConfiguration;
import net.minecraft.world.level.levelgen.carver.WorldCarver;

public class LargeMenelausCavesWorldCaver extends ExtendedCaveWorldCaver
{
	public LargeMenelausCavesWorldCaver() 
	{
		super();
	}

	public static final CarveConfig MENEALUS = new CarveConfig(15, WorldCarver.WATER);
	
	protected Set<Block> menelausBlocks = ImmutableSet.of(TerrainBlocks.stone_m, TerrainBlocks.sandstone_m, TerrainBlocks.sand_m);

	
	@Override
	protected void createRoom(CarvingContext pContext, CaveCarverConfiguration pConfig, ChunkAccess pChunk, Function<BlockPos, Holder<Biome>> pBiomeAccessor, Aquifer pAquifer, double pX, double pY, double pZ, float pRadius, double pHorizontalVerticalRatio, CarvingMask pCarvingMask, WorldCarver.CarveSkipChecker pSkipChecker)
	{
		super.createRoom(pContext, pConfig, pChunk, pBiomeAccessor, pAquifer, pX, pY, pZ, 2F + pRadius, 2F * pHorizontalVerticalRatio, pCarvingMask, pSkipChecker);
	}
	
	@Override
	protected void createTunnel(CarvingContext pContext, CaveCarverConfiguration pConfig, ChunkAccess pChunk, Function<BlockPos, Holder<Biome>> pBiomeAccessor, long pSeed, Aquifer pAquifer, double pX, double pY, double pZ, double pHorizontalRadiusMultiplier, double pVerticalRadiusMultiplier, float pThickness, float pYaw, float pPitch, int pBranchIndex, int pBranchCount, double pHorizontalVerticalRatio, CarvingMask pCarvingMask, WorldCarver.CarveSkipChecker pSkipChecker)
	{
		if(pY < 45)
		{
			pThickness *= 2.5F;
//			dy *= 0.5F;
		}
		super.createTunnel(pContext, pConfig, pChunk, pBiomeAccessor, pSeed, pAquifer, pX, pY, pZ, pHorizontalRadiusMultiplier, pVerticalRadiusMultiplier, pThickness, pYaw, pPitch, pBranchIndex, pBranchCount, pHorizontalVerticalRatio, pCarvingMask, pSkipChecker);
	}
	

//	@Override
//	protected boolean hasWater(ChunkAccess chunk, int mainChunkX, int mainChunkZ, int minXPos, int maxXPos, int minYPos, int maxYPos, int minZPos, int maxZPos)
//	{
//		if(minYPos < MENEALUS.FLUID_HEIGHT+1)
//		{
//			minYPos= MENEALUS.FLUID_HEIGHT+1;
//		}
//		if(maxYPos < minYPos)
//			return false;
//		
//		return super.hasWater(chunk, mainChunkX, mainChunkZ, minXPos, maxXPos, minYPos, maxYPos, minZPos, maxZPos);
//	}

	@Override
	protected boolean canReplaceBlock(BlockState target)
	{
		if(menelausBlocks.contains(target.getBlock()))
			return true;
		
		return super.canReplaceBlock(target);
	}
	
	@Override
	public CarveConfig getConfig() 
	{
		return MENEALUS;
	}
}
