package futurepack.world;

import futurepack.common.FPLog;
import futurepack.common.block.deco.DecoBlocks;
import futurepack.common.block.misc.MiscBlocks;
import futurepack.common.block.misc.TileEntityDungeonTeleporter;
import futurepack.common.dim.structures.ManagerDungeonStructures;
import futurepack.common.dim.structures.generation.BakedDungeon;
import futurepack.common.dim.structures.generation.DungeonGeneratorBase;
import futurepack.common.dim.structures.generation.IDungeonEventListener;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.research.Research;
import futurepack.common.research.ResearchManager;
import futurepack.depend.api.helper.HelperEntities;
import futurepack.world.dimensions.Dimensions;
import futurepack.world.protection.FPDungeonProtection;
import net.minecraft.Util;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Registry;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerChunkCache;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.level.TicketType;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.animal.Chicken;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraftforge.common.util.LazyOptional;

public class WorldGenTecDungeon
{	

	private static boolean isProtected(Level w, BoundingBox box)
	{
		if(isProtected(w, new BlockPos(box.minX(), box.minY(), box.minZ())))
		{
			return true;	
		}			
		if(isProtected(w, new BlockPos(box.maxX(), box.minY(), box.minZ())))
		{
			return true;
		}
		if(isProtected(w, new BlockPos(box.minX(), box.minY(), box.maxZ())))
		{
			return true;
		}
		if(isProtected(w, new BlockPos(box.maxX(), box.minY(), box.maxZ())) )
		{
			return true;	
		}
		if(isProtected(w, new BlockPos(box.minX(), box.minY(), box.minZ()).offset(box.getXSpan() / 2, 0, box.getZSpan() / 2)))
		{
			return true;
		}
		
		return false;
	}
	
	private static boolean isProtected(Level w, BlockPos pos)
	{
		LevelChunk c = w.getChunkAt(pos);
		LazyOptional<Boolean> opt = c.getCapability(FPDungeonProtection.cap_PROTECTION, null).lazyMap(p -> p.hasChunkProtection());
		return opt.orElse(false);
	}
	
	public static void handleTeleporterClick(Level w, MinecraftServer server, BlockPos pos, Player pl, BlockState beam)
	{
		
		int ymin = w.getMinBuildHeight(), ymax = 255;
		final boolean baseTeleporter;
		
		if(beam.getBlock() == MiscBlocks.beam_up)//up
		{
			ymin = pos.getY();		
			baseTeleporter = false;
		}
		else if(beam.getBlock() == MiscBlocks.beam_down)//down		
		{
			ymax = pos.getY();	
			baseTeleporter = false;
		}
		else if(beam.getBlock() == MiscBlocks.beam)//none	
			baseTeleporter = true;
		else
			baseTeleporter = false;
		
		
		boolean succsess = false;
		boolean bad_random = false;
		TileEntityDungeonTeleporter tile = (TileEntityDungeonTeleporter) w.getBlockEntity(pos);
		if(w.dimension().location().equals(Dimensions.BELOW_BEDROCK_ID))
		{
			boolean allSet;
			ServerLevel overworld;
			if(tile.getTargetDimension()!=null)
			{
				allSet = true;
				overworld = server.getLevel(tile.getTargetDimension());
			}
			else
			{
				allSet = false;
				overworld = server.getLevel(ResourceKey.create(Registry.DIMENSION_REGISTRY, DimensionType.OVERWORLD_EFFECTS));
			}
			
			if(tile.getOtherTeleporter()!=null)
			{
				if(doTeleport(w, pos, pl, baseTeleporter, overworld, tile.getOtherTeleporter()))
				{
					succsess = true;
				}
				else
				{
					tile.setTargetTeleporter(null, overworld);
				}
			}
			else
			{
				allSet = false;
			}
			
			for(BlockPos mutable : BlockPos.betweenClosed(pos.getX(), w.getMinBuildHeight(),  pos.getZ(), pos.getX(), w.getMaxBuildHeight(), pos.getZ()))
			{
				if(doTeleport(w, pos, pl, baseTeleporter, overworld, mutable))
				{
					if(!allSet)
					{
						tile.setTargetTeleporter(mutable.immutable(), overworld);
					}
					succsess = true;
					break;
				}
			}
		}
		else
		{
			ServerLevel belowbedrock = server.getLevel(Dimensions.BELOW_BEDROCK);
			
			if(!Dimensions.BELOW_BEDROCK_ID.equals(tile.getTargetDimension().location()))
			{
				tile.setTargetTeleporter(tile.getOtherTeleporter(), belowbedrock);
			}
			
			boolean found = false;
			if(tile.getOtherTeleporter()!=null)
			{
				if(doTeleport(w, pos, pl, baseTeleporter, belowbedrock, tile.getOtherTeleporter()))
				{
					found = true;
					succsess = true;
				}
			}
			if(!found)
			{
				for(BlockPos mutable : BlockPos.betweenClosed(pos.getX(), w.getMinBuildHeight(),  pos.getZ(), pos.getX(), w.getMaxBuildHeight(), pos.getZ()))
				{
					if(doTeleport(w, pos, pl, baseTeleporter, belowbedrock, mutable))
					{
						found = true;
						succsess = true;
						
						if(tile.getOtherTeleporter()==null)
						{
							tile.setTargetTeleporter(mutable, belowbedrock);
						}
						
						break;
					}
				}
			}
			
			if(!found && baseTeleporter)
			{
				long start = System.currentTimeMillis();
//				PortallessTeleporter tele = new PortallessTeleporter(belowbedrock);
//				server.getPlayerList().changePlayerDimension((ServerPlayerEntity) pl, Dimensions.BELOW_BEDROCK, tele);
				belowbedrock.setChunkForced(pos.getX()>>4, pos.getZ()>>4, true);
				
				pl.addEffect(new MobEffectInstance(MobEffects.DAMAGE_RESISTANCE, 60 * 60 * 20, 10, true, false));
				HelperEntities.transportPlayerToDimnsion((ServerPlayer)pl, belowbedrock);
				
				pl.teleportTo(pl.getX(), 257, pl.getZ());
				
				float oldHealth = pl.getHealth()+0.2F;
				
				ChunkPos forcedChunk = new ChunkPos(pos);
				if(w.getChunkSource() instanceof ServerChunkCache)
				{
					((ServerChunkCache)w.getChunkSource()).addRegionTicket(TicketType.FORCED, forcedChunk, 5, forcedChunk);
				}
				
				//spawn dungeon
				pl.sendMessage(new TranslatableComponent("chat.futurepack.dungeon.generation_in_progress"), Util.NIL_UUID);
				EnumGenResult dungeonGenResult = spawnDungeonAtAsync(belowbedrock, pos.offset(-4,0,-4), (ServerPlayer) pl, (world, baked, p) ->
				{
//					HelperEntities.transportPlayerToDimnsion((ServerPlayerEntity)pl, belowbedrock);
					
					pl.teleportTo(p.getX()+4.5, p.getY()+1.2, p.getZ()+4.5);
					pl.setHealth(oldHealth);
					if(!w.isClientSide)
						w.setBlockAndUpdate(pos, p.getY() < pos.getY() ? MiscBlocks.beam_down.defaultBlockState() : MiscBlocks.beam_up.defaultBlockState());//2 down ; 1 up
				
					TileEntityDungeonTeleporter dungeonTeleport = (TileEntityDungeonTeleporter) belowbedrock.getBlockEntity(p.offset(4, 0, 4));
					dungeonTeleport.setTargetTeleporter(pos.immutable(), w);
					
					if(w.getChunkSource() instanceof ServerChunkCache)
					{
						((ServerChunkCache)w.getChunkSource()).removeRegionTicket(TicketType.FORCED, forcedChunk, 5, forcedChunk);
					}
					
					pl.removeEffectNoUpdate(MobEffects.DAMAGE_RESISTANCE);
				});
				succsess = dungeonGenResult == EnumGenResult.FINISHED;
				bad_random = dungeonGenResult == EnumGenResult.BAD_RANDOM;
				belowbedrock.setChunkForced(pos.getX()>>4, pos.getZ()>>4, false);

				FPLog.logger.debug("Dungeon Generated after %sms.", System.currentTimeMillis() - start);
			}
		}
		if(!succsess)//the player is stuck somewhere in the walls
		{
			if(!bad_random)
			{
				HelperEntities.disableItemSpawn();
				boolean removed = w.setBlockAndUpdate(pos, DecoBlocks.color_iron_white.defaultBlockState());//deleting the teleporter
				HelperEntities.enableItemSpawn();
				
				if(removed)
				{
					Chicken soul = EntityType.CHICKEN.create(w);
					soul.setNoGravity(true);
					soul.setGlowingTag(true);
					soul.setAge(-6000);
					soul.addEffect(new MobEffectInstance(MobEffects.LEVITATION, 20 * 60));
					soul.setCustomNameVisible(true);
					soul.setCustomName(new TextComponent("Soul"));
					soul.setPos(pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5);
					w.addFreshEntity(soul);
				}
				else
				{
					FPLog.logger.warn("Unable to delete broken dungeon Teleporter at " + pos);
				}
			}
			else
			{
				pl.sendMessage(new TranslatableComponent("chat.futurepack.dungeon.generation_try_again"), Util.NIL_UUID);
			}
			
			if(!pl.getCommandSenderWorld().dimension().location().equals(w.dimension().location()))
			{
//				PortallessTeleporter tele = new PortallessTeleporter((ServerWorld)w);
//				server.getPlayerList().changePlayerDimension((ServerPlayerEntity) pl, w.getDimensionType(), tele);
				HelperEntities.transportPlayerToDimnsion((ServerPlayer)pl, (ServerLevel) w);

				pl.teleportTo(pos.getX()+0.5, pos.getY()+1.0, pos.getZ()+0.5);
			}	
		}
		else
		{
			Research r = ResearchManager.getResearch("story.kryptographie");
			CustomPlayerData.getDataFromPlayer(pl).addResearch(r);
		}
	}

	private static boolean doTeleport(Level w, BlockPos pos, Player pl, final boolean baseTeleporter, ServerLevel overworld, BlockPos targetTeleporter) 
	{
		BlockState state = overworld.getBlockState(targetTeleporter);
		if(isBeam(state.getBlock()))
		{
			HelperEntities.transportPlayerToDimnsion((ServerPlayer)pl, overworld);
			pl.teleportTo(targetTeleporter.getX()+0.5, targetTeleporter.getY()+1.2, targetTeleporter.getZ()+0.5);
			if(baseTeleporter)
			{
				if(pos.getY() < targetTeleporter.getY())
				{
					if(!w.isClientSide)
						w.setBlockAndUpdate(pos, MiscBlocks.beam_up.defaultBlockState());
					overworld.setBlockAndUpdate(targetTeleporter, MiscBlocks.beam_down.defaultBlockState());
				}
				else
				{
					if(!w.isClientSide)
						w.setBlockAndUpdate(pos, MiscBlocks.beam_down.defaultBlockState());
					overworld.setBlockAndUpdate(targetTeleporter, MiscBlocks.beam_up.defaultBlockState());
				}
			}
			return true;
		}
		return false;
	}
	
	
	static enum EnumGenResult
	{
		FINISHED,
		BLOCKED,
		BAD_RANDOM;
	}
	
	private static EnumGenResult spawnDungeonAtAsync(ServerLevel w, BlockPos bpos, ServerPlayer pl, IDungeonEventListener listener)
	{
		DungeonGeneratorBase base = new DungeonGeneratorBase(w.random.nextLong()+ 7845L);
		ManagerDungeonStructures.init(base, false);
		BakedDungeon dun = null;
		
		for(int i=0;i<10;i++)
		{
			dun = base.generate();
			if(dun!=null)
				break;
		}
		if(dun == null)
			return EnumGenResult.BAD_RANDOM;		
		
		dun.listener.add( (world,baked,pos) -> {
			boolean flag = bpos.getY() < pos.getY();
			if(!world.isClientSide)
				world.setBlockAndUpdate(pos.offset(4,0,4), (flag ? MiscBlocks.beam_down : MiscBlocks.beam_up).defaultBlockState()); // 1 up; 2 down
		});
		if(listener!=null)
			dun.listener.add(listener);
				
		int h = dun.totalBox.getYSpan();
		int y = 127;
				
		if(y + dun.totalBox.maxY() > 250)
			y =  255 - (y + dun.totalBox.maxY());
		else if (y + dun.totalBox.minY() < 5)
			y = 5 - (y+dun.totalBox.minY());
		
		
		BlockPos start = new BlockPos(bpos.getX(), y, bpos.getZ());
		BoundingBox box = dun.getBoundingBox(start);
		if(isProtected(w, box))
			return EnumGenResult.BLOCKED;
				
		dun.spawnThreaded(w, start, 50);
		return EnumGenResult.FINISHED;
	}

	private static boolean isBeam(Block bl)
	{
		return bl == MiscBlocks.beam || bl == MiscBlocks.beam_up || bl == MiscBlocks.beam_down || bl == MiscBlocks.beam_both;
	}
}
