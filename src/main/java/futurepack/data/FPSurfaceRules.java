package futurepack.data;

import java.util.Arrays;
import java.util.stream.Stream;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.gson.JsonElement;
import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.JsonOps;

import futurepack.api.Constants;
import futurepack.common.block.terrain.TerrainBlocks;
import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.Noises;
import net.minecraft.world.level.levelgen.SurfaceRules;
import net.minecraft.world.level.levelgen.VerticalAnchor;

public class FPSurfaceRules
{
	public static JsonElement writeJson(SurfaceRules.RuleSource src)
	{
		return SurfaceRules.RuleSource.CODEC.encodeStart(JsonOps.INSTANCE, src).getOrThrow(false, s -> new RuntimeException(s));
	}

	public static final ResourceKey<Biome> MENELAUS = register("menelaus");
	public static final ResourceKey<Biome> MENELAUS_FLAT = register("menelaus_flat");
	public static final ResourceKey<Biome> MENELAUS_FOREST = register("menelaus_forest");
	public static final ResourceKey<Biome> MENELAUS_MUSHROOM = register("menelaus_mushroom");
	public static final ResourceKey<Biome> MENELAUS_PLATAU = register("menelaus_platau");
	public static final ResourceKey<Biome> MENELAUS_SEA = register("menelaus_sea");

	public static final ResourceKey<Biome> TYROS = register("tyros");
	public static final ResourceKey<Biome> TYROS_MOUNTAIN = register("tyros_mountain");
	public static final ResourceKey<Biome> TYROS_PALIRIE_FOREST = register("tyros_palirie_forest");
	public static final ResourceKey<Biome> TYROS_ROCK_DESERT = register("tyros_rockdesert");
	public static final ResourceKey<Biome> TYROS_ROCKDESERT_FLAT = register("tyros_rockdesert_flat");
	public static final ResourceKey<Biome> TYROS_SWAMP = register("tyros_swamp");

	private static ResourceKey<Biome> register(String pKey)
	{
		return ResourceKey.create(Registry.BIOME_REGISTRY, new ResourceLocation(pKey));
	}

	private static final SurfaceRules.RuleSource AIR = makeStateRule(Blocks.AIR);
	private static final SurfaceRules.RuleSource BEDROCK = makeStateRule(Blocks.BEDROCK);
//	private static final SurfaceRules.RuleSource WHITE_TERRACOTTA = makeStateRule(Blocks.WHITE_TERRACOTTA);
//	private static final SurfaceRules.RuleSource ORANGE_TERRACOTTA = makeStateRule(Blocks.ORANGE_TERRACOTTA);
//	private static final SurfaceRules.RuleSource TERRACOTTA = makeStateRule(Blocks.TERRACOTTA);
//	private static final SurfaceRules.RuleSource RED_SAND = makeStateRule(Blocks.RED_SAND);
//	private static final SurfaceRules.RuleSource RED_SANDSTONE = makeStateRule(Blocks.RED_SANDSTONE);
	private static final SurfaceRules.RuleSource STONE_M = makeStateRule(TerrainBlocks.stone_m);// fixed
	private static final SurfaceRules.RuleSource STONE_VANILLA = makeStateRule(Blocks.STONE);
//	private static final SurfaceRules.RuleSource DEEPSLATE = makeStateRule(Blocks.DEEPSLATE);
	private static final SurfaceRules.RuleSource DIRT_M = makeStateRule(TerrainBlocks.dirt_m);// fixed
	private static final SurfaceRules.RuleSource DIRT_VANILLA = makeStateRule(Blocks.DIRT);// fixed
//	private static final SurfaceRules.RuleSource PODZOL = makeStateRule(Blocks.PODZOL);
	private static final SurfaceRules.RuleSource COARSE_DIRT = makeStateRule(Blocks.COARSE_DIRT);
	private static final SurfaceRules.RuleSource MYCELIUM = makeStateRule(Blocks.MYCELIUM);
	private static final SurfaceRules.RuleSource GRASS_BLOCK = makeStateRule(Blocks.GRASS_BLOCK);
	private static final SurfaceRules.RuleSource GRASS_BLOCK_T = makeStateRule(TerrainBlocks.grass_t);
//	private static final SurfaceRules.RuleSource CALCITE = makeStateRule(Blocks.CALCITE);
	private static final SurfaceRules.RuleSource GRAVEL_M = makeStateRule(TerrainBlocks.gravel_m);// fixed
	private static final SurfaceRules.RuleSource SAND_M = makeStateRule(TerrainBlocks.sand_m);// fixed
	private static final SurfaceRules.RuleSource SANDSTONE_M = makeStateRule(TerrainBlocks.sandstone_m);// fixed
	private static final SurfaceRules.RuleSource GRAVEL_VANILLA = makeStateRule(Blocks.GRAVEL);// fixed
	private static final SurfaceRules.RuleSource SAND_VANILLA = makeStateRule(Blocks.SAND);// fixed
	private static final SurfaceRules.RuleSource SANDSTONE_VANILLA = makeStateRule(Blocks.SANDSTONE);// fixed
//	private static final SurfaceRules.RuleSource PACKED_ICE = makeStateRule(Blocks.PACKED_ICE);
//	private static final SurfaceRules.RuleSource SNOW_BLOCK = makeStateRule(Blocks.SNOW_BLOCK);
//	private static final SurfaceRules.RuleSource POWDER_SNOW = makeStateRule(Blocks.POWDER_SNOW);
	private static final SurfaceRules.RuleSource ICE = makeStateRule(Blocks.ICE);
	private static final SurfaceRules.RuleSource WATER = makeStateRule(Blocks.WATER);

	private static SurfaceRules.RuleSource makeStateRule(Block pBlock)
	{
		return SurfaceRules.state(pBlock.defaultBlockState());
	}

	public static SurfaceRules.RuleSource menelaus()
	{
		return menelaus(true, false, true);
	}

	public static SurfaceRules.RuleSource menelaus(boolean p_198381_, boolean bedrockRoof, boolean bedrockFloor)
	{
//		SurfaceRules.ConditionSource surfacerules$conditionsource = SurfaceRules.yBlockCheck(VerticalAnchor.absolute(97), 2);
//		SurfaceRules.ConditionSource surfacerules$conditionsource1 = SurfaceRules.yBlockCheck(VerticalAnchor.absolute(256), 0);
		SurfaceRules.ConditionSource above63 = SurfaceRules.yStartCheck(VerticalAnchor.absolute(63), -1);
//		SurfaceRules.ConditionSource surfacerules$conditionsource3 = SurfaceRules.yStartCheck(VerticalAnchor.absolute(74), 1);
//		SurfaceRules.ConditionSource surfacerules$conditionsource4 = SurfaceRules.yBlockCheck(VerticalAnchor.absolute(62), 0);
//		SurfaceRules.ConditionSource surfacerules$conditionsource5 = SurfaceRules.yBlockCheck(VerticalAnchor.absolute(63), 0);
		SurfaceRules.ConditionSource surfacerules$conditionsource6 = SurfaceRules.waterBlockCheck(-1, 0);
		SurfaceRules.ConditionSource noWater = SurfaceRules.waterBlockCheck(0, 0);
		SurfaceRules.ConditionSource surfacerules$conditionsource8 = SurfaceRules.waterStartCheck(-6, -1);
		SurfaceRules.ConditionSource isHole = SurfaceRules.hole();
		SurfaceRules.ConditionSource isFrozenOcean = SurfaceRules.isBiome(Biomes.FROZEN_OCEAN, Biomes.DEEP_FROZEN_OCEAN);
//		SurfaceRules.ConditionSource isSteep = SurfaceRules.steep();

		SurfaceRules.RuleSource grass_top_dirt = SurfaceRules.sequence(SurfaceRules.ifTrue(noWater, GRASS_BLOCK), DIRT_M);
		SurfaceRules.RuleSource sand_top_sandstone = SurfaceRules.sequence(SurfaceRules.ifTrue(SurfaceRules.ON_CEILING, SANDSTONE_M), SAND_M);
		SurfaceRules.RuleSource stone_top_gravel = SurfaceRules.sequence(SurfaceRules.ifTrue(SurfaceRules.ON_CEILING, STONE_M), GRAVEL_M);

		SurfaceRules.ConditionSource isBeach = SurfaceRules.isBiome(Biomes.WARM_OCEAN, Biomes.BEACH, Biomes.SNOWY_BEACH);
		SurfaceRules.ConditionSource isDesert = SurfaceRules.isBiome(Biomes.DESERT, MENELAUS, MENELAUS_FLAT, MENELAUS_FOREST, MENELAUS_MUSHROOM, MENELAUS_PLATAU, MENELAUS_SEA);

		SurfaceRules.RuleSource surfacerules$rulesource3 = SurfaceRules.sequence(
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.STONY_PEAKS),
//						SurfaceRules.sequence(SurfaceRules.ifTrue(SurfaceRules.noiseCondition(Noises.CALCITE, -0.0125D, 0.0125D), CALCITE), STONE)),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.STONY_SHORE),
//						SurfaceRules.sequence(SurfaceRules.ifTrue(SurfaceRules.noiseCondition(Noises.GRAVEL, -0.05D, 0.05D), stone_top_gravel), STONE)),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.WINDSWEPT_HILLS), SurfaceRules.ifTrue(surfaceNoiseAbove(1.0D), STONE)),
				SurfaceRules.ifTrue(isBeach, sand_top_sandstone), SurfaceRules.ifTrue(isDesert, sand_top_sandstone),
				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.DRIPSTONE_CAVES), STONE_M));

//		SurfaceRules.RuleSource surfacerules$rulesource4 = SurfaceRules.ifTrue(SurfaceRules.noiseCondition(Noises.POWDER_SNOW, 0.45D, 0.58D),
//				SurfaceRules.ifTrue(surfacerules$conditionsource7, POWDER_SNOW));
//		SurfaceRules.RuleSource surfacerules$rulesource5 = SurfaceRules.ifTrue(SurfaceRules.noiseCondition(Noises.POWDER_SNOW, 0.35D, 0.6D),
//				SurfaceRules.ifTrue(surfacerules$conditionsource7, POWDER_SNOW));
		SurfaceRules.RuleSource surfacerules$rulesource6 = SurfaceRules.sequence(
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.FROZEN_PEAKS),
//						SurfaceRules.sequence(SurfaceRules.ifTrue(isSteep, PACKED_ICE),
//								SurfaceRules.ifTrue(SurfaceRules.noiseCondition(Noises.PACKED_ICE, -0.5D, 0.2D), PACKED_ICE),
//								SurfaceRules.ifTrue(SurfaceRules.noiseCondition(Noises.ICE, -0.0625D, 0.025D), ICE), SurfaceRules.ifTrue(surfacerules$conditionsource7, SNOW_BLOCK))),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.SNOWY_SLOPES),
//						SurfaceRules.sequence(SurfaceRules.ifTrue(isSteep, STONE), surfacerules$rulesource4,
//								SurfaceRules.ifTrue(surfacerules$conditionsource7, SNOW_BLOCK))),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.JAGGED_PEAKS), STONE),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.GROVE), SurfaceRules.sequence(surfacerules$rulesource4, DIRT)), surfacerules$rulesource3,
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.WINDSWEPT_SAVANNA), SurfaceRules.ifTrue(surfaceNoiseAbove(1.75D), STONE)),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.WINDSWEPT_GRAVELLY_HILLS), SurfaceRules.sequence(SurfaceRules.ifTrue(surfaceNoiseAbove(2.0D), stone_top_gravel),
//						SurfaceRules.ifTrue(surfaceNoiseAbove(1.0D), STONE), SurfaceRules.ifTrue(surfaceNoiseAbove(-1.0D), DIRT), stone_top_gravel)),
				DIRT_M);
		SurfaceRules.RuleSource surfacerules$rulesource7 = SurfaceRules.sequence(
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.FROZEN_PEAKS),
//						SurfaceRules.sequence(SurfaceRules.ifTrue(isSteep, PACKED_ICE),
//								SurfaceRules.ifTrue(SurfaceRules.noiseCondition(Noises.PACKED_ICE, 0.0D, 0.2D), PACKED_ICE),
//								SurfaceRules.ifTrue(SurfaceRules.noiseCondition(Noises.ICE, 0.0D, 0.025D), ICE), SurfaceRules.ifTrue(surfacerules$conditionsource7, SNOW_BLOCK))),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.SNOWY_SLOPES),
//						SurfaceRules.sequence(SurfaceRules.ifTrue(isSteep, STONE), surfacerules$rulesource5,
//								SurfaceRules.ifTrue(surfacerules$conditionsource7, SNOW_BLOCK))),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.JAGGED_PEAKS),
//						SurfaceRules.sequence(SurfaceRules.ifTrue(isSteep, STONE), SurfaceRules.ifTrue(surfacerules$conditionsource7, SNOW_BLOCK))),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.GROVE), SurfaceRules.sequence(surfacerules$rulesource5, SurfaceRules.ifTrue(surfacerules$conditionsource7, SNOW_BLOCK))),

//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.WINDSWEPT_SAVANNA),
//						SurfaceRules.sequence(SurfaceRules.ifTrue(surfaceNoiseAbove(1.75D), STONE), SurfaceRules.ifTrue(surfaceNoiseAbove(-0.5D), COARSE_DIRT))),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.WINDSWEPT_GRAVELLY_HILLS),
//						SurfaceRules.sequence(SurfaceRules.ifTrue(surfaceNoiseAbove(2.0D), stone_top_gravel), SurfaceRules.ifTrue(surfaceNoiseAbove(1.0D), STONE),
//								SurfaceRules.ifTrue(surfaceNoiseAbove(-1.0D), grass_top_dirt), stone_top_gravel)),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.OLD_GROWTH_PINE_TAIGA, Biomes.OLD_GROWTH_SPRUCE_TAIGA),
//						SurfaceRules.sequence(SurfaceRules.ifTrue(surfaceNoiseAbove(1.75D), COARSE_DIRT), SurfaceRules.ifTrue(surfaceNoiseAbove(-0.95D), PODZOL))),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.ICE_SPIKES), SurfaceRules.ifTrue(surfacerules$conditionsource7, SNOW_BLOCK)),
				SurfaceRules.ifTrue(SurfaceRules.isBiome(MENELAUS_MUSHROOM), MYCELIUM),
				SurfaceRules.ifTrue(SurfaceRules.isBiome(MENELAUS_FOREST),
						SurfaceRules.sequence(SurfaceRules.ifTrue(surfaceNoiseAbove(1.75D), DIRT_M), SurfaceRules.ifTrue(surfaceNoiseAbove(-0.95D), COARSE_DIRT))),
				surfacerules$rulesource3, SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.FLOWER_FOREST, Biomes.SUNFLOWER_PLAINS), grass_top_dirt), sand_top_sandstone);
//		SurfaceRules.ConditionSource surfacerules$conditionsource14 = SurfaceRules.noiseCondition(Noises.SURFACE, -0.909D, -0.5454D);
//		SurfaceRules.ConditionSource surfacerules$conditionsource15 = SurfaceRules.noiseCondition(Noises.SURFACE, -0.1818D, 0.1818D);
//		SurfaceRules.ConditionSource surfacerules$conditionsource16 = SurfaceRules.noiseCondition(Noises.SURFACE, 0.5454D, 0.909D);
		SurfaceRules.RuleSource surfacerules$rulesource8 = SurfaceRules.sequence(
//				SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, SurfaceRules.sequence(
//						SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.WOODED_BADLANDS), SurfaceRules.ifTrue(surfacerules$conditionsource,
//								SurfaceRules.sequence(SurfaceRules.ifTrue(surfacerules$conditionsource14, COARSE_DIRT), SurfaceRules.ifTrue(surfacerules$conditionsource15, COARSE_DIRT),
//										SurfaceRules.ifTrue(surfacerules$conditionsource16, COARSE_DIRT), grass_top_dirt))),
//						SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.SWAMP),
//								SurfaceRules.ifTrue(surfacerules$conditionsource4,
//										SurfaceRules.ifTrue(SurfaceRules.not(surfacerules$conditionsource5), SurfaceRules.ifTrue(SurfaceRules.noiseCondition(Noises.SWAMP, 0.0D), WATER))))

//						)),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.BADLANDS, Biomes.ERODED_BADLANDS, Biomes.WOODED_BADLANDS), SurfaceRules.sequence(
//						SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, SurfaceRules.sequence(SurfaceRules.ifTrue(surfacerules$conditionsource1, ORANGE_TERRACOTTA), SurfaceRules.ifTrue(
//								surfacerules$conditionsource3,
//								SurfaceRules.sequence(SurfaceRules.ifTrue(surfacerules$conditionsource14, TERRACOTTA), SurfaceRules.ifTrue(surfacerules$conditionsource15, TERRACOTTA),
//										SurfaceRules.ifTrue(surfacerules$conditionsource16, TERRACOTTA), SurfaceRules.bandlands())),
//								SurfaceRules.ifTrue(surfacerules$conditionsource6, SurfaceRules.sequence(SurfaceRules.ifTrue(SurfaceRules.ON_CEILING, RED_SANDSTONE), RED_SAND)),
//								SurfaceRules.ifTrue(SurfaceRules.not(isHole), ORANGE_TERRACOTTA), SurfaceRules.ifTrue(surfacerules$conditionsource8, WHITE_TERRACOTTA),
//								stone_top_gravel)),
//						SurfaceRules.ifTrue(surfacerules$conditionsource2,
//								SurfaceRules.sequence(
//										SurfaceRules.ifTrue(surfacerules$conditionsource5, SurfaceRules.ifTrue(SurfaceRules.not(surfacerules$conditionsource3), ORANGE_TERRACOTTA)),
//										SurfaceRules.bandlands())),
//						SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, SurfaceRules.ifTrue(surfacerules$conditionsource8, WHITE_TERRACOTTA)))),
				SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR,
						SurfaceRules.ifTrue(surfacerules$conditionsource6,
								SurfaceRules.sequence(
										SurfaceRules.ifTrue(isFrozenOcean,
												SurfaceRules.ifTrue(isHole,
														SurfaceRules.sequence(SurfaceRules.ifTrue(noWater, AIR), SurfaceRules.ifTrue(SurfaceRules.temperature(), ICE), WATER))),
										surfacerules$rulesource7))),
				SurfaceRules.ifTrue(surfacerules$conditionsource8,
						SurfaceRules.sequence(SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, SurfaceRules.ifTrue(isFrozenOcean, SurfaceRules.ifTrue(isHole, WATER))),
								SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, surfacerules$rulesource6),
								SurfaceRules.ifTrue(isBeach, SurfaceRules.ifTrue(SurfaceRules.DEEP_UNDER_FLOOR, SANDSTONE_M)),
								SurfaceRules.ifTrue(isDesert, SurfaceRules.ifTrue(SurfaceRules.VERY_DEEP_UNDER_FLOOR, SANDSTONE_M)))),
				SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, SurfaceRules.sequence(SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.FROZEN_PEAKS, Biomes.JAGGED_PEAKS), STONE_M),
						SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.WARM_OCEAN, Biomes.LUKEWARM_OCEAN, Biomes.DEEP_LUKEWARM_OCEAN), sand_top_sandstone), stone_top_gravel)));


		Builder<SurfaceRules.RuleSource> builder = ImmutableList.builder();
		if (bedrockRoof)
		{
			builder.add(SurfaceRules.ifTrue(SurfaceRules.not(SurfaceRules.verticalGradient("bedrock_roof", VerticalAnchor.belowTop(5), VerticalAnchor.top())), BEDROCK));
		}

		if (bedrockFloor)
		{
			builder.add(SurfaceRules.ifTrue(SurfaceRules.verticalGradient("bedrock_floor", VerticalAnchor.bottom(), VerticalAnchor.aboveBottom(5)), BEDROCK));
		}

		SurfaceRules.RuleSource aboveSurface = SurfaceRules.ifTrue(SurfaceRules.abovePreliminarySurface(), surfacerules$rulesource8);
		builder.add(p_198381_ ? aboveSurface : surfacerules$rulesource8);
		builder.add(SurfaceRules.ifTrue(SurfaceRules.verticalGradient("menelaus_stone", VerticalAnchor.absolute(20), VerticalAnchor.absolute(40)), STONE_VANILLA));
		builder.add(SurfaceRules.ifTrue(SurfaceRules.not(above63), STONE_VANILLA));
		return SurfaceRules.sequence(builder.build().toArray((p_198379_) -> {
			return new SurfaceRules.RuleSource[p_198379_];
		}));
	}

	private static SurfaceRules.ConditionSource surfaceNoiseAbove(double p_194809_)
	{
		return SurfaceRules.noiseCondition(Noises.SURFACE, p_194809_ / 8.25D, Double.MAX_VALUE);
	}

	public static SurfaceRules.RuleSource tyros(boolean p_198381_, boolean bedrockRoof, boolean bedrockFloor)
	{
//		SurfaceRules.ConditionSource surfacerules$conditionsource = SurfaceRules.yBlockCheck(VerticalAnchor.absolute(97), 2);
//		SurfaceRules.ConditionSource surfacerules$conditionsource1 = SurfaceRules.yBlockCheck(VerticalAnchor.absolute(256), 0);
		SurfaceRules.ConditionSource above63 = SurfaceRules.yStartCheck(VerticalAnchor.absolute(63), -1);
//		SurfaceRules.ConditionSource surfacerules$conditionsource3 = SurfaceRules.yStartCheck(VerticalAnchor.absolute(74), 1);
//		SurfaceRules.ConditionSource surfacerules$conditionsource4 = SurfaceRules.yBlockCheck(VerticalAnchor.absolute(62), 0);
//		SurfaceRules.ConditionSource surfacerules$conditionsource5 = SurfaceRules.yBlockCheck(VerticalAnchor.absolute(63), 0);
		SurfaceRules.ConditionSource surfacerules$conditionsource6 = SurfaceRules.waterBlockCheck(-1, 0);
		SurfaceRules.ConditionSource noWater = SurfaceRules.waterBlockCheck(0, 0);
		SurfaceRules.ConditionSource surfacerules$conditionsource8 = SurfaceRules.waterStartCheck(-6, -1);
		SurfaceRules.ConditionSource isHole = SurfaceRules.hole();
		SurfaceRules.ConditionSource isFrozenOcean = SurfaceRules.isBiome(Biomes.FROZEN_OCEAN, Biomes.DEEP_FROZEN_OCEAN);
//		SurfaceRules.ConditionSource isSteep = SurfaceRules.steep();

		SurfaceRules.RuleSource grass_top_dirt = SurfaceRules.sequence(SurfaceRules.ifTrue(noWater, GRASS_BLOCK), DIRT_VANILLA);
		SurfaceRules.RuleSource grass_t_top_dirt = SurfaceRules.sequence(SurfaceRules.ifTrue(noWater, GRASS_BLOCK_T), DIRT_VANILLA);
		SurfaceRules.RuleSource sand_top_sandstone = SurfaceRules.sequence(SurfaceRules.ifTrue(SurfaceRules.ON_CEILING, SANDSTONE_VANILLA), SAND_VANILLA);
		SurfaceRules.RuleSource stone_top_gravel = SurfaceRules.sequence(SurfaceRules.ifTrue(SurfaceRules.ON_CEILING, STONE_VANILLA), GRAVEL_VANILLA);

		SurfaceRules.ConditionSource isBeach = SurfaceRules.isBiome(Biomes.WARM_OCEAN, Biomes.BEACH, Biomes.SNOWY_BEACH);
		SurfaceRules.ConditionSource isDesert = SurfaceRules.isBiome(Biomes.DESERT);

		SurfaceRules.RuleSource surfacerules$rulesource3 = SurfaceRules.sequence(
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.STONY_PEAKS),
//						SurfaceRules.sequence(SurfaceRules.ifTrue(SurfaceRules.noiseCondition(Noises.CALCITE, -0.0125D, 0.0125D), CALCITE), STONE)),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.STONY_SHORE),
//						SurfaceRules.sequence(SurfaceRules.ifTrue(SurfaceRules.noiseCondition(Noises.GRAVEL, -0.05D, 0.05D), stone_top_gravel), STONE)),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.WINDSWEPT_HILLS), SurfaceRules.ifTrue(surfaceNoiseAbove(1.0D), STONE)),
				SurfaceRules.ifTrue(isBeach, sand_top_sandstone), SurfaceRules.ifTrue(isDesert, sand_top_sandstone),
				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.DRIPSTONE_CAVES), STONE_VANILLA));

//		SurfaceRules.RuleSource surfacerules$rulesource4 = SurfaceRules.ifTrue(SurfaceRules.noiseCondition(Noises.POWDER_SNOW, 0.45D, 0.58D),
//				SurfaceRules.ifTrue(surfacerules$conditionsource7, POWDER_SNOW));
//		SurfaceRules.RuleSource surfacerules$rulesource5 = SurfaceRules.ifTrue(SurfaceRules.noiseCondition(Noises.POWDER_SNOW, 0.35D, 0.6D),
//				SurfaceRules.ifTrue(surfacerules$conditionsource7, POWDER_SNOW));
		SurfaceRules.RuleSource surfacerules$rulesource6 = SurfaceRules.sequence(
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.FROZEN_PEAKS),
//						SurfaceRules.sequence(SurfaceRules.ifTrue(isSteep, PACKED_ICE),
//								SurfaceRules.ifTrue(SurfaceRules.noiseCondition(Noises.PACKED_ICE, -0.5D, 0.2D), PACKED_ICE),
//								SurfaceRules.ifTrue(SurfaceRules.noiseCondition(Noises.ICE, -0.0625D, 0.025D), ICE), SurfaceRules.ifTrue(surfacerules$conditionsource7, SNOW_BLOCK))),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.SNOWY_SLOPES),
//						SurfaceRules.sequence(SurfaceRules.ifTrue(isSteep, STONE), surfacerules$rulesource4,
//								SurfaceRules.ifTrue(surfacerules$conditionsource7, SNOW_BLOCK))),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.JAGGED_PEAKS), STONE),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.GROVE), SurfaceRules.sequence(surfacerules$rulesource4, DIRT)), surfacerules$rulesource3,
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.WINDSWEPT_SAVANNA), SurfaceRules.ifTrue(surfaceNoiseAbove(1.75D), STONE)),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.WINDSWEPT_GRAVELLY_HILLS), SurfaceRules.sequence(SurfaceRules.ifTrue(surfaceNoiseAbove(2.0D), stone_top_gravel),
//						SurfaceRules.ifTrue(surfaceNoiseAbove(1.0D), STONE), SurfaceRules.ifTrue(surfaceNoiseAbove(-1.0D), DIRT), stone_top_gravel)),
				DIRT_VANILLA);
		SurfaceRules.RuleSource surfacerules$rulesource7 = SurfaceRules.sequence(
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.FROZEN_PEAKS),
//						SurfaceRules.sequence(SurfaceRules.ifTrue(isSteep, PACKED_ICE),
//								SurfaceRules.ifTrue(SurfaceRules.noiseCondition(Noises.PACKED_ICE, 0.0D, 0.2D), PACKED_ICE),
//								SurfaceRules.ifTrue(SurfaceRules.noiseCondition(Noises.ICE, 0.0D, 0.025D), ICE), SurfaceRules.ifTrue(surfacerules$conditionsource7, SNOW_BLOCK))),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.SNOWY_SLOPES),
//						SurfaceRules.sequence(SurfaceRules.ifTrue(isSteep, STONE), surfacerules$rulesource5,
//								SurfaceRules.ifTrue(surfacerules$conditionsource7, SNOW_BLOCK))),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.JAGGED_PEAKS),
//						SurfaceRules.sequence(SurfaceRules.ifTrue(isSteep, STONE), SurfaceRules.ifTrue(surfacerules$conditionsource7, SNOW_BLOCK))),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.GROVE), SurfaceRules.sequence(surfacerules$rulesource5, SurfaceRules.ifTrue(surfacerules$conditionsource7, SNOW_BLOCK))),

//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.WINDSWEPT_SAVANNA),
//						SurfaceRules.sequence(SurfaceRules.ifTrue(surfaceNoiseAbove(1.75D), STONE), SurfaceRules.ifTrue(surfaceNoiseAbove(-0.5D), COARSE_DIRT))),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.WINDSWEPT_GRAVELLY_HILLS),
//						SurfaceRules.sequence(SurfaceRules.ifTrue(surfaceNoiseAbove(2.0D), stone_top_gravel), SurfaceRules.ifTrue(surfaceNoiseAbove(1.0D), STONE),
//								SurfaceRules.ifTrue(surfaceNoiseAbove(-1.0D), grass_top_dirt), stone_top_gravel)),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.OLD_GROWTH_PINE_TAIGA, Biomes.OLD_GROWTH_SPRUCE_TAIGA),
//						SurfaceRules.sequence(SurfaceRules.ifTrue(surfaceNoiseAbove(1.75D), COARSE_DIRT), SurfaceRules.ifTrue(surfaceNoiseAbove(-0.95D), PODZOL))),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.ICE_SPIKES), SurfaceRules.ifTrue(surfacerules$conditionsource7, SNOW_BLOCK)),
				SurfaceRules.ifTrue(SurfaceRules.isBiome(TYROS_ROCK_DESERT, TYROS_ROCKDESERT_FLAT), STONE_VANILLA),
				SurfaceRules.ifTrue(SurfaceRules.isBiome(TYROS, TYROS_MOUNTAIN, TYROS_SWAMP),
						SurfaceRules.sequence(SurfaceRules.ifTrue(surfaceNoiseAbove(1.75D), grass_top_dirt), SurfaceRules.ifTrue(surfaceNoiseAbove(-0.95D), grass_t_top_dirt))),
				surfacerules$rulesource3, grass_top_dirt);
//		SurfaceRules.ConditionSource surfacerules$conditionsource14 = SurfaceRules.noiseCondition(Noises.SURFACE, -0.909D, -0.5454D);
//		SurfaceRules.ConditionSource surfacerules$conditionsource15 = SurfaceRules.noiseCondition(Noises.SURFACE, -0.1818D, 0.1818D);
//		SurfaceRules.ConditionSource surfacerules$conditionsource16 = SurfaceRules.noiseCondition(Noises.SURFACE, 0.5454D, 0.909D);
		SurfaceRules.RuleSource surfacerules$rulesource8 = SurfaceRules.sequence(
//				SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, SurfaceRules.sequence(
//						SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.WOODED_BADLANDS), SurfaceRules.ifTrue(surfacerules$conditionsource,
//								SurfaceRules.sequence(SurfaceRules.ifTrue(surfacerules$conditionsource14, COARSE_DIRT), SurfaceRules.ifTrue(surfacerules$conditionsource15, COARSE_DIRT),
//										SurfaceRules.ifTrue(surfacerules$conditionsource16, COARSE_DIRT), grass_top_dirt))),
//						SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.SWAMP),
//								SurfaceRules.ifTrue(surfacerules$conditionsource4,
//										SurfaceRules.ifTrue(SurfaceRules.not(surfacerules$conditionsource5), SurfaceRules.ifTrue(SurfaceRules.noiseCondition(Noises.SWAMP, 0.0D), WATER))))

//						)),
//				SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.BADLANDS, Biomes.ERODED_BADLANDS, Biomes.WOODED_BADLANDS), SurfaceRules.sequence(
//						SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, SurfaceRules.sequence(SurfaceRules.ifTrue(surfacerules$conditionsource1, ORANGE_TERRACOTTA), SurfaceRules.ifTrue(
//								surfacerules$conditionsource3,
//								SurfaceRules.sequence(SurfaceRules.ifTrue(surfacerules$conditionsource14, TERRACOTTA), SurfaceRules.ifTrue(surfacerules$conditionsource15, TERRACOTTA),
//										SurfaceRules.ifTrue(surfacerules$conditionsource16, TERRACOTTA), SurfaceRules.bandlands())),
//								SurfaceRules.ifTrue(surfacerules$conditionsource6, SurfaceRules.sequence(SurfaceRules.ifTrue(SurfaceRules.ON_CEILING, RED_SANDSTONE), RED_SAND)),
//								SurfaceRules.ifTrue(SurfaceRules.not(isHole), ORANGE_TERRACOTTA), SurfaceRules.ifTrue(surfacerules$conditionsource8, WHITE_TERRACOTTA),
//								stone_top_gravel)),
//						SurfaceRules.ifTrue(surfacerules$conditionsource2,
//								SurfaceRules.sequence(
//										SurfaceRules.ifTrue(surfacerules$conditionsource5, SurfaceRules.ifTrue(SurfaceRules.not(surfacerules$conditionsource3), ORANGE_TERRACOTTA)),
//										SurfaceRules.bandlands())),
//						SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, SurfaceRules.ifTrue(surfacerules$conditionsource8, WHITE_TERRACOTTA)))),
				SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR,
						SurfaceRules.ifTrue(surfacerules$conditionsource6,
								SurfaceRules.sequence(
										SurfaceRules.ifTrue(isFrozenOcean,
												SurfaceRules.ifTrue(isHole,
														SurfaceRules.sequence(SurfaceRules.ifTrue(noWater, AIR), SurfaceRules.ifTrue(SurfaceRules.temperature(), ICE), WATER))),
										surfacerules$rulesource7))),
				SurfaceRules.ifTrue(surfacerules$conditionsource8,
						SurfaceRules.sequence(SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, SurfaceRules.ifTrue(isFrozenOcean, SurfaceRules.ifTrue(isHole, WATER))),
								SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, surfacerules$rulesource6),
								SurfaceRules.ifTrue(isBeach, SurfaceRules.ifTrue(SurfaceRules.DEEP_UNDER_FLOOR, SANDSTONE_VANILLA)),
								SurfaceRules.ifTrue(isDesert, SurfaceRules.ifTrue(SurfaceRules.VERY_DEEP_UNDER_FLOOR, SANDSTONE_VANILLA)))),
				SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, SurfaceRules.sequence(SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.FROZEN_PEAKS, Biomes.JAGGED_PEAKS), STONE_VANILLA),
						SurfaceRules.ifTrue(SurfaceRules.isBiome(Biomes.WARM_OCEAN, Biomes.LUKEWARM_OCEAN, Biomes.DEEP_LUKEWARM_OCEAN), sand_top_sandstone), stone_top_gravel)));


		Builder<SurfaceRules.RuleSource> builder = ImmutableList.builder();
		if (bedrockRoof)
		{
			builder.add(SurfaceRules.ifTrue(SurfaceRules.not(SurfaceRules.verticalGradient("bedrock_roof", VerticalAnchor.belowTop(5), VerticalAnchor.top())), BEDROCK));
		}

		if (bedrockFloor)
		{
			builder.add(SurfaceRules.ifTrue(SurfaceRules.verticalGradient("bedrock_floor", VerticalAnchor.bottom(), VerticalAnchor.aboveBottom(5)), BEDROCK));
		}

		SurfaceRules.RuleSource aboveSurface = SurfaceRules.ifTrue(SurfaceRules.abovePreliminarySurface(), surfacerules$rulesource8);
		builder.add(p_198381_ ? aboveSurface : surfacerules$rulesource8);
		//builder.add(SurfaceRules.ifTrue(SurfaceRules.verticalGradient("menelaus_stone", VerticalAnchor.absolute(20), VerticalAnchor.absolute(40)), STONE_VANILLA));
		builder.add(SurfaceRules.ifTrue(SurfaceRules.not(above63), STONE_VANILLA));
		return SurfaceRules.sequence(builder.build().toArray((p_198379_) -> {
			return new SurfaceRules.RuleSource[p_198379_];
		}));
	}

}
