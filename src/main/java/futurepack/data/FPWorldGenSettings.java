package futurepack.data;

import com.google.common.collect.ImmutableList;
import com.mojang.datafixers.util.Pair;

import futurepack.api.Constants;
import net.minecraft.core.Registry;
import net.minecraft.core.RegistryAccess;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.biome.Climate;
import net.minecraft.world.level.biome.MultiNoiseBiomeSource;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.NoiseBasedChunkGenerator;
import net.minecraft.world.level.levelgen.NoiseGeneratorSettings;
import net.minecraft.world.level.levelgen.structure.StructureSet;
import net.minecraft.world.level.levelgen.synth.NormalNoise;


public class FPWorldGenSettings
{

	public static final MultiNoiseBiomeSource.Preset PRESET_MENELAUS = new MultiNoiseBiomeSource.Preset(new ResourceLocation(Constants.MOD_ID, "menelaus"), (p_204283_) -> {
		return new Climate.ParameterList<>(ImmutableList.of(
				//temperature,humidity, continentalness(y_level), erosion(flat ness),depth (cave biome - 0 is at surface),  weirdness (how rare is it),  offset (how rare)) {

				Pair.of(Climate.parameters(1.0F, 0.0F, 0.7F, 0.8F, 0.0F, 0.0F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.MENELAUS)),
				Pair.of(Climate.parameters(1.0F, 0.0F, 0.7F, 0.8F, 0.0F, 0.0F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.MENELAUS_MUSHROOM)),
				Pair.of(Climate.parameters(1.0F, 0.4F, 0.1F, 0.34F, 0.0F, 0.0F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.MENELAUS_FOREST)),
				Pair.of(Climate.parameters(1.0F, 0.2F, 0.08F, 0.5F, 0.0F, 0.2F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.MENELAUS_FLAT)),
				Pair.of(Climate.parameters(1.0F, 0.2F, 1.0F, 1F, 0F, 0.1F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.MENELAUS_PLATAU)),
				Pair.of(Climate.parameters(1.0F, 0.3F, 0.5F, -0.1F, 0.0F, 0.0F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.MENELAUS_SEA)),
				Pair.of(Climate.parameters(0.9F, 0.5F, 0.5F, 0.7F, 0.0F, 0.9F, 0.0F), p_204283_.getOrCreateHolder(Biomes.FLOWER_FOREST)),
				Pair.of(Climate.parameters(0.7F, 0.4F, 0.5F, 0.7F, 0.0F, 0.5F, 0.0F), p_204283_.getOrCreateHolder(Biomes.SUNFLOWER_PLAINS))
				));
	});

	public static final MultiNoiseBiomeSource.Preset PRESET_TYROS = new MultiNoiseBiomeSource.Preset(new ResourceLocation(Constants.MOD_ID, "menelaus"), (p_204283_) -> {
		return new Climate.ParameterList<>(ImmutableList.of(
				//temperature,humidity, continentalness(y_level), erosion(flat ness),depth (cave biome - 0 is at surface),  weirdness (how rare is it),  offset (how rare)) {

				Pair.of(Climate.parameters(1.0F, 1.0F, 0.2F, 0.21F, 0.0F, 0.0F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.TYROS)),
				Pair.of(Climate.parameters(1.0F, 0.9F, 0.6F, 0.61F, 0.0F, 0.0F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.TYROS_MOUNTAIN)),
				Pair.of(Climate.parameters(0.8F, 0.8F, 0.15F, 0.32F, 0.0F, 0.0F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.TYROS_PALIRIE_FOREST)),
				Pair.of(Climate.parameters(0.5F, 0.5F, 0.2F, 0.31F, 0.0F, 0.2F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.TYROS_ROCK_DESERT)),
				Pair.of(Climate.parameters(0.5F, 0.5F, 0.6F, 0.11F, 0F, 0.1F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.TYROS_ROCKDESERT_FLAT)),
				Pair.of(Climate.parameters(0.8F, 0.8F, -0.2F, 0.1F, 0.0F, 0.0F, 0.0F), p_204283_.getOrCreateHolder(FPWorldData.TYROS_SWAMP))
				));
	});

	public static NoiseBasedChunkGenerator makeDefaultMenelaus(RegistryAccess p_190040_, long p_190041_, boolean p_190042_)
	{
		return makeMenelaus(p_190040_, p_190041_, FPWorldData.SETTING_MENELAUS, p_190042_, PRESET_MENELAUS);
	}

	public static NoiseBasedChunkGenerator makeMenelaus(RegistryAccess p_190035_, long p_190036_, ResourceKey<NoiseGeneratorSettings> p_190037_, boolean p_190038_, MultiNoiseBiomeSource.Preset preset)
	{
		Registry<Biome> registry = p_190035_.registryOrThrow(Registry.BIOME_REGISTRY);
		Registry<StructureSet> registry1 = p_190035_.registryOrThrow(Registry.STRUCTURE_SET_REGISTRY);
		Registry<NoiseGeneratorSettings> registry2 = p_190035_.registryOrThrow(Registry.NOISE_GENERATOR_SETTINGS_REGISTRY);
		Registry<NormalNoise.NoiseParameters> registry3 = p_190035_.registryOrThrow(Registry.NOISE_REGISTRY);
		return new NoiseBasedChunkGenerator(registry1, registry3, preset.biomeSource(registry, p_190038_), p_190036_, registry2.getOrCreateHolder(p_190037_));
	}

	public static NoiseBasedChunkGenerator makeDefaultTyros(RegistryAccess p_190040_, long p_190041_, boolean p_190042_)
	{
		return makeMenelaus(p_190040_, p_190041_, FPWorldData.SETTING_TYROS, p_190042_, PRESET_TYROS);
	}

}
