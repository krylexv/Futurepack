package futurepack.data;

import java.util.OptionalLong;
import java.util.function.Function;

import com.mojang.serialization.Lifecycle;

import futurepack.api.Constants;
import futurepack.common.block.terrain.TerrainBlocks;
import net.minecraft.core.Holder;
import net.minecraft.core.MappedRegistry;
import net.minecraft.core.Registry;
import net.minecraft.core.RegistryAccess;
import net.minecraft.core.WritableRegistry;
import net.minecraft.data.BuiltinRegistries;
import net.minecraft.data.worldgen.TerrainProvider;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraft.world.level.dimension.LevelStem;
import net.minecraft.world.level.levelgen.NoiseGeneratorSettings;
import net.minecraft.world.level.levelgen.NoiseRouterData;
import net.minecraft.world.level.levelgen.NoiseSamplingSettings;
import net.minecraft.world.level.levelgen.NoiseSettings;
import net.minecraft.world.level.levelgen.NoiseSlider;
import net.minecraft.world.level.levelgen.WorldGenSettings;

public class FPWorldData
{

	public static final ResourceKey<NoiseGeneratorSettings> SETTING_MENELAUS = ResourceKey.create(Registry.NOISE_GENERATOR_SETTINGS_REGISTRY, new ResourceLocation(Constants.MOD_ID, "menelaus"));
	public static final ResourceKey<NoiseGeneratorSettings> SETTING_TYROS = ResourceKey.create(Registry.NOISE_GENERATOR_SETTINGS_REGISTRY, new ResourceLocation(Constants.MOD_ID, "tyros"));

	public static final ResourceKey<DimensionType> MENELAUS_LOCATION = ResourceKey.create(Registry.DIMENSION_TYPE_REGISTRY, new ResourceLocation(Constants.MOD_ID, "menelaus"));
	public static final ResourceKey<DimensionType> TYROS_LOCATION = ResourceKey.create(Registry.DIMENSION_TYPE_REGISTRY, new ResourceLocation(Constants.MOD_ID, "tyros"));


	protected static final DimensionType DEFAULT_MENELAUS = DimensionType.create(OptionalLong.empty(), true, false, false, true, 1.0D, false, false, true, false, false, 0, 256, 256, BlockTags.INFINIBURN_OVERWORLD, new ResourceLocation(Constants.MOD_ID, "menelaus"), 0.0F);
	protected static final DimensionType DEFAULT_TYROS = DimensionType.create(OptionalLong.empty(), true, false, false, true, 1.0D, false, false, true, false, false, 0, 256, 256, BlockTags.INFINIBURN_OVERWORLD, new ResourceLocation(Constants.MOD_ID, "tyros"), 0.0F);


	public static final ResourceKey<Biome> MENELAUS = register("menelaus");
	public static final ResourceKey<Biome> MENELAUS_FLAT = register("menelaus_flat");
	public static final ResourceKey<Biome> MENELAUS_FOREST = register("menelaus_forest");
	public static final ResourceKey<Biome> MENELAUS_MUSHROOM = register("menelaus_mushroom");
	public static final ResourceKey<Biome> MENELAUS_PLATAU = register("menelaus_platau");
	public static final ResourceKey<Biome> MENELAUS_SEA = register("menelaus_sea");

	public static final ResourceKey<Biome> TYROS = register("tyros");
	public static final ResourceKey<Biome> TYROS_MOUNTAIN = register("tyros_mountain");
	public static final ResourceKey<Biome> TYROS_PALIRIE_FOREST = register("tyros_palirie_forest");
	public static final ResourceKey<Biome> TYROS_ROCK_DESERT = register("tyros_rockdesert");
	public static final ResourceKey<Biome> TYROS_ROCKDESERT_FLAT = register("tyros_rockdesert_flat");
	public static final ResourceKey<Biome> TYROS_SWAMP = register("tyros_swamp");

	private static ResourceKey<Biome> register(String pKey)
	{
		return ResourceKey.create(Registry.BIOME_REGISTRY, new ResourceLocation(Constants.MOD_ID, pKey));
	}

	public static void init()
	{
//		@SuppressWarnings("unchecked")
//		Registry<DimensionType> reg = (Registry<DimensionType>) BuiltinRegistries.REGISTRY.getOrThrow(Registry.DIMENSION_TYPE_REGISTRY);
//		((WritableRegistry<DimensionType>)reg).register(MENELAUS_LOCATION, DEFAULT_MENELAUS, Lifecycle.stable());

		NoiseGeneratorSettings.register(SETTING_MENELAUS, menelaus(false, false));
		NoiseGeneratorSettings.register(SETTING_TYROS, tyros(false, false));

//		RegistryAccess registryaccess = RegistryAccess.BUILTIN.get();
//		WritableRegistry<LevelStem> writableregistry = new MappedRegistry<>(Registry.LEVEL_STEM_REGISTRY, Lifecycle.experimental(), (Function<LevelStem, Holder.Reference<LevelStem>>)null);
//		ChunkGenerator chunkgenerator = WorldGenSettings.makeDefaultOverworld(registryaccess, 0L, false);
//		Registry<LevelStem> registry1 = WorldGenSettings.withOverworld(registryaccess.ownedRegistryOrThrow(Registry.DIMENSION_TYPE_REGISTRY), registry, chunkgenerator);
	}

	public static NoiseGeneratorSettings menelaus(boolean amplified, boolean large_biomes)
	{
		NoiseSettings noisesettings = NoiseSettings.create(0, 256, new NoiseSamplingSettings(1.0D, 1.0D, 80.0D, 160.0D), new NoiseSlider(-0.078125D, 2, amplified ? 0 : 8), new NoiseSlider(amplified ? 0.4D : 0.1171875D, 3, 0), 1, 2, TerrainProvider.overworld(amplified));
		   ;
		return new NoiseGeneratorSettings(noisesettings, TerrainBlocks.stone_m.defaultBlockState(), Blocks.WATER.defaultBlockState(), NoiseRouterData.overworld(noisesettings, large_biomes), FPSurfaceRules.menelaus(), 63, false, true, true, false);
	}

	public static NoiseGeneratorSettings tyros(boolean amplified, boolean large_biomes)
	{
		NoiseSettings noisesettings = NoiseSettings.create(0, 256, new NoiseSamplingSettings(1.0D, 1.0D, 80.0D, 160.0D), new NoiseSlider(-0.078125D, 2, amplified ? 0 : 8), new NoiseSlider(amplified ? 0.4D : 0.1171875D, 3, 0), 1, 2, TerrainProvider.overworld(amplified));
		   ;
		return new NoiseGeneratorSettings(noisesettings, Blocks.STONE.defaultBlockState(), Blocks.WATER.defaultBlockState(), NoiseRouterData.overworld(noisesettings, large_biomes), FPSurfaceRules.menelaus(), 63, false, true, true, false);
	}

}
