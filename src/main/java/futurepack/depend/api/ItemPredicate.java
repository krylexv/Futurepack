package futurepack.depend.api;

import java.util.List;

import futurepack.api.ItemPredicateBase;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;

public class ItemPredicate extends ItemPredicateBase
{
	private final Item item;
	private final int size;
	
	public ItemPredicate(Item item, int size)
	{
		this.item = item;
		this.size = size;
	}
	
	public ItemPredicate(Item item)
	{
		this(item, 1);
	}
	
	public ItemPredicate(ItemStack item)
	{
		this.item = item.getItem();
		this.size = item.getCount();
	}

	@Override
	public boolean apply(ItemStack input)
	{
		return input!=null && input.getItem() == item;
	}

	@Override
	public ItemStack getRepresentItem()
	{
		return new ItemStack(item, size);
	}

	@Override
	public int getMinimalItemCount(ItemStack input)
	{
		return size;
	}
	
	@Override
	public List<ItemStack> collectAcceptedItems(List<ItemStack> list)
	{
		list.add(new ItemStack(item, size));
		return list;
	}
	
	@Override
	public String toString()
	{
		return HelperItems.getRegistryName(item).toString() + "@" + size;
	}
	
	public void write(FriendlyByteBuf buf)
	{
		buf.writeItem(new ItemStack(item, size));
	}
	
	public static ItemPredicate read(FriendlyByteBuf buf)
	{
		return new ItemPredicate(buf.readItem());
	}
}
