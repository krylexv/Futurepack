//package futurepack.depend.api.main;
//
//import java.util.concurrent.Callable;
//
//import futurepack.api.interfaces.tilentity.ITileXpStorage;
//import net.minecraft.core.Direction;
//import net.minecraft.nbt.IntTag;
//import net.minecraft.nbt.Tag;
//import net.minecraftforge.common.capabilities.Capability;
//import net.minecraftforge.common.capabilities.Capability.IStorage;
//
//public class CapabilityXp implements ITileXpStorage
//{
//	
//	public static class Factory implements Callable<ITileXpStorage>
//	{
//		@Override
//		public ITileXpStorage call() throws Exception 
//		{
//			return new CapabilityXp();
//		}
//		
//	}
//	
//	public static class Storage implements IStorage<ITileXpStorage>
//	{
//
//		@Override
//		public Tag writeNBT(Capability<ITileXpStorage> capability, ITileXpStorage instance, Direction side)
//		{
//			return IntTag.valueOf(instance.getXp());
//		}
//
//		@Override
//		public void readNBT(Capability<ITileXpStorage> capability, ITileXpStorage instance, Direction side, Tag nbt)
//		{
//			instance.setXp( ((IntTag)nbt).getAsInt() );
//		}
//		
//	}
//	
//	private int xp = 0;
//	
//	@Override
//	public int getXp()
//	{
//		return xp;
//	}
//
//	@Override
//	public int getMaxXp()
//	{
//		return 100;
//	}
//
//	@Override
//	public void setXp(int lvl)
//	{
//		xp = lvl;
//	}
//
//}
