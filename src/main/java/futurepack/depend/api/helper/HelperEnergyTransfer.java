package futurepack.depend.api.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

import com.google.common.base.Supplier;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.FacingUtil;
import futurepack.api.LogisticStorage;
import futurepack.api.ParentCoords;
import futurepack.api.capabilities.CapabilityLogistic;
import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.CapabilitySupport;
import futurepack.api.capabilities.IEnergyStorageBase;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.api.capabilities.ISupportStorage;
import futurepack.api.interfaces.IBlockSelector;
import futurepack.api.interfaces.IBlockValidator;
import futurepack.common.FPBlockSelector;
import futurepack.common.FPSelectorHelper;
import futurepack.common.block.logistic.LogisticFluidWrapper;
import futurepack.common.block.logistic.LogisticNeonEnergyWrapper;
import futurepack.common.block.logistic.LogisticSupportPointsWrapper;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.DustParticleOptions;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.material.Material;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.capability.IFluidHandler;

public class HelperEnergyTransfer
{
	public static WeakHashMap<Capability<? extends IEnergyStorageBase>, IBlockSelector> blockSelectors = new WeakHashMap<>();
	public static WeakHashMap<Capability<? extends IEnergyStorageBase>, IBlockValidator> blockValidators = new WeakHashMap<>();
	
	public static void spreadEnergyBase(Level w, BlockPos pos, ICapabilityProvider provider, Capability<? extends IEnergyStorageBase> cap)
	{
		if(w==null)
		{
			System.err.println("the world is null");
			return;
		}
		
		if(w.isClientSide)
			return;
		
		boolean onlywire = true;
		List<EnergyObject> valid = new ArrayList<EnergyObject>(6);
		Set<IEnergyStorageBase> sources = new HashSet<IEnergyStorageBase>();
		for(Direction dir : FacingUtil.VALUES)
		{
			BlockPos xyz = pos.relative(dir);
			
			BlockEntity tile = w.getBlockEntity(xyz);
			if(tile != null && tile.getLevel()!=null)
			{
				LazyOptional<? extends IEnergyStorageBase> optT = tile.getCapability(cap, dir.getOpposite());
				LazyOptional<? extends IEnergyStorageBase> optP = provider.getCapability(cap, dir);
				
				if(optT.isPresent() && optP.isPresent())
				{
					IEnergyStorageBase from = optP.orElseThrow(null);
					IEnergyStorageBase to = optT.orElseThrow(null);
					
					if(from!=null && from.getType() == EnumEnergyMode.WIRE)
						sources.add(from);
					
					if(to.getType()==EnumEnergyMode.NONE || from.getType()==EnumEnergyMode.NONE)
						continue;
					if(to.get() >= to.getMax())
						continue;
					
					if(from.getType() == EnumEnergyMode.WIRE && to.getType().getPriority() <= from.getType().getPriority())
						continue;
					
					if(from.canTransferTo(to) && to.canAcceptFrom(from))
					{
						valid.add(new EnergyObject(from, to));
						onlywire = onlywire && to.getType() == EnumEnergyMode.WIRE;
					}
				}
			}
		}
		
		if(onlywire)
		{
			for(IEnergyStorageBase base : sources)
			{
				if(base.get() >= base.getMax()/2)
				{
					spreadPower(w, pos, base, cap);
				}
			}
//			return;
		}
		
		if(!valid.isEmpty())
		{
			valid.sort(new Comparator<EnergyObject>()
			{
				@Override
				public int compare(EnergyObject o1, EnergyObject o2)
				{
					return o2.to.getType().getPriority() - o1.to.getType().getPriority();
				}
				
			});
			
			double d = 1D / valid.size();
			valid.forEach(e -> e.transfer(d));
		}
	}
	
	public static void powerLowestBlock(BlockEntity tile)
	{
		spreadEnergyBase(tile.getLevel(), tile.getBlockPos(), tile, CapabilityNeon.cap_NEON);
	}
	
	public static void sendSupportPoints(BlockEntity tile)
	{
		spreadEnergyBase(tile.getLevel(), tile.getBlockPos(), tile, CapabilitySupport.cap_SUPPORT);
	}
	
	private static class EnergyObject
	{
		IEnergyStorageBase from, to;

		public EnergyObject(IEnergyStorageBase from, IEnergyStorageBase to) {
			super();
			this.from = from;
			this.to = to;
		}
		
		public void transfer(double factor)
		{
			from.use(to.add((int) (factor * from.get())));
		}

		@Override
		public String toString() 
		{
			return from +  " -> " + to;
		}
	}
	
	
	private static class WireSelector implements IBlockSelector
	{
		public final Capability<? extends IEnergyStorageBase> cap;
		
		public WireSelector(Capability<? extends IEnergyStorageBase> cap)
		{
			super();
			this.cap = cap;
		}

		@Override
		public boolean isValidBlock(Level w, BlockPos pos, Material m, boolean dia, ParentCoords parent)
		{
			if(dia)
				return false;
			BlockEntity t = w.getBlockEntity(pos);
			
			if(t==null || !t.hasLevel())
				return false;
			
			Direction face = FacingUtil.getSide(parent, pos);
			
			LazyOptional<? extends IEnergyStorageBase> optT = t.getCapability(cap, face.getOpposite());
			
			if(optT.isPresent())
			{
				IEnergyStorageBase store = optT.orElseThrow(null);
				if(store.getType() == EnumEnergyMode.NONE)
					return false;
				
				BlockEntity p = w.getBlockEntity(parent);
				LazyOptional<? extends IEnergyStorageBase> optP = p.getCapability(cap, face);
				if(optP.isPresent())
				{
					IEnergyStorageBase before =  optP.orElseThrow(null);
					
					if(before.canTransferTo(store) && store.canAcceptFrom(before))
					{
						return true;
					}
				}

			}
			
			return false;
		}
		
		@Override
		public boolean canContinue(Level w, BlockPos pos, Material m, boolean dia, ParentCoords parent) 
		{
			Direction face = FacingUtil.getSide(parent, pos);
			BlockEntity t = w.getBlockEntity(pos);
			IEnergyStorageBase store = t.getCapability(cap, face.getOpposite()).orElseThrow(null);
			
			if(store.getType() == EnumEnergyMode.WIRE)
				return true;
			
			return false;
		}
	}

	private static class EnergyUser implements IBlockValidator
	{
		public final Capability<? extends IEnergyStorageBase> cap;
		
		final boolean debug;
		
		public EnergyUser(Capability<? extends IEnergyStorageBase> cap)
		{
			super();
			this.cap = cap;
			
			debug = true;
		}
		
		@Override
		public boolean isValidBlock(Level w, ParentCoords pos) 
		{	
			BlockPos parent = pos.getParent();
			if(parent!=null)
			{
				BlockEntity p = w.getBlockEntity(parent);
				BlockEntity tile = w.getBlockEntity(pos);
				if(p != null && tile != null)
				{
					Direction dir = FacingUtil.getSide(parent, pos);
					
					LazyOptional<? extends IEnergyStorageBase> optT = tile.getCapability(cap,  dir.getOpposite());
					
					if(optT.isPresent())
					{
						IEnergyStorageBase neon = optT.orElseThrow(null);
						if(neon.getType() != EnumEnergyMode.USE)
							return false;
						
						if(neon.get() >= neon.getMax())
							return false;
						
						IEnergyStorageBase wire = p.getCapability(cap, dir).orElseThrow(null);
						if(wire.canTransferTo(neon) && neon.canAcceptFrom(wire))
						{
							return true;
						}
						else if(debug)
						{
							w.addParticle(ParticleTypes.NOTE, pos.getX()+0.5, pos.getY()+0.7, pos.getZ()+0.5, 0, 0.1, 0);
						}
					}
				}
			}
			return false;
		}
	}
	
	public static final int MIN_WIRE_CAPACITY = 500; //this is used to prevent unneeded calculations 
	
	public static void spreadPower(Level w, BlockPos pos, IEnergyStorageBase base, Capability<? extends IEnergyStorageBase> cap)
	{
		boolean debug = false;
		
		final boolean runnable = base instanceof Runnable;
		
		if(base.get() > 0)
		{
			if(!w.isClientSide)
			{			
				if(debug)
				{
					((ServerLevel)w).sendParticles( DustParticleOptions.REDSTONE, pos.getX()+0.5,  pos.getY()+1.1,  pos.getZ()+0.5, 5, 0.0F, 0.0F, 0.0F, 0.0F);
				}
				
				FPBlockSelector sel = FPSelectorHelper.getSelectorSave(w, pos, blockSelectors.computeIfAbsent(cap, WireSelector::new), true);//TileEntity only in mianthread
				
				Collection<ParentCoords> coords = sel.getValidBlocks(blockValidators.computeIfAbsent(cap, EnergyUser::new));
				
				if(coords.isEmpty())
				{
					if(debug)
					{
						((ServerLevel)w).sendParticles(ParticleTypes.SMOKE, pos.getX()+0.5,  pos.getY()+0.5,  pos.getZ()+0.5, 5, 0.0F, 0.0F, 0.0F, 0.0F);
					}
					return;
				}
				else if(debug)
				{
					for(ParentCoords f : coords)
					{
						((ServerLevel)w).sendParticles(ParticleTypes.CLOUD, f.getX()+0.5,  f.getY()+1.5,  f.getZ()+0.5, 5, 0.0F, 0.0F, 0.0F, 0.0F);
					}
				}
				
				int E_per_Block = base.get() / coords.size();//energy per block
				if(E_per_Block <= 2)
				{
					int max = base.getMax() / coords.size();
					if(max <= 2)
					{
						E_per_Block = 3;
					}
				}
				if(E_per_Block > 2)
				{
 					for(ParentCoords machine : coords)
					{
						if(base.get()<=0)
							break;
						
						int amount = E_per_Block;
						if(amount > MIN_WIRE_CAPACITY)
						{
							ParentCoords wire = machine.getParent();
							BlockPos last = machine;
							while(wire!=null)
							{	
								Direction face = FacingUtil.getSide(wire, last);
								BlockEntity t = w.getBlockEntity(wire);
								if(t!=null)
								{
									LazyOptional<? extends IEnergyStorageBase> optT = t.getCapability(cap, face);
									if(optT.isPresent())
									{
										IEnergyStorageBase tileWire = optT.orElseThrow(null);
										amount = Math.min(amount, tileWire.getMax());
										if(amount<= MIN_WIRE_CAPACITY)
											break;
									}
									else
									{
										break;
									}
								}
								else
									break;
								
								last = wire;
								wire = wire.getParent();
							}
									
						}
						
						Direction face = FacingUtil.getSide(machine.getParent(), machine);
						IEnergyStorageBase tileMachine = w.getBlockEntity(machine).getCapability(cap, face.getOpposite()).orElseThrow(null);
						
						int needed = tileMachine.getMax() - tileMachine.get();
						amount = Math.min(base.get(), amount);
						int transfer = Math.min(amount, needed);
						if(transfer > 0)
						{
							base.use(tileMachine.add(transfer));
							
							if(debug)
								((ServerLevel)w).sendParticles(ParticleTypes.FIREWORK, machine.getX()+0.5,  machine.getY()+2,  machine.getZ()+0.5, 3, 0.0F, 0.0F, 0.0F, 0.0F);
							
							if(runnable)
							{
								ParentCoords c = machine;
								while(c.getParent()!=null)
								{
									Direction facing = FacingUtil.getSide(c.getParent(), c);
									BlockEntity t = w.getBlockEntity(c);
									String crash = "tile had no capability! tile " +  t + " @ " + c.toString();
									Runnable run = (Runnable) t.getCapability(cap, facing).orElseThrow(() -> new NullPointerException(crash + ";" + t.getCapability(CapabilityLogistic.cap_LOGISTIC, facing)
																																						.map(l -> l.isTypeSupported(EnumLogisticType.SUPPORT) ? ("support is mode "+ l.getMode(EnumLogisticType.SUPPORT).name()) : "'support' not used" ).orElse("no logistic support")));
									run.run();
									c = c.getParent();
								}
								((Runnable)base).run();
								
							}
							
						}
					}	
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public static <T> LazyOptional<T> getNeonCap(LazyOptional<INeonEnergyStorage>[] neonOpt, Direction side, Supplier<LogisticStorage> getLog, Supplier<INeonEnergyStorage> power)
	{
		if(neonOpt[side.get3DDataValue()] != null)
		{
			return (LazyOptional<T>) neonOpt[side.get3DDataValue()];
		}
		else
		{
			if(getLog.get().getModeForFace(side, EnumLogisticType.ENERGIE) == EnumLogisticIO.NONE)
			{
				return LazyOptional.empty();
			}
			else
			{
				neonOpt[side.get3DDataValue()] = LazyOptional.of(() -> new LogisticNeonEnergyWrapper(getLog.get().getInterfaceforSide(side), power.get()));
				neonOpt[side.get3DDataValue()].addListener(p -> neonOpt[side.get3DDataValue()]=null);
				return (LazyOptional<T>) neonOpt[side.get3DDataValue()];
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public static <T> LazyOptional<T> getSupportCap(LazyOptional<ISupportStorage>[] supportOpt, Direction side, Supplier<LogisticStorage> getLog, Supplier<ISupportStorage> support)
	{
		if(supportOpt[side.get3DDataValue()] != null)
		{
			return (LazyOptional<T>) supportOpt[side.get3DDataValue()];
		}
		else
		{
			if(getLog.get().getModeForFace(side, EnumLogisticType.SUPPORT) == EnumLogisticIO.NONE)
			{
				return LazyOptional.empty();
			}
			else
			{
				supportOpt[side.get3DDataValue()] = LazyOptional.of(() -> new LogisticSupportPointsWrapper(getLog.get().getInterfaceforSide(side), support.get()));
				supportOpt[side.get3DDataValue()].addListener(p -> supportOpt[side.get3DDataValue()] = null);
				return (LazyOptional<T>) supportOpt[side.get3DDataValue()];
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public static <T> LazyOptional<T> getFluidCap(LazyOptional<IFluidHandler>[] fluidOpt, Direction side, Supplier<LogisticStorage> getLog, Supplier<IFluidHandler> fluid)
	{
		if(fluidOpt[side.get3DDataValue()] != null)
		{
			return (LazyOptional<T>) fluidOpt[side.get3DDataValue()];
		}
		else
		{
			if(getLog.get().getModeForFace(side, EnumLogisticType.FLUIDS) == EnumLogisticIO.NONE)
			{
				return LazyOptional.empty();
			}
			else
			{
				fluidOpt[side.get3DDataValue()] = LazyOptional.of(() -> new LogisticFluidWrapper(getLog.get().getInterfaceforSide(side), fluid.get()));
				fluidOpt[side.get3DDataValue()].addListener(p -> fluidOpt[side.get3DDataValue()] = null);
				return (LazyOptional<T>) fluidOpt[side.get3DDataValue()];
			}
		}
	}
	
	public static void invalidateCaps(LazyOptional<?>...caps)
	{
		for(LazyOptional<?> lo : caps)
			if(lo!=null)
				lo.invalidate();
	}
}
