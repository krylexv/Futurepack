package futurepack.depend.api.helper;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.IntFunction;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Registry;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.registries.IForgeRegistryEntry;

public class HelperSerialisation
{
	
	public static void putBlockPos(CompoundTag nbt, String name, BlockPos pos)
	{
		nbt.putIntArray(name, new int[] {pos.getX(), pos.getY(), pos.getZ()});
	}
	
	public static BlockPos getBlockPos(CompoundTag nbt, String name)
	{
		int[] arr = nbt.getIntArray(name);
		return new BlockPos(arr[0], arr[1], arr[2]);
	}
	
	public static class Factory<T extends IForgeRegistryEntry<T>>
	{
		public final Registry<T> registry;
		
		private final HashMap<T, Integer> map = new HashMap<T, Integer>();
		private int entrys = 0;
		private final List<Integer> baked = new IntArrayList();
		
		public Factory(Registry<T> registry)
		{
			this.registry = registry;
		}
		
		public void add(T object)
		{
			baked.add(get(object));
		}
		
		public int get(T obj)
		{
			if(obj==null)
				throw new NullPointerException();
			
			return map.computeIfAbsent(obj, t -> { return entrys++;});
		}
		
		public CompoundTag store()
		{
			CompoundTag nbt = new CompoundTag();
			String[] s = new String[entrys];
			for(Entry<T, Integer> e : map.entrySet())
			{
				s[e.getValue()] = registry
						.getKey(e.getKey())
						.toString();
			}
			StringBuilder bld = new StringBuilder();
			boolean a = false;
			for(String ss : s)
			{
				if(a)
					bld.append(';');
				bld.append(ss);
				a = true;
			}
			nbt.putString("map", bld.toString());
			bld = null;
			s = null;
			
			final EnumDataType type = EnumDataType.getTypeFromSize(entrys);
			final ByteBuffer buf = ByteBuffer.allocate(1 + type.byteSize(baked.size())).order(ByteOrder.LITTLE_ENDIAN);
			buf.put((byte) type.ordinal());
			baked.forEach(i -> type.write(buf, i));
			nbt.putByteArray("B", buf.array());			
			return nbt;
		}
		
		public T[] load(CompoundTag nbt, IntFunction<T[]> arrayCreator)
		{
			HashMap<Integer, T> map = new HashMap<>();
			
			String[] s = nbt.getString("map").split(";");
			for(int i=0;i<s.length;i++)
			{
				map.put(i, registry.get(new ResourceLocation(s[i])));
			}
			
			byte[] bb = nbt.getByteArray("B");
			ByteBuffer buf = ByteBuffer.wrap(bb).order(ByteOrder.LITTLE_ENDIAN);
			byte typeID = buf.get();
			EnumDataType type = EnumDataType.values()[typeID];
			int length = type.normalSize(bb.length - 1);
			T[] tt = arrayCreator.apply(length);
			
			for(int i=0;i<length;i++)
			{
				tt[i] = map.get(type.read(buf));
			}
			
			return tt;
		}
		
	}
	
	public enum EnumDataType
	{
		BYTE(
				(b,i)->b.put( (byte)(i + Byte.MIN_VALUE)),
				(b) -> b.get() - Byte.MIN_VALUE),
		SHORT(
				(b,i)->b.putShort( (short)(i + Short.MIN_VALUE)),
				(b) -> b.getShort() - Short.MIN_VALUE),
		INTEGER(
				ByteBuffer::putInt,
				ByteBuffer::getInt);
		
		private final BiConsumer<ByteBuffer, Integer> write;
		private final Function<ByteBuffer, Integer> read;
		
		EnumDataType(BiConsumer<ByteBuffer, Integer> write, Function<ByteBuffer, Integer> read)
		{
			this.write = write;
			this.read = read;
		}
		
		public void write(ByteBuffer buf, int i)
		{
			write.accept(buf, i);
		}
		
		public int read(ByteBuffer buf)
		{
			return read.apply(buf);
		}
		
		public static EnumDataType getTypeFromSize(int size)
		{
			if(size <= 255)
				return EnumDataType.BYTE;
			else if(size <= 65535)
			{
				return EnumDataType.SHORT;
			}
			else
				return EnumDataType.INTEGER;
		}
		
		public int bytes()
		{
			switch(this)
			{
			case BYTE:
				return Byte.BYTES;
			case SHORT:
				return Short.BYTES;
			case INTEGER:
				return Integer.BYTES;
			default:
				throw new IllegalArgumentException(this.name());
			}
		}
		
		public int byteSize(int size)
		{
			return size * bytes();
		}
		
		public int normalSize(int byteSize)
		{
			return byteSize / bytes();
		}
	}
}
