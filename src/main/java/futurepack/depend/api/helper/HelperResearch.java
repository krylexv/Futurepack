package futurepack.depend.api.helper;

import futurepack.common.FPConfig;
import futurepack.common.research.CustomPlayerData;
import net.minecraft.ChatFormatting;
import net.minecraft.Util;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class HelperResearch
{
//	public static boolean isUseable(EntityPlayer pl, Object o)
//	{
//		return true;
//	}
//	
	public static boolean isUseable(Player pl, Block o)
	{
		return isUseable(pl, new ItemStack(o));
	}
	
	public static boolean isUseable(Player pl, Item o)
	{
		return isUseable(pl, new ItemStack(o));
	}
	
	public static boolean isUseable(Player pl, BlockEntity o)
	{
		return isUseable(pl, o.getLevel().getBlockState(o.getBlockPos()));
	}
	
	public static boolean isUseable(Player pl, BlockState o)
	{
		return isUseable(pl, new ItemStack(o.getBlock()));
	}
	
	public static boolean isUseable(Player pl, ItemStack o)
	{
		CustomPlayerData cd = CustomPlayerData.getDataFromPlayer(pl);	
		return cd.canProduce(o);
	}
	
	public static boolean canOpen(Player pl, BlockState bl)
	{
		if(pl.level.isClientSide)
		{
			return false;
		}
		else if(isUseable(pl, bl) || FPConfig.SERVER.disableMachineLock.get())
		{
			return true;
		}
		else
		{
			TranslatableComponent trans = new TranslatableComponent("research.useblock.missing", "");
			Style style = Style.EMPTY;
			style.withColor(ChatFormatting.RED);
			trans.setStyle(style);
			pl.sendMessage(trans, Util.NIL_UUID);
			return false;
		}
	}

	public static boolean canOpen(Player pl, Entity entity)
	{
		return true;
	}
}
