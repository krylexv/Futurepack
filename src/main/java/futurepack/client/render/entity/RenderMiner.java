package futurepack.client.render.entity;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Vector3f;

import futurepack.api.Constants;
import futurepack.common.entity.EntityMiner;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BeaconRenderer;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;

//TODO: irgendwas Bug rum wenn man den Mine rund das Waser anzeiht, das wasser wird irgendwie heller
public class RenderMiner extends EntityRenderer<EntityMiner>
{
	public RenderMiner(EntityRendererProvider.Context m)
	{
		super(m);
		model = new ModelMiner(m.bakeLayer(ModelMiner.LAYER_LOCATION));
	}

	ModelMiner model;
	
	@Override
	public void render(EntityMiner miner, float entityYaw, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn) 
	{
		miner.getCommandSenderWorld().getProfiler().push("renderMiner");
		super.render( miner, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
		
		matrixStackIn.pushPose();
		matrixStackIn.translate(0.0, 0.3, 0.0);
		matrixStackIn.mulPose(Vector3f.ZP.rotationDegrees(180F));
		matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(180F + miner.getYRot()));
		
		VertexConsumer builder = bufferIn.getBuffer(model.renderType(this.getTextureLocation( miner)));
		
		model.setupAnim(miner, packedLightIn, packedLightIn, entityYaw, partialTicks, packedLightIn);
		
		model.renderToBuffer(matrixStackIn, builder, packedLightIn, OverlayTexture.NO_OVERLAY, 1F, 1F, 1F, 1F);
		
		float scale = 1F/4F;
		matrixStackIn.scale(scale, scale, scale);
		matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(-90F));
		
		matrixStackIn.translate(0.0, 0.5, 0);
		renderBeamSegment(matrixStackIn, bufferIn, partialTicks, miner.level.getGameTime()*2, 0, (int) (miner.dis * 8), new float[] {1F, 0.125F, 0.625F});
		matrixStackIn.translate(-1, 0, 0);
		renderBeamSegment(matrixStackIn, bufferIn, partialTicks, miner.level.getGameTime()*-2, 0, (int) (miner.dis * 8), new float[] {1F, 0.125F, 0.125F});
		
		matrixStackIn.popPose();
		miner.getCommandSenderWorld().getProfiler().pop();
	}
	
	public static void renderBeamSegment(PoseStack matrixStackIn, MultiBufferSource bufferIn, float partialTicks, long totalWorldTime, int yOffset, int height, float[] colors) 
	{
		BeaconRenderer.renderBeaconBeam(matrixStackIn, bufferIn, BeaconRenderer.BEAM_LOCATION, partialTicks, 1.0F, totalWorldTime, yOffset, height, colors, 0.2F, 0.25F);
	}
	
	@Override
	public ResourceLocation getTextureLocation(EntityMiner p_110775_1_) 
	{
		return new ResourceLocation(Constants.MOD_ID, "textures/model/miner.png");
	}
}
