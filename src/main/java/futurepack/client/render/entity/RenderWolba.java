package futurepack.client.render.entity;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.common.entity.living.EntityWolba;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.resources.ResourceLocation;

public class RenderWolba extends net.minecraft.client.renderer.entity.MobRenderer<EntityWolba, ModelWolba>
{
	private static final ResourceLocation tex = new ResourceLocation(Constants.MOD_ID,"textures/entity/wolba.png");    
    
    public RenderWolba(EntityRendererProvider.Context m)
    {
        super(m, new ModelWolba(m.bakeLayer(ModelWolba.LAYER_LOCATION)), 0.75F);
    }

    @Override
    public void render(EntityWolba entityIn, float entityYaw, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn) 
    {
    	matrixStackIn.pushPose();
    	matrixStackIn.translate(0, -0.5, 0);
    	if(entityIn.isBaby()) {
    		matrixStackIn.scale(0.9f, 0.9f, 0.9f);
    	}
    	else {
    		matrixStackIn.scale(1.5f, 1.5f, 1.5f);
    		matrixStackIn.translate(0, -0.18, 0);
    	}
    	super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
    	matrixStackIn.popPose();
    }
    
    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    @Override
	public ResourceLocation getTextureLocation(EntityWolba p_110775_1_)
    {
        return tex;
    }
}
