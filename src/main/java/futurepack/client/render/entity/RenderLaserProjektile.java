package futurepack.client.render.entity;

import com.mojang.math.Vector4f;

import futurepack.api.Constants;
import futurepack.client.render.RenderThrowable3DBase;
import futurepack.common.entity.throwable.EntityLaserProjectile;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.resources.ResourceLocation;

public class RenderLaserProjektile extends RenderThrowable3DBase<EntityLaserProjectile>
{

	public RenderLaserProjektile(EntityRendererProvider.Context renderManager)
	{
		super(renderManager);
	}

	@Override
    public ResourceLocation getTextureLocation(EntityLaserProjectile entity)
	{
		return new ResourceLocation(Constants.MOD_ID,"textures/model/lsgeschoss.png");
	}

	private final Vector4f side = new Vector4f(0, 0F, 7F/16F, 1F);
	
	@Override
	public Vector4f getUVForSides(EntityLaserProjectile entity) 
	{
		return side;
	}

	private final Vector4f front = new Vector4f(8F/16F, 0F, 15f/16F , 7f/16F);
	
	@Override
	public Vector4f getUVForFront(EntityLaserProjectile entity) 
	{
		return front;
	}

	@Override
	public float getWidth(EntityLaserProjectile entity) 
	{
		return 0.0625F * 3.5F;
	}

	@Override
	public float getDepth(EntityLaserProjectile entity) 
	{
		return 0.1875F;
	}
}
