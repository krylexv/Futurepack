package futurepack.client.render.entity;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Vector3f;

import futurepack.api.Constants;
import futurepack.common.entity.living.EntityDungeonSpider;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;

public class RenderDungeonSpider extends MobRenderer<EntityDungeonSpider, ModelDungeonSpider>
{	
	private static final ResourceLocation RES = new ResourceLocation(Constants.MOD_ID, "textures/entity/dungeon_spider.png");
	
    public RenderDungeonSpider(EntityRendererProvider.Context renderManagerIn)
    {
        super(renderManagerIn, new ModelDungeonSpider(renderManagerIn.bakeLayer(ModelDungeonSpider.LAYER_LOCATION)), 0.4F);
    }

    @Override
    public void render(EntityDungeonSpider entity, float entityYaw, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn) 
    {
    	entity.getCommandSenderWorld().getProfiler().push("renderDungeonSpider");
		matrixStackIn.pushPose();
		if(entity.isAttachedToWallOrCeiling())
		{
			Direction attach = entity.getAttachedDirection();
			Direction facing = entity.getFacingDirection();
			
			
			switch(attach)
			{
			case NORTH:
				matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(90));
				matrixStackIn.translate(0, -0.0625F * 4, 0);
				break;
			case SOUTH:
				matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(-90));
				matrixStackIn.translate(0, -0.0625F * 4, 0);
				break;
			case EAST:
				matrixStackIn.mulPose(Vector3f.ZP.rotationDegrees(90));
				matrixStackIn.translate(0, -0.0625F * 4, 0);
				break;
			case WEST:
				matrixStackIn.mulPose(Vector3f.ZP.rotationDegrees(-90));
				matrixStackIn.translate(0, -0.0625F * 4, 0);
				break;
			case UP:
				matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(180));
				matrixStackIn.translate(0, -0.0625F * 4, 0);
				break;
			case DOWN:
			default:
				break;
			}
			
			entityYaw = 0F;
			
			if(attach == Direction.WEST)
			{
				if(facing==Direction.SOUTH)
					entityYaw = 0;
				else if(facing == Direction.NORTH)
					entityYaw = 180;
				else if(facing == Direction.UP)
					entityYaw = 90F;
				else if (facing == Direction.DOWN)
					entityYaw = -90F;
				
			}
			else if(attach == Direction.SOUTH)
			{
				if(facing == Direction.EAST)
					entityYaw = -90;
				else if(facing == Direction.WEST)
					entityYaw = 90;
				else if(facing == Direction.UP)
					entityYaw=0F;
			}
			else if(attach == Direction.EAST)
			{
				if(facing == Direction.NORTH)
					entityYaw = 180;
				else if(facing == Direction.SOUTH)
					entityYaw = 0F;
				else if(facing == Direction.UP)
					entityYaw = 90F;
				else if (facing == Direction.DOWN)
					entityYaw = -90F;
			}
			else if(attach == Direction.NORTH)
			{
				if(facing == Direction.WEST)
					entityYaw = 90;
				else if(facing == Direction.EAST)
					entityYaw = -90;
				else if(facing == Direction.UP)
					entityYaw=0F;
			}
			else if(attach == Direction.UP)
			{
				entityYaw = facing.toYRot();
				if(facing == Direction.NORTH || facing == Direction.SOUTH)
				{
					entityYaw += 180F;
				}
				
			}
			else if(attach == Direction.DOWN)
			{
				entityYaw = facing.toYRot();
				
			}
			
//			entity.rotationYaw = entityYaw ;
//			entity.prevRenderYawOffset = entity.renderYawOffset;
			entity.yBodyRot = entityYaw;
			entity.yHeadRot = entity.yBodyRotO;
		}
		
//		
//		matrixStackIn.translate(0, 0.5, 0);
//		matrixStackIn.rotate(Vector3f.ZP.rotationDegrees(180F));
//		matrixStackIn.rotate(Vector3f.YP.rotationDegrees(entity.rotationYaw + 180F));
//		matrixStackIn.rotate(Vector3f.XP.rotationDegrees(entity.rotationPitch));
//		
//		IVertexBuilder builder = bufferIn.getBuffer(model.getRenderType(getEntityTexture(entity)));
//		
//		model.render(matrixStackIn, builder, packedLightIn, OverlayTexture.NO_OVERLAY, 1F, 1F, 1F, 1F);
//

		
		super.render(entity, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
		
		matrixStackIn.popPose();
		entity.getCommandSenderWorld().getProfiler().pop();
    }
    
    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    @Override
    public ResourceLocation getTextureLocation(EntityDungeonSpider entity)
    {
        return RES;
    }
}
