package futurepack.client.render.block;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.model.Model;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.resources.ResourceLocation;

public abstract class SegmentedBlockModel extends Model 
{
	
	private List<ModelPart> parts;

	public SegmentedBlockModel(Function<ResourceLocation, RenderType> renderTypeIn) 
	{
		super(renderTypeIn);
	}

	public abstract ModelPart[] getParts();
	
	@Override
	public void renderToBuffer(PoseStack matrixStackIn, VertexConsumer bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha) 
	{
		if(parts==null)
		{
			parts = Arrays.asList(getParts());
		}
		parts.forEach(m -> {
			m.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn, red, green, blue, alpha);
		});
	}

}
