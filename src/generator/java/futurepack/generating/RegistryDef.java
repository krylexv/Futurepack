package futurepack.generating;

import java.io.IOException;

public class RegistryDef 
{
	final String block_class;
	final String registry_name;
	final String varName;
	final String args;
	final String type;
	
	public RegistryDef(String type, String block_class, String args, String registry_name)
	{
		super();
		this.type = type;
		this.block_class = block_class;
		this.args = args;
		this.registry_name = registry_name;
		int l = registry_name.lastIndexOf(':');
		varName = registry_name.substring(l+1);
		
	}

	@Override
	public String toString()
	{
		String s = "\tpublic static final %s %s = new %s(%s).setRegistryName(\"%s\");\r\n";
		return String.format(s,type, getVariableName(), block_class, args, registry_name);
	}
	
	public String getVariableName()
	{
		return varName;
	}
	
	public String getRegistryeName()
	{
		return registry_name;
	}
	

	
	public static void writeRegistryDefs(ClassBuilder build, RegistryDef[] defs, String type)
	{
		build.imports.add("net.minecraftforge.event.RegistryEvent");
		build.imports.add("net.minecraftforge.registries.IForgeRegistry");
			
		build.methods.add(w -> {
			try
			{
				for(RegistryDef d : defs)
					w.write(d.toString());
				w.append("\r\n");
				w.flush();
			}
			catch(IOException e){
				e.printStackTrace();
			}
		});
		final String s = "	public static void register"+type+"s(RegistryEvent.Register<"+type+"> event)\r\n" + 
				"	{\r\n" + 
				"		IForgeRegistry<"+type+"> r = event.getRegistry();\r\n" + 
				"		\r\n";
		
		build.methods.add(w -> {
			
			try
			{
				w.write(s);
				for(RegistryDef d : defs)
					w.append("\t\tr.register(").append(d.getVariableName()).append(");\r\n");
				w.write("	}\r\n\r\n");
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		});
	}
}
