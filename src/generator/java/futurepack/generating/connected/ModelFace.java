package futurepack.generating.connected;

public class ModelFace {
	public String texture;
	public String cullface;
	public int rotation = 0;
	
	public ModelFace(String cullface) {
		this(cullface, 0);
	}
	
	public ModelFace(String cullface, int rotation) {
		this.texture = "#" + cullface;
		this.cullface = cullface;
		this.rotation = rotation;
	}
}
