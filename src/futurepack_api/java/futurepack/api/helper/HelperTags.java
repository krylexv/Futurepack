package futurepack.api.helper;

import java.util.stream.Stream;

import futurepack.api.interfaces.filter.IItem;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;

public class HelperTags 
{
	public static final HelperTags INSTANCE = new HelperTags();
	
	public boolean hasTag(Item item, ResourceLocation res)
	{
		return item.builtInRegistryHolder().containsTag(ItemTags.create(res));
	}
	
	public boolean hasTag(IItem iitem, String res)
	{
		Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(iitem.getRegisteredName()));
		if(item==null)
			return false;
		else
			return hasTag(item, new ResourceLocation(res));
	}
	
	public boolean isOre(IItem iitem)
	{
		return hasTag(iitem, "forge:ores");
	}
	
	public boolean isIngot(IItem iitem)
	{
		return hasTag(iitem, "forge:ingots");
	}
	
	public boolean isDust(IItem iitem)
	{
		return hasTag(iitem, "forge:dusts");
	}
	
//	private Map<Item, Set<Tag<Item>>> map;
//	private TagCollection<Item> mapBase;
//	
//	public Set<Tag<Item>> getTags(Item item)
//	{
//		if(mapBase != ItemTags.getAllTags())
//		{
//			map = new HashMap<Item, Set<Tag<Item>>>();
//			mapBase = ItemTags.getAllTags();
//		}
//		
//		return map.computeIfAbsent(item, this::createTagCollection);
//	}
//	
//	private Set<Tag<Item>> createTagCollection(Item item)
//	{
//		Set<Tag<Item>> set = new HashSet<Tag<Item>>();
//		for(Entry<ResourceLocation, Tag<Item>> e : mapBase.getAllTags().entrySet())
//		{
//			if(e.getValue().contains(item))
//			{
//				set.add(e.getValue());
//			}
//		}
//		return set;
//	}
//	
//	public Set<ResourceLocation> getTags(IItem iitem)
//	{
//		Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(iitem.getRegisteredName()));
//		if(item==null)
//			return Collections.EMPTY_SET;
//		
//		Collection<Tag<Item>> col = getTags(item);
//		return col.stream()
//				.filter(t -> t instanceof Named)
//				.map(t -> (Named) t)
//				.map(Named::getName).collect(Collectors.toSet());
//	}
	
	public static boolean areTagsLoaded()
	{
		try
		{
			return Items.COAL.builtInRegistryHolder().containsTag(ItemTags.COALS);
		}
		catch(IllegalStateException e)
		{
			return false;
		}
	}
	
	public static Stream<Item> getValues(TagKey<Item> item)
	{
		return ForgeRegistries.ITEMS.tags().getTag(item).stream();
	}

	public static Stream<Block> getValuesBlocks(TagKey<Block> oreTag) 
	{
		return ForgeRegistries.BLOCKS.tags().getTag(oreTag).stream();
	}

	public static boolean isBlock(Block b, TagKey<Block> tag) 
	{
		return is(ForgeRegistries.BLOCKS, b, tag);
	}
	
	public static <T extends IForgeRegistryEntry<T>> boolean is(IForgeRegistry<T> reg, T b, TagKey<T> tag) 
	{
		return reg.tags().getTag(tag).contains(b);
	}
	
}
