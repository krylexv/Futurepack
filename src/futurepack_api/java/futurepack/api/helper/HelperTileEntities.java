package futurepack.api.helper;

import javax.annotation.Nullable;

import futurepack.api.interfaces.tilentity.ITileClientTickable;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;

public class HelperTileEntities 
{
	@SuppressWarnings("unchecked")
	@Nullable
	public static <E extends BlockEntity, A extends BlockEntity> BlockEntityTicker<A> createTickerHelper(BlockEntityType<A> expectedType, BlockEntityType<E> type, BlockEntityTicker<? super E> ticker) 
	{
		return type == expectedType ? (BlockEntityTicker<A>)ticker : null;
	}
	
	@SuppressWarnings("deprecation")
	@Nullable
	public static <T extends BlockEntity & ITileClientTickable> BlockEntityTicker<T> createClientTicker(BlockEntityType<T> type)
	{
		return type == null ? null : (level,pos,state,tile) -> {tile.setBlockState(state);tile.tickClient(level, pos, state);};
	}
	
	@SuppressWarnings("deprecation")
	@Nullable
	public static <T extends BlockEntity & ITileServerTickable> BlockEntityTicker<T> createServerTicker(BlockEntityType<T> type)
	{
		return type == null ? null : (level,pos,state,tile) -> {tile.setBlockState(state);tile.tickServer(level, pos, state);};
	}
	
	@Nullable
	public static <E extends BlockEntity & ITileServerTickable, A extends BlockEntity> BlockEntityTicker<A> createServerTickerHelper(BlockEntityType<A> expectedType, BlockEntityType<E> type) 
	{
		return createTickerHelper(expectedType, type, createServerTicker(type));
	}
	
	@Nullable
	public static <E extends BlockEntity & ITileClientTickable, A extends BlockEntity> BlockEntityTicker<A> createClientTickerHelper(BlockEntityType<A> expectedType, BlockEntityType<E> type) 
	{
		return createTickerHelper(expectedType, type, createClientTicker(type));
	}
}
