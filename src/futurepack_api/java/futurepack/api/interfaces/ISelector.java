package futurepack.api.interfaces;

import java.util.Collection;

import javax.annotation.Nullable;

import futurepack.api.ParentCoords;
import net.minecraft.world.level.Level;

public interface ISelector
{
	/**
	 * It will iterate over every found Block and if the Block is valid it will add to the Collection.
	 * @param valid checks if the Block is valid or not. Can be null to get all BlockPositions 
	 * @return a Collection of all found ParentCoords.
	 */
	public Collection<ParentCoords> getValidBlocks(@Nullable IBlockValidator valid);
	
	/**
	 * @return the World this selector had used.
	 */
	public Level getWorld();
	
	
	public IStatisticsManager getStatisticsManager();
	
	
	public default Collection<ParentCoords> getAllBlocks()
	{
		return getValidBlocks(null);
	}
}
