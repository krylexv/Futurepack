// Made with Blockbench 4.0.0-beta.3
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


public class external_core_module<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("modid", "external_core_module"), "main");
	private final ModelPart base;

	public external_core_module(ModelPart root) {
		this.base = root.getChild("base");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition base = partdefinition.addOrReplaceChild("base", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition Stecksockel = base.addOrReplaceChild("Stecksockel", CubeListBuilder.create().texOffs(0, 17).addBox(-8.0F, -3.0F, -8.0F, 16.0F, 3.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Zentralkern = base.addOrReplaceChild("Zentralkern", CubeListBuilder.create().texOffs(0, 53).addBox(-5.0F, -11.0F, -5.0F, 10.0F, 8.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Stutze1 = base.addOrReplaceChild("Stutze1", CubeListBuilder.create().texOffs(0, 0).addBox(4.0F, -11.0F, -6.0F, 1.0F, 8.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Stutze2 = base.addOrReplaceChild("Stutze2", CubeListBuilder.create().texOffs(0, 0).addBox(-5.0F, -11.0F, -6.0F, 1.0F, 8.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Stutze3 = base.addOrReplaceChild("Stutze3", CubeListBuilder.create().texOffs(0, 0).addBox(4.0F, -11.0F, -6.0F, 1.0F, 8.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.5708F, 0.0F));

		PartDefinition Stutze4 = base.addOrReplaceChild("Stutze4", CubeListBuilder.create().texOffs(0, 0).addBox(-5.0F, -11.0F, -6.0F, 1.0F, 8.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.5708F, 0.0F));

		PartDefinition Stutze5 = base.addOrReplaceChild("Stutze5", CubeListBuilder.create().texOffs(0, 0).addBox(4.0F, -11.0F, -6.0F, 1.0F, 8.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -3.1416F, 0.0F));

		PartDefinition Stutze6 = base.addOrReplaceChild("Stutze6", CubeListBuilder.create().texOffs(0, 0).addBox(-5.0F, -11.0F, -6.0F, 1.0F, 8.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -3.1416F, 0.0F));

		PartDefinition Stutze7 = base.addOrReplaceChild("Stutze7", CubeListBuilder.create().texOffs(0, 0).addBox(-5.0F, -11.0F, -6.0F, 1.0F, 8.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.5708F, 0.0F));

		PartDefinition Stutze8 = base.addOrReplaceChild("Stutze8", CubeListBuilder.create().texOffs(0, 0).addBox(4.0F, -11.0F, -6.0F, 1.0F, 8.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.5708F, 0.0F));

		PartDefinition StatusLED1 = base.addOrReplaceChild("StatusLED1", CubeListBuilder.create().texOffs(40, 0).addBox(5.0F, -11.0F, -1.5F, 2.0F, 6.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition StatusLED2 = base.addOrReplaceChild("StatusLED2", CubeListBuilder.create().texOffs(40, 0).addBox(5.0F, -11.0F, -1.5F, 2.0F, 6.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -3.1416F, 0.0F));

		PartDefinition Kerndeckplatte = base.addOrReplaceChild("Kerndeckplatte", CubeListBuilder.create().texOffs(0, 0).addBox(-6.0F, -14.0F, -6.0F, 12.0F, 3.0F, 12.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition DeckplatteKuhler = base.addOrReplaceChild("DeckplatteKuhler", CubeListBuilder.create().texOffs(0, 37).addBox(-4.0F, -15.0F, -7.0F, 8.0F, 1.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Deckplatte1 = base.addOrReplaceChild("Deckplatte1", CubeListBuilder.create().texOffs(44, 37).addBox(-4.0F, -14.0F, -7.0F, 8.0F, 11.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -3.1416F, 0.0F));

		PartDefinition Deckplatte2 = base.addOrReplaceChild("Deckplatte2", CubeListBuilder.create().texOffs(44, 37).addBox(-4.0F, -14.0F, -7.0F, 8.0F, 11.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Khulerblock = base.addOrReplaceChild("Khulerblock", CubeListBuilder.create().texOffs(0, 71).addBox(-3.0F, -16.0F, -6.0F, 6.0F, 1.0F, 12.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 64, 128);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		base.render(poseStack, buffer, packedLight, packedOverlay);
	}
}