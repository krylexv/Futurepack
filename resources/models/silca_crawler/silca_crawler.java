// Made with Blockbench 4.0.0-beta.3
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


public class silca_crawler<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("modid", "silca_crawler"), "main");
	private final ModelPart silca_crawler;

	public silca_crawler(ModelPart root) {
		this.silca_crawler = root.getChild("silca_crawler");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition silca_crawler = partdefinition.addOrReplaceChild("silca_crawler", CubeListBuilder.create().texOffs(0, 0).addBox(-3.0F, -3.0F, -3.0F, 6.0F, 6.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 20.0F, 0.0F));

		PartDefinition head = silca_crawler.addOrReplaceChild("head", CubeListBuilder.create().texOffs(32, 4).addBox(-4.0F, -4.0F, -8.0F, 8.0F, 8.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, -3.0F));

		PartDefinition teeth_2 = head.addOrReplaceChild("teeth_2", CubeListBuilder.create(), PartPose.offsetAndRotation(3.0F, 3.0F, -8.0F, 0.0F, 0.0F, 0.0F));

		PartDefinition teeth_2_r1 = teeth_2.addOrReplaceChild("teeth_2_r1", CubeListBuilder.create().texOffs(37, 21).addBox(0.0F, -1.0F, -3.01F, 2.0F, 2.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -3.1416F));

		PartDefinition teeth_1 = head.addOrReplaceChild("teeth_1", CubeListBuilder.create().texOffs(37, 21).addBox(0.0F, -1.0F, -3.01F, 2.0F, 2.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(-4.0F, 3.0F, -8.0F));

		PartDefinition back = silca_crawler.addOrReplaceChild("back", CubeListBuilder.create().texOffs(0, 12).addBox(-5.0F, -4.0F, 0.0F, 10.0F, 8.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 3.0F));

		PartDefinition tail = back.addOrReplaceChild("tail", CubeListBuilder.create().texOffs(65, 5).addBox(-4.0F, -3.0F, 0.0F, 8.0F, 6.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 8.0F));

		PartDefinition leg_l_1 = silca_crawler.addOrReplaceChild("leg_l_1", CubeListBuilder.create().texOffs(18, 0).addBox(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, 0.0F, -1.0F, 0.0F, 0.576F, 0.5236F));

		PartDefinition shin_l_1 = leg_l_1.addOrReplaceChild("shin_l_1", CubeListBuilder.create().texOffs(55, 0).addBox(0.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(15.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.576F));

		PartDefinition leg_r_1 = silca_crawler.addOrReplaceChild("leg_r_1", CubeListBuilder.create().texOffs(18, 0).addBox(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-4.0F, 0.0F, -1.0F, 0.0F, -0.576F, -0.5236F));

		PartDefinition shin_r_1 = leg_r_1.addOrReplaceChild("shin_r_1", CubeListBuilder.create().texOffs(55, 0).addBox(-16.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-15.0F, 0.0F, 0.0F, 0.0F, 0.0F, -0.576F));

		PartDefinition leg_r_2 = silca_crawler.addOrReplaceChild("leg_r_2", CubeListBuilder.create().texOffs(18, 0).addBox(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-4.0F, 0.0F, 0.0F, 0.0F, -0.2793F, -0.5236F));

		PartDefinition shin_r_2 = leg_r_2.addOrReplaceChild("shin_r_2", CubeListBuilder.create().texOffs(55, 0).addBox(-16.0F, -1.0F, -0.5F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-15.0F, 0.0F, -0.5F, 0.0F, 0.0F, -0.576F));

		PartDefinition leg_r_3 = silca_crawler.addOrReplaceChild("leg_r_3", CubeListBuilder.create().texOffs(18, 0).addBox(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-4.0F, 0.0F, 1.0F, 0.0F, 0.2793F, -0.5236F));

		PartDefinition shin_r_3 = leg_r_3.addOrReplaceChild("shin_r_3", CubeListBuilder.create().texOffs(55, 0).addBox(-16.0F, -1.0F, -1.2F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-15.0F, 0.0F, 0.0F, 0.0F, 0.0F, -0.576F));

		PartDefinition leg_r_4 = silca_crawler.addOrReplaceChild("leg_r_4", CubeListBuilder.create().texOffs(18, 0).addBox(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-4.0F, 0.0F, 2.0F, 0.0F, 0.576F, -0.5236F));

		PartDefinition shin_r_4 = leg_r_4.addOrReplaceChild("shin_r_4", CubeListBuilder.create().texOffs(55, 0).addBox(-16.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-15.0F, 0.0F, 0.0F, 0.0F, 0.0F, -0.576F));

		PartDefinition leg_l_2 = silca_crawler.addOrReplaceChild("leg_l_2", CubeListBuilder.create().texOffs(18, 0).addBox(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, 0.0F, 0.0F, 0.0F, 0.2793F, 0.5236F));

		PartDefinition shin_l_2 = leg_l_2.addOrReplaceChild("shin_l_2", CubeListBuilder.create().texOffs(55, 0).addBox(0.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(15.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.576F));

		PartDefinition leg_l_3 = silca_crawler.addOrReplaceChild("leg_l_3", CubeListBuilder.create().texOffs(18, 0).addBox(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, 0.0F, 1.0F, 0.0F, -0.2793F, 0.5236F));

		PartDefinition shin_l_3 = leg_l_3.addOrReplaceChild("shin_l_3", CubeListBuilder.create(), PartPose.offset(13.0F, -7.0F, 3.8F));

		PartDefinition shin_l_3_r1 = shin_l_3.addOrReplaceChild("shin_l_3_r1", CubeListBuilder.create().texOffs(55, 0).addBox(0.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, 7.0F, -3.8F, 0.0F, 0.0F, 0.576F));

		PartDefinition leg_l_4 = silca_crawler.addOrReplaceChild("leg_l_4", CubeListBuilder.create().texOffs(18, 0).addBox(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, 0.0F, 2.0F, 0.0F, -0.576F, 0.5236F));

		PartDefinition shin_l_4 = leg_l_4.addOrReplaceChild("shin_l_4", CubeListBuilder.create().texOffs(55, 0).addBox(0.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(15.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.576F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		silca_crawler.render(poseStack, buffer, packedLight, packedOverlay);
	}
}