// Made with Blockbench 4.0.0-beta.3
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


public class heat_exchanger<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("modid", "heat_exchanger"), "main");
	private final ModelPart root;

	public heat_exchanger(ModelPart root) {
		this.root = root.getChild("root");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition root = partdefinition.addOrReplaceChild("root", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition kuehlerplatte = root.addOrReplaceChild("kuehlerplatte", CubeListBuilder.create().texOffs(0, 0).addBox(-8.0F, -4.0F, -8.0F, 16.0F, 4.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Pumpenplatte1 = root.addOrReplaceChild("Pumpenplatte1", CubeListBuilder.create().texOffs(64, 3).addBox(-7.0F, -5.0F, -7.0F, 14.0F, 1.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Kolben1 = root.addOrReplaceChild("Kolben1", CubeListBuilder.create().texOffs(0, 20).addBox(-5.0F, -10.0F, -8.0F, 4.0F, 4.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -3.1416F, 0.0F));

		PartDefinition Kolben2 = root.addOrReplaceChild("Kolben2", CubeListBuilder.create().texOffs(0, 20).addBox(-5.0F, -9.0F, -8.0F, 4.0F, 4.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Pumpenplatte2 = root.addOrReplaceChild("Pumpenplatte2", CubeListBuilder.create().texOffs(64, 3).addBox(-7.0F, -11.0F, -7.0F, 14.0F, 1.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition PumpenplatteS1 = root.addOrReplaceChild("PumpenplatteS1", CubeListBuilder.create().texOffs(35, 20).addBox(-1.0F, -9.0F, -8.0F, 1.0F, 4.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition PumpenplatteS2 = root.addOrReplaceChild("PumpenplatteS2", CubeListBuilder.create().texOffs(35, 20).addBox(5.0F, -10.0F, -8.0F, 1.0F, 4.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition PumpenplatteS3 = root.addOrReplaceChild("PumpenplatteS3", CubeListBuilder.create().texOffs(35, 20).addBox(-1.0F, 6.0F, -8.0F, 1.0F, 4.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 3.1416F));

		PartDefinition PumpenplatteS4 = root.addOrReplaceChild("PumpenplatteS4", CubeListBuilder.create().texOffs(35, 20).addBox(5.0F, 5.0F, -8.0F, 1.0F, 4.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 3.1416F));

		PartDefinition PumpenplatteS5 = root.addOrReplaceChild("PumpenplatteS5", CubeListBuilder.create().texOffs(35, 20).addBox(-10.0F, 1.0F, -8.0F, 1.0F, 4.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.5708F));

		PartDefinition PumpenplatteS6 = root.addOrReplaceChild("PumpenplatteS6", CubeListBuilder.create().texOffs(35, 20).addBox(5.0F, 1.0F, -8.0F, 1.0F, 4.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -1.5708F));

		PartDefinition Pumpenring1 = root.addOrReplaceChild("Pumpenring1", CubeListBuilder.create().texOffs(70, 32).addBox(-5.0F, -5.0F, -8.0F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Pumpenring2 = root.addOrReplaceChild("Pumpenring2", CubeListBuilder.create().texOffs(70, 32).addBox(-5.0F, 10.0F, -8.0F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 3.1416F));

		PartDefinition Pumpenring3 = root.addOrReplaceChild("Pumpenring3", CubeListBuilder.create().texOffs(70, 32).addBox(-5.0F, -5.0F, 7.0F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Pumpenring4 = root.addOrReplaceChild("Pumpenring4", CubeListBuilder.create().texOffs(70, 32).addBox(-5.0F, 10.0F, 7.0F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 3.1416F));

		PartDefinition kuehlerabschlussplatte = root.addOrReplaceChild("kuehlerabschlussplatte", CubeListBuilder.create().texOffs(0, 40).addBox(-8.0F, -16.0F, -8.0F, 16.0F, 5.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Strebe1 = root.addOrReplaceChild("Strebe1", CubeListBuilder.create().texOffs(72, 22).addBox(-7.0F, -10.0F, -5.0F, 14.0F, 5.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Strebe2 = root.addOrReplaceChild("Strebe2", CubeListBuilder.create().texOffs(72, 22).addBox(-7.0F, -10.0F, 3.0F, 14.0F, 5.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		root.render(poseStack, buffer, packedLight, packedOverlay);
	}
}